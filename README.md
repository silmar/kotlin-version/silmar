This project represents the Silmar game program.  It is built on top of the "Silmar
engine" project, the repo for which is found at
https://gitlab.com/silmar/kotlin-version/silmar-engine.  This project extends that one by
providing extensions of the engine's types, as well as its other extension points which
are marked with an @ExtensionPoint annotation.

The code in this project is proprietary and the intellectual property of its author, Jeff
Mather.  Whereas, the code in the "Silmar engine" project, which comprises the vast
majority of the game's codebase, is provided under an open-source license.
  
Note that this project's codebase follows the same Domain Driven Design organization used
by the "Silmar engine" project.

##### Running the game program 

Note that while this project's gradle build employs the gradle application plugin, such
that its "run" task may be used to run the game program, for development I prefer to use
an IntelliJ IDEA Kotlin run-configuration. This is because you can change the working
directory for such a config.  Whereas, I could not find a way to do so with a gradle
"run".  Also, the debugging experience is nicer in IDEA using the run-config, since a
gradle task is not running in the background.  That task seems to prevent the correct
toolbar from appearing for the Console tab on the Debug pane.

For deployment, the gradle build creates distribution files in the build/distributions
folder: a ZIP file for Windows, and a TAR file for Linux and Mac.  These contain a startup
script in their Silmar folder for each of those platforms: a batch file for Windows, and a
shell script for Linux and Mac.  The batch file works well on my Windows system.
  
##### Debugging the game program 

Debugging the game's code from within IDEA works as expected, including the ability to set
breakpoints in the Kotlin source code for the "Silmar engine" project.  For this to work,
however, the source folder must be specified for the engine project, if it isn't already.
To do this, select File > Project Structure > Modules > (Silmar>main) (in the tree) >
Dependencies. Click the silmar-engine-\[version #\].jar entry in the list and then click
the Edit icon If a Sources entry in the popup is not present, click the Add icon and
select the src or src/main/kotlin folder.