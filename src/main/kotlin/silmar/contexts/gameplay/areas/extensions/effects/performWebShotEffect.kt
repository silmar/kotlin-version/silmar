package silmar.contexts.gameplay.areas.extensions.effects

import silmar.contexts.gameplay.entities.beings.extensions.attacking.SilmarAttackTypes.web
import silmar.contexts.gameplay.entities.terrains.specializations.Web
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.effects.performAttackEffect
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Performs a web-shot effect between the given pixel-locations.
 *
 * @param spinner       The being (if any) that is spinning the web being shot.
 */
fun Area.performWebShotEffect(from: PixelPoint, to: PixelPoint, spinner: Being?) {
    performAttackEffect(from, to, web)

    // place a web at the given to-location
    val web = createTerrain(SilmarTerrainTypes.web) as Web
    addEntity(web, to)
    if (spinner != null) web.setSpinner(spinner)
}
