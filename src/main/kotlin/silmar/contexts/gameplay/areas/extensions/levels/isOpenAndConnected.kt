package silmar.contexts.gameplay.areas.extensions.levels

import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.contexts.gameplay.areas.Area

fun Area.isOpenAndConnected(): Boolean = !(this as SilmarArea).level!!.isRandomlyGenerated