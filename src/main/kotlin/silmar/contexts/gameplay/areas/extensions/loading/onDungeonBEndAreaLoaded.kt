package silmar.contexts.gameplay.areas.extensions.loading

import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.createItemEntity
import silmarengine.contexts.gameplay.entities.items.types.itemTypes
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.generation.stockLocations
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.util.math.randomInt

fun Area.onDungeonBEndAreaLoaded() {
    // for each chest terrain in this area
    val chests = terrains.filter {
        it.isOfType(TerrainTypes.chest)
    }
    val itemLocations = ArrayList<PixelPoint>()
    chests.forEach {
        // remove this chest, but remember its location
        val location = it.location
        removeEntity(it)

        // if a random chance says yes
        if (randomInt(1, 3) <= 2) {
            // put a gold item where the chest was
            val gold = createGroupableItem(GroupableItemTypes.gold)
            addEntity(createItemEntity(gold), location)
        }
        // else, add the chest's location to the list of locations at which we'll place
        // random items, below
        else itemLocations.add(location)
    }

    // stock the locations remembered above with random items from
    // all the different types in existence
    stockLocations(itemLocations, itemTypes.map { it.entityType })
}