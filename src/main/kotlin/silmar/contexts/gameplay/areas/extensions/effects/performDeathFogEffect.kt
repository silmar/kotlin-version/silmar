package silmar.contexts.gameplay.areas.extensions.effects

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmar.contexts.gameplay.entities.terrains.specializations.DeathFog
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.containsLocation
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.awt.geom.Point2D
import kotlin.math.abs

/**
 * Performs a death-fog effect at the given pixel-location on the given area
 * at the given strength.
 */
fun Area.performDeathFogEffect(location: PixelPoint, strength: Int) {
    onSoundIssued(SilmarSounds.deathFog, location)

    // for each tile-sized location in the 5x5 square around the given pixel-location,
    // working outwards from that location one tile-square at a time
    val spot = MutablePixelPoint()
    val maxRange = 2
    for (k in 0..maxRange) {
        for (i in -k..k) {
            for (j in -k..k) {
                // if this location is in the interior of the current tile-square, skip it
                if (i != k && i != -k && j != k && j != -k) continue

                // if this location is one of the four corners of the 5x5 square, skip it
                // (to make the cloud look a bit rounded)
                if (k == maxRange && (i == k || i == -k) && (j == k || j == -k)) continue

                // if the area doesn't contain this location, skip it
                spot.set(location.x + i * tileWidth, location.y + j * tileHeight)
                if (!containsLocation(spot)) continue

                // create a death fog terrain and add it to the area at this location
                val fog = createTerrain(SilmarTerrainTypes.deathFog) as DeathFog
                addEntity(fog, spot)
                fog.setStrength(strength)

                // if this isn't the center fog in the overall cloud
                if (k != 0) {
                    // set this fog to drift in the direction it is away
                    // from the center of the cloud
                    val absI = abs(i)
                    val absJ = abs(j)
                    fog.setDriftDirection(Point2D.Float(i / (absI + absJ).toFloat() * k,
                        j / (absI + absJ).toFloat() * k))
                }
            }
        }
    }
}
