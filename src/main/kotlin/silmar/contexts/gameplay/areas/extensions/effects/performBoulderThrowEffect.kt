package silmar.contexts.gameplay.areas.extensions.effects

import silmar.contexts.gameplay.entities.beings.extensions.attacking.SilmarAttackTypes
import silmar.contexts.gameplay.entities.beings.player.throwBoulderStrengthRequired
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.getPenaltyForRange
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.geometry.points.getIndexOfNearestPoint
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.effects.performAttackEffect
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getLineLocations
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomBellCurveInt
import silmarengine.util.math.randomInt
import silmarengine.util.sleepThread

/**
 * Performs a throw effect of the given boulder between the given pixel-locations,
 * with the given strength and accuracy.  The subsequent bouncing of
 * the boulder may hurt beings beyond the target location.
 */
fun Area.performBoulderThrowEffect(
    boulder: Terrain, from: PixelPoint, to: PixelPoint, strength: Int,
    accuracy: Int
) {
    performAttackEffect(from, to, SilmarAttackTypes.boulder)

    // determine the range to the target and the penalty-to-hit at that range
    val range = getDistance(from, to)
    val rangePenalty =
        getPenaltyForRange(range, PixelDistance(strength / 2 * tileWidth))

    // determine how many squares the boulder will bounce according to
    // the range to the target
    val bounceDistance = PixelDistance(range.distance / 3 + tileWidth)

    // determine the locations on the tile line from the given from-location to the
    // given to-location and beyond, out to the bounce-distance
    val line = getLineLocations(from, to, tileWidth / 2,
        PixelDistance(range.distance + bounceDistance.distance), 0)

    // for each location from the target location to where the boulder comes to rest
    val toIndex = getIndexOfNearestPoint(to, line)
    val finalLocation = MutablePixelPoint(line[toIndex])
    val numBounces = line.size - toIndex
    for (i in 0 until numBounces) {
        // if this location blocks sight
        val bounce = PixelPoint(line[toIndex + i])
        if (getTile(bounce).blocksLOS) {
            // the previous location is where the boulder stopped
            break
        }

        finalLocation.set(bounce)

        // move the boulder thrown to this location, with a 'thud'-like sound to indicate
        // that it's bouncing
        moveEntity(boulder, finalLocation)
        onSoundIssued(Sounds.damage, finalLocation)
        val bounceTime = 125L
        sleepThread(bounceTime)

        // for each being at this location
        val beings = beings.getEntitiesInRectangle(bounce, boulder.size)
        beings.forEach { being ->
            // if the player makes an accuracy check modified
            // by the range to the target
            val roll = randomInt(1, 20)
            if (roll <= accuracy + rangePenalty) {
                // the boulder hits
                val damage = randomBellCurveInt(1, 10, 4)
                val result = being.avoid(strength - throwBoulderStrengthRequired)
                val finalDamage = if (result.avoided) damage / 2 else damage
                being.takeDamage(Damage(finalDamage, DamageForms.bluntImpact))
            }
        }
    }
}
