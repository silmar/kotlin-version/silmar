package silmar.contexts.gameplay.areas.extensions.effects

import silmar.contexts.gameplay.entities.beings.player.extensions.harm.onHitByDragonBreath
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForm
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getLineLocations
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesAt
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesOccurred
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesToOccur
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.sleepThread
import java.util.*

/**
 * Does a cone-shaped breath effect of the given breath-type between the given source and
 * target pixel-locations, up to the given range (in tiles), with the given strength, and
 * of the given damage, without affecting the given source-being.
 */
fun Area.performDragonBreathEffect(
    source: PixelPoint, target: PixelPoint, breathType: DamageForm,
    range: TileDistance, strength: Int, damage: Int, sourceBeing: Being
) {
    // find the location at the far end of the tile-line from
    // this monster to the target
    val lineThroughTarget = getLineLocations(source, target, tileWidth,
        PixelDistance(range.distance * tileWidth), 0)
    val end = lineThroughTarget[lineThroughTarget.size - 1]

    // determine the radius (in tile-sized squares) of the wide end of the cone
    var radius = 1
    if (range.distance >= 8) {
        radius = 3
    }
    else if (range.distance >= 5) {
        radius = 2
    }

    // for each location within a certain distance of the
    // end location
    val lines = ArrayList<List<PixelPoint>>()
    val spot = MutablePixelPoint()
    for (i in -radius..radius) {
        for (j in -radius..radius) {
            // if this location is the same (or close to the same)
            // distance away from this monster as the end
            // location is
            spot.set(end.x + i * tileWidth, end.y + j * tileHeight)
            val distance = getDistance(source, spot)
            if (distance.distance >= (range.distance - 1) * tileWidth && distance.distance <= (range.distance + 1) * tileWidth) {
                // add the line from the source to this location
                // to the list of lines we must process
                lines.add(getLineLocations(source, spot, tileWidth,
                    PixelDistance(range.distance * tileWidth), 0))
            }
        }
    }

    // for each step in distance from this monster to the breath
    // range (we are looping this way because we want the
    // description of the cone to start at this monster and work
    // its way out to the max range)
    val visited = HashMap<PixelPoint, PixelPoint>()
    val coneLocations = ArrayList<PixelPoint>()
    for (i in 0 until range.distance) {
        // for each line of breath to process
        lines.forEach forEach@{ line ->
            // if this line doesn't go out this far, skip it
            if (i >= line.size) return@forEach

            // if the location at this distance along
            // this line has yet to be visited by this cone
            val location = line[i]
            if (visited[location] == null) {
                // add this location to the cone
                coneLocations.add(location)
                visited[location] = location
            }

            // if the location at this distance along this line blocks los
            if (getTile(location).blocksLOS) {
                // this line is finished
                lines[i] = emptyList()
            }
        }
    }

    // issue an accompanying sound
    onSoundIssued(SilmarSounds.dragonBreathed, source)

    // make the effects below be reported as a group, so their display doesn't slow down
    onGroupOfChangesToOccur()

    // for each location in the given list
    val imageName = if (breathType == DamageForms.acid) "acidDragonBreath"
    else "redDragonBreath"
    val effects = ArrayList<VisualEffect>()
    var lastLocation: PixelPoint? = null
    val newArcIndicatorDistance = PixelDistance(2 * tileWidth)
    coneLocations.forEachIndexed { i, location ->
        // if this location is far enough from the last one to indicate a new arc
        // outward is beginning
        if (lastLocation != null && !isDistanceAtMost(lastLocation!!, location,
                newArcIndicatorDistance)) {
            // the previous arc of breath effects is complete, and so it may now be displayed
            onGroupOfChangesOccurred()

            // pause a bit so the new outermost arc of breath effects can be seen
            sleepThread(30)

            // make the next arc of effects be reported as a group,
            // so its display doesn't slow down
            onGroupOfChangesToOccur()
        }

        // add a visual effect at this location, where one every so many emits light
        val effect = if (i % 8 == 0) LightSourceVisualEffect(imageName,
            PixelDistance(TileDistance(5)))
        else VisualEffect1(imageName)
        addEntity(effect, location)
        effects.add(effect)

        lastLocation = location
    }

    // inform the area that the last arc of the cone has been fully added
    onGroupOfChangesOccurred()

    // pause a bit so the overall breath effect can be seen
    sleepThread(400)

    // make the reporting of the removal of the cone occur all at once
    onGroupOfChangesToOccur()

    // remove each visual effect added to the area above
    effects.forEach { removeEntity(it) }

    // inform the area that it may now report the full removal of the cone
    onGroupOfChangesOccurred()

    // for each location in the cone
    val hitBeings = ArrayList<Being>()
    coneLocations.forEach { location ->
        // for each being at this location
        val beings = getEntitiesAt(location, beings)
        beings.forEach { being ->
            // if this being isn't the source of the breath, and we haven't already
            // processed this being
            if (being !== sourceBeing && !hitBeings.contains(being)) {
                // if this being avoids the breath
                val result = being.avoid(strength / 2)
                val finalDamage = if (result.avoided) {
                    // the damage is halved
                    damage / 2
                }
                else damage

                being.takeDamage(Damage(finalDamage, breathType))
                if (being is Player) being.onHitByDragonBreath(breathType, strength)

                hitBeings.add(being)
            }
        }
    }
}
