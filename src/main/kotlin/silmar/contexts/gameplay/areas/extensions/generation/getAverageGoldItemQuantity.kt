package silmar.contexts.gameplay.areas.extensions.generation

import silmar.contexts.gameplay.game.SilmarGame
import silmar.contexts.gameplay.areas.AreaLevel
import silmarengine.contexts.gameplay.game.Game

fun getAverageGoldItemQuantity(): Int {
    // query the most recently-generated level (since we're assuming that's
    // its the area-level for which this item was created)
    val game = Game.currentGame as SilmarGame?
    return if (game != null) AreaLevel.getLevel(
        game.furthestDungeonLevelReached)!!.averageGoldItemQuantity
    else 1
}