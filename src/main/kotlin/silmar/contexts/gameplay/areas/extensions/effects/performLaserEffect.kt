package silmar.contexts.gameplay.areas.extensions.effects

import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesOccurred
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesToOccur
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getLineLocations
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.sleepThread
import java.util.*

/**
 * Performs a laser effect between the given two locations in the given area.
 */
fun Area.performLaserEffect(
    from: PixelPoint, to: PixelPoint, imageName: String = "laser"
) {
    // determine the locations on the line between the given two locations
    val locations = getLineLocations(from, to, tileWidth / 2, null, 0)

    // make the effects below be reported as a group, so their display doesn't slow down
    onGroupOfChangesToOccur()

    // for each location in the list determined above
    val effects = ArrayList<VisualEffect>()
    locations.forEachIndexed { i, location ->
        // add a visual effect at this location, where one every so many emits light
        val effect = if (i % 8 == 0) LightSourceVisualEffect(imageName,
            PixelDistance(TileDistance(2)))
        else VisualEffect1(imageName)
        addEntity(effect, location)
        effects.add(effect)
    }

    // the above grouping of effects is complete, and so it may now be displayed
    onGroupOfChangesOccurred()

    // pause a bit so the overall laser effect can be seen
    sleepThread(400)

    // make the effect-removals below be reported as a group, so their display
    // doesn't slow down
    onGroupOfChangesToOccur()

    // remove each visual effect added to the area above
    effects.forEach { removeEntity(it) }

    // the above grouping of effect removals is complete, and so it may now be displayed
    onGroupOfChangesOccurred()
}
