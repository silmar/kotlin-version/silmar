package silmar.contexts.gameplay.areas.extensions.effects

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt
import silmarengine.util.math.randomBoolean

/**
 * Performs a meteor shower effect where a fireball explodes at each enemy position in LOS
 * of the given pixel location.
 *
 * @param targetMonsters    Whether to target monsters (vs. players), or vice-versa.
 * @param strength          The strength of each fireball.
 */
fun Area.performMeteorShowerEffect(
    location: PixelPoint, targetMonsters: Boolean, strength: Int
) {
    // for each being in LOS of the given location
    val beings = getEntitiesInLOS(location, beings)
    beings.forEach { being ->
        // if this being is the right kind of target (i.e. player or monster)
        if (targetMonsters && being is Monster || !targetMonsters && being is Player) {
            // do a fireball attack on this being's position, modified by up to 1 tile
            // vertically and/or horizontally
            val spot = MutablePixelPoint(being.location)
            spot.translate(randomInt(0, tileWidth) * if (randomBoolean) 1 else -1,
                randomInt(0, tileHeight) * if (randomBoolean) 1 else -1)
            performFireballEffect(null, spot, strength)
        }
    }
}
