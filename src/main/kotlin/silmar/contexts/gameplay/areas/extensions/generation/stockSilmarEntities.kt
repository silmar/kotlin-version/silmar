package silmar.contexts.gameplay.areas.extensions.generation

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.generation.stockTypes

fun stockSilmarEntities(area: Area, areaSizeFactor: Float) {
    // stock the boulders
    val averageNumBoulders = 10
    area.stockTypes(listOf(SilmarTerrainTypes.boulder),
        (averageNumBoulders * areaSizeFactor).toInt())

    // stock the radiations
    val averageNumRadiations = 14
    area.stockTypes(listOf(SilmarTerrainTypes.radiation),
        (averageNumRadiations * areaSizeFactor).toInt())
}