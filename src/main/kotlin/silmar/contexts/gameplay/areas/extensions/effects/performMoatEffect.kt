package silmar.contexts.gameplay.areas.extensions.effects

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.geometry.asPixelLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.*
import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.Tiles.water
import silmarengine.contexts.gameplay.tiles.tileSize
import java.io.Serializable
import java.util.*

/**
 * A hollow square of tiles that are all water, loosely center around the summon location.
 */
class Moat(
    val summonLocation: PixelPoint, val squares: List<MoatSquare>
) : Serializable

/**
 * A square that takes up a tile-location in the area and is all water.
 */
class MoatSquare(
    /**
     * The tile-location of this square.
     */
    val location: TilePoint,
    /**
     * The tile that was at this square before it was turned into part of a moat.
     */
    val oldTile: Tile?
) : Serializable

/**
 * Returns a list of moat-squares whose creation forms a moat around the given
 * tile-location.
 *
 * @param tileDistance  The distance in tiles between the center of the moat and
 * the moat water squares at the center of each moat side.
 */
fun Area.performMoatCreationEffect(
    location: PixelPoint, tileDistance: TileDistance
): Moat {
    onSoundIssued(SilmarSounds.waterAppears, location)

    // for each location around the user, out to the given distance
    val tileLocation = location.asTileLocation()
    val squares = ArrayList<MoatSquare>()
    val spot = MutableTilePoint()
    val pixelSpot = MutablePixelPoint()
    val distance = tileDistance.distance
    for (i in -distance..distance) {
        for (j in -distance..distance) {
            // if this location isn't in the outer square of locations
            // where the moat will go, skip it
            if (i != -distance && i != distance && j != -distance && j != distance) {
                continue
            }

            // if this location isn't passible, or the tile there blocks los, skip it
            spot.set(tileLocation.x + i, tileLocation.y + j)
            val tile = getTile(spot)
            spot.asPixelLocation(pixelSpot)
            if (tile.blocksLOS || !isRectanglePassible(pixelSpot, tileSize,
                    MovementType.WALKING, true,
                    null, null, false).passible) {
                continue
            }

            // set the tile to water
            setTile(spot, water)

            // remember what tile was previously at this location
            val square = MoatSquare(TilePoint(spot), tile)
            squares.add(square)
        }
    }

    return Moat(location, squares)
}

/**
 * Removes the water squares in the given list from the given area, replacing them
 * with the previous tiles that occupied their locations.
 */
fun Area.performMoatRemovalEffect(moat: Moat) {
    // issue a water-disappearing sound
    onSoundIssued(SilmarSounds.waterDisappears, moat.summonLocation)

    // return the tile at each square in the moat back to normal
    moat.squares.filter { it.oldTile != null }
        .forEach { setTile(it.location, it.oldTile!!) }
}
