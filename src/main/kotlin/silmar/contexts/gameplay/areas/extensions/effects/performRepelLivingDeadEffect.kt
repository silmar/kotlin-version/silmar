package silmar.contexts.gameplay.areas.extensions.effects

import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.harm.onLivingDeadRepel
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS

/**
 * Performs a repel effect emanating from the given player, with the given strength.
 */
fun performRepelLivingDeadEffect(repeller: Player, strength: Int) {
    // inform each monster within los of the repel
    val area = repeller.area
    val monsters = area.getEntitiesInLOS(repeller.location, area.monsters)
    monsters.forEach { it.onLivingDeadRepel(repeller, strength) }
}
