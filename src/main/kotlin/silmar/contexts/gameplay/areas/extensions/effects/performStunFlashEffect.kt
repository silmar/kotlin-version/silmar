package silmar.contexts.gameplay.areas.extensions.effects

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeParalyzed
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOSAndRange
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesOccurred
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesToOccur
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.sleepThread
import java.util.*

/**
 * Performs a stun-flash effect at the given pixel-location, with the given strength.
 *
 * @param emitter       The being that is emitting this flash (if any).
 */
fun Area.performStunFlashEffect(
    location: PixelPoint, strength: Int, emitter: Being?
) {
    // make the effects below be reported as a group, so their display doesn't slow down
    onGroupOfChangesToOccur()

    // for each location in the square around the given source
    // location at the given range (as we know all points
    // truly in range also lie within this square)
    val spot = MutablePixelPoint()
    val range = TileDistance(strength)
    val pixelDistance = PixelDistance(range.distance * tileWidth)
    val effects = ArrayList<VisualEffect>()
    val imageName = "flash"
    for (i in -range.distance..range.distance) {
        for (j in -range.distance..range.distance) {
            // if this location is truly in range and can be
            // seen from the given source location
            spot.set(location.x + i * tileWidth, location.y + j * tileHeight)
            if (isDistanceAtMost(location, spot,
                    pixelDistance) && canSeeExt(location, spot).canSee) {
                // add a visual effect at this location, where one every so many emits light
                val effect = if ((i + j) % 8 == 0) LightSourceVisualEffect(
                    imageName, PixelDistance(TileDistance(2)))
                else VisualEffect1(imageName)
                addEntity(effect, spot)
                effects.add(effect)
            }
        }
    }

    // the above grouping of effects is complete, and so it may now be displayed
    onGroupOfChangesOccurred()

    // issue an accompanying sound
    onSoundIssued(SilmarSounds.flash, location)

    // pause a bit so the overall flash effect can be seen
    sleepThread(100)

    // make the effect-removals below be reported as a group, so their display
    // doesn't slow down
    onGroupOfChangesToOccur()

    // remove each visual effect added to the area above
    effects.forEach { removeEntity(it) }

    // the above grouping of effect removals is complete, and so it may now be displayed
    onGroupOfChangesOccurred()

    // for each being in LOS and within a certain range
    val beings =
        getEntitiesInLOSAndRange(location, PixelDistance(strength * tileWidth),
            beings)
    beings.forEach { being ->
        // if this being is not the emitter
        if (emitter == null || being != emitter) {
            // if this being doesn't avoid the flash
            val result = being.avoid(strength / 2)
            if (!result.avoided) {
                being.becomeParalyzed(3)
            }
        }
    }
}
