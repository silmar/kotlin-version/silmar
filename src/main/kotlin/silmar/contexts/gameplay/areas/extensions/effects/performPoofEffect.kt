package silmar.contexts.gameplay.areas.extensions.effects

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.sleepThread

/**
 * Performs a poof effect of the given size at the given location.
 */
fun Area.performPoofEffect(location: PixelPoint, size: PixelDimensions) {
    // determine the poof-image that best fits the given size
    val imageName: String
    val width = size.width
    val height = size.height
    imageName = if (width <= tileWidth || height <= tileHeight) {
        "poof"
    }
    else if (width <= tileWidth * 3 / 2 || height <= tileHeight * 3 / 2) {
        "poofBig"
    }
    else {
        "poofHuge"
    }

    // add a poof visual effect at the given location
    val effect = VisualEffect1(imageName)
    addEntity(effect, location)

    // issue an accompanying sound
    onSoundIssued(SilmarSounds.poof, location)

    // if the player can see this poof
    if (player?.isInLOSOf(location) == true) {
        // pause a bit for the effect to be seen
        sleepThread(500)
    }

    // remove this poof's visual effect from the area
    removeEntity(effect)
}
