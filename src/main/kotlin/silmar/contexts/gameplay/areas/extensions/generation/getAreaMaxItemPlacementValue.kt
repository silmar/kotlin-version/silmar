package silmar.contexts.gameplay.areas.extensions.generation

import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.contexts.gameplay.areas.Area

fun Area.getAreaMaxItemPlacementValue(): Int =
    (this as SilmarArea).level!!.maxItemPlacementValue