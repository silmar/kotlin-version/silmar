package silmar.contexts.gameplay.areas.extensions.generation

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.createTerrain

fun createAreaEntrance(): Terrain = createTerrain(SilmarTerrainTypes.upExit)

fun createAreaExit(): Terrain = createTerrain(SilmarTerrainTypes.downExit)