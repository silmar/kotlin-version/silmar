package silmar.contexts.gameplay.areas.extensions.effects

import silmar.contexts.gameplay.entities.beings.extensions.attacking.SilmarAttackTypes.fireball
import silmar.contexts.gameplay.entities.terrains.extensions.onHitByFireball
import silmar.sounds.SilmarSounds
import silmar.contexts.gameplay.entities.beings.extensions.reactions.onHitByFireball
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.entities.items.extensions.condition.avoidOrDegrade
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.extensions.effects.performAttackEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.sleepThread

/**
 * Performs a fireball effect between the given pixel-locations at the given strength.
 * The fireball's explosion can affect items.
 *
 * @param from      If null, means to just explode a fireball (with no whoosh) at the
 * to-location.
 */
fun Area.performFireballEffect(
    from: PixelPoint?, to: PixelPoint, strength: Int
) {
    // if a from location was given
    if (from != null) {
        // do an effect showing the fireball travelling from source to target
        performAttackEffect(from, to, fireball)
    }

    // add a visual effect at the to-location
    val effect =
        LightSourceVisualEffect("fireball1", PixelDistance(TileDistance(15)))
    addEntity(effect, to)

    // issue an accompanying sound
    onSoundIssued(SilmarSounds.fireballBlast, to)

    // for each step in the effect
    for (i in 1..2) {
        // perform this step
        effect.imageName = "fireball$i"

        // pause a bit for the step to be seen
        sleepThread(400)
    }

    // remove the visual effect from the area
    removeEntity(effect)

    // check each item in the fireball's blast for being damaged (we do items
    // before beings and terrains since dying beings and destroyed terrains
    // may leave behind items that we don't want destroyed by this blast; it is
    // assumed that entity absorbs the impact of the blast, thus protecting the item)
    val itemEntities =
        items.getEntitiesInRange(to, PixelDistance(3 * tileWidth / 2))
    itemEntities.forEach {
        it.item.avoidOrDegrade((if (it.item.type.isGroupable) 12 else 0) + strength / 2)
    }

    // inform each terrain in the fireball's blast
    val terrains =
        terrains.getEntitiesInRange(to, PixelDistance(3 * tileWidth / 2))
    terrains.forEach { it.onHitByFireball(strength) }

    // inform each being in the fireball's blast
    val beings = beings.getEntitiesInRange(to, PixelDistance(3 * tileWidth / 2))
    beings.forEach { it.onHitByFireball(strength) }
}

