package silmar.contexts.gameplay.areas.extensions.loading

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.areas.Area

fun Area.onCityEndAreaLoaded() {
    monsters.filter {
        it.isOfType(SilmarMonsterTypes.grenadier) || it.isOfType(
            SilmarMonsterTypes.mercenary)
    }.forEach { it.makeStationary() }
}