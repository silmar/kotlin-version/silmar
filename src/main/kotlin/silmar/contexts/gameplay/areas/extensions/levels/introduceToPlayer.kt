package silmar.contexts.gameplay.areas.extensions.levels

import silmar.contexts.gameplay.areas.AreaLevel
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage

fun AreaLevel.introduceToPlayer(player: Player) {
    if (intro != null) player.onMessage(intro, MessageType.important)
}