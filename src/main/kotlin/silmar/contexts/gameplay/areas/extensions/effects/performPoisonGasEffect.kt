package silmar.contexts.gameplay.areas.extensions.effects

import silmar.contexts.gameplay.entities.beings.player.extensions.harm.onPoisonGas
import silmarengine.contexts.gameplay.entities.extensions.proximity.isWithinDistanceOf
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance

/**
 * Performs a gas effect at the given pixel-location that spreads out from that
 * location the given tile-range, with the given strength.
 */
fun Area.performPoisonGasEffect(
    location: PixelPoint, strength: Int, range: TileDistance
) {
    // inform the player if it's within the given tile-range of the given location
    if (player?.isWithinDistanceOf(location, PixelDistance(range)) == true)
        player?.onPoisonGas(strength)
}
