package silmar.contexts.gameplay.areas.extensions.lighting

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.areas.AreaLevel
import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.specializations.TempLightSource
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance

/**
 * Has this area check to see if it should add a lightsource at its up- and/or down-exit due to the
 * previous or next area-level being self-lit.
 */
fun SilmarArea.addExitLightSources() {
    // skip this if this area's level is self-lit
    if (level!!.isSelfLit) return

    // resolve the area-level object for the previous area level
    val levelIndex = level!!.index
    val previous = AreaLevel.getLevel(levelIndex - 1)

    // adds a small light-source at this area's exit, to model the light coming in
    // from the vertically adjacent level
    fun addLightSource(forUpExit: Boolean) {
        val lightSource = createTerrain(TerrainTypes.tempLightSource) as TempLightSource
        lightSource.lightRadius = PixelDistance(TileDistance(2))
        val terrainType =
            if (forUpExit) SilmarTerrainTypes.upExit else SilmarTerrainTypes.downExit
        addEntity(lightSource, getTerrainOfType(terrainType)!!.location)
    }

    // if the preceding area-level is self-lit, add a light source
    if (previous != null && previous.isSelfLit) addLightSource(true)

    // resolve the area-level object for the next area level
    val next = AreaLevel.getLevel(levelIndex + 1)

    // if the next area-level is self-lit, add a light source
    if (next != null && next.isSelfLit) addLightSource(false)
}

