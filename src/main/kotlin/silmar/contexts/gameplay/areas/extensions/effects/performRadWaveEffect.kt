package silmar.contexts.gameplay.areas.extensions.effects

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.extensions.proximity.contains
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesOccurred
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesToOccur
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.sleepThread
import java.util.*

/**
 * Performs a rad-wave effect that emanates from the given pixel-location,
 * with the given strength.
 */
fun Area.performRadWaveEffect(location: PixelPoint, strength: Int) {
    // issue an accompanying sound
    onSoundIssued(SilmarSounds.radWave, location)

    // for each range out to the given range
    val effects = ArrayList<VisualEffect>()
    val imageName = "radWave"
    for (i in 1..strength) {
        effects.clear()

        // make the effects below be reported as a group, so their display doesn't slow down
        onGroupOfChangesToOccur()

        // for each tile-spaced location along the top and bottom sides of
        // the square at this range
        for (k in -i..i) {
            // for the bottom and then the top location
            var m = -1
            while (m <= 1) {
                // add a wave visual effect at this location
                val effect = VisualEffect1(imageName)
                addEntity(effect, PixelPoint(location.x + k * tileWidth,
                    location.y + m * i * tileHeight))
                effects.add(effect)
                m += 2
            }
        }

        // for each tile-spaced location along the left and right sides of
        // the square at this range
        for (k in -i + 1 until i) {
            // for the left and then the right location
            var m = -1
            while (m <= 1) {
                // add a wave visual effect at this location
                val effect = VisualEffect1(imageName)
                addEntity(effect, PixelPoint(location.x + m * i * tileWidth,
                    location.y + k * tileHeight))
                effects.add(effect)
                m += 2
            }
        }

        // the above grouping of effect removals is complete, and so it may now be displayed
        onGroupOfChangesOccurred()

        // pause a bit so the wave effect at this range can be seen
        sleepThread(30)

        // make the effect-removals below be reported as a group, so their display
        // doesn't slow down
        onGroupOfChangesToOccur()

        // remove each visual effect added to the area above
        effects.forEach { removeEntity(it) }

        // the above grouping of effect removals is complete, and so it may now be displayed
        onGroupOfChangesOccurred()
    }

    // for each being within range of the given location
    val beings =
        beings.getEntitiesInRange(location, PixelDistance(strength * tileWidth))
    beings.forEach { being ->
        // if this being isn't at the source location of the wave
        if (!being.contains(location)) {
            // if this being doesn't avoid
            val result = being.avoid(8 + strength / 2)
            if (!result.avoided) {
                // this being dies, or loses half its hit points if it super-avoids
                val damage =
                    if (result.superAvoided) being.hitPoints / 2 else being.hitPoints
                being.takeDamage(Damage(damage, DamageForms.radiation))
            }
        }
    }
}
