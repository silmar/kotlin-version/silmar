package silmar.contexts.gameplay.areas.extensions.effects

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.util.sleepThread

/**
 * Perform a disintegration effect at the given pixel-location on the given 
 */
fun Area.performDisintegrationEffect(location: PixelPoint) {
    // add a visual effect at the given location
    val effect = LightSourceVisualEffect("disintegration1",
        PixelDistance(TileDistance(3)))
    addEntity(effect, location)

    // issue an accompanying sound
    onSoundIssued(SilmarSounds.disintegration, location)

    // for each step in the effect
    for (i in 1..3) {
        // perform this step
        effect.imageName = "disintegration$i"

        // pause a bit for the step to be seen
        sleepThread(200)
    }

    // remove the visual effect from the area
    removeEntity(effect)
}
