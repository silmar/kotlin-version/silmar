package silmar.contexts.gameplay.areas.extensions.loading

import silmar.contexts.gameplay.entities.terrains.specializations.FogPlacer
import silmarengine.contexts.gameplay.areas.Area

fun Area.onDaveDungeonEndAreaLoaded() {
    // have the fog-placer act once, so that there are fogs present when the level
    // is first displayed to the user
    (terrains.first { it is FogPlacer } as FogPlacer).act()
}