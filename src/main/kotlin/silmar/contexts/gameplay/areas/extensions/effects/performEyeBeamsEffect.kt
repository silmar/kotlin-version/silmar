package silmar.contexts.gameplay.areas.extensions.effects

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onAttackedAndMissed
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.util.math.randomBellCurveInt

/**
 * Performs an eye-beams effect between the given pixel-locations.
 *
 * @param willHit   Whether the beams actually hit their target.
 */
fun Area.performEyeBeamsEffect(
    from: PixelPoint, to: PixelPoint, willHit: Boolean
) {
    onSoundIssued(SilmarSounds.laser, from)

    // do a laser effect to represent the beams
    performLaserEffect(from, to)

    // if there is a being at the target location
    val target = getFirstEntityAt(to, beings)
    if (target != null) {
        // if we were given that the beams will hit
        if (willHit) {
            // the being takes damage
            val damage = randomBellCurveInt(1, 2, 6)
            target.takeDamage(Damage(damage, DamageForms.laser))
        }
        // else, inform the target that the beams missed it
        else target.onAttackedAndMissed()
    }
}
