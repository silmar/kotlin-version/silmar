package silmar.contexts.gameplay.areas.extensions.loading

import silmarengine.contexts.gameplay.entities.items.types.itemTypes
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.entities.terrains.types.trapTypes
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.generation.stockLocations
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.util.math.randomInt

fun Area.onBaseEndAreaLoaded() {
    // for each event-marker terrain in this area
    val markers = terrains.filter {
        it.isOfType(TerrainTypes.eventMarker)
    }
    val itemLocations = ArrayList<PixelPoint>()
    val trapLocations = ArrayList<PixelPoint>()
    markers.forEach {
        // remove this event-marker, but remember its location
        val location = it.location
        removeEntity(it)

        // add the location to either the list of locations to trap,
        // the list of locations at which to place items, both, or none
        val roll = randomInt(1, 12)
        if (roll <= 9) trapLocations.add(location)
        if (roll in 9..10) itemLocations.add(location)
    }

    // stock the locations determined above with traps and/or items
    stockLocations(trapLocations, trapTypes)
    trapLocations.clear()
    stockLocations(itemLocations, itemTypes.map { it.entityType })
    itemLocations.clear()
}