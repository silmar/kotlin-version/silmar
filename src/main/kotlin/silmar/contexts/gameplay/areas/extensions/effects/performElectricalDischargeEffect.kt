package silmar.contexts.gameplay.areas.extensions.effects

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Performs an electrical-discharge effect between the given pixel-locations, at the given
 * strength, and with the given damage.
 */
fun Area.performElectricalDischargeEffect(
    from: PixelPoint, to: PixelPoint, strength: Int, damage: Int
) {
    onSoundIssued(SilmarSounds.electricity, from)

    // do a laser effect to represent the discharge
    performLaserEffect(from, to)

    // if there is a victim at the target location
    val victim = getFirstEntityAt(to, beings)
    if (victim != null) {
        // if the victim avoids the discharge
        val result = victim.avoid(strength / 2)
        if (result.superAvoided) return
        val finalDamage = if (result.avoided) {
            // the damage is halved
            damage / 2
        }
        else damage

        victim.takeDamage(Damage(finalDamage, DamageForms.electricity))
    }
}
