package silmar.contexts.gameplay.areas.extensions.effects

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.harm.onRadiationExposure
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.extensions.proximity.isWithinDistanceOf
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Performs a radiation effect at the given pixel-location.
 */
fun Area.performRadiationEffect(location: PixelPoint) {
    // if the player can see the given location, and is within range of it
    val player = player as SilmarPlayer? ?: return
    if (player.isInLOSOf(location) && player.isWithinDistanceOf(location, PixelDistance(10 * tileWidth))) {
        // the player suffers the radiation effect
        performPowerDischargeEffect(location, 0, SilmarSounds.radiation)
        player.onRadiationExposure()
    }
}
