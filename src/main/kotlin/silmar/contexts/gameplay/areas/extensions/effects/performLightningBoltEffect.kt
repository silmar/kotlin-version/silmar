package silmar.contexts.gameplay.areas.extensions.effects

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.geometry.getLineBearingAfterCollision
import silmarengine.contexts.gameplay.areas.extensions.geometry.getLineLocations
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesAt
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesOccurred
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesToOccur
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomBellCurveInt
import silmarengine.util.sleepThread
import java.util.*
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min

/**
 * Performs a lightning bolt effect that starts from a location and heads towards a target location,
 * but may bounce off one or more intervening light-of-blocking tiles.  Thus, the bolt's
 * final path might consist of a set of segments.
 *
 * @param affectFromLocation    Whether the bolt should strike the given from location.
 */
fun Area.performLightningBoltEffect(from: PixelPoint, to: PixelPoint, strength: Int,
    affectFromLocation: Boolean) {
    // if the bolt is to affect the from-location
    val locations = ArrayList<PixelPoint>()
    val beingsStruck = ArrayList<Being>()
    if (affectFromLocation) {
        // add the from-location to our list of locations struck by the bolt
        locations.add(from)

        // if the starting square contains a being
        val being = getFirstEntityAt(from, beings)
        if (being != null) {
            // add it to our list of beings hit by this bolt
            beingsStruck.add(being)
        }
    }

    // while the bolt hasn't yet gone it's full distance
    var lengthLeft = 12f + strength * 2
    var line: List<PixelPoint>
    var segmentFrom = PixelPoint(from)
    var segmentTo = PixelPoint(to)
    val beingsHitThisSegment = ArrayList<Being>()
    while (lengthLeft > 0) {
        // if the bolt no longer has a valid direction in which to travel
        if (segmentFrom == segmentTo) break

        // for each location along the next segment of the bolt
        line = getLineLocations(segmentFrom, segmentTo, 3 * tileWidth / 4, null,
            ceil(lengthLeft * 4 / 3).toInt())
        var previous = segmentFrom
        beingsHitThisSegment.clear()
        run line@{
            line.forEach { location ->
                lengthLeft -= 0.75f

                // if the tile at this location blocks los
                if (getTile(location).blocksLOS) {
                    // have the bolt bounce off this tile like a pool shot
                    segmentTo = getLineBearingAfterCollision(this, segmentFrom, location,
                        previous)
                    segmentFrom = previous

                    // we are done with this segment
                    return@line
                }

                // add this location to our list of locations visited
                // by this bolt
                locations.add(location)

                // for each being at this location
                val beingsAt = getEntitiesAt(location, beings)
                beingsAt.forEach { being ->
                    // if this being was not just hit by the previous bolt location
                    // (since we don't want both of two adjacent bolt locations hitting the same
                    // victim, but rather, only one)
                    if (!beingsHitThisSegment.contains(being)) {
                        // add it to our list of beings hit by this bolt
                        beingsStruck.add(being)
                        beingsHitThisSegment.add(being)
                    }
                }

                previous = location
            }
        }
    }

    // issue an accompanying sound
    onSoundIssued(SilmarSounds.lightning, from)

    // for each location in the entire bolt
    val effects = ArrayList<VisualEffect>()
    val imageName = "lightning"
    val lightDistance = PixelDistance(TileDistance(4))
    locations.forEachIndexed { i, _ ->
        // create a lightning visual effect for this location
        val effect = if (i % 4 == 0) LightSourceVisualEffect(imageName, lightDistance)
        else VisualEffect1(imageName)
        effects.add(effect)
    }

    // for each successive grouping of bolt locations we are going to display
    val visibleLength = 5
    for (i in 0 until effects.size + visibleLength) {
        // the following addition of effects should be displayed as a group
        onGroupOfChangesToOccur()

        // add each effect to the area that is part of the currently visible section of the bolt
        val first = max(0, i - visibleLength + 1)
        val last = min(effects.size - 1, i)
        (first..last).forEach { addEntity(effects[it], locations[it]) }

        // the previous grouping of effects is now complete, and should be displayed
        onGroupOfChangesOccurred()

        // pause a bit so this grouping of  bolt effects can be seen
        sleepThread(50)

        // the following addition of effects should be displayed as a group
        onGroupOfChangesToOccur()

        // remove each effect that is part of the currently visible section of the bolt
        (first..last).forEach { removeEntity(effects[it]) }
    }

    // get the last grouping of removals from the above loop displayed
    onGroupOfChangesOccurred()

    // for each being hit by the bolt
    beingsStruck.forEach { being ->
        // if this being isn't already dead (due to being damaged from appearing previously
        // in the list we are iterating), inform it of getting hit
        if (!being.isDead) {
            // if this being avoids the bolt
            var damage = randomBellCurveInt(1, 6, strength)
            val result = being.avoid(strength / 2)
            if (result.avoided) {
                // the damage is halved
                damage /= 2
            }

            // if this being didn't super-avoid the bolt
            if (!result.superAvoided) {
                // take the damage
                being.takeDamage(Damage(damage, DamageForms.electricity))
            }
        }
    }
}
