package silmar.contexts.gameplay.areas.extensions.loading

import silmar.sounds.SilmarSounds
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.specializations.EventMarker
import silmarengine.contexts.gameplay.entities.terrains.specializations.Summoner
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.tiles.Tiles.door
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.io.Serializable

fun Area.onDungeonCEndAreaLoaded() {
    // set up the stalkin summoner
    val summoners =
        terrains.filterIsInstance<Summoner>().sortedByDescending { it.location.y }
    val activator = EventMarkersActivator()
    val startCriterion = StartCriterion()
    val stalkinSummoner = summoners[0]
    stalkinSummoner.setSummoningParameters(SilmarMonsterTypes.stalkin, null, 3, 2,
        startCriterion, activator)

    // set up the warp-demon summoner
    val warpDemonSummoner = summoners[1]
    warpDemonSummoner.setSummoningParameters(SilmarMonsterTypes.warpDemon, null, 3, 1,
        startCriterion, activator)

    // set up the vampire summoner
    val vampireSummoner = summoners[2]
    vampireSummoner.setSummoningParameters(SilmarMonsterTypes.vampire, null, 3, 3,
        startCriterion, activator)

    // set up the living-dead-wizard summoner
    val wizardSummoner = summoners[3]
    val throne = terrains.first { it.isOfType(TerrainTypes.throne) }
    wizardSummoner.setSummoningParameters(SilmarMonsterTypes.livingDeadWizard,
        throne.location, 1, 1, startCriterion, null)

    // imbue the event-markers with a door-appears event
    val doorAppearsEvent = DoorAppearsEvent()
    val markers = terrains.filterIsInstance<EventMarker>()
    markers.forEach { it.setEvent(doorAppearsEvent) }
}

/**
 * Listens for a summoner's being done with its summoning, and once that happens it
 * triggers all event-marker terrains within LOS of the summoner.
 */
class EventMarkersActivator : Summoner.DoneListener, Serializable {
    override fun onDone(summoner: Terrain) {
        // for each terrain in LOS of the given summoner
        val area = summoner.area
        val terrains = area.getEntitiesInLOS(summoner.location, area.terrains)
        terrains.forEach { terrain ->
            // if this terrain is an event-marker
            if (terrain.isOfType(TerrainTypes.eventMarker)) {
                // trigger this event-marker
                (terrain as EventMarker).onTriggered()
            }
        }
    }
}

/**
 * The criterion for when most of the summoners in this level may start
 * summoning.
 */
private class StartCriterion : Summoner.StartCriterion {
    override fun getCanStart(summoner: Terrain, player: Player): Boolean {
        // return whether the player is above the summoner y-wise
        return player.location.y <= summoner.location.y
    }
}

/**
 * This event-marker event says to place a door tile adjacent to the marker's location.
 */
private class DoorAppearsEvent : EventMarker.Event {
    override fun occur(marker: EventMarker) {
        // for each of the four tile-locations that are one-tile orthogonally distant from
        // the one under this marker
        val area = marker.area
        val location = marker.location
        val spot = MutablePixelPoint()
        for (i in 0..3) {
            // determine the tile at this location
            spot.set(location)
            when (i) {
                0 -> spot.translate(-tileWidth, 0)
                1 -> spot.translate(tileWidth, 0)
                2 -> spot.translate(0, -tileHeight)
                3 -> spot.translate(0, tileHeight)
            }
            val tile = area.getTile(spot)

            // if the tile blocks los (meaning, it is likely a wall)
            if (tile.blocksLOS) {
                // replace this tile with a door
                area.onSoundIssued(SilmarSounds.doorAppears, spot)
                area.setTile(spot, door)

                // only one door should be placed
                break
            }
        }
    }
}
