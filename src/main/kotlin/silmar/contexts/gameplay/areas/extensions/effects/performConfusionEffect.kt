package silmar.contexts.gameplay.areas.extensions.effects

import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onConfusionAttempt
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOSAndRange
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Performs a confusion effect at the given pixel-location, with the given strength.
 *
 * @param affectMonsters        Whether this effect affects monsters (vs. non-monsters).
 */
fun Area.performConfusionEffect(
    location: PixelPoint, strength: Int, affectMonsters: Boolean
) {
    performPowerDischargeEffect(location)

    // for each being in LOS and within range of the given location
    val beings =
        getEntitiesInLOSAndRange(location, PixelDistance(strength * tileWidth), beings)
    beings.forEach { being ->
        // if this being is either a monster or not (depending on
        // what we were told to affect)
        if (affectMonsters && being is Monster || !affectMonsters && being !is Monster) {
            being.onConfusionAttempt(strength)
        }
    }
}
