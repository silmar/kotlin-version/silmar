package silmar.contexts.gameplay.areas

object AreaMotifs {
    /**
     * The flyweight sets of image names for each motif.
     */
    private val topsideTileImageNames =
        arrayOf("ground0", null, "roomFloor0", "roomWall0", "door0", null, null,
            "openDoor0", null, null, "water", "waterNW", "waterNE", "waterSW", "waterSE",
            "road", "roadNW", "roadNE", "roadSW", "roadSE", "roomWindow0", null,
            "outOfBounds")
    private val dungeonATileImageNames =
        arrayOf("floor11", "wall11", "roomFloor11", "roomWall11", "door11", null, null,
            "openDoor11", "openSecretDoor11", "openRoomSecretDoor11", "water")
    private val swampTileImageNames =
        arrayOf("floor21", "wall21", "roomFloor21", "roomWall21", "door21", null, null,
            "openDoor21", "openSecretDoor21", "openRoomSecretDoor21", "water", null, null,
            null, null, null, null, null, null, null, null, "slow21")
    private val cityTileImageNames =
        arrayOf("floor31", "wall31", "roomFloor31", "roomWall31", "door31", null, null,
            "openDoor31", "openSecretDoor31", "openRoomSecretDoor31", "water", null, null,
            null, null, "road31", null, null, null, null, "roomWindow31")
    private val dungeonBTileImageNames =
        arrayOf("floor12", "wall12", "roomFloor12", "roomWall12", "door12", null, null,
            "openDoor12", "openSecretDoor12", "openRoomSecretDoor12", "water")
    private val ruinsTileImageNames =
        arrayOf("floor22", "wall22", "roomFloor22", "roomWall22", "door22", null, null,
            "openDoor22", "openSecretDoor22", "openRoomSecretDoor22", "water")
    private val baseTileImageNames =
        arrayOf("floor32", "wall32", "roomFloor32", "roomWall32", "door32", null, null,
            "openDoor32", "openSecretDoor32", "openRoomSecretDoor32", "water")
    private val dungeonCTileImageNames =
        arrayOf("floor13", "wall13", "roomFloor13", "roomWall13", "door13", null, null,
            "openDoor13", "openSecretDoor13", "openRoomSecretDoor13", "water")
    private val daveDungeonTileImageNames =
        arrayOf("floor23", "wall23", "roomFloor23", "roomWall23", "door23", null, null,
            "openDoor23", "openSecretDoor23", "openRoomSecretDoor23", "water")
    private val moonTileImageNames =
        arrayOf("floor33", "wall33", "roomFloor33", "roomWall33", "door33", null, null,
            "openDoor33", "openSecretDoor33", "openRoomSecretDoor33", "water")

    /**
     * The flyweight motifs.
     */
    val topside = SilmarAreaMotif("topside", topsideTileImageNames, false)
    val dungeonA = SilmarAreaMotif("dungeonA", dungeonATileImageNames, false)
    val swamp = SilmarAreaMotif("swamp", swampTileImageNames, false)
    val city = SilmarAreaMotif("city", cityTileImageNames, true)
    val dungeonB = SilmarAreaMotif("dungeonB", dungeonBTileImageNames, false)
    val ruins = SilmarAreaMotif("ruins", ruinsTileImageNames, false)
    val base = SilmarAreaMotif("base", baseTileImageNames, true)
    val dungeonC = SilmarAreaMotif("dungeonC", dungeonCTileImageNames, false)
    val daveDungeon = SilmarAreaMotif("daveDungeon", daveDungeonTileImageNames, false)
    val moon = SilmarAreaMotif("moon", moonTileImageNames, true)
}