package silmar.contexts.gameplay.areas

import silmarengine.contexts.gameplay.areas.AreaMotif

class SilmarAreaMotif(
    name: String, tileImageNames: Array<String?>,
    /**
     * Whether or not this motif is meant to represent a technological
     * setting.
     */
    val isTech: Boolean
) : AreaMotif(name, tileImageNames)