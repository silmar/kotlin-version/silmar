package silmar.contexts.gameplay.areas

import silmar.contexts.gameplay.areas.AreaLevels.topside
import java.util.*
import kotlin.math.max

/**
 * An object describing where an area fits into the game's sequence
 * of levels.
 */
class AreaLevel(
    /**
     * The motif to be used for this level.
     */
    val motif: SilmarAreaMotif,
    /**
     * If this level uses a set area, this is the name of the image file
     * depicting the layout for that area.
     */
    val areaFileName: String? = null,
    /**
     * The text (if any) to presented to the player when this level is first
     * encountered.
     */
    val intro: String? = null
) {
    /**
     * The index of this level amongst all others.
     */
    val index: Int

    /**
     * Returns whether this level is always completely lit.
     */
    val isSelfLit: Boolean
        get() = equals(topside) || motif.isTech

    /**
     * Returns the average amount of gold found in a gold-pile item on
     * this level.
     */
    val averageGoldItemQuantity: Int
        get() = (index + 1) * 50

    /**
     * Returns what the maximum value should be of any randomly-generated
     * item to be placed within this level.
     */
    val maxItemPlacementValue: Int
        get() = max(index, 1) * 1000

    /**
     * Returns the strength (in damage dice) of a fireball trap on this level.
     */
    val fireballTrapStrength: Int
        get() = 4 + index / 6

    /**
     * Returns the strength (in damage dice) of a gasser trap on this level.
     */
    val gasserTrapStrength: Int
        get() = 4 + index / 3

    /**
     * Returns the strength (in damage dice) of a lightning-bolt trap on this level.
     */
    val lightningBoltTrapStrength: Int
        get() = 4 + index / 6

    /**
     * Returns whether this is a randomly-generated (vs. a set) level.
     */
    val isRandomlyGenerated: Boolean
        get() = areaFileName == null

    init {
        levels.add(this)
        index = levels.size - 1
    }

    override fun equals(other: Any?): Boolean {
        return other is AreaLevel && other.index == index
    }

    override fun hashCode(): Int {
        return index
    }

    @Suppress("unused")
    companion object {
        /**
         * Holds all the levels in order, so that we can figure out
         * which level comes next after each previous level.
         */
        private val levels = ArrayList<AreaLevel>()

        /**
         * Returns the level with the given index.
         */
        fun getLevel(index: Int): AreaLevel? {
            return if (index < 0 || index >= levels.size) null else levels[index]
        }
    }
}
