package silmar.contexts.gameplay.areas

/**
 * The flyweight area-levels.
 */
@Suppress("unused")
object AreaLevels {
    val topside = AreaLevel(AreaMotifs.topside, "0",
        intro = "You arrive at the ruins of the tower.  Makeshift structures house a few brave souls willing to sell their wares to adventurers like you.")
    val dungeonA1 = AreaLevel(AreaMotifs.dungeonA,
        intro = "The musty, dust-filled air of the dungeons enters your lungs...  If you cannot see, use a torch!")
    val dungeonA2 = AreaLevel(AreaMotifs.dungeonA)
    val dungeonAEnd = AreaLevel(AreaMotifs.dungeonA, "11e",
        intro = "There is some jagged writing on a wall of this room:<br><br><b>How fortunate are the fleet of foot!</b>")
    val swamp1 = AreaLevel(AreaMotifs.swamp,
        intro = "You feel an odd sort of queasiness that passes abruptly.  The air becomes far more humid, and it reeks of putrescent gases.  Perhaps you are entering some sort of swamp...")
    val swamp2 = AreaLevel(AreaMotifs.swamp)
    val swampEnd = AreaLevel(AreaMotifs.swamp, "21e")
    val city1 = AreaLevel(AreaMotifs.city,
        intro = "Another odd sensation sweeps over you.  Your surroundings come to resemble a strange-looking town that has experienced some sort of catastrophic event, one which affected living things but left structures intact...")
    val city2 = AreaLevel(AreaMotifs.city)
    val cityEnd = AreaLevel(AreaMotifs.city, "31e")
    val dungeonB1 = AreaLevel(AreaMotifs.dungeonB,
        intro = "You feel something like a hard tug on your very essence.  The dungeons resume a more normal appearance...")
    val dungeonB2 = AreaLevel(AreaMotifs.dungeonB)
    val dungeonBEnd = AreaLevel(AreaMotifs.dungeonB, "12e")
    val ruins1 = AreaLevel(AreaMotifs.ruins,
        intro = "After another bizarre sense of shifting fades, you arrive in what appears to be the forgotten ruins of an ancient temple...")
    val ruins2 = AreaLevel(AreaMotifs.ruins)
    val ruinsEnd = AreaLevel(AreaMotifs.ruins, "22e")
    val base1 = AreaLevel(AreaMotifs.base,
        intro = "A violent shift sends you to some sort of fortified military complex...")
    val base2 = AreaLevel(AreaMotifs.base)
    val baseEnd = AreaLevel(AreaMotifs.base, "32e")
    val dungeonC1 = AreaLevel(AreaMotifs.dungeonC,
        intro = "Another tug at your being brings you back to more-familiar dungeon surroundings.  However, you begin to sense the proximity of a very dark presence...")
    val dungeonC2 = AreaLevel(AreaMotifs.dungeonC)
    val dungeonCEnd = AreaLevel(AreaMotifs.dungeonC, "13e")
    val daveDungeon1 = AreaLevel(AreaMotifs.daveDungeon,
        intro = "At this point the dungeon's appearance gains an almost regal elegance that seems very much out of place down here...")
    val daveDungeon2 = AreaLevel(AreaMotifs.daveDungeon)
    val daveDungeonEnd = AreaLevel(AreaMotifs.daveDungeon, "23e")
    val moon1 = AreaLevel(AreaMotifs.moon,
        intro = "A massive, almost paralyzing wave of disorientation passes through you.  The scenery becomes stark and barren, almost otherworldly in its coldness...")
    val moon2 = AreaLevel(AreaMotifs.moon)
    val moonEnd = AreaLevel(AreaMotifs.moon, "33e")
}