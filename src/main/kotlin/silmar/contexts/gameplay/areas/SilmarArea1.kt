package silmar.contexts.gameplay.areas

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.areas.extensions.lighting.addExitLightSources
import silmar.contexts.gameplay.areas.extensions.loading.*
import silmar.sounds.SilmarSounds
import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.SerializationOverride
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.areas.Area1
import silmarengine.sounds.Sound
import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.Tiles
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable

class SilmarArea1(levelNum: Int) : SilmarArea, Area1(), Serializable {
    /**
     * This isn't serialized with this area because its values never change.
     */
    @Transient
    override var level: AreaLevel = AreaLevel.getLevel(levelNum)!!
        private set

    /**
     * The index number of the above level object, to be serialized with this object
     * rather than the level object itself (which never changes).  Do not use this
     * field for any other purpose - use level.index, instead.
     */
    private var levelIndex: Int = 0

    override val isSelfLit = level.isSelfLit

    init {
        // determine this area's level and motif
        motif = level.motif

        loadOrGenerate(level.areaFileName)

        addExitLightSources()
    }

    override val outOfBoundsTile: Tile get() =
        if (level.index > 0) Tiles.wall else Tiles.outOfBounds

    @SerializationOverride
    @Throws(IOException::class)
    private fun writeObject(@Suppress("UNUSED_PARAMETER") stream: ObjectOutputStream) {
        // store a lookup for this area's level (rather than the level itself)
        levelIndex = level.index

        stream.defaultWriteObject()
    }

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(@Suppress("UNUSED_PARAMETER") stream: ObjectInputStream) {
        stream.defaultReadObject()

        // determine this area's level from its lookup
        level = AreaLevel.getLevel(levelIndex)!!
    }

    override fun onLoaded() {
        when (level) {
            AreaLevels.cityEnd -> onCityEndAreaLoaded()
            AreaLevels.dungeonBEnd -> onDungeonBEndAreaLoaded()
            AreaLevels.baseEnd -> onBaseEndAreaLoaded()
            AreaLevels.dungeonCEnd -> onDungeonCEndAreaLoaded()
            AreaLevels.daveDungeonEnd -> onDaveDungeonEndAreaLoaded()
        }
    }

    override fun getEntityAddedSound(entity: Entity): Sound? {
        var sound: Sound? = null
        when (entity) {
            is Terrain -> {
                when (entity.type) {
                    SilmarTerrainTypes.vengefulSymbol -> sound = SilmarSounds.vengefulSymbol
                    SilmarTerrainTypes.web -> sound = SilmarSounds.web
                    SilmarTerrainTypes.landMine -> sound = SilmarSounds.landMinePlaced
                    SilmarTerrainTypes.brokenVendingMachine -> sound = SilmarSounds.machineBreaks
                    SilmarTerrainTypes.fire -> sound = SilmarSounds.fire
                    SilmarTerrainTypes.downExit -> sound = SilmarSounds.exitAppears
                }
            }
            is Monster -> {
                if (entity.isOfType(
                        SilmarMonsterTypes.troll)) sound = SilmarSounds.trollBackToLife
            }
        }

        return sound
    }

    override val openDoorSound get() = when (motif) {
        AreaMotifs.city, AreaMotifs.base -> SilmarSounds.modernDoorOpens
        AreaMotifs.dungeonB -> SilmarSounds.barsDoorOpens
        AreaMotifs.daveDungeon -> SilmarSounds.daveDoorOpens
        AreaMotifs.moon -> SilmarSounds.autoDoorOpens
        else -> super.openDoorSound
    }

    override val closeDoorSound get() = when (motif) {
        AreaMotifs.city, AreaMotifs.base -> SilmarSounds.modernDoorCloses
        AreaMotifs.dungeonB -> SilmarSounds.barsDoorCloses
        AreaMotifs.daveDungeon -> SilmarSounds.daveDoorCloses
        AreaMotifs.moon -> SilmarSounds.autoDoorCloses
        else -> super.closeDoorSound
    }
}