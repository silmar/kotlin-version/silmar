package silmar.contexts.gameplay.areas

import silmarengine.contexts.gameplay.areas.Area

interface SilmarArea : Area {
    /**
     * An object describing which level in the game this area represents.
     */
    val level: AreaLevel
}