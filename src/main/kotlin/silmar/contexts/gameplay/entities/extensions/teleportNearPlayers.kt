package silmar.contexts.gameplay.entities.extensions

import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocationInRange
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.entities.Entity
import silmarengine.contexts.gameplay.entities.extensions.proximity.isWithinDistanceOf
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Teleports the given entity within its area to a passible location nearby to its
 * current location that is also at least a certain distance away from the player.
 */
fun Entity.teleportNearPlayer() {
    // try this a certain number of times
    val maxDistance = PixelDistance(10 * tileWidth)
    val maxTries = 30
    for (i in 0..maxTries) {
        // find a random passible rectangle within a certain range of the
        // entity's current location
        val test =
            area.getRandomPassibleRectangleLocationInRange(location, size, maxDistance, 50,
                movementType, true) ?: break

        // if the above call could not produce a potential destination, don't try again,
        // as that call makes a lot of attempts

        // if the distance from the chosen location to the player in LOS isn't enough,
        // try again
        val minDistanceToPlayer = PixelDistance(6 * tileWidth)
        val player = area.player
        if (player?.isInLOSOf(test) == true && player.isWithinDistanceOf(test,
                minDistanceToPlayer)) continue

        // teleport the entity to the chosen location
        area.performTeleportEffect(location, out = true, leaveFinalFrame = false)
        area.moveEntity(this, test)
        area.performTeleportEffect(test, out = false, leaveFinalFrame = false)
        break
    }
}
