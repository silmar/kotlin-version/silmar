package silmar.contexts.gameplay.entities.beings.player.extensions.actions

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.detectTraps
import silmarengine.contexts.gameplay.tiles.Tile

fun Player.onMovedOntoNewTile(@Suppress("UNUSED_PARAMETER") tile: Tile): Boolean {
    this as SilmarPlayer
    return !detectTraps()
}