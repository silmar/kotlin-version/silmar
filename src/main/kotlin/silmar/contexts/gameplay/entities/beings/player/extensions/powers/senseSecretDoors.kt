package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect
import silmarengine.contexts.gameplay.areas.effects.VisualEffect1
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.tiles.getLocationsOfTilesInLOSAndRange
import silmarengine.contexts.gameplay.tiles.Tile
import silmarengine.contexts.gameplay.tiles.Tiles.roomSecretDoor
import silmarengine.contexts.gameplay.tiles.Tiles.secretDoor
import silmarengine.util.math.randomInt
import silmarengine.util.sleepThread
import java.util.ArrayList

/**
 * Has this player try to sense every secret door in LOS and within a certain range.
 */
fun Player.senseSecretDoors() {
    performPowerDischargeEffect(this)

    // find all secret door tiles in LOS and within a certain range of this player
    val tiles = ArrayList<Tile>()
    tiles.add(secretDoor)
    tiles.add(roomSecretDoor)
    val locations =
        area.getLocationsOfTilesInLOSAndRange(tiles, location, TileDistance(10))

    // for each secret door location found
    val sensed = locations.filter { location ->
        // make an intelligence check to sense this door location
        val roll = randomInt(1, 20)
        val tileDistance = TileDistance(getDistance(this.location, location))
        roll <= attributeValues.intelligence - tileDistance.distance / 3
    }

    // for each secret door location sensed above
    val effects = ArrayList<VisualEffect>()
    sensed.forEach { location ->
        // add a mark visual effect at this location
        val effect = VisualEffect1("mark")
        area.addEntity(effect, location)
        effects.add(effect)

        // issue an accompanying sound
        area.onSoundIssued(SilmarSounds.markLocation, location)

    }
    // pause a bit for the effects to be seen
    if (sensed.isNotEmpty()) sleepThread(1000)

    // remove all mark visual-effects added to the area above
    effects.forEach { area.removeEntity(it) }
}

