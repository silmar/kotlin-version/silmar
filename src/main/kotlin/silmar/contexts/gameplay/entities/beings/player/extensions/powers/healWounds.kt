package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.util.math.randomInt

/**
 * Has this player perform a heal-wounds power on itself.
 */
fun Player.healWounds() {
    performPowerDischargeEffect(this)

    takeHealing(randomInt(2, 16))
}

