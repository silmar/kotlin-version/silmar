package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performFireballEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.geometry.getSkeweredTargetLocation
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt

class DarkKnight : Monster1() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if it's time to hurl a fireball
        if (randomInt(1, 5) == 1) {
            // hurl at a fireball at a skewered version of player's location
            val to =
                getSkeweredTargetLocation(
                    area, location, player.location, PixelDistance(3 * tileWidth))
            area.performFireballEffect(location, to, 10)
            return true
        }

        return false
    }
}