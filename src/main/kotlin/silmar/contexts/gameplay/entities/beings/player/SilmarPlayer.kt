package silmar.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth

interface SilmarPlayer : Player {
    val silmarLevelValues: SilmarPlayerLevelValues

    val silmarStats: SilmarPlayerStats

    val silmarStatuses: SilmarPlayerStatuses

    /**
     * The powers that are currently usable by this player.
     */
    val powers: List<PlayerPower>

    /**
     * Sets this player's size to that of the image for its current experience level.
     */
    fun setSizeFromLevelImage()

    fun getMemorizedLocation(level: Int): PixelPoint?
    fun onLocationMemorized()

    companion object {
        /**
         * The maximum range at which this player can use one of its powers on monsters.
         */
        val maxPowerRange = PixelDistance(17 * tileWidth)
    }
}

const val startingSilmarPlayerLevel = 5