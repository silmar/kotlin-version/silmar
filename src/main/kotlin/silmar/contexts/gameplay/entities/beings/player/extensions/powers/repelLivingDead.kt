package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.clericalCastingLevel
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmar.contexts.gameplay.areas.extensions.effects.performRepelLivingDeadEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import kotlin.math.max

/**
 * Has this player perform a repel-living-dead power.
 */
fun SilmarPlayer.repelLivingDead() {
    performPowerDischargeEffect(this)

    // determine the strength of the repel as the greater between half this player's level
    // (if this player is a paladin) and a multiple of its clerical-casting level
    // (if it's a cleric)
    val paladin = hasGainedLevel(PlayerLevels.paladin)
    val cleric = hasGainedLevel(PlayerLevels.cleric)
    val clericStrength = if (cleric) clericalCastingLevel else 0
    val paladinStrength = if (paladin) 2 else 0
    val strength = max(clericStrength, paladinStrength)
    performRepelLivingDeadEffect(this, strength)

    // if this player is a paladin
    if (paladin) {
        // damage is taken for exercising this power
        takeDamage(
            Damage(PlayerPowers.repelLivingDeadCostForPaladin, DamageForms.exertion))
    }
}

