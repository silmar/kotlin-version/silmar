package silmar.contexts.gameplay.entities.beings.talkerBeings.types

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.TalkerBeingType

fun createSilmarTalkerBeingType(
    name: String, imageName: String, frequency: Int, movementPoints: Int,
    greeting: String
): TalkerBeingType =
    SilmarTalkerBeingType1(name, imageName, frequency, movementPoints, greeting)
