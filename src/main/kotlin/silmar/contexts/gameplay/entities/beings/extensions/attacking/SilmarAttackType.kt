package silmar.contexts.gameplay.entities.beings.extensions.attacking

import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.defaultAttackTypeMinAnimationDelay
import silmarengine.sounds.Sound

class SilmarAttackType(
    imageName: String, sound: Sound,
    minAnimationDelay: Int = defaultAttackTypeMinAnimationDelay, lightTileRadius: Int = 0,
    val penetrationType: PenetrationType = PenetrationTypes.normal
) : AttackType(imageName, sound, minAnimationDelay, lightTileRadius)