package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes

class Blob : Monster1() {

    override fun affectDamage(damage: Damage) {
        super.affectDamage(damage)
        damage.amount = 1
    }

    override fun onDamaged(damage: Damage) {
        // if a foe with a non-ranged (and non-hand) weapon caused the damage
        val foe = damage.foe ?: return
        val weapon = damage.weapon ?: return
        if (!weapon.isOfType(WeaponTypes.hand) && !weapon.weaponType.isRanged) {
            // if the foe doesn't avoid, this blob grabs the weapon
            val result = foe.avoid(powerRating / 2)
            if (!result.avoided) {
                weapon.holder?.onMessage("Your weapon is absorbed by the blob!")
                weapon.destroy()
            }
        }
    }
}