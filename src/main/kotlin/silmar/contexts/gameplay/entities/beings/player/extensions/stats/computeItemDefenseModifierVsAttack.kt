package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationTypes
import silmar.contexts.gameplay.entities.beings.extensions.attacking.SilmarAttackType
import silmar.contexts.gameplay.entities.items.types.SilmarItemType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.items.types.ItemType

fun computeItemDefenseModifierVsAttack(itemType: ItemType, attackType: AttackType): Int? {
    val itemSubType = itemType.subType
    return when (attackType) {
        is SilmarAttackType -> when {
            itemSubType is SilmarItemType -> itemSubType.defenseModifiers[attackType.penetrationType.rank]
            attackType.penetrationType == PenetrationTypes.normal -> itemType.defenseModifier
            else -> if ((itemType.isArmor || itemType.isShield) && itemType.isMetal) 1 + itemType.plus else 0
        }
        else -> null
    }
}
