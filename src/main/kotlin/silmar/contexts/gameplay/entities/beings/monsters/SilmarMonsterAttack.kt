package silmar.contexts.gameplay.entities.beings.monsters

import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationType
import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationTypes
import silmar.contexts.gameplay.entities.beings.extensions.attacking.SilmarAttackTypes
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.deathbotGrasp
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.deathbotRay
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.grenadierPistol
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.livingDeadWizard2VortexBeam
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.mercenaryAssaultRifle
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.mutatedWarriorPistol
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.orcCrossbow
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.sentryborgPincer
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.sentryborgStunRay
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.slimeChuckerSlime
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks.weirdThingZap
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackTypes
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.damage.DamageForm

class SilmarMonsterAttack(
    description: String, minDamage: Int, maxDamage: Int, maxTileRange: Int,
    halfAccuracy: Int, damageForm: DamageForm
) : MonsterAttack(description, minDamage, maxDamage, maxTileRange, halfAccuracy,
    damageForm) {
    override val attackType: AttackType
        get() = when (this) {
            weirdThingZap -> SilmarAttackTypes.electricity
            deathbotRay -> SilmarAttackTypes.deathRay
            orcCrossbow -> AttackTypes.oldProjectile
            slimeChuckerSlime -> SilmarAttackTypes.slimeChuck
            livingDeadWizard2VortexBeam -> SilmarAttackTypes.vortexBeam
            else -> when {
                maxRange.distance > 1 -> {
                    when (penetrationType) {
                        PenetrationTypes.laser -> SilmarAttackTypes.laser
                        PenetrationTypes.firearm -> SilmarAttackTypes.bullet
                        else -> AttackTypes.oldProjectile
                    }
                }
                else -> super.attackType
            }
        }

    /**
     * Returns the penetration level of this attack.
     */
    val penetrationType: PenetrationType
        get() = when (this) {
            orcCrossbow, grenadierPistol, mercenaryAssaultRifle, sentryborgPincer, mutatedWarriorPistol -> PenetrationTypes.firearm
            sentryborgStunRay, deathbotGrasp, deathbotRay, livingDeadWizard2VortexBeam -> PenetrationTypes.laser
            else -> PenetrationTypes.normal
        }
}