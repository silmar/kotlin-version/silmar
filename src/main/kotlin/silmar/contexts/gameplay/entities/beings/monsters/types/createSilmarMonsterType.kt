package silmar.contexts.gameplay.entities.beings.monsters.types

import silmar.contexts.gameplay.areas.AreaLevel
import silmarengine.contexts.gameplay.entities.beings.Alignment
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.sounds.Sound

fun createSilmarMonsterType(
    frequency: Int, name: String, imageName: String?, powerRating: Int,
    alignment: Alignment, experienceValue: Int, movementPoints: Int, defenseNormal: Int,
    defenseFirearm: Int, defenseLaser: Int, isLivingDead: Boolean,
    meleeAttacks: Array<MonsterAttack>?, rangedAttacks: Array<MonsterAttack>?,
    rangedAttackFrequency: Int, validLevels: Array<AreaLevel>?,
    movementType: MovementType, weaponPlusNeededToHit: Int, avoidanceModifier: Int,
    isLessVulnerableToEdgedWeapons: Boolean, sound: Sound?, soundFrequency: Float,
    isStationary: Boolean, mustSeePlayerToUsePower: Boolean = true,
    lightRadiusInTiles: Int? = null
): SilmarMonsterType =
    SilmarMonsterType1(frequency, name, imageName, powerRating, alignment,
        experienceValue, movementPoints, defenseNormal, defenseFirearm, defenseLaser,
        isLivingDead, meleeAttacks, rangedAttacks, rangedAttackFrequency, validLevels,
        movementType, weaponPlusNeededToHit, avoidanceModifier,
        isLessVulnerableToEdgedWeapons, sound, soundFrequency, isStationary,
        mustSeePlayerToUsePower, lightRadiusInTiles)
