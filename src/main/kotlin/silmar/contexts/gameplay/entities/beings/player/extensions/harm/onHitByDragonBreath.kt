package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.DamageForm
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.items.extensions.condition.avoidOrDegrade

/**
 * Informs this player that it has been hit by dragon breath of the given type,
 * with the given strength.
 */
fun Player.onHitByDragonBreath(breathType: DamageForm, strength: Int) {
    // if this player doesn't avoid the breath
    val result = avoid(strength / 2)
    if (!result.avoided) {
        // if the breath type is acid
        if (breathType == DamageForms.acid) {
            // check equipped items for being damaged
            items.compressEquippedItems().forEach {
                it.avoidOrDegrade(
                    strength / 2 + if (breathType == DamageForms.acid) 6 else 0)
            }
        }
    }
}

