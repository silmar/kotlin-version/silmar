package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes

fun Player.computeExtraAttacksPerTurn(weapon: Weapon): Int {
    // if this player is a fighter
    var attacks = 0
    this as SilmarPlayer
    if (hasGainedLevel(PlayerLevels.fighter)) {
        // if the weapon in use is a melee weapon
        if (!weapon.weaponType.isRanged) {
            // this player gets an extra attack
            attacks++
        }
    }

    // if this player is a monk
    if (hasGainedLevel(PlayerLevels.monk)) {
        // if the weapon is use is the hand
        if (weapon.isOfType(WeaponTypes.hand)) {
            // this player gets an extra attack
            attacks++
        }
    }

    // if this player is an archer
    if (hasGainedLevel(PlayerLevels.archer)) {
        // if the weapon in use is a subtype of the bow or crossbow
        if (weapon.type.isDescendedFromType(
                WeaponTypes.bow.itemType) || weapon.type.isDescendedFromType(
                WeaponTypes.crossbow.itemType)) {
            // this player gets an extra attack
            attacks++
        }
    }

    // if this player has a helm of combat mastery equipped
    if (items.isItemOfTypeEquipped(SilmarMagicItemTypes.helmOfCombatMastery.itemType)) {
        // if the weapon in use is a melee weapon
        if (!weapon.weaponType.isRanged) {
            // this player gets an extra attack
            attacks++
        }
    }

    if (silmarStatuses.isQuickened) attacks *= 2

    return attacks
}