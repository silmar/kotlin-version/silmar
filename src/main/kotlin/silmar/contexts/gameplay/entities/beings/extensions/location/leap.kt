package silmar.contexts.gameplay.entities.beings.extensions.location

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.passibility.getRandomPassibleRectangleLocationInRange
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.extensions.visibility.canSeeExt
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt
import kotlin.math.min

/**
 * Has the given being leap directly to the given to-location.  If the to-location
 * is not within range of the being (where the range is determined by the given strength rating),
 * the leap is not allowed.  If the being fails an agility test using the given rating
 * (and modified by the range), the to-location is modified according to the leap distance
 * and how badly the test failed.
 *
 * @return  Whether the leap was allowed.
 */
fun Being.leap(
    to: PixelPoint, strength: Int, agility: Int
): Boolean {
    // if the given location isn't passible, or can't be
    // seen from the given being's location
    val from = location
    val beingSize = size
    if (!area.isRectanglePassible(to, beingSize, MovementType.WALKING, true, null, null,
            false).passible || !area.canSeeExt(from, to).canSee) {
        return false
    }

    // if the given location is beyond the max leap range
    val maxRange = PixelDistance(strength / 2 * tileWidth)
    val distance = getDistance(from, to)
    if (distance.distance > maxRange.distance) {
        if (this is Player) {
            onMessage("You cannot leap that far")
        }

        return false
    }

    // if the being fails an agility check modified by half the tile-distance of the leap
    val roll = randomInt(1, 20) + distance.distance / tileWidth / 2
    val missedBy = roll - agility
    var adjustedTo = to
    if (missedBy > 0) {
        // the location shifts to some random passible rectangle-location
        // (that must still be visible from the being)
        // within a range of the target determined by how badly
        // the being missed the check
        val maxError =
            PixelDistance(min(distance.distance / 2, missedBy / 2 * tileWidth))
        var adjusted: PixelPoint?
        do {
            adjusted =
                area.getRandomPassibleRectangleLocationInRange(to, size, maxError,
                    50, movementType, true)
            if (adjusted == null) break
        } while (adjusted != null && !area.canSeeExt(from, adjusted).canSee)
        if (adjusted != null) adjustedTo = adjusted
    }

    performPowerDischargeEffect(this)

    area.onSoundIssued(SilmarSounds.leap, from)

    // move the being to the leap location
    area.moveEntity(this, adjustedTo)
    return true
}
