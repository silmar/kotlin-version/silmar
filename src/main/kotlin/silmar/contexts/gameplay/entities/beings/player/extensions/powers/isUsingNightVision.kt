package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.areas.extensions.lighting.isLit

val SilmarPlayer.isUsingNightVision: Boolean
    get() {
        if (isEmittingLight || !hasGainedLevel(PlayerLevels.werewolf)) return false

        // if this player's location is lit, this player can't use its night-vision
        return !area.isLit(location).isHalfOrMoreLit
    }

