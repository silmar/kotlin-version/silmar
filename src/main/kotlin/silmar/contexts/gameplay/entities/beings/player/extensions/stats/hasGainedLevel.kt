package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerLevel
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.startingPlayerLevel

/**
 * Returns whether this player has achieved the given player-level (and still currently
 * holds it).
 */
fun SilmarPlayer.hasGainedLevel(level: PlayerLevel): Boolean {
    // return whether this player's levels list contains the given level, and if it does,
    // whether it's before or after the player's current level in the list
    val index = silmarLevelValues.levels.indexOf(level)
    return index >= 0 && index <= this.levelValues.level - startingPlayerLevel
}

