package silmar.contexts.gameplay.entities.beings.monsters.extensions.harm

import silmar.contexts.gameplay.areas.extensions.effects.performPoofEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

fun Monster.indicateSilmarDeath(area: Area, location: PixelPoint) =
    area.performPoofEffect(location, size)

