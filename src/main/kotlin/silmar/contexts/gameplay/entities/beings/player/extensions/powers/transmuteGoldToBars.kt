package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.entities.items.createItemEntity
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.types.TreasureTypes
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Transform each set of a certain amount of gold at and around this player's location
 * (but not on its person) into a gold bar, which encumbers less.
 */
fun Player.transmuteGoldToBars() {
    performPowerDischargeEffect(this)

    // if this player is carrying gold
    var gold = 0
    val held = items.inventory.getFirstItemOfType(GroupableItemTypes.gold)
    if (held != null) {
        // add the carried gold to our running total gold that is available
        // for transmutation
        gold += held.quantity

        // remove the gold item from this player
        items.inventory.tryToRemoveItem(held)
    }

    // for each gold item within a short range of this player
    val itemEntities =
        area.items.getEntitiesInRange(location, PixelDistance(2 * tileWidth))
    itemEntities.filter { it.item.isOfType(GroupableItemTypes.gold) }.forEach { itemEntity ->
        // add the quantity of this gold to the running total of
        // available gold we are keeping
        gold += itemEntity.item.quantity

        // get rid of this item
        area.removeEntity(itemEntity)
    }

    // for each item-condition except the 'destroyed' one
    val barValue = TreasureTypes.goldBar.price
    var barMade = false
    ItemCondition.values().drop(1).reversed()
        .forEach { condition ->
            // while there's enough gold to make a bar of this condition
            val conditionBarValue = (barValue * condition.valueFraction).toInt()
            while (gold >= conditionBarValue) {
                // subtract the bar-value from the gold
                gold -= conditionBarValue

                // create a gold-bar item of this condition
                val bar = createItem(TreasureTypes.goldBar, condition)

                // add the gold-bar item to this player's inventory
                items.inventory.addItem(bar)

                barMade = true
            }
        }

    // if there wasn't enough gold to make even one bar above
    if (!barMade) {
        // inform the player
        onMessage(
            "There's not enough gold in or around your location for you to productively use this power.",
            MessageType.error)
    }

    // if there is any gold leftover
    if (gold > 0) {
        // create a new gold item with a quantity equal to the leftover gold
        // and add it to the area
        val goldItem = createGroupableItem(GroupableItemTypes.gold, gold)
        area.addEntity(createItemEntity(goldItem), location)
    }
}

