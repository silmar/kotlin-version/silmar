package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.extensions.location.teleportRandomlyWithinArea
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.damage.Damage

class DreadedPansy : Monster1() {
    override fun onDamaged(damage: Damage) {
        // if this pansy is still alive
        if (hitPoints > 0) {
            teleportRandomlyWithinArea()
        }
    }
}