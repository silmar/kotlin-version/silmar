package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.areas.extensions.effects.performMeteorShowerEffect
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.magicUserCastingLevel
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Has this player summon a meteor shower around itself.
 */
fun SilmarPlayer.summonMeteorShower() {
    performPowerDischargeEffect(this)

    area.performMeteorShowerEffect(location, true, 2 + magicUserCastingLevel)
}

