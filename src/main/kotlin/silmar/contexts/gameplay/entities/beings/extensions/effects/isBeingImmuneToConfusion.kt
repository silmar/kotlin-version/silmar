package silmar.contexts.gameplay.entities.beings.extensions.effects

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.Being
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel

fun isBeingImmuneToConfusion(being: Being): Boolean =
    // players who are minotaurs are immune
    being is SilmarPlayer && being.hasGainedLevel(PlayerLevels.minotaur)