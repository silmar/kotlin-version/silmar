package silmar.contexts.gameplay.entities.beings.player.extensions.experience

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.entities.onEntityChangedAppearance
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds
import silmarengine.util.sleepThread

fun Player.onLevelChanged(forInit: Boolean, levelGained: Boolean) {
    this as SilmarPlayer
    setSizeFromLevelImage()

    // if this isn't an initialization call
    if (!forInit) {
        // notify this player and its area that its image is going to change
        onImageToChange()
        area.onEntityChangedAppearance()

        // if a level was gained
        if (levelGained) {
            // play a level-gained sound, and wait for it to finish
            area.onSoundIssued(Sounds.levelGained, location)
            sleepThread(1000)
        }
    }
}