package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.PlayerPower
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS
import silmarengine.contexts.gameplay.areas.extensions.entities.getMonstersInPassibleRange

/**
 * Returns whether this player can currently employ the given power.  Point-costs
 * for using the power are incurred here.
 */
fun SilmarPlayer.canEmployPowerNow(power: PlayerPower): Boolean {
    var canEmploy = true

    // if the power is throw-boulder, see if a boulder can indeed be thrown now
    if (canEmploy && power == PlayerPowers.throwBoulder && !canThrowBoulder) {
        canEmploy = false
    }

    // if the player doesn't have enough clerical or magic points to pay for the power use
    if (canEmploy && !incurPowerCost(power)) canEmploy = false

    // if there are either monsters in LOS of this player, or monsters within a certain
    // passible range of this player, and this player fails its check to successfully
    // use the given power
    if (canEmploy && (area.getEntitiesInLOS(location,
            area.monsters).isNotEmpty() || area.getMonstersInPassibleRange(location,
            SilmarPlayer.maxPowerRange).isNotEmpty()) && !checkForPowerUseSuccess(power)) {
        // the power use fails, and the player loses the rest of its turn
        onMessage("Power use failed!")
        canEmploy = false
        endTurn()
    }

    return canEmploy
}

