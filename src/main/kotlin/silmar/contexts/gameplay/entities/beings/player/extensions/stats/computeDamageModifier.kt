package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.cyborgIntelligencePerBonus
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.util.math.randomInt

fun Player.computeSilmarDamageModifier(isForOffhand: Boolean): Int {
    // if the weapon in use is a strength bow
    var modifier = 0
    val weapon = if (!isForOffhand) weapon else offhandWeapon
    if (weapon!!.isOfType(SilmarWeaponTypes.strengthBow)) {
        modifier += attributeValues.strength - 15
    }

    // if this player is a monk
    this as SilmarPlayer
    if (hasGainedLevel(PlayerLevels.monk)) {
        // if the weapon in use is the hand
        if (weapon.isOfType(WeaponTypes.hand)) {
            modifier += levelValues.level / 3
        }
    }

    // if this player is an archer
    if (hasGainedLevel(PlayerLevels.archer)) {
        // if the weapon in use is a subtype of the bow
        if (weapon.type.isDescendedFromType(WeaponTypes.bow.itemType)) {
            modifier += levelValues.level / 5
        }
    }

    // if this player is a cyborg
    if (hasGainedLevel(PlayerLevels.cyborg)) {
        // if the weapon in use is ranged
        if (weapon.weaponType.isRanged) {
            modifier += attributeValues.intelligence / cyborgIntelligencePerBonus
        }
    }

    // if this player is a werewolf
    if (hasGainedLevel(PlayerLevels.werewolf)) {
        // if the weapon in use is the hand
        if (weapon.isOfType(WeaponTypes.hand)) {
            modifier += randomInt(0, 6)
        }
    }

    // if the weapon in use isn't ranged
    if (!weapon.weaponType.isRanged) {
        // if this player is a minotaur
        if (hasGainedLevel(PlayerLevels.minotaur)) {
            // add a bonus for having horns
            modifier += randomInt(1, 4)
        }
    }

    return modifier
}