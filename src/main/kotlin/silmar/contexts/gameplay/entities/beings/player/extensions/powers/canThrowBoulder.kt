package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmar.contexts.gameplay.entities.beings.player.throwBoulderStrengthRequired
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle

/**
 * Whether this player is currently able to throw a boulder, including
 * whether there is actually a boulder at its current location.
 */
val Player.canThrowBoulder: Boolean
    get() {
        // for each terrain at this player's location
        val from = location
        val terrains = area.terrains.getEntitiesInRectangle(from, size)
        terrains.forEach { terrain ->
            // if this terrain is a boulder
            if (terrain.isOfType(SilmarTerrainTypes.boulder)) {
                // if this player isn't strong enough
                if (attributeValues.strength < throwBoulderStrengthRequired) {
                    // the throw attempt fails
                    onMessage(
                        "You lack the necessary strength of $throwBoulderStrengthRequired")
                    return false
                }

                // lift the boulder
                return true
            }
        }

        // if we make it here, no boulder was found at this player's location
        onMessage("There is no boulder here.")
        return false
    }
