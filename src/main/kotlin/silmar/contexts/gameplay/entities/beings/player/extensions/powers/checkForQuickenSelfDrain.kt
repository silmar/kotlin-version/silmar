package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.decrementBaseAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.util.math.randomInt

/**
 * Checks to see if this player is employing the quicken-self power, and if the number of
 * turns left on it should be decremented.
 */
fun SilmarPlayer.checkForQuickenSelfDrain() {
    // if the quicken-self power is currently in effect
    val statuses = silmarStatuses
    if (statuses.isQuickened) {
        // mark off one more turn of its duration
        statuses.drainQuickenedSelf()

        // if this was the last turn for the power to be in effect
        if (!statuses.isQuickened) {
            // report the change in quickened status
            reportToListeners { it.onVitalStatChanged() }

            // inform the user of the change in quickened status
            onMessage("You are no longer quickened.")

            // if a significant random chance says yes (in order to preserve play balance
            // in the presence of such a potent power)
            if (randomInt(1, 2) == 1) {
                // this player permanently loses a point of endurance due to the exertion
                // caused by the power use
                decrementBaseAttribute(PlayerAttributes.endurance)
            }
        }
    }
}

