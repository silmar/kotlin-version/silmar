package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performRadiationEffect
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.util.math.randomInt

class GammaVampire : Vampire() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if a random chance says yes
        if (randomInt(1, 2) == 1) {
            // do a radiation effect at this vampire's location
            area.performRadiationEffect(location)
        }

        // this vampire's power-use shouldn't affect its taking the rest of its turn
        return false
    }
}