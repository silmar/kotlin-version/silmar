package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.util.math.randomInt

/**
 * Returns whether this player disarms the given trap.
 */
fun SilmarPlayer.getDisarmsTrap(): Boolean {
    // if this player is not a thief
    if (!hasGainedLevel(PlayerLevels.thief)) return false

    // if this player makes its disarm check
    if (randomInt(1, 20) <= (attributeValues.intelligence + attributeValues.agility) / 2) {
        onMessage("You disarmed a trap!")
        return true
    }

    return false
}

