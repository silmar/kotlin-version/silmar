package silmar.contexts.gameplay.entities.beings.monsters.types

import silmar.Names
import silmar.contexts.gameplay.areas.AreaLevels
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Alignment
import silmarengine.contexts.gameplay.entities.beings.MovementType.*

typealias Attacks = SilmarMonsterAttacks

@Suppress("unused")
object SilmarMonsterTypes {
    /**
     * The flyweight sets of dungeon levels that define which monsters are on which levels.
     */
    val dungeon1Levels1 = arrayOf(AreaLevels.dungeonA1, AreaLevels.dungeonA2)
    val dungeon1Levels2 = arrayOf(AreaLevels.dungeonA2, AreaLevels.swamp1)
    val swampLevels1 = arrayOf(AreaLevels.swamp1, AreaLevels.swamp2)
    val swampLevels2 = arrayOf(AreaLevels.swamp2, AreaLevels.city1)
    val cityLevels1 = arrayOf(AreaLevels.city1, AreaLevels.city2)
    val cityLevels2 = arrayOf(AreaLevels.city2, AreaLevels.dungeonB1)
    val dungeon2Levels1 = arrayOf(AreaLevels.dungeonB1, AreaLevels.dungeonB2)
    val dungeon2Levels2 = arrayOf(AreaLevels.dungeonB2, AreaLevels.ruins1)
    val ruinsLevels1 = arrayOf(AreaLevels.ruins1, AreaLevels.ruins2)
    val ruinsLevels2 = arrayOf(AreaLevels.ruins2, AreaLevels.base1)
    val baseLevels1 = arrayOf(AreaLevels.base1, AreaLevels.base2)
    val baseLevels2 = arrayOf(AreaLevels.base2, AreaLevels.dungeonC1)
    val dungeon3Levels1 = arrayOf(AreaLevels.dungeonC1, AreaLevels.dungeonC2)
    val dungeon3Levels2 = arrayOf(AreaLevels.dungeonC2, AreaLevels.daveDungeon1)
    val daveDungeonLevels1 = arrayOf(AreaLevels.daveDungeon1, AreaLevels.daveDungeon2)
    val daveDungeonLevels2 = arrayOf(AreaLevels.daveDungeon2, AreaLevels.moon1)
    val moonLevels1 = arrayOf(AreaLevels.moon1, AreaLevels.moon2)
    val moonLevels2 = arrayOf(AreaLevels.moon2)

    /**
     * The flyweight monster types.
     */
    val giantRat =
        createSilmarMonsterType(5, "giant rat", "giantRat", 1, Alignment.evil, 1, 4, 3, 0,
            0, false, arrayOf(Attacks.giantRatBite), null, 0, dungeon1Levels1, WALKING, 0,
            0, false, null, 0f, false)
    val bat =
        createSilmarMonsterType(5, "bat", "bat", 1, Alignment.neutral, 1, 8, 2, 2, 2,
            false, arrayOf(Attacks.batBite), null, 0, dungeon1Levels1, FLYING_NO_HANDS, 0,
            0, false, SilmarSounds.bat, 1f, false)
    val orc =
        createSilmarMonsterType(5, "orc", "orc", 1, Alignment.evil, 3, 3, 4, 0, 0, false,
            arrayOf(Attacks.orcSword), arrayOf(Attacks.orcCrossbow), 2, dungeon1Levels1,
            WALKING, 0, 0, false, SilmarSounds.orc, 0.33f, false)
    val goblin =
        createSilmarMonsterType(5, "goblin", "goblin", 1, Alignment.evil, 2, 2, 4, 0, 0,
            false, arrayOf(Attacks.goblinSword), null, 0, dungeon1Levels1, WALKING, 0, 0,
            false, null, 0f, false)
    val skeleton =
        createSilmarMonsterType(5, "skeleton", "skeleton", 1, Alignment.neutral, 2, 4, 3,
            3, 3, true, arrayOf(Attacks.skeletonSword), null, 0, dungeon1Levels1, WALKING,
            0, 0, true, null, 0f, false)

    val reptileMan =
        createSilmarMonsterType(5, "reptile man", "reptileMan", 2, Alignment.neutral, 7,
            4, 5, 1, 0, false, arrayOf(Attacks.reptileManClaw, Attacks.reptileManClaw,
                Attacks.reptileManBite), null, 0, dungeon1Levels2, WALKING, 0, 0, false,
            null, 0f, false)
    val floatingSpheroid =
        createSilmarMonsterType(3, "floating spheroid", "floatingSpheroid", 1,
            Alignment.neutral, 4, 1, 0, 0, 0, false,
            arrayOf(Attacks.floatingSpheroidBite), null, 0, dungeon1Levels2,
            FLYING_NO_HANDS, 0, 0, false, SilmarSounds.floatingSpheroid, 1f, false)
    val zombie =
        createSilmarMonsterType(5, "zombie", "zombie", 2, Alignment.neutral, 4, 1, 2, 2,
            2, true, arrayOf(Attacks.zombieRend), null, 0, dungeon1Levels2, WALKING, 0, 0,
            false, null, 0f, false)
    val slime =
        createSilmarMonsterType(5, "slime", "slime", 2, Alignment.neutral, 8, 1, 1, 1, 1,
            false, arrayOf(Attacks.slimeOoze), null, 0, dungeon1Levels2, WALKING_NO_HANDS,
            0, 0, true, SilmarSounds.slime, 1f, false)
    val warlock =
        createSilmarMonsterType(5, "warlock", "warlock", 2, Alignment.evil, 8, 4, 3, 0, 0,
            false, arrayOf(Attacks.warlockDagger),
            arrayOf(Attacks.warlockDarts, Attacks.warlockDarts), 10, dungeon1Levels2,
            WALKING, 0, 0, false, null, 0f, false, lightRadiusInTiles = 6)
    val munchkin =
        createSilmarMonsterType(5, "munchkin", "munchkin", 2, Alignment.neutral, 5, 3, 5,
            5, 5, false, arrayOf(Attacks.munchkinGrab), null, 0, dungeon1Levels2, WALKING,
            0, 0, false, SilmarSounds.gremlin, .33f, false)

    val cannibal =
        createSilmarMonsterType(5, "cannibal", "cannibal", 1, Alignment.evil, 7, 5, 1, 0,
            0, false, arrayOf(Attacks.cannibalSpear),
            arrayOf(Attacks.cannibalBlowgunDart), 2, swampLevels1, WALKING, 0, 0, false,
            SilmarSounds.cannibal, .33f, false, lightRadiusInTiles = 4)
    val livingDeadPirate =
        createSilmarMonsterType(5, "living dead pirate", "livingDeadPirate", 1,
            Alignment.evil, 10, 3, 6, 0, 0, true,
            arrayOf(Attacks.livingDeadPirateGrab, Attacks.livingDeadPirateRake), null, 0,
            swampLevels1, WALKING, 0, 0, false, null, 0f, false)
    val painkiller =
        createSilmarMonsterType(5, "painkiller", "painkiller", 1, Alignment.neutral, 5, 3,
            0, 0, 0, false, arrayOf(Attacks.painkillerInjection), null, 0, swampLevels1,
            WALKING_NO_HANDS, 0, 0, false, null, 0f, false)
    val giantAnt =
        createSilmarMonsterType(5, "giant ant", "giantAnt", 2, Alignment.neutral, 4, 4, 4,
            2, 2, false, arrayOf(Attacks.giantAntBite), null, 0, swampLevels1,
            WALKING_NO_HANDS, 0, 0, false, null, 0f, false)
    val dreadedPansy =
        createSilmarMonsterType(5, "dreaded pansy", "dreadedPansy", 6, Alignment.neutral,
            40, 1, 2, 2, 2, false, null,
            arrayOf(Attacks.dreadedPansyNeedle, Attacks.dreadedPansyNeedle), 10,
            swampLevels1, WALKING_NO_HANDS, 0, 0, false, null, 0f, true)
    val poisonousSnake =
        createSilmarMonsterType(5, "poisonous snake", "poisonousSnake", 4,
            Alignment.neutral, 49, 5, 5, 2, 1, false,
            arrayOf(Attacks.poisonousSnakeFangs), null, 0, swampLevels1, WALKING_NO_HANDS,
            0, 0, false, null, 0f, false)
    val giantCentipede =
        createSilmarMonsterType(5, "giant centipede", "giantCentipede", 2,
            Alignment.neutral, 13, 3, 1, 0, 0, false, arrayOf(Attacks.giantCentipedeBite),
            null, 0, swampLevels1, WALKING_NO_HANDS, 0, 0, false, null, 0f, false)

    val ogre =
        createSilmarMonsterType(5, "ogre", "ogre", 4, Alignment.evil, 19, 3, 5, 1, 0,
            false, arrayOf(Attacks.ogreFist), null, 0, swampLevels2, WALKING, 0, 0, false,
            SilmarSounds.ogre, .25f, false)
    val scumShamble =
        createSilmarMonsterType(5, "scum shamble", "scumShamble", 8, Alignment.neutral,
            120, 2, 8, 8, 8, false,
            arrayOf(Attacks.scumShambleArm, Attacks.scumShambleArm), null, 0,
            swampLevels2, WALKING, 0, 0, false, SilmarSounds.scumShamble, 1f, false)
    val giantWasp =
        createSilmarMonsterType(5, "giant wasp", "giantWasp", 4, Alignment.neutral, 23, 4,
            6, 4, 4, false, arrayOf(Attacks.giantWaspBite, Attacks.giantWaspSting), null,
            0, swampLevels2, FLYING_NO_HANDS, 0, 0, false, null, 0f, false)
    val weirdThing =
        createSilmarMonsterType(5, "weird thing", "weirdThing", 5, Alignment.neutral, 37,
            5, 6, 6, 6, false, null, arrayOf(Attacks.weirdThingZap), 10, swampLevels2,
            WALKING, 0, 12, false, null, 0f, false)
    val killerTree =
        createSilmarMonsterType(3, "killer tree", "killerTree", 7, Alignment.evil, 100, 4,
            10, 7, 4, false, arrayOf(Attacks.killerTreeLimb, Attacks.killerTreeLimb),
            null, 0, swampLevels2, WALKING, 0, 0, false, null, 0f, false)
    val giantSpider =
        createSilmarMonsterType(5, "giant spider", "giantSpider", 5, Alignment.evil, 45,
            2, 6, 3, 1, false, arrayOf(Attacks.giantSpiderBite), null, 0, swampLevels2,
            WALKING_NO_HANDS, 0, 0, false, null, 0f, false)
    val slimeChucker =
        createSilmarMonsterType(5, "slime chucker", "slimeChucker", 4, Alignment.evil, 40,
            2, 1, 0, 0, false, null, arrayOf(Attacks.slimeChuckerSlime), 10, swampLevels2,
            WALKING, 0, 0, true, SilmarSounds.slimeChucker, 1f, false)

    val berzerker =
        createSilmarMonsterType(5, "berzerker", "berzerker", 1, Alignment.evil, 3, 5, 3,
            0, 0, false, arrayOf(Attacks.berzerkerSword, Attacks.berzerkerDagger), null,
            0, cityLevels1, WALKING, 0, 0, false, null, 0f, false, lightRadiusInTiles = 6)
    val typhoidZombie =
        createSilmarMonsterType(5, "typhoid zombie", "typhoidZombie", 4, Alignment.evil,
            25, 1, 2, 0, 0, true, arrayOf(Attacks.typhoidZombieRend), null, 0,
            cityLevels1, WALKING, 0, 0, false, null, 0f, false)
    val gremlin =
        createSilmarMonsterType(5, "gremlin", "gremlin", 2, Alignment.neutral, 10, 5, 8,
            8, 8, false, arrayOf(Attacks.gremlinSwipe), null, 0, cityLevels1, WALKING, 0,
            0, false, SilmarSounds.gremlin, .33f, false)
    val cyclops =
        createSilmarMonsterType(5, "cyclops", "cyclops", 5, Alignment.evil, 40, 4, 3, 0,
            0, false, arrayOf(Attacks.cyclopsClub), null, 0, cityLevels1, WALKING, 0, 0,
            false, SilmarSounds.cyclops, .25f, false)
    val landShark =
        createSilmarMonsterType(5, "land shark", "landShark", 6, Alignment.neutral, 50, 4,
            4, 0, 0, false, arrayOf(Attacks.landSharkBite), null, 0, cityLevels2,
            WALKING_NO_HANDS, 0, 0, false, null, 0f, false)
    val blob =
        createSilmarMonsterType(5, "blob", "blob", 5, Alignment.neutral, 100, 1, 0, 0, 0,
            false, arrayOf(Attacks.blobAcid), null, 0, cityLevels2, WALKING_NO_HANDS, 0,
            0, false, SilmarSounds.blob, 1f, false)
    val rogue =
        createSilmarMonsterType(5, "rogue", "rogue", 4, Alignment.evil, 55, 4, 4, 0, 0,
            false, arrayOf(Attacks.rogueDagger), null, 0, cityLevels2, WALKING, 0, 0,
            false, null, 0f, false, lightRadiusInTiles = 6)

    val transparentGelatinoid =
        createSilmarMonsterType(5, "transparent gelatinoid", null, 4, Alignment.neutral,
            23, 2, 2, 2, 2, false, arrayOf(Attacks.transparentGelatinoidAcid), null, 0,
            dungeon2Levels1, WALKING_NO_HANDS, 0, 0, true, SilmarSounds.blob, 1f, false)
    val gargoyle =
        createSilmarMonsterType(5, "gargoyle", "gargoyle", 4, Alignment.evil, 28, 4, 5, 2,
            1, false,
            arrayOf(Attacks.gargoyleClaw, Attacks.gargoyleClaw, Attacks.gargoyleBite,
                Attacks.gargoyleTail), null, 0, dungeon2Levels1, FLYING, 1, 0, false,
            SilmarSounds.bat, 1f, false)
    val ruster =
        createSilmarMonsterType(5, "ruster", "ruster", 5, Alignment.neutral, 28, 3, 8, 5,
            3, false, arrayOf(Attacks.rusterAntenna, Attacks.rusterAntenna), null, 0,
            dungeon2Levels1, WALKING, 0, 0, false, null, 0f, false)
    val preteg =
        createSilmarMonsterType(5, "preteg", "preteg", 6, Alignment.neutral, 43, 4, 7, 3,
            2, false, arrayOf(Attacks.pretegClaw), null, 0, dungeon2Levels1, WALKING, 0,
            0, false, null, 0f, false)
    val ghoul =
        createSilmarMonsterType(5, "ghoul", "ghoul", 2, Alignment.evil, 25, 3, 4, 0, 0,
            true, arrayOf(Attacks.ghoulClaw, Attacks.ghoulClaw, Attacks.ghoulBite), null,
            0, dungeon2Levels1, WALKING, 0, 0, false, null, 0f, false)
    val doppleganger =
        createSilmarMonsterType(5, "doppleganger", null, 4, Alignment.neutral, 43, 4, 5,
            0, 0, false, arrayOf(Attacks.dopplegangerFist), null, 0, dungeon2Levels1,
            WALKING, 0, 0, false, null, 0f, false)

    val minotaur =
        createSilmarMonsterType(5, "minotaur", "minotaur", 6, Alignment.evil, 65, 4, 4, 0,
            0, false,
            arrayOf(Attacks.minotaurFistHornCombo, Attacks.minotaurFistHornCombo), null,
            0, dungeon2Levels2, WALKING, 0, 0, false, null, 0f, false)
    val medusa =
        createSilmarMonsterType(5, "medusa", "medusa", 6, Alignment.evil, 85, 3, 5, 0, 0,
            false, arrayOf(Attacks.medusaSnakes), null, 0, dungeon2Levels2, WALKING, 0, 0,
            false, null, 0f, false)
    val troll =
        createSilmarMonsterType(5, "troll", "troll", 7, Alignment.evil, 70, 4, 6, 4, 2,
            false, arrayOf(Attacks.trollRake, Attacks.trollBite), null, 0,
            dungeon2Levels2, WALKING, 0, 0, false, null, 0f, false)
    val negadead =
        createSilmarMonsterType(5, "negadead", "negadead", 4, Alignment.evil, 65, 4, 5, 0,
            0, true, arrayOf(Attacks.negadeadClaw), null, 0, dungeon2Levels2, WALKING, 0,
            0, false, null, 0f, false)
    val ghast =
        createSilmarMonsterType(5, "ghast", "ghast", 4, Alignment.evil, 35, 3, 6, 0, 0,
            true, arrayOf(Attacks.ghastClaw, Attacks.ghastClaw, Attacks.ghastBite), null,
            0, dungeon2Levels2, WALKING, 0, 0, false, null, 0f, false)
    val atoman =
        createSilmarMonsterType(5, "atoman", "atoman", 4, Alignment.neutral, 23, 3, 6, 6,
            6, false, arrayOf(Attacks.atomanFist, Attacks.atomanFist, Attacks.atomanBite),
            null, 0, dungeon2Levels2, WALKING, 0, 0, false, SilmarSounds.atoman, .5f,
            false, lightRadiusInTiles = 2)

    // note that the generators below are given double the normal frequency,
    // since they have only one level in which to appear
    val grave =
        createSilmarMonsterType(10, "grave", "grave", 7, Alignment.evil, 68, 1, 0, 0, 0,
            false, null, null, 0, ruinsLevels1, WALKING, 0, 0, true, null, 0f, true,
            false)
    val giantZombie =
        createSilmarMonsterType(0, "giant zombie", "giantZombie", 8, Alignment.evil, 50,
            1, 2, 0, 0, true, arrayOf(Attacks.giantZombieBlow), null, 0, ruinsLevels1,
            WALKING, 0, 0, false, null, 0f, false)
    val bones =
        createSilmarMonsterType(10, "pile of bones", "bones", 9, Alignment.evil, 25, 1, 0,
            0, 0, false, null, null, 0, ruinsLevels1, WALKING, 0, 0, true, null, 0f, true,
            false)
    val moldySkeleton =
        createSilmarMonsterType(0, "moldy skeleton", "moldySkeleton", 5, Alignment.evil,
            30, 2, 4, 4, 4, true, arrayOf(Attacks.moldySkeletonRapier), null, 0,
            ruinsLevels1, WALKING, 0, 0, true, null, 0f, false)
    val hydra =
        createSilmarMonsterType(1, "hydra", "hydra", 10, Alignment.neutral, 205, 3, 5, 3,
            1, false, arrayOf(Attacks.hydraBite, Attacks.hydraBite, Attacks.hydraBite),
            null, 0, ruinsLevels1, WALKING, 0, 0, false, null, 0f, false)
    val vortex =
        createSilmarMonsterType(10, "magical vortex", "vortex", 5, Alignment.evil, 75, 1,
            8, 8, 8, false, null, null, 0, ruinsLevels2, WALKING, 1, 0, false, null, 0f,
            true, false)
    val ghost =
        createSilmarMonsterType(0, "ghost", "ghost", 2, Alignment.evil, 20, 3, 10, 10, 10,
            true, arrayOf(Attacks.ghostTouch), null, 0, ruinsLevels2, ETHEREAL, 1, 0,
            false, SilmarSounds.ghost, .5f, false)
    val casket =
        createSilmarMonsterType(10, "casket", "casket", 7, Alignment.evil, 100, 1, 0, 0,
            0, false, null, null, 0, ruinsLevels2, WALKING, 0, 0, true, null, 0f, true,
            false)
    val killerCorpse =
        createSilmarMonsterType(0, "killer corpse", "killerCorpse", 12, Alignment.evil,
            475, 2, 4, 0, 0, true,
            arrayOf(Attacks.killerCorpseTouch, Attacks.killerCorpseTouch), null, 0,
            ruinsLevels2, WALKING, 2, 0, false, SilmarSounds.killerCorpse, .33f, false)

    val grenadier =
        createSilmarMonsterType(5, "grenadier", "grenadier", 2, Alignment.evil, 60, 4, 3,
            3, 1, false, null, arrayOf(Attacks.grenadierPistol, Attacks.grenadierPistol,
                Attacks.grenadierPistol), 10, baseLevels1, WALKING, 0, 0, false,
            SilmarSounds.mercenary, .2f, false, lightRadiusInTiles = 6)
    val maniac =
        createSilmarMonsterType(5, "maniac", "maniac", 2, Alignment.evil, 50, 2, 1, 0, 0,
            false, arrayOf(Attacks.maniacKnife), null, 0, baseLevels1, WALKING, 0, 0,
            false, null, 0f, false, lightRadiusInTiles = 6)
    val maniacEnraged =
        createSilmarMonsterType(0, "enraged maniac", "maniac", 2, Alignment.evil, 50, 8,
            1, 0, 0, false,
            arrayOf(Attacks.maniacKnife, Attacks.maniacKnife, Attacks.maniacKnife,
                Attacks.maniacKnife), null, 0, null, WALKING, 0, 0, false, null, 0f,
            false, lightRadiusInTiles = 6)
    val mercenary =
        createSilmarMonsterType(5, "mercenary", "mercenary", 3, Alignment.evil, 70, 4, 6,
            6, 1, false, null,
            arrayOf(Attacks.mercenaryAssaultRifle, Attacks.mercenaryAssaultRifle,
                Attacks.mercenaryAssaultRifle, Attacks.mercenaryAssaultRifle), 10,
            baseLevels1, WALKING, 0, 0, false, SilmarSounds.mercenary, .33f, false,
            lightRadiusInTiles = 6)
    val assassin =
        createSilmarMonsterType(5, "assassin", "assassin", 6, Alignment.evil, 40, 4, 3, 0,
            0, false, arrayOf(Attacks.assassinSword), null, 0, baseLevels1, WALKING, 0, 0,
            false, null, 0f, false)
    val sentryborg =
        createSilmarMonsterType(5, "sentryborg", "sentryborg", 6, Alignment.evil, 80, 4,
            7, 7, 7, false, arrayOf(Attacks.sentryborgPincer),
            arrayOf(Attacks.sentryborgStunRay), 4, baseLevels2, WALKING, 1, 0, true,
            SilmarSounds.sentryborg, .5f, false)
    val mutivider =
        createSilmarMonsterType(5, "mutivider", "mutivider", 3, Alignment.neutral, 20, 4,
            5, 5, 5, false, arrayOf(Attacks.mutividerRazorSwipe), null, 0, baseLevels2,
            WALKING, 0, 0, false, SilmarSounds.mutivider, .33f, false)
    val mutatedRat =
        createSilmarMonsterType(5, "mutated rat", "mutatedRat", 4, Alignment.neutral, 40,
            3, 3, 0, 0, false, arrayOf(Attacks.mutatedRatBite), null, 0, baseLevels2,
            WALKING, 0, 0, false, null, 0f, false)

    val redDragon =
        createSilmarMonsterType(0, "red dragon", "redDragon", 12, Alignment.evil, 600, 3,
            11, 9, 7, false,
            arrayOf(Attacks.redDragonClaw, Attacks.redDragonClaw, Attacks.redDragonBite),
            null, 0, dungeon3Levels1, WALKING, 0, 3, false, SilmarSounds.redDragon, 1f,
            false)
    val mummy =
        createSilmarMonsterType(5, "mummy", "mummy", 6, Alignment.evil, 120, 2, 7, 4, 0,
            true, arrayOf(Attacks.mummyArm), null, 0, dungeon3Levels1, WALKING, 1, 0,
            false, null, 0f, false)
    val hulkster =
        createSilmarMonsterType(5, "hulkster", "hulkster", 9, Alignment.evil, 180, 2, 8,
            5, 2, false, arrayOf(Attacks.hulksterClaw, Attacks.hulksterClaw,
                Attacks.hulksterMandibles), null, 0, dungeon3Levels1, WALKING, 0, 0,
            false, null, 0f, false)
    val earthElemental =
        createSilmarMonsterType(5, "earth elemental", "earthElemental", 8,
            Alignment.neutral, 335, 2, 8, 8, 8, false,
            arrayOf(Attacks.earthElementalFist, Attacks.earthElementalFist), null, 0,
            dungeon3Levels1, EARTHING, 2, 0, false, SilmarSounds.earthElemental, 1f,
            false)
    val stalkin =
        createSilmarMonsterType(5, "stalkin", null, 8, Alignment.neutral, 145, 4, 9, 9, 9,
            false, arrayOf(Attacks.stalkinFist), null, 0, dungeon3Levels2, WALKING, 0, 4,
            false, null, 0f, false)
    val vampire =
        createSilmarMonsterType(5, "vampire", "vampire", 8, Alignment.evil, 430, 4, 9, 6,
            3, true, arrayOf(Attacks.vampireDrainStrike), null, 0, dungeon3Levels2,
            WALKING, 1, 0, false, null, 0f, false)
    val giantWorm =
        createSilmarMonsterType(3, "giant worm", "giantWorm", 12, Alignment.neutral, 500,
            2, 4, 2, 1, false, arrayOf(Attacks.giantWormBite), null, 0, dungeon3Levels2,
            WALKING, 0, 0, false, SilmarSounds.giantWorm, 1f, false)

    val livingDeadWizard =
        createSilmarMonsterType(0, Names.archEnemy, "livingDeadWizard", 16,
            Alignment.evil, 1400, 2, 10, 7, 4, true,
            arrayOf(Attacks.livingDeadWizardTouch), null, 0, null, WALKING, 1, 0, false,
            SilmarSounds.livingDeadWizard, 0.4f, false)

    val acidDragon =
        createSilmarMonsterType(1, "acid dragon", "acidDragon", 10, Alignment.evil, 300,
            3, 7, 5, 3, false, arrayOf(Attacks.acidDragonClaw, Attacks.acidDragonClaw,
                Attacks.acidDragonBite), null, 0, daveDungeonLevels1, WALKING, 0, 0,
            false, SilmarSounds.acidDragon, .33f, false)
    val darkKnight =
        createSilmarMonsterType(5, "dark knight", "darkKnight", 9, Alignment.evil, 450, 4,
            10, 3, 3, true, arrayOf(Attacks.darkKnightSword, Attacks.darkKnightSword),
            null, 0, daveDungeonLevels1, WALKING, 0, 10, false, SilmarSounds.killerCorpse,
            .33f, false)
    val warpDemon =
        createSilmarMonsterType(4, "warp demon", "warpDemon", 8, Alignment.evil, 350, 4,
            10, 7, 4, false,
            arrayOf(Attacks.warpDemonArm, Attacks.warpDemonArm, Attacks.warpDemonArm,
                Attacks.warpDemonArm, Attacks.warpDemonArm), null, 0, daveDungeonLevels1,
            WALKING, 2, 12, false, SilmarSounds.warpDemon, 1f, false)
    // note that the visculoid experience value is so low because it is expected that many
    // other visculoids will be spawned from it, each of which will grant experience
    // when vanquished
    val visculoid =
        createSilmarMonsterType(5, "visculoid", "visculoid", 10, Alignment.neutral, 28,
            2, 4, 0, 0, false, arrayOf(Attacks.visculoidTouch), null, 0,
            daveDungeonLevels1, WALKING_NO_HANDS, 0, 0, true, SilmarSounds.slime, 1f,
            false)
    val ferrousGolem =
        createSilmarMonsterType(2, "ferrous golem", "ferrousGolem", 18, Alignment.neutral,
            1400, 2, 7, 7, 7, false, arrayOf(Attacks.ferrousGolemSword), null, 0,
            daveDungeonLevels2, WALKING, 3, 0, false, SilmarSounds.ferrousGolem, 1f,
            false)
    val mirroredArmor = createSilmarMonsterType(5, "mirrored armor", "mirroredArmor", 14,
        Alignment.neutral, 700, 3, 10, 1, 1, false, arrayOf(Attacks.mirroredArmorRam),
        null, 0, daveDungeonLevels2, WALKING, 2, 0, false, SilmarSounds.mirroredArmor, 1f,
        false)
    val guardianSword = createSilmarMonsterType(5, "guardian sword", "guardianSword", 9,
        Alignment.neutral, 350, 4, 12, 10, 8, false, arrayOf(Attacks.guardianSwordSlash),
        null, 0, daveDungeonLevels2, WALKING, 3, 0, false, SilmarSounds.guardianSword, 1f,
        false)

    val gammaVampire =
        createSilmarMonsterType(5, "gamma vampire", "gammaVampire", 10, Alignment.evil,
            500, 4, 9, 6, 3, true, arrayOf(Attacks.vampireDrainStrike), null, 0,
            moonLevels1, WALKING, 1, 8, false, null, 0f, false)
    val dragonman =
        createSilmarMonsterType(5, "dragonman", "dragonMan", 8, Alignment.evil, 200, 4, 8,
            6, 4, false, arrayOf(Attacks.dragonmanBite), null, 0, moonLevels1, WALKING, 0,
            0, false, null, 0f, false)
    val cetharg =
        createSilmarMonsterType(5, "cetharg", "cetharg", 12, Alignment.evil, 400, 3, 11,
            11, 11, false, arrayOf(Attacks.cethargFist), null, 0, moonLevels1, WALKING, 1,
            0, true, SilmarSounds.cetharg, 0.5f, false, false)

    // note that the monsters of those below that don't appear in the finale level are given
    // double the normal frequency, since they have only one level in which to appear
    val mutatedWarrior =
        createSilmarMonsterType(10, "mutated warrior", "mutatedWarrior", 8,
            Alignment.evil, 270, 4, 8, 8, 8, false,
            arrayOf(Attacks.mutatedWarriorSabre, Attacks.mutatedWarriorDagger,
                Attacks.mutatedWarriorSabre, Attacks.mutatedWarriorDagger),
            arrayOf(Attacks.mutatedWarriorPistol, Attacks.mutatedWarriorPistol,
                Attacks.mutatedWarriorPistol, Attacks.mutatedWarriorPistol,
                Attacks.mutatedWarriorPistol, Attacks.mutatedWarriorPistol), 4,
            moonLevels2, WALKING, 0, 0, false, null, 0f, false)
    val deathbot =
        createSilmarMonsterType(5, "deathbot", "deathbot", 16, Alignment.evil, 1200, 6,
            13, 13, 13, false, arrayOf(Attacks.deathbotGrasp),
            arrayOf(Attacks.deathbotRay), 5, moonLevels2, WALKING, 2, 0, true,
            SilmarSounds.deathbot, 1f, false)
    val poltergeist =
        createSilmarMonsterType(10, "poltergeist", null, 8, Alignment.evil, 1000, 4, 12,
            12, 12, true, arrayOf(Attacks.poltergeistTouch), null, 0, moonLevels2,
            ETHEREAL, 2, 0, false, SilmarSounds.ghost, 0.33f, false)

    val robotGenerator =
        createSilmarMonsterType(0, "robot generator", "robotGenerator", 16,
            Alignment.evil, 2000, 1, 7, 7, 7, false, null, null, 0, null, WALKING, 2, 0,
            false, null, 0f, true, false)
    val livingDeadWizard2 = createSilmarMonsterType(
        // note that we slightly alter the name so this type won't hash over
        // its related, earlier form
        0, "neo-" + Names.archEnemy, "livingDeadWizard2", 20, Alignment.evil, 2000, 5, 15,
        15, 15, true, null,
        arrayOf(Attacks.livingDeadWizard2VortexBeam, Attacks.livingDeadWizard2VortexBeam),
        8, null, WALKING, 3, 0, false, SilmarSounds.livingDeadWizard2, 0.5f, false)
}