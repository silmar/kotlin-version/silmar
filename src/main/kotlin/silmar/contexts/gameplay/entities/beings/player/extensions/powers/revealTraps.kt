package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS

/**
 * Has this player perform a reveal-traps power.
 */
fun Player.revealTraps() {
    performPowerDischargeEffect(this)

    area.getEntitiesInLOS(location, area.traps).forEach { it.onDetected() }
}

