package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks
import silmar.contexts.gameplay.entities.beings.monsters.specializations.Gremlin
import silmar.contexts.gameplay.entities.beings.monsters.specializations.Munchkin
import silmar.contexts.gameplay.entities.beings.monsters.specializations.Mutivider
import silmar.contexts.gameplay.entities.beings.monsters.specializations.Vampire
import silmar.contexts.gameplay.entities.beings.player.SilmarDiseases
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeParalyzed
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.decrementBaseAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.loseExperienceLevel
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.loseGold
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onStruckByDisease
import silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory.firstLitTorch
import silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory.loseRandomNonGroupableInventoryItem
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.ItemLostReason
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.loseRation
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.items.extensions.condition.avoidOrDegrade
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.util.math.randomInt
import kotlin.math.round

typealias Attacks = SilmarMonsterAttacks

fun Player.onHitByGameSpecificMonster(
    monster: Monster, attack: MonsterAttack, damageAmount: Int, hitBy: Int
) {
    when (attack) {
        Attacks.mummyArm -> {
            // if this player doesn't avoid
            val result = avoid(damageAmount)
            if (!result.avoided) {
                onStruckByDisease(SilmarDiseases.mummy)
            }
        }
        Attacks.ghoulClaw -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                // this player is paralyzed for a random duration related to this player's endurance
                val maxDuration = 5
                val duration =
                    round(maxDuration * (20 - attributeValues.endurance) / 20.0).toInt()
                becomeParalyzed(duration)
            }
        }
        Attacks.rogueDagger -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                statuses.onPoisoned(3)
            }
        }
        Attacks.cannibalBlowgunDart -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                statuses.onPoisoned(1)
            }
        }
        Attacks.giantWaspSting -> {
            // if this player doesn't avoid
            val result = avoid(hitBy - 4)
            if (!result.avoided) {
                statuses.onPoisoned(1)
            }
        }
        Attacks.poisonousSnakeFangs -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                statuses.onPoisoned(1)
            }
        }
        Attacks.livingDeadPirateGrab -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                // some gold is stolen from this player
                loseGold(randomInt(1, 20), ItemLostReason.stolen)
            }
        }
        Attacks.munchkinGrab -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                // this player loses a ration
                loseRation(ItemLostReason.stolen)

                // if this player now has no rations
                if (items.inventory.getFirstItemOfType(GroupableItemTypes.ration) == null) {
                    // inform the munchkin
                    (monster as Munchkin).onNoRationsLeft(this)
                }
            }
        }
        Attacks.rusterAntenna -> onHitByRusterAntenna(hitBy)
        Attacks.gremlinSwipe -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                // this player loses a random inventory item
                loseRandomNonGroupableInventoryItem(ItemLostReason.stolen)

                // inform the gremlin
                (monster as Gremlin).onItemSwiped()
            }
        }
        Attacks.scumShambleArm -> {
            // if a don't avoid
            val result = avoid(damageAmount - 4)
            if (!result.avoided) {
                onStruckByDisease(SilmarDiseases.scumShamble)
            }
        }
        Attacks.ghastClaw -> {
            // if this player doesn't avoid
            val result = avoid(hitBy - 6)
            if (!result.avoided) {
                // this player loses 1 strength
                decrementBaseAttribute(PlayerAttributes.strength)
            }
        }
        Attacks.negadeadClaw -> {
            // if this player doesn't avoid
            val result = avoid(hitBy - 4)
            if (!result.avoided) {
                loseExperienceLevel()
            }
        }
        Attacks.killerCorpseTouch -> {
            // if this player doesn't avoid
            val result = avoid(hitBy - 2)
            if (!result.avoided) {
                loseExperienceLevel()
            }
        }
        Attacks.typhoidZombieRend -> {
            // if this player doesn't avoid
            val result = avoid(damageAmount - 2)
            if (!result.avoided) {
                onStruckByDisease(SilmarDiseases.typhoidZombie1)
                onStruckByDisease(SilmarDiseases.typhoidZombie2)
                onStruckByDisease(SilmarDiseases.typhoidZombie3)
            }
        }
        Attacks.slimeChuckerSlime -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                val item = items.randomItem
                item?.avoidOrDegrade(hitBy / 2)
            }
        }
        Attacks.vampireDrainStrike -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                // this player loses 2 experience levels
                loseExperienceLevel()
                loseExperienceLevel()

                // inform the vampire
                (monster as Vampire).onDrainStrikeWorked()
            }
        }
        Attacks.batBite -> {
            // if this player has a torch going
            val torch = items.inventory.firstLitTorch
            if (torch != null) {
                // if this player doesn't avoid
                val result = avoid(-6)
                if (!result.avoided) {
                    // the torch is blown out
                    onMessage("Your torch is extinguished!")
                    torch.extinguish()
                }
            }
        }
        Attacks.minotaurFistHornCombo -> // if the hit was by a certain amount
            if (hitBy >= 4) {
                // I suffer extra damage for a horn hit
                takeDamage(Damage(randomInt(1, 4), DamageForms.pierce))
            }
        Attacks.mutatedRatBite -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                onStruckByDisease(SilmarDiseases.mutatedRat)
            }
        }
        Attacks.giantRatBite -> {
            // if this player doesn't avoid
            val result = avoid(-6)
            if (!result.avoided) {
                onStruckByDisease(SilmarDiseases.rat)
            }
        }
        Attacks.deathbotGrasp -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                val item = items.randomEquippedItem
                item?.avoidOrDegrade(hitBy / 2)
            }
        }
        Attacks.poltergeistTouch -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                loseExperienceLevel()
            }
        }
        Attacks.livingDeadWizard2VortexBeam -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                val item = items.randomEquippedItem
                item?.avoidOrDegrade((hitBy / 2) + 5)
            }
        }
        Attacks.mutividerRazorSwipe ->
            // if this player has a ration (which is now stolen from this player)
            if (loseRation(ItemLostReason.stolen)) {
                // inform the mutivider it has stolen a ration
                (monster as Mutivider).onRationTheftWorked()
            }
        Attacks.medusaSnakes, Attacks.giantSpiderBite -> {
            // if this player doesn't avoid
            val result = avoid(hitBy)
            if (!result.avoided) {
                statuses.onPoisoned(1)
            }
        }
    }
}