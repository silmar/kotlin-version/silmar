package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performDeathFogEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.healFromForm
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.damage.extensions.affectForNonAttacks
import silmarengine.contexts.gameplay.game.Game

class FerrousGolem : Monster1() {
    override fun affectDamage(damage: Damage) {
        super.affectDamage(damage)

        healFromForm(DamageForms.fire, damage, 1f)

        if (damage.form != DamageForms.crushing) {
            damage.affectForNonAttacks(0f)
        }
    }

    override fun usePreMovementPower(player: Player): Boolean {
        // if it's time to breathe (but, don't have all golems breathe at the same time)
        val interval = 7
        val game = Game.currentGame
        if ((game!!.turn + area.monsters.indexOf(this)) % interval == 0) {
            // breathe
            area.performDeathFogEffect(location, powerRating)
            return true
        }

        return false
    }
}