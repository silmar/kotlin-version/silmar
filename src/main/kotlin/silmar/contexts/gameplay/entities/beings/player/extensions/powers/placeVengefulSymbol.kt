package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.clericalCastingLevel
import silmar.contexts.gameplay.entities.terrains.specializations.VengefulSymbol
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Has this player place a vengeful symbol at the given pixel-location.
 */
fun SilmarPlayer.placeVengefulSymbol(location: PixelPoint) {
    performPowerDischargeEffect(this)

    // create the symbol
    val symbol = createTerrain(SilmarTerrainTypes.vengefulSymbol) as VengefulSymbol
    symbol.setStrength(clericalCastingLevel * 2)

    // add the symbol to the area
    val adjustedLocation =
        if (isConfused) modifyLocationDueToConfusion(area, location, this.location)
        else location
    area.addEntity(symbol, adjustedLocation)
}

