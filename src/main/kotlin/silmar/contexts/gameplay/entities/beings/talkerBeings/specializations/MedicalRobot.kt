package silmar.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmar.contexts.gameplay.entities.beings.player.extensions.health.onHealedByMedicalRobot
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing1
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sound
import silmarengine.util.math.randomInt

class MedicalRobot : TalkerBeing1() {
    /**
     * Whether this robot is alerting that a player is nearby (versus instead
     * performing its usual healing function.
     */
    private var alerting = shouldAlert()
    override val greeting: String
        get() = if (!alerting) "This robot drones, \"Please remain still, that I may alleviate your ailments.\""
        else "This robot drones, \"You are not one of us...  Security alert!  Security alert!\""
    override // sometimes, a medical robot makes its own special sound
    val movementSound: Sound?
        get() = if (randomInt(1, 2) == 1) SilmarSounds.medicalRobot
        else super.movementSound

    /**
     * Returns whether this robot should start issuing an alert that a player is nearby.
     */
    private fun shouldAlert(): Boolean {
        return randomInt(1, 10) <= 4
    }

    override fun onGreetingHeardByPlayer(player: Player) {
        if (!alerting) {
            player.onHealedByMedicalRobot()
        }
        else {
            // sound an alarm
            area.onSoundIssued(SilmarSounds.alarm, location)
        }

        alerting = alerting or shouldAlert()
    }
}