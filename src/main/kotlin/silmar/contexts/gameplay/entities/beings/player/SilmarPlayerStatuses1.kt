package silmar.contexts.gameplay.entities.beings.player

import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerStatuses1

class SilmarPlayerStatuses1(player: Player) : SilmarPlayerStatuses,
    PlayerStatuses1(player) {
    private var canBeStealthy: Boolean = false

    override val isStealthy: Boolean
        get() = if ((player as SilmarPlayer).hasGainedLevel(
                PlayerLevels.ninja)) canBeStealthy
        else false

    override fun updateIsStealthy() {
        val hasNoisyItem = !player.items.compressEquippedItems().any { it.type.isNoisy }
        canBeStealthy = hasNoisyItem
    }

    /**
     * If this player has a retributive shield going, how many game turns are left before
     * it shuts off.
     */
    private var retributiveShieldTurnsLeft = 0
    override val hasRetributiveShield get() = retributiveShieldTurnsLeft > 0
    override fun onRetributiveShieldRaised(duration: Int) =
        run { retributiveShieldTurnsLeft = duration }

    override fun drainRetributiveShield() = run { retributiveShieldTurnsLeft -= 1 }

    /**
     * How many times this player has been exposed to radiation since its last
     * suffering of radiation damage.
     */
    private var radiationExposures = 0

    override fun incrementRadiationExposure() = run { radiationExposures += 1 }
    override fun resetRadiationExposure() = run { radiationExposures = 0 }
    override val isOverSafeRadiationExposureLimit get() = radiationExposures >= 3

    /**
     * How much has been subtracted from this player's max-hit-points value due to
     * radiation exposure.
     */
    override var radiationDamage = 0

    override fun incrementRadiationDamage() = run { radiationDamage += 1 }

    /**
     * If this player is using the bionic strength power, how many game turns are left
     * before it runs out.
     */
    private var bionicStrengthTurnsLeft = 0
    override val hasBionicStrength get() = bionicStrengthTurnsLeft > 0
    override fun onBionicStrengthAchieved(duration: Int) =
        run { bionicStrengthTurnsLeft = duration }

    override fun drainBionicStrength() = run { bionicStrengthTurnsLeft -= 1 }

    /**
     * If this player is using the quicken-self power, how many game turns are left
     * before it runs out.
     */
    private var quickenSelfTurnsLeft = 0
    override val isQuickened get() = quickenSelfTurnsLeft > 0
    override fun onQuickened(duration: Int) = run { quickenSelfTurnsLeft = duration }
    override fun drainQuickenedSelf() = run { quickenSelfTurnsLeft -= 1 }

    /**
     * Whether this player has been damaged since the last time it
     * performed a lay-hands power on itself.
     */
    private var mutableDamagedSinceLayHands: Boolean = false
    override val damagedSinceLayHands get() = mutableDamagedSinceLayHands
    override fun onDamagedSinceLayHands() = run { mutableDamagedSinceLayHands = true }
    override fun onLayHandsUse() = run { mutableDamagedSinceLayHands = false }
}