package silmar.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.items.types.TreasureTypes

/**
 * The flyweight player powers.
 */
object PlayerPowers {
    /**
     * The flyweight warrior powers.
     */
    val combatProwess = PlayerPower(PlayerLevels.fighter, "Combat Prowess", false, 0,
        "This grants an extra melee attack per turn.")
    val unarmedCombatProwess =
        PlayerPower(PlayerLevels.monk, "Unarmed Combat Prowess", false, 0,
            "This adds +1 to hit and +1 damage per 3 experience levels to attacks made using the hands.  It also grants an extra hand attack per turn.")
    val unarmoredDefensiveProwess =
        PlayerPower(PlayerLevels.monk, "Unarmored Defensive Prowess", false, 0,
            "This adds +1 per 2 experience levels to defense when not wearing armor.")
    val archerySkill = PlayerPower(PlayerLevels.archer, "Archery Skill", false, 0,
        "This adds +1 to hit and +1 damage per 5 experience levels to attacks made using a bow or crossbow.  It also grants an extra attack per turn with such weapons.")
    val immunityToDisease =
        PlayerPower(PlayerLevels.paladin, "Immunity to Disease", false, 0,
            "This grants absolute immunity to all diseases.")
    val layHands = PlayerPower(PlayerLevels.paladin, "Lay Hands", true, 0,
        "This lets you heal yourself for up to 1 hit point per experience level. You may only reuse this power once you have been damaged again for a non-trivial amount.")
    val protectionAgainstEvil =
        PlayerPower(PlayerLevels.paladin, "Protection Against Evil", false, 0,
            "This adds +2 to hit and +2 to defense while in melee combat with evil monsters.  (Note that many monsters are simply hungry or animated, rather than evil.)")
    val repelLivingDeadPaladin =
        PlayerPower(PlayerLevels.paladin, "Repel Living Dead", true, 0,
            "This wards off any living dead creature within sight that fails an avoidance check.  Costs 2 hit points per use.")
    val immunityToPoison =
        PlayerPower(PlayerLevels.superhero, "Immunity to Poison", false, 0,
            "This grants complete immunity to all poisons.")
    val fireEyeBeams = PlayerPower(PlayerLevels.superhero, "Fire Eye Beams", true, 0,
        "This attack form hits opponents based solely on your agility score.  Costs 2 hit points per use.")
    val employSuperVision =
        PlayerPower(PlayerLevels.superhero, "Employ Super Vision", true, 0,
            "This enables you to see through obstructions for an instant.  Costs 5 hit points per use.")
    val immunityToRadiation =
        PlayerPower(PlayerLevels.neonKnight, "Immunity to Radiation", false, 0,
            "This grants complete immunity to the effects of radiation.")
    val emitRadWave = PlayerPower(PlayerLevels.neonKnight, "Emit Rad Wave", true, 0,
        "This generates a massive wave of radiation that will kill most nearby monsters.  You must have at least half of your hit points left to use this, and such usage will leave you with 1 hit point remaining.  You also must first succeed at a +0 endurance check to use this.")

    /**
     * The cost in hit points for a paladin to repel living dead.
     */
    const val repelLivingDeadCostForPaladin = 2

    /**
     * The flyweight thief powers.
     */
    val hunterSpeed = PlayerPower(PlayerLevels.barbarian, "Hunter Speed", false, 0,
        "This improves your movement rate.")
    val fastRecuperation =
        PlayerPower(PlayerLevels.barbarian, "Fast Recuperation", false, 0,
            "This means you heal on your own at a perceptible rate.")
    val detectTraps = PlayerPower(PlayerLevels.thief, "Trap Detection", false, 0,
        "This gives a percentage chance (based on your intelligence and agility) to detect adjacent traps.")
    val disarmTraps = PlayerPower(PlayerLevels.thief, "Trap Disarming", false, 0,
        "This gives a percentage chance (based on your intelligence and agility) to disarm any trap you make contact with.")
    val stealth = PlayerPower(PlayerLevels.ninja, "Stealthy Movement", false, 0,
        "This keeps monsters from noticing you if you aren't wearing anything noisy.  Your chance of success in remaining unnoticed depends on both your intelligence and your agility.  The first attack on an unaware opponent causes double damage!")

    /**
     * The flyweight monster1 powers.
     */
    val horns = PlayerPower(PlayerLevels.minotaur, "Horns", false, 0,
        "These add additional damage to any melee attack which hits an opponent.")
    val senseCreatures = PlayerPower(PlayerLevels.minotaur, "Sense Creatures", true, 0,
        "This lets you sense if there are any creatures nearby.")
    val senseSecretDoors =
        PlayerPower(PlayerLevels.minotaur, "Sense Secret Doors", true, 0,
            "This gives you a +0 intelligence check to find any secret doors within sight.")
    val immunityToConfusion =
        PlayerPower(PlayerLevels.minotaur, "Immunity to Confusion", false, 0,
            "This grants complete immunity from the effects of confusion.")
    val regeneration = PlayerPower(PlayerLevels.troll, "Regeneration", false, 0,
        "This heals you at a fast rate.  However, it cannot bring you back from the dead.")
    val nightVision = PlayerPower(PlayerLevels.werewolf, "Night Vision", false, 0,
        "This lets you see in the dark without needing a torch.")
    val claws = PlayerPower(PlayerLevels.werewolf, "Claws", false, 0,
        "This means your hand attacks can do much greater base damage than normal hands.")
    val invulnerabilityToLesserCreatures =
        PlayerPower(PlayerLevels.werewolf, "Invulnerability to Lesser Creatures", false,
            0, "This prevents the normal attacks of lesser creatures from damaging you.")
    val giantStrength = PlayerPower(PlayerLevels.giant, "Giant Strength", false, 0,
        "This adds $giantStrengthBonus to your strength.")
    val throwBoulder = PlayerPower(PlayerLevels.giant, "Throw Boulder", true, 0,
        "This hurls a boulder (if there is one at your current location) at an opponent if you have a $throwBoulderStrengthRequired strength.  Your agility affects the accuracy of your throw.  If the target monster is hit, it may make an avoidance check to take half damage.")
    val invulnerabilityToAverageCreatures =
        PlayerPower(PlayerLevels.subvampire, "Invulnerability to Average Creatures",
            false, 0,
            "This prevents the normal attacks of average-power creatures from damaging you.")
    val immunityToParalyzation =
        PlayerPower(PlayerLevels.subvampire, "Immunity to Paralyzation", false, 0,
            "This prevents any attempt to paralyze you.")
    val lifeDraining = PlayerPower(PlayerLevels.subvampire, "Life Draining", false, 0,
        "This channels half the damage you cause with a hand attack into your own hit point total, if you make a +0 agility check.")

    /**
     * The flyweight monster2 powers.
     */
    val spinWeb = PlayerPower(PlayerLevels.mutatedSpider, "Spin Web", true, 0,
        "This leaves a web at your current location.  The webbing is strong enough to trap (or at least slow) many types of creatures.  You lose one hit point per web spun, due to the exertion involved.")
    val teleportingTouch = PlayerPower(PlayerLevels.pixie, "Teleportng Touch", false, 0,
        "This teleports away any monster you hit with a hand attack which fails an avoidance check.")
    val leap = PlayerPower(PlayerLevels.kangadillo, "Leap", true, 0,
        "This moves you directly to any visible location within a certain distance (which is determined by your strength).  How close you land to your target is affected by your agility.")
    val toughExterior = PlayerPower(PlayerLevels.kangadillo, "Tough Exterior", false, 0,
        "This reduces by half the damage you take from most attack forms.")
    val disintegratingTouch =
        PlayerPower(PlayerLevels.demon, "Disintegrating Touch", false, 0,
            "This disintegrates any monster you hit with a hand attack, if you make a +0 endurance check, and if the victim fails an avoidance check.")
    val cyberneticAccuracy =
        PlayerPower(PlayerLevels.cyborg, "Cybernetic Accuracy", false, 0,
            "When using ranged weapons, this adds +1 to hit and +1 damage per $cyborgIntelligencePerBonus intelligence you possess.")

    /**
     * The flyweight clerical powers.
     */
    val healWounds = PlayerPower(PlayerLevels.cleric, "Heal Wounds", true, 2,
        "This heals a limited amount of hit points of damage.")
    val curePoison = PlayerPower(PlayerLevels.cleric, "Cure Poison", true, 4,
        "This cures one level of poisoning from the recipient.  Multiple uses might be required for complete cleansing.")
    val repelLivingDeadCleric =
        PlayerPower(PlayerLevels.cleric, "Repel Living Dead", true, 1,
            "This wards off any living dead creature within sight that fails an avoidance check (modified by your casting level).")
    val cureDisease = PlayerPower(PlayerLevels.cleric, "Cure Disease", true, 3,
        "This cures the recipient of one disease.")
    val emitLightning = PlayerPower(PlayerLevels.druid, "Emit Lightning", true, 3,
        "This emits a lightning bolt that causes damage according to your casting level.  Victims may make an avoidance check (modified by your casting level) to take half damage.")
    val healFully = PlayerPower(PlayerLevels.priest, "Heal Fully", true, 6,
        "This heals all of your lost hit points.")
    val revealTraps = PlayerPower(PlayerLevels.priest, "Reveal Traps", true, 2,
        "This automatically reveals all traps within sight.")
    val placeVengefulSymbol =
        PlayerPower(PlayerLevels.priest, "Place Vengeful Symbol", true, 1,
            "This places a symbol that does damage to evil creatures passing over it.  The amount of damage is affected by your casting level.")
    val conjureDeathFog =
        PlayerPower(PlayerLevels.deathCaster, "Conjure Death Fog", true, 5,
            "This creates a cloud of deadly vapors which damages monsters passing through it for an amount dependent on your casting level.  Victims may make an avoidance check (modified by your casting level) to take half damage.")

    /**
     * The flyweight magic-user powers.
     */
    val teleportSelf = PlayerPower(PlayerLevels.wizard, "Teleport Self", true, 2,
        "This transports you to the last location you memorized on a specified level.")
    val memorizeLocation = PlayerPower(PlayerLevels.wizard, "Memorize Location", true, 0,
        "This specifies your current location as the spot on this level where the Teleport Self power will take you.")
    val hurlFireball = PlayerPower(PlayerLevels.wizard, "Hurl Fireball", true, 3,
        "This hurls a fireball that causes both explosion and fire damage according to your casting level.  This spell can also damage or destroy any loose items within its blast radius.  You must make a successful +0 agility check to have the fireball explode exactly where you target it.  Victims may make an avoidance check (modified by your casting level) to take half damage.")
    val findExit = PlayerPower(PlayerLevels.wizard, "Find Exit", true, 2,
        "This gives you an idea of where the exit to the next level is.")
    val transmuteGoldToBars =
        PlayerPower(PlayerLevels.alchemist, "Transmute Gold to Bars", true, 3,
            "This converts each ${TreasureTypes.goldBar.price} gold at and around your location (but not being held by you) into a gold bar, which weighs less (because the impurities are stripped out), and takes up less space.")
    val transmuteBarsToGold =
        PlayerPower(PlayerLevels.alchemist, "Transmute Bars to Gold", true, 3,
            "This converts each of your gold bars back into regular gold.")
    val findTreasure = PlayerPower(PlayerLevels.sorcerer, "Find Treasure", true, 1,
        "This gives an idea of where the nearest treasure is, which is of at least a certain value.")
    val projectRetributiveShield =
        PlayerPower(PlayerLevels.sorcerer, "Project Retributive Shield", true, 3,
            "This causes melee damage inflicted on you to be inflicted back on your attacker, as well.  The effect lasts for a duration that is dependent on your casting level.")
    val freezeBeing = PlayerPower(PlayerLevels.battlemage, "Freeze Monster", true, 5,
        "This will paralyze a monster if it fails an avoidance check (modified by your casting level).  The effect lasts for a duration that is dependent on your casting level.")
    val quickenSelf = PlayerPower(PlayerLevels.battlemage, "Quicken Self", true, 2,
        "This grants you a doubled movement rate and a doubled attack rate for a duration that is dependent on your casting level.  However, with each use you run the risk of a permanent loss of endurance, due to the exertion involved.")
    val achieveBionicStrength =
        PlayerPower(PlayerLevels.scientist, "Achieve Bionic Strength", true, 2,
            "This grants you +$bionicStrengthBonus strength for a duration that is dependent on your casting level.")
    val summonMeteorShower =
        PlayerPower(PlayerLevels.scientist, "Summon Meteor Shower", true, 7,
            "This explodes a fiery meteor on each enemy within sight.  Damage per meteor is as per a fireball.")
}

const val giantStrengthBonus = 8
const val throwBoulderStrengthRequired = 20
const val bionicStrengthBonus = 6

/**
 * Per how many intelligence a cyborg receives a bonus when using ranged weapons.
 */
const val cyborgIntelligencePerBonus = 4

