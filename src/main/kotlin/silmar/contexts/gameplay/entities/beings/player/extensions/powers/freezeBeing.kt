package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.magicUserCastingLevel
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeParalyzed
import silmarengine.contexts.gameplay.areas.extensions.entities.getFirstEntityAt
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Has this player perform a freeze-being power on the first being found at the
 * given pixel-location.
 */
fun SilmarPlayer.freezeBeing(at: PixelPoint) {
    performPowerDischargeEffect(this)

    // if there isn't a being at the given location
    val being = getFirstEntityAt(at, area.beings) ?: return

    if (being.isParalyzed) return

    // if the being doesn't avoid
    val strength = magicUserCastingLevel
    val result = being.avoid(strength)
    if (!result.avoided) {
        being.becomeParalyzed(Integer.max(strength * 2, 1))
    }
}

