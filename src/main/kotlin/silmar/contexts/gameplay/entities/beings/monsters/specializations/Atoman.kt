package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.LightSourceMonster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.damage.extensions.affectForm

class Atoman : LightSourceMonster() {
    override fun affectDamage(damage: Damage) {
        super.affectDamage(damage)

        damage.affectForm(DamageForms.fire, 0f)

        // if a metal weapon caused the damage
        val weapon = damage.weapon
        if (damage.amount > 0 && weapon != null && weapon.type.isMetal) {
            // no damage is taken
            damage.amount = 0

            // if the attacker is a player
            val foe = damage.foe
            if (foe != null && foe is Player) {
                // inform the player of the atoman's immunity
                foe.onMessage("The atoman appears to be unaffected by metal weapons.",
                    MessageType.status)
            }
        }
    }
}