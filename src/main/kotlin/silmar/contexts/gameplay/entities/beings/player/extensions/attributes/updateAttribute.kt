package silmar.contexts.gameplay.entities.beings.player.extensions.attributes

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.bionicStrengthBonus
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmar.contexts.gameplay.entities.beings.player.giantStrengthBonus
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes

fun Player.updateAttribute(attribute: PlayerAttribute, newValueSoFar: Int): Int {
    // if the attribute is strength
    var newValue = newValueSoFar
    if (attribute == PlayerAttributes.strength) {
        this as SilmarPlayer
        if (silmarStatuses.hasBionicStrength) newValue += bionicStrengthBonus

        val ogreStrength = 19
        if (newValue < ogreStrength && items.isItemOfTypeEquipped(
                SilmarMagicItemTypes.necklaceOfOgreStrength.itemType)) newValue = ogreStrength

        if (hasGainedLevel(PlayerLevels.giant)) newValue += giantStrengthBonus
    }

    return newValue
}