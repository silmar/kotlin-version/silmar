package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerPower
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.items.specializations.RingOfPowerStorage
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage

/**
 * Has this player pay the magic- or clerical- point cost of using the given power.
 *
 * @return  Whether this player had enough magic or clerical points to use the power.
 */
fun Player.incurPowerCost(power: PlayerPower): Boolean {
    // determine the magic- or clerical- point-cost associated with the given power
    var clerical = false
    var cost = power.magicPointCost
    if (cost == 0) {
        cost = power.clericalPointCost
        clerical = true
    }

    // if this power has a point-cost
    if (cost > 0) {
        // if this player is wearing a ring of power storage
        val ring =
            items.getEquippedItemOfType(SilmarMagicItemTypes.ringOfPowerStorage.itemType)
        if (ring != null) {
            (ring as RingOfPowerStorage).onSuppliedMagicPoints(cost)
        }
        else {
            // if this player is wearing a robe of magical energy
            if (items.isItemOfTypeEquipped(
                    SilmarMagicItemTypes.robeOfMagicalEnergy.itemType)) {
                // the cost is less than it would usually be
                cost = 2 * cost / 3
            }

            // if this player doesn't have enough of the right kind of points to pay
            // for the power use
            this as SilmarPlayer
            val stats = silmarStats
            if (clerical && stats.clericalPoints < cost || !clerical && stats.magicPoints < cost) {
                onMessage(
                    "You do not have enough " + (if (clerical) "clerical" else "magic") + " points!")
                return false
            }

            if (clerical) stats.onClericalPointsUsed(cost)
            else stats.onMagicPointsUsed(cost)
        }
    }

    return true
}

