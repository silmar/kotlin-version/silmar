package silmar.contexts.gameplay.entities.beings.monsters.extensions.activation

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayerAttributeModifierProvider
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster

fun Monster.shouldNotActivate(): Boolean {
    // if the player is being stealthy
    val player = area.player as SilmarPlayer
    if (player.silmarStatuses.isStealthy) {
        // if this monster fails its check to see the player, don't activate
        val provider = SilmarPlayerAttributeModifierProvider
        val values = player.attributeValues
        val avoidResult =
            avoid(provider.stealthAvoidancePenalty(values.agility, values.intelligence))
        if (!avoidResult.avoided) return true
    }

    return false
}