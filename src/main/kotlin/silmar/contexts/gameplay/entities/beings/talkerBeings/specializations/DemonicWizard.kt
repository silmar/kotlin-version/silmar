package silmar.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.Repairer
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon

class DemonicWizard : Repairer() {
    override fun getAdjustedItemPrice(item: Item, normalPrice: Int): Int {
        return normalPrice * 7 / 5
    }

    override fun getCanFixItem(item: Item): Boolean {
        return (super.getCanFixItem(item) && item !is Weapon && !item.type.isArmor && !item.type.isShield && item.type.isMagical)
    }
}