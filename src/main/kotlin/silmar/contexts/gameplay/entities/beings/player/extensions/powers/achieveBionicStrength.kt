package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.magicUserCastingLevel
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.updateAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

/**
 * Has this player achieve extra strength through the use of bionics.
 */
fun SilmarPlayer.achieveBionicStrength() {
    performPowerDischargeEffect(this)

    area.onSoundIssued(SilmarSounds.bionicStrength, location)

    // start the strength's duration
    val duration = magicUserCastingLevel * 5
    silmarStatuses.onBionicStrengthAchieved(duration)

    updateAttributes()

    onMessage("Strengthened!")
}

