package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performPoisonGasEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.areas.geometry.TileDistance

class MoldySkeleton : Monster1() {
    override fun onDamaged(damage: Damage) {
        area.performPoisonGasEffect(location, powerRating, TileDistance(2))
    }
}