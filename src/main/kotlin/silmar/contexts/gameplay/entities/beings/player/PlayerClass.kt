package silmar.contexts.gameplay.entities.beings.player

import silmar.contexts.playerlevelselection.domain.model.PLSPlayerClass
import java.util.*

/**
 * A group of related player levels that forms a progression.  That is, a player
 * may not reach a certain level in a class before having reached all previous
 * levels in that class's progression.  However, a player may select as its next
 * level the next level in any class's progression.  That is, a player is not
 * limited to gaining levels from just one class, but may choose from all classes.
 *
 * @param levels  This class's progression of player-levels, which is presumed to contain no
 * levels found in any of the other classes.
 */
class PlayerClass(override val name: String, val levels: List<PlayerLevel>) : PLSPlayerClass {
    init {
        classes.add(this)
        classesMap[name] = this
    }

    /**
     * Returns whether this class contains the given player-level.
     */
    fun contains(level: PlayerLevel): Boolean = levels.contains(level)

    @Suppress("unused")
    companion object {
        /**
         * A list of all the player-classes.
         */
        var classes = ArrayList<PlayerClass>()

        /**
         * A mapping of all player-classes by their names.
         */
        private val classesMap = HashMap<String, PlayerClass>()

        /**
         * Returns the player-class of the given name.
         */
        fun getClass(name: String): PlayerClass? = classesMap[name]
    }
}
