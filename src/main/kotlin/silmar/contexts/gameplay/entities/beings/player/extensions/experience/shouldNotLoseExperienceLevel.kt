package silmar.contexts.gameplay.entities.beings.player.extensions.experience

import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage

fun Player.shouldNotLoseExperienceLevel(): Boolean {
    // if this player is wearing a necklace of essence
    val necklace =
        items.getEquippedItemOfType(SilmarMagicItemTypes.necklaceOfEssence.itemType)
    if (necklace != null) {
        // the loss is prevented
        necklace.holder?.onMessage("Your necklace of essence prevented a level drain!")
        necklace.degrade()
        return true
    }

    return false
}