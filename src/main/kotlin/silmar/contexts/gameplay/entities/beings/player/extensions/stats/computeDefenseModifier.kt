package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

fun Player.computeDefenseModifierVsAttack(
    @Suppress("UNUSED_PARAMETER") attackType: AttackType, attacker: Being?
): Int {
    var modifier = 0

    // if this player is a monk
    this as SilmarPlayer
    if (hasGainedLevel(PlayerLevels.monk)) {
        // if the player isn't wearing armor
        val item = items.getEquippedItem(EquipLocation.BODY)
        if (item == null || !item.type.isArmor) {
            modifier += levelValues.level / 2
        }
    }

    // if this player is a paladin
    if (hasGainedLevel(PlayerLevels.paladin)) {
        // if the attacker is evil
        if (attacker != null && attacker.isEvil) {
            modifier += 2
        }
    }

    return modifier
}