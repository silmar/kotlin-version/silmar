package silmar.contexts.gameplay.entities.beings.player.extensions.health

import silmar.contexts.gameplay.entities.beings.player.SilmarDiseases
import silmarengine.contexts.gameplay.entities.beings.player.Disease
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.decrementBaseAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.incrementBaseAttribute
import silmarengine.util.math.randomInt

fun Disease.affectPlayerAsSilmarDisease(player: Player): Boolean {
    // if the disease is the mutated rat variety
    if (this == SilmarDiseases.mutatedRat) {
        // randomly add or subtract one from a randomly
        // selected attribute, which odds slightly favoring a loss, as in a casino
        val attribute = PlayerAttribute.randomAttribute
        if (randomInt(1, 100) <= 55) player.decrementBaseAttribute(attribute)
        else player.incrementBaseAttribute(attribute)
        return true
    }

    return false
}