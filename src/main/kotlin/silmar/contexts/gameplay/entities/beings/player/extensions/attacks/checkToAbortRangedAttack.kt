package silmar.contexts.gameplay.entities.beings.player.extensions.attacks

import silmar.contexts.gameplay.entities.items.weapons.SilmarWeapon
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponType
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.util.math.randomInt

fun Player.checkToAbortRangedAttack(weapon: Weapon): Boolean {
    when (val weaponType = weapon.weaponType.weaponSubType) {
        is SilmarWeaponType -> {
            // check whether a reload is needed
            weapon as SilmarWeapon
            if ((weaponType.isFirearm || weaponType.isLaserWeapon) && weapon.firedCountSinceReload >= weaponType.ammoCapacity) {
                onMessage("Reloading")
                weapon.onReloaded()
                endTurn()
                return true
            }

            // check for jams or overheats
            when {
                weaponType.isFirearm -> if (randomInt(1, 20) == 1) {
                    onMessage("Your ${weapon.name} jammed!")
                    checkForCatastrophicJam(weapon)
                    return true
                }
                weaponType.isLaserWeapon -> if (randomInt(1, 25) == 1) {
                    onMessage("Your ${weapon.name} overheated!")
                    checkForCatastrophicOverheat(weapon)
                    return true
                }
            }
        }
    }

    return false
}

private fun Player.checkForCatastrophicJam(weapon: Weapon) {
    if (randomInt(1, 6) == 1) {
        onMessage("The jam breaks your ${weapon.name}!")
        weapon.destroy()
    }
}

private fun Player.checkForCatastrophicOverheat(weapon: Weapon) {
    if (randomInt(1, 8) == 1) {
        onMessage("The overheating is catastrophic!")
        weapon.destroy()
        takeDamage(Damage(randomInt(1, 14), DamageForms.fire))
    }
}