package silmar.contexts.gameplay.entities.beings.player

interface SilmarPlayerStats {
    /**
     * The maximum number of magic points this player may currently possess.
     */
    val maxMagicPoints: Int

    /**
     * How many points this player currently has to spend on using its magical powers.
     */
    val magicPoints: Int
    fun onMagicPointsUsed(amount: Int)
    fun rechargeMagicPoint()

    fun updateMaxMagicPoints()

    /**
     * The maximum number of clerical points this player may currently possess.
     */
    val maxClericalPoints: Int

    /**
     * How many points this player currently has to spend on using its clerical powers.
     */
    val clericalPoints: Int
    fun onClericalPointsUsed(amount: Int)
    fun rechargeClericalPoint()

    fun updateMaxClericalPoints()
}