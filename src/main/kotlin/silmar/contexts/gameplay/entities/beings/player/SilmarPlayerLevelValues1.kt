package silmar.contexts.gameplay.entities.beings.player

import silmarengine.annotations.DeserializationOverride
import silmarengine.annotations.SerializationOverride
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerLevelValues1
import silmarengine.contexts.gameplay.entities.beings.player.startingPlayerLevel
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.util.ArrayList
import kotlin.math.max

class SilmarPlayerLevelValues1(player: Player) : SilmarPlayerLevelValues,
    PlayerLevelValues1(player) {
    /**
     * This list is transient because player-levels are immutable, and reference other
     * immutable objects such as player-powers.
     */
    @Transient
    override var levels = ArrayList<PlayerLevel>()

    override fun onPlayerLevelChosen(level: PlayerLevel) {
        levels.add(level)
    }

    override val levelsRetained: List<PlayerLevel>
        // the -1 and +1 here are to account for the presence of the default squire level
        get() = if (level == startingPlayerLevel + (levels.size - 1)) levels
        else levels.take(max(0, (level - startingPlayerLevel) + 1))

    /**
     * The list of player-level names that is serialized in place of the above levels list.
     */
    private var levelNames: Array<String>? = null

    @SerializationOverride
    @Throws(IOException::class)
    private fun writeObject(s: ObjectOutputStream) {
        // build this player's level-names array from its levels-list
        levelNames = levels.map { l -> l.name }.toTypedArray()

        s.defaultWriteObject()
    }

    @DeserializationOverride
    @Throws(IOException::class)
    private fun readObject(stream: ObjectInputStream) {
        stream.defaultReadObject()

        // build this player's levels-list from the level-names array just read in
        levels = ArrayList(levelNames?.map { name -> PlayerLevel.getLevel(name)!! }!!)
    }
}