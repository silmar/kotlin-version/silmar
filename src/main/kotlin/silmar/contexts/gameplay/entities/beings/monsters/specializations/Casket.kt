package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.CreateMonsterReasons
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.createMonsterForReason
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.game.Game

class Casket : Monster1() {

    /**
     * The game turn that this casket became activated (if it has).
     */
    private var turnActivated: Int = 0

    /**
     * Remembers the status of this casket's activated field to the next game turn.
     */
    private var wasActivated = false

    override fun act() {
        // if this casket was activated during the last game turn
        if (!wasActivated && isActivated) {
            // remember what turn the activation took place on
            val game = Game.currentGame
            turnActivated = game!!.turn
            wasActivated = true
        }

        super.act()
    }

    override fun usePreMovementPower(player: Player): Boolean {
        // if this casket has been activated and it's time to generate
        val turnsUntilGeneration = 5
        val game = Game.currentGame
        if (turnActivated != 0 && game!!.turn - turnActivated >= turnsUntilGeneration) {
            // create a killer corpse
            createMonsterForReason(area, location, SilmarMonsterTypes.killerCorpse,
                CreateMonsterReasons.generation)

            // remove this casket
            area.removeEntity(this)

            return true
        }

        return false
    }
}