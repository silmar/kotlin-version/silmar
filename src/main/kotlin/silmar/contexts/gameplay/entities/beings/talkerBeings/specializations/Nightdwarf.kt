package silmar.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmar.contexts.gameplay.entities.items.types.SilmarItemType
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.Repairer
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon

class Nightdwarf : Repairer() {
    override fun getAdjustedItemPrice(item: Item, normalPrice: Int): Int {
        return normalPrice * 6 / 5
    }

    override fun getCanFixItem(item: Item): Boolean {
        val type = item.type
        val subType = type.subType
        return super.getCanFixItem(
            item) && (item is Weapon || type.isArmor || type.isShield) && (subType !is SilmarItemType || !subType.isTech)
    }
}