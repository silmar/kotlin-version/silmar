package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.createItemEntity
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.types.TreasureTypes

/**
 * Transforms each gold bar possessed by this player into a certain amount of gold.
 */
fun Player.transmuteBarsToGold() {
    performPowerDischargeEffect(this)

    // if this player has no gold bars
    if (items.inventory.getFirstItemOfType(TreasureTypes.goldBar) == null) {
        // inform the player
        onMessage("You don't have any gold bars!", MessageType.important)
        return
    }

    // create a gold item to hold the gold from the bars
    val gold = createGroupableItem(GroupableItemTypes.gold, 0)

    // for each gold bar in this player's inventory
    val bars = items.filterInventory { it.isOfType(TreasureTypes.goldBar) }
    bars.forEach { bar ->
        // remove this bar from this player's inventory
        items.inventory.tryToRemoveItem(bar)

        // add this bar's value to the gold item
        gold.quantity += bar.getValue(ItemValueContext.PLACEMENT)
    }

    // add the gold item at this player's location
    area.addEntity(createItemEntity(gold), location)
}

