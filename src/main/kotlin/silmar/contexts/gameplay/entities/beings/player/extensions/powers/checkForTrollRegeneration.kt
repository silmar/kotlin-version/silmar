package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel

/**
 * Checks to see if it's time for this player to healed due to the
 * troll's regeneration power.
 */
fun SilmarPlayer.checkForTrollRegeneration() {
    // if this player is a troll
    if (hasGainedLevel(PlayerLevels.troll)) {
        // if this player is hurt
        if (isDamaged) {
            // heal one hit-point
            addHitPoints(1)
        }
    }
}

