package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.areas.extensions.effects.performWebShotEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getSkeweredTargetLocation
import silmarengine.contexts.gameplay.areas.extensions.entities.isTerrainOfTypeInRectangle
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt

class GiantSpider : Monster1() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if it's time to shoot a web
        if (randomInt(1, 3) == 1) {
            // if the player isn't already stuck in a web
            val isPlayerStuckInWebs =
                area.isTerrainOfTypeInRectangle(SilmarTerrainTypes.web,
                    player.bounds.location, player.size)
            if (!isPlayerStuckInWebs) {
                // if the player is in range of this spider
                val range = getDistance(location, player.location)
                if (range.distance <= tileWidth * 8) {
                    // determine a skewered version of the player's location at which to shoot a web
                    range.distance /= 6
                    val to =
                        getSkeweredTargetLocation(area, location, player.location, range)

                    // shoot the web
                    area.performWebShotEffect(location, to, this)
                    return true
                }
            }
        }

        return false
    }
}