package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Has this player memorize it current location to recall later for teleportation purposes.
 */
fun SilmarPlayer.memorizeLocation() {
    performPowerDischargeEffect(this)
    onLocationMemorized()
}

