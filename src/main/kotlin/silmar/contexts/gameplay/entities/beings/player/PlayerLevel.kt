package silmar.contexts.gameplay.entities.beings.player

import silmar.contexts.playerlevelselection.domain.model.PLSLevel
import silmar.contexts.playerpowerselection.domain.model.PPSLevel
import silmarengine.contexts.gameplay.areas.geometry.PixelDimensions
import silmarengine.util.image.getImageSizeWithoutLoading
import java.util.*

/**
 * An experience-level which a player may achieve.
 */
class PlayerLevel(override val name: String, val imageName: String) : PPSLevel, PLSLevel {
    /**
     * The relative path (+ filename) of this level's associated image with
     * which to represent the player.
     */
    val imagePath = "player/$imageName"

    /**
     * The dimensions (in pixels) of a player of this level when it is
     * depicted onscreen.
     */
    lateinit var size: PixelDimensions
        private set

    /**
     * The powers granted by this level.
     */
    lateinit var powers: Array<PlayerPower>
        internal set

    init {
        levels.add(this)
        levelsMap[name] = this

        determineSize()
    }

    override fun equals(other: Any?): Boolean {
        return other === this || (other is PlayerLevel && other.name == name)
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    /**
     * Determines the size (in pixels) of a player of this level.
     */
    private fun determineSize() {
        // determine the dimensions from this level's image-file
        size = PixelDimensions(getImageSizeWithoutLoading(imagePath))
    }

    companion object {
        /**
         * A list of all player-levels.
         */
        var levels = ArrayList<PlayerLevel>()

        /**
         * A mapping of all player-levels by their names.
         */
        var levelsMap = HashMap<String, PlayerLevel>()

        /**
         * Returns the player-level of the given name.
         */
        fun getLevel(name: String): PlayerLevel? = levelsMap[name]
    }
}
