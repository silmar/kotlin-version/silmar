package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.split
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms

class Visculoid : Monster1() {
    override fun onDamaged(damage: Damage) {
        // if this visculoid has 2+ hit points left, and the given cause isn't fire
        if (hitPoints >= 2 && damage.form != DamageForms.fire) split()
    }
}