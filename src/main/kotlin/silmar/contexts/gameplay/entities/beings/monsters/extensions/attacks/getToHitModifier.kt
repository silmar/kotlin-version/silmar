package silmar.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmarengine.contexts.gameplay.entities.beings.monsters.Assassin
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.player.Player

@Suppress("UNUSED_PARAMETER")
fun Monster.getToHitModifier(player: Player, attack: MonsterAttack): Int =
    when {
        this is Assassin && !isVisible -> 6
        else -> 0
    }
