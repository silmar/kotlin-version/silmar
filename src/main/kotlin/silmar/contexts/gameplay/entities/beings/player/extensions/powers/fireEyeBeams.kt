package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.areas.extensions.effects.performEyeBeamsEffect
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.util.math.randomInt

/**
 * Has this player fire eye-beams at the given pixel-location.
 */
fun Player.fireEyeBeams(at: PixelPoint) {
    performPowerDischargeEffect(this)

    val from = location
    val adjustedAt = if (isConfused) modifyLocationDueToConfusion(area, at, from) else at

    // whether the beams hit depends on whether this player makes a agility check
    // modified by the range to the target
    val tileDistance = TileDistance(getDistance(from, adjustedAt))
    val roll = randomInt(1, 20) + tileDistance.distance / 2
    val willHit = roll <= attributeValues.agility

    area.performEyeBeamsEffect(from, adjustedAt, willHit)

    // this player takes damage from the exertion
    takeDamage(Damage(2, DamageForms.exertion))
}

