package silmar.contexts.gameplay.entities.beings.monsters

import silmarengine.contexts.gameplay.entities.damage.DamageForms

typealias Attack = SilmarMonsterAttack

/**
 * The flyweight game-specific monster attacks.
 */
object SilmarMonsterAttacks {
    val giantRatBite = Attack("bite", 1, 3, 0, 0, DamageForms.pierce)
    val batBite = Attack("bite", 1, 1, 0, 0, DamageForms.pierce)
    val orcSword = Attack("long sword", 1, 8, 0, 0, DamageForms.slash)
    val orcCrossbow = Attack("crossbow", 1, 4, 20, 6, DamageForms.pierce)
    val goblinSword = Attack("short sword", 1, 6, 0, 0, DamageForms.slash)
    val skeletonSword = Attack("short sword", 1, 6, 0, 0, DamageForms.slash)
    val reptileManClaw = Attack("claw", 1, 2, 0, 0, DamageForms.slash)
    val reptileManBite = Attack("bite", 1, 8, 0, 0, DamageForms.pierce)
    val floatingSpheroidBite = Attack("bite", 1, 6, 0, 0, DamageForms.pierce)
    val zombieRend = Attack("rend", 1, 8, 0, 0, DamageForms.slash)
    val slimeOoze = Attack("ooze", 1, 8, 0, 0, DamageForms.acid)
    val warlockDagger = Attack("dagger", 1, 4, 0, 0, DamageForms.pierce)
    val warlockDarts = Attack("dart", 1, 2, 8, 7, DamageForms.pierce)
    val transparentGelatinoidAcid =
        Attack("acid", 2, 8, 0, 0, DamageForms.acid)
    val ogreFist = Attack("fist", 1, 10, 0, 0, DamageForms.bluntImpact)
    val munchkinGrab = Attack("grab", 0, 0, 0, 0, DamageForms.pierce)
    val berzerkerSword = Attack("long sword", 1, 8, 0, 0, DamageForms.slash)
    val berzerkerDagger = Attack("dagger", 1, 4, 0, 0, DamageForms.pierce)
    val poisonousSnakeFangs = Attack("fangs", 1, 3, 0, 0, DamageForms.pierce)
    val atomanFist = Attack("fist", 1, 6, 0, 0, DamageForms.bluntImpact)
    val atomanBite = Attack("bite", 2, 12, 0, 0, DamageForms.pierce)
    val gargoyleClaw = Attack("claw", 1, 3, 0, 0, DamageForms.slash)
    val gargoyleBite = Attack("bite", 1, 6, 0, 0, DamageForms.pierce)
    val gargoyleTail = Attack("tail", 1, 4, 0, 0, DamageForms.pierce)
    val rusterAntenna = Attack("antenna", 0, 0, 0, 0, DamageForms.pierce)
    val blobAcid = Attack("acid", 1, 10, 0, 0, DamageForms.acid)
    val pretegClaw = Attack("claw", 1, 8, 0, 0, DamageForms.slash)
    val minotaurFistHornCombo =
        Attack("fist/horn combo", 3, 5, 0, 0, DamageForms.bluntImpact)
    val dopplegangerFist = Attack("fist", 1, 12, 0, 0, DamageForms.bluntImpact)
    val gremlinSwipe = Attack("swipe", 1, 1, 0, 0, DamageForms.slash)
    val ghoulClaw = Attack("claw", 1, 3, 0, 0, DamageForms.slash)
    val ghoulBite = Attack("bite", 1, 6, 0, 0, DamageForms.pierce)
    val redDragonClaw = Attack("claw", 1, 8, 0, 0, DamageForms.slash)
    val redDragonBite = Attack("bite", 3, 30, 0, 0, DamageForms.pierce)
    val medusaSnakes = Attack("snakes", 1, 4, 0, 0, DamageForms.pierce)
    val trollRake = Attack("rake", 5, 8, 0, 0, DamageForms.slash)
    val trollBite = Attack("bite", 2, 12, 0, 0, DamageForms.pierce)
    val negadeadClaw = Attack("claw", 1, 4, 0, 0, DamageForms.slash)
    val assassinSword = Attack("long sword", 1, 8, 0, 0, DamageForms.slash)
    val killerTreeLimb = Attack("limb", 2, 16, 0, 0, DamageForms.bluntImpact)
    val mummyArm = Attack("arm", 1, 12, 0, 0, DamageForms.bluntImpact)
    val hulksterClaw = Attack("claw", 3, 12, 0, 0, DamageForms.slash)
    val hulksterMandibles =
        Attack("mandibles", 2, 10, 0, 0, DamageForms.pierce)
    val earthElementalFist =
        Attack("fist", 4, 32, 0, 0, DamageForms.bluntImpact)
    val ferrousGolemSword =
        Attack("huge sword", 4, 40, 0, 0, DamageForms.slash)
    val darkKnightSword =
        Attack("two-handed sword", 7, 20, 0, 0, DamageForms.slash)
    val stalkinFist = Attack("fist", 4, 16, 0, 0, DamageForms.bluntImpact)
    val warpDemonArm = Attack("arm", 2, 8, 0, 0, DamageForms.bluntImpact)
    val vampireDrainStrike =
        Attack("drain strike", 5, 10, 0, 0, DamageForms.pierce)
    val livingDeadWizardTouch =
        Attack("touch", 1, 10, 0, 0, DamageForms.pierce)
    val livingDeadPirateGrab = Attack("grab", 1, 4, 0, 0, DamageForms.pierce)
    val livingDeadPirateRake = Attack("rake", 1, 8, 0, 0, DamageForms.slash)
    val painkillerInjection =
        Attack("injection", 1, 4, 0, 0, DamageForms.pierce)
    val giantAntBite = Attack("bite", 1, 6, 0, 0, DamageForms.pierce)
    val dreadedPansyNeedle = Attack("needle", 1, 4, 10, 5, DamageForms.pierce)
    val scumShambleArm = Attack("arm", 1, 8, 0, 0, DamageForms.bluntImpact)
    val giantWaspBite = Attack("bite", 2, 8, 0, 0, DamageForms.pierce)
    val giantWaspSting = Attack("sting", 1, 4, 0, 0, DamageForms.pierce)
    val weirdThingZap = Attack("zap", 1, 12, 7, 7, DamageForms.electricity)
    val hydraBite = Attack("bite", 1, 10, 0, 0, DamageForms.pierce)
    val giantZombieBlow =
        Attack("crushing blow", 2, 24, 0, 0, DamageForms.bluntImpact)
    val moldySkeletonRapier = Attack("rapier", 1, 8, 0, 0, DamageForms.slash)
    val ghostTouch = Attack("touch", 4, 12, 0, 0, DamageForms.pierce)
    val killerCorpseTouch = Attack("touch", 1, 3, 0, 0, DamageForms.pierce)
    val giantSpiderBite = Attack("bite", 2, 8, 0, 0, DamageForms.pierce)
    val ghastClaw = Attack("claw", 1, 4, 0, 0, DamageForms.slash)
    val ghastBite = Attack("bite", 1, 8, 0, 0, DamageForms.pierce)
    val giantWormBite = Attack("bite", 2, 20, 0, 0, DamageForms.swallow)
    val acidDragonClaw = Attack("claw", 1, 5, 0, 0, DamageForms.slash)
    val acidDragonBite = Attack("bite", 3, 16, 0, 0, DamageForms.pierce)
    val visculoidTouch =
        Attack("corrosive touch", 3, 21, 0, 0, DamageForms.acid)
    val mirroredArmorRam =
        Attack("ramming attempt", 5, 10, 0, 0, DamageForms.bluntImpact)
    val guardianSwordSlash =
        Attack("vorpal slash", 3, 30, 0, 0, DamageForms.slash)
    val landSharkBite = Attack("bite", 2, 16, 0, 0, DamageForms.pierce)
    val cyclopsClub = Attack("club", 3, 8, 0, 0, DamageForms.bluntImpact)
    val typhoidZombieRend = Attack("rend", 1, 10, 0, 0, DamageForms.slash)
    val slimeChuckerSlime = Attack("slime", 1, 4, 5, 8, DamageForms.acid)
    val cannibalSpear = Attack("spear", 3, 7, 0, 0, DamageForms.pierce)
    val cannibalBlowgunDart =
        Attack("blowgun dart", 1, 2, 5, 5, DamageForms.pierce)
    val giantCentipedeBite = Attack("bite", 1, 6, 0, 0, DamageForms.pierce)
    val rogueDagger = Attack("dagger", 1, 4, 0, 0, DamageForms.pierce)
    val grenadierPistol = Attack("pistol", 1, 5, 20, 8, DamageForms.pierce)
    val maniacKnife = Attack("knife", 1, 4, 0, 0, DamageForms.slash)
    val mercenaryAssaultRifle =
        Attack("assault rifle", 1, 7, 30, 7, DamageForms.pierce)
    val sentryborgPincer = Attack("pincer", 5, 20, 0, 0, DamageForms.pierce)
    val sentryborgStunRay =
        Attack("stun ray", 1, 1, 10, 8, DamageForms.electricity)
    val mutividerRazorSwipe =
        Attack("razor swipe", 1, 8, 0, 0, DamageForms.slash)
    val mutatedRatBite = Attack("bite", 4, 12, 0, 0, DamageForms.pierce)
    val dragonmanBite = Attack("bite", 1, 12, 0, 0, DamageForms.pierce)
    val cethargFist = Attack("fist", 3, 14, 0, 0, DamageForms.bluntImpact)
    val mutatedWarriorSabre = Attack("sabre", 4, 11, 0, 0, DamageForms.slash)
    val mutatedWarriorDagger = Attack("dagger", 4, 7, 0, 0, DamageForms.pierce)
    val mutatedWarriorPistol =
        Attack("pistol", 1, 5, 20, 8, DamageForms.pierce)
    val deathbotGrasp =
        Attack("crushing grasp", 10, 30, 0, 0, DamageForms.pierce)
    val deathbotRay = Attack("death ray", 10, 30, 50, 9, DamageForms.laser)
    val poltergeistTouch = Attack("touch", 1, 8, 0, 0, DamageForms.pierce)
    val livingDeadWizard2VortexBeam =
        Attack("vortex beam", 10, 40, 40, 9, DamageForms.laser)
}