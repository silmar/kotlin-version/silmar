package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid

/**
 * Informs this player that it has been affected by poison gas of the given strength.
 */
fun Player.onPoisonGas(strength: Int) {
    // if this player doesn't avoid the gas
    val result = avoid(strength / 2)
    if (!result.avoided) {
        statuses.onPoisoned(1)
    }
}

