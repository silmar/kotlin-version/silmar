package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.clericalCastingLevel
import silmar.contexts.gameplay.areas.extensions.effects.performLightningBoltEffect
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Has this player emit lightning at the given pixel-location.
 */
fun SilmarPlayer.emitLightning(location: PixelPoint) {
    performPowerDischargeEffect(this)

    val adjustedLocation =
        if (isConfused) modifyLocationDueToConfusion(area, location, this.location)
        else location
    area.performLightningBoltEffect(this.location, adjustedLocation, clericalCastingLevel,
        false)
}

