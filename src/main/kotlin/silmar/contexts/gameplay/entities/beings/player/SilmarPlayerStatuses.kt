package silmar.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.PlayerStatuses

interface SilmarPlayerStatuses : PlayerStatuses {
    /**
     * Whether this player is currently moving stealthily.
     */
    val isStealthy: Boolean

    fun updateIsStealthy()

    val hasRetributiveShield: Boolean
    fun onRetributiveShieldRaised(duration: Int)
    fun drainRetributiveShield()

    val hasBionicStrength: Boolean
    fun onBionicStrengthAchieved(duration: Int)
    fun drainBionicStrength()

    val isQuickened: Boolean
    fun onQuickened(duration: Int)
    fun drainQuickenedSelf()

    val damagedSinceLayHands: Boolean
    fun onDamagedSinceLayHands()
    fun onLayHandsUse()

    fun incrementRadiationExposure()
    fun resetRadiationExposure()
    val isOverSafeRadiationExposureLimit: Boolean
    fun incrementRadiationDamage()
    var radiationDamage: Int
}