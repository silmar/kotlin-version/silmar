package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.CreateMonsterReasons
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.createMonsterForReason

class Mutivider : Monster1() {
    /**
     * Informs this mutivider that one of its attempts at stealing a ration worked.
     */
    fun onRationTheftWorked() {
        // generate a new mutivider
        createMonsterForReason(area, location, type, CreateMonsterReasons.generation)
    }
}