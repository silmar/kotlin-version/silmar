package silmar.contexts.gameplay.entities.beings.player.extensions.health

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.healFully
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onCureDiseaseReceived
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onCurePoisonReceived

/**
 * Inform this player that it is being healed by a medical robot.
 */
fun Player.onHealedByMedicalRobot() {
    // while this player is still poisoned
    while (statuses.isPoisoned) onCurePoisonReceived()

    // while this player is still diseased
    while (statuses.isDiseased) onCureDiseaseReceived()

    // heal this player fully
    healFully()
}

