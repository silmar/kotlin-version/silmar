package silmar.contexts.gameplay.entities.beings.player

import silmar.contexts.gameplay.entities.beings.player.extensions.stats.clericalCastingLevel
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.magicUserCastingLevel
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerStats1
import kotlin.math.max
import kotlin.math.min

class SilmarPlayerStats1(player: Player) : SilmarPlayerStats, PlayerStats1(player) {
    override var maxMagicPoints = 0
        private set(value) {
            field = value
            magicPoints = min(magicPoints, value)
            player.reportVitalStatChange()
        }

    override var magicPoints = 0
        private set(value) {
            field = min(value, maxMagicPoints)
            player.reportVitalStatChange()
        }

    override fun onMagicPointsUsed(amount: Int) {
        magicPoints -= amount
    }

    override fun rechargeMagicPoint() {
        magicPoints++
    }

    override var maxClericalPoints = 0
        private set(value) {
            field = value
            clericalPoints = min(clericalPoints, value)
            player.reportVitalStatChange()
        }

    override var clericalPoints = 0
        private set(value) {
            field = min(value, maxClericalPoints)
            player.reportVitalStatChange()
        }

    override fun onClericalPointsUsed(amount: Int) {
        clericalPoints -= amount
    }

    override fun rechargeClericalPoint() {
        clericalPoints++
    }

    override fun updateMaxMagicPoints() {
        player as SilmarPlayer
        val provider = SilmarPlayerAttributeModifierProvider
        val values = player.attributeValues
        maxMagicPoints = max(0,
            player.magicUserCastingLevel * (3 + provider.magicPointsModifierForIntelligence(
                values.intelligence) + provider.magicPointsModifierForEndurance(
                values.endurance)))
    }

    override fun updateMaxClericalPoints() {
        player as SilmarPlayer
        val provider = SilmarPlayerAttributeModifierProvider
        val values = player.attributeValues
        maxClericalPoints = max(0,
            player.clericalCastingLevel * (3 + provider.clericalPointsModifierForJudgement(
                values.judgement) + provider.clericalPointsModifierForEndurance(
                values.endurance)))
    }
}