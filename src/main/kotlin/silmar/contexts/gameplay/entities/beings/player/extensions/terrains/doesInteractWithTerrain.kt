package silmar.contexts.gameplay.entities.beings.player.extensions.terrains

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.game.SilmarGame
import silmar.contexts.gameplay.game.extensions.movePlayerThroughDownExit
import silmar.contexts.gameplay.game.extensions.movePlayerThroughUpExit
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.game.Game

fun Player.doesInteractWithTerrain(terrain: Terrain): Boolean = when (terrain.type) {
    SilmarTerrainTypes.downExit -> {
        reportToListeners { it.onAreaLoading() }

        // inform the game
        val game = Game.currentGame as SilmarGame
        game.movePlayerThroughDownExit(this)

        endTurn()

        true
    }
    SilmarTerrainTypes.upExit -> {
        reportToListeners { it.onAreaLoading() }

        // inform the game
        val game = Game.currentGame as SilmarGame
        game.movePlayerThroughUpExit(this)

        endTurn()

        true
    }
    else -> false
}
