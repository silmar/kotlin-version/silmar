package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.game.Game
import silmarengine.isDebugOn

/**
 * Checks to see if it's time for this player to recharge a magic point.
 */
fun SilmarPlayer.checkForMagicPointRecharge() {
    // if this player is a wizard
    if (hasGainedLevel(PlayerLevels.wizard)) {
        // if it's time to recharge a magic point
        val magicRechargeInterval = 5
        val game = Game.currentGame
        if (game!!.turn % magicRechargeInterval == 0 || isDebugOn) {
            silmarStats.rechargeMagicPoint()
        }
    }
}

