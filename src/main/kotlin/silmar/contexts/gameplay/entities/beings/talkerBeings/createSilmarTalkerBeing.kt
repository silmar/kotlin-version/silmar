package silmar.contexts.gameplay.entities.beings.talkerBeings

import silmar.contexts.gameplay.entities.beings.talkerBeings.specializations.*
import silmar.contexts.gameplay.entities.beings.talkerBeings.types.SilmarTalkerBeingTypes
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.*
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.TalkerBeingType

private typealias Types = SilmarTalkerBeingTypes

fun createSilmarTalkerBeing(type: TalkerBeingType): TalkerBeing? =
    when (type) {
        Types.porter -> Porter()
        Types.blacksmith -> TopsideBlacksmith()
        Types.nightdwarf -> Nightdwarf()
        Types.demonicWizard -> DemonicWizard()
        Types.alienBuyer -> AlienBuyer()
        Types.chamberlain -> Buyer()
        Types.priest -> TopsidePriest()
        Types.storekeeper -> TopsideStorekeeper()
        Types.medicalRobot -> MedicalRobot()
        Types.sage -> ItemIdentifier()
        else -> null
    }