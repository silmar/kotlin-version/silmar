package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerClasses
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer

/**
 * At what level of experience this player is considered to be casting magic-user spells.
 */
val SilmarPlayer.magicUserCastingLevel: Int
    get() {
        // find the highest magic-user level this player has achieved and retained
        val magicLevels = PlayerClasses.magicUser.levels
        val magicLevel = silmarLevelValues.levelsRetained.lastOrNull { level ->
            magicLevels.contains(level)
        } ?: return 0

        // return the casting level corresponding to the magic-user level found above
        return magicLevels.indexOf(magicLevel) + 1
    }

/**
 * At what level of experience this player is considered to be casting clerical spells.
 */
val SilmarPlayer.clericalCastingLevel: Int
    get() {
        // find the highest clerical level this player has achieved and retained
        val clericalLevels = PlayerClasses.cleric.levels
        val clericalLevel = silmarLevelValues.levelsRetained.lastOrNull { level ->
            clericalLevels.contains(level)
        } ?: return 0

        // return the casting level corresponding to the clerical level found above
        return clericalLevels.indexOf(clericalLevel) + 1
    }

