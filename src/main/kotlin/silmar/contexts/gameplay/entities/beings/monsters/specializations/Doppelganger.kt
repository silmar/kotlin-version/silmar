package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.talkerBeingTypes
import silmarengine.contexts.gameplay.areas.extensions.entities.onEntityChangedAppearance
import silmarengine.getImage
import silmarengine.util.math.randomInt

class Doppelganger : Monster1() {
    /**
     * The filename (with path) of the image this doppleganger is using for its
     * appearance.
     */
    private var imageName: String? = null

    override fun act() {
        // if this doppelganger hasn't yet determined its image
        if (imageName == null) {
            // use the image-name of a random talker-being type, so that this
            // doppelganger appears as a friendly talker
            val types = talkerBeingTypes
            val index = randomInt(0, types.size - 1)
            imageName = types[index].imagePath!!

            onImageToChange()
            area.onEntityChangedAppearance()
        }

        // if this doppelganger hasn't yet retrieved its image, do so
        val imageName = imageName
        if (image == null && imageName != null) image = getImage(imageName)

        super.act()
    }
}