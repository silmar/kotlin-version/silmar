package silmar.contexts.gameplay.entities.beings.player

/**
 * The flyweight player classes.
 */
object PlayerClasses {
    val warrior =
        PlayerClass("Warrior",
            listOf(
                PlayerLevels.fighter,
                PlayerLevels.monk,
                PlayerLevels.paladin,
                PlayerLevels.archer,
                PlayerLevels.superhero,
                PlayerLevels.neonKnight))
    val thief = PlayerClass("Thief",
        listOf(
            PlayerLevels.thief,
            PlayerLevels.barbarian,
            PlayerLevels.ninja))
    val magicUser =
        PlayerClass("Magic-User",
            listOf(
                PlayerLevels.wizard,
                PlayerLevels.sorcerer,
                PlayerLevels.alchemist,
                PlayerLevels.battlemage,
                PlayerLevels.scientist))
    val cleric =
        PlayerClass("Cleric", listOf(
            PlayerLevels.cleric,
            PlayerLevels.druid,
            PlayerLevels.priest,
            PlayerLevels.deathCaster))
    // This arrangement of monster levels achieves the following:
    // - keeps werewolf and subvampire in the same class, which is good since they
    // have the invulnerability powers that are in succession with each other
    // - make subvampire harder to reach than if there were three monster classes,
    // which is good since its invulnerability power is so powerful
    // - keeps the subvampire and demon hand attack powers from getting in each other's
    // way
    // - puts the werewolf's night-vision and lesser invulnerability powers first,
    // where they will do the most good
    // - puts minotaur's secret-door and monster-sensing powers next,
    // since they also be more helpful earlier
    val monster =
        PlayerClass("Monster",
            listOf(
                PlayerLevels.werewolf,
                PlayerLevels.minotaur,
                PlayerLevels.troll,
                PlayerLevels.giant,
                PlayerLevels.subvampire))
    val exotic =
        PlayerClass("Exotic", listOf(
            PlayerLevels.mutatedSpider,
            PlayerLevels.pixie,
            PlayerLevels.kangadillo,
            PlayerLevels.demon,
            PlayerLevels.cyborg))
}