package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.entities.terrains.specializations.TrollCarcass
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.areas.extensions.effects.performPoofEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.terrains.createTerrain

class Troll : Monster1() {
    override fun act() {
        // if this troll is damaged
        if (hitPoints < maxHitPoints) {
            // it regenerates
            takeHealing(regenerationRate)
        }

        super.act()
    }

    override fun die(damage: Damage) {
        // if it wasn't fire or explosion that caused the death
        if (damage.form != DamageForms.fire && damage.form != DamageForms.explosion) {
            // create a troll carcass terrain and add it to the area
            val carcass = createTerrain(SilmarTerrainTypes.trollCarcass) as TrollCarcass
            carcass.setTroll(this)
            area.addEntity(carcass, location)

            // do a poof, since this monster *is* going away temporarily
            area.performPoofEffect(location, size)

            // remove this troll from the area
            area.removeEntity(this)

            // this monster is now not truly dying, so don't execute the super-class
            // behavior for dying, below
            return
        }

        super.die(damage)
    }

    companion object {
        /**
         * How many points a troll regenerates per game turn.
         */
        const val regenerationRate = 3
    }
}