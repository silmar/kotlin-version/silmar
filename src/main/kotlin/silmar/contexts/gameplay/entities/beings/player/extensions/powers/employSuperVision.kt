package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayerListener
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onSoundHeard
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms

/**
 * Has this player employ super vision for a brief moment.
 */
fun Player.employSuperVision() {
    performPowerDischargeEffect(this)

    takeDamage(Damage(5, DamageForms.exertion))

    onSoundHeard(SilmarSounds.superVision, location)

    reportToListeners { (it as SilmarPlayerListener).onSuperVisionEmployed() }
}

