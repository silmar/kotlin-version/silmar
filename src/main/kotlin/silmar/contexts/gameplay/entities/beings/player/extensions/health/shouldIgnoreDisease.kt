package silmar.contexts.gameplay.entities.beings.player.extensions.health

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Disease
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel

fun Player.shouldIgnoreDisease(@Suppress("UNUSED_PARAMETER") disease: Disease): Boolean {
    // paladins aren't affected by disease
    this as SilmarPlayer
    return hasGainedLevel(PlayerLevels.paladin)
}