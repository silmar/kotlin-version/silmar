package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.extensions.entities.getNearestItem
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDirectionString

/**
 * Has this player try to sense the direction of the nearest valuable item on
 * the current area-level.
 */
fun Player.findTreasure() {
    performPowerDischargeEffect(this)

    // report the direction of the nearest valuable item to this player
    val itemEntity = area.getNearestItem(location, 100)
    onMessage(
        if (itemEntity != null) "The nearest treasure is to the " + getDirectionString(
            location, itemEntity.location) + "."
        else "You sense no valuable items around here!")
}

