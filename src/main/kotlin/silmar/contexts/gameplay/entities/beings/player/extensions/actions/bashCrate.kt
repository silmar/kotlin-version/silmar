package silmar.contexts.gameplay.entities.beings.player.extensions.actions

import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmar.contexts.gameplay.areas.extensions.effects.performPoofEffect
import silmar.contexts.gameplay.entities.items.types.SilmarArmorTypes
import silmar.contexts.gameplay.entities.items.types.SilmarShieldTypes
import silmar.contexts.gameplay.entities.items.types.SilmarAmmoTypes
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.ItemEntity1
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.tiles.tileSize
import silmarengine.util.math.randomInt
import silmarengine.util.sleepThread

fun Player.bashCityCrate(crate: Terrain) {
    // issue a bash sound, and wait for most of it to play
    area.onSoundIssued(SilmarSounds.bashingCrate, location)
    sleepThread(1500)

    // if a +0 strength check is made
    if (randomInt(1, 20) <= attributeValues.strength) {
        // poof the crate and remove it
        val location = crate.location
        area.performPoofEffect(location, tileSize)
        area.removeEntity(crate)

        // if the crate held an item
        val (itemType, quantity) = determineCrateItemType()
        if (itemType != null) {
            // place the item where the crate was
            val item = createItem(itemType, ItemCondition.EXCELLENT)
            if (itemType.isGroupable) item.quantity =
                randomInt(quantity / 2, quantity * 3 / 2)
            val entity = ItemEntity1(item)
            area.addEntity(entity, location)
        }
    }
    else receiveMessageIfInLOSOf(location, "The crate holds strong")

    endTurn()
}

private fun determineCrateItemType(): Pair<ItemType?, Int> {
    var itemType: ItemType? = null
    var quantity = 1
    when (randomInt(1, 22)) {
        in 10..12 -> {
            itemType = GroupableItemTypes.ration
            quantity = 25
        }
        in 13..14 -> {
            itemType = SilmarAmmoTypes.pistolBullet.itemType
            quantity = 1000
        }
        15 -> {
            itemType = SilmarAmmoTypes.assaultRifleBullet.itemType
            quantity = 500
        }
        in 16..17 -> itemType = SilmarWeaponTypes.pistol.weaponType.itemType
        18 -> itemType = SilmarWeaponTypes.assaultRifle.weaponType.itemType
        in 19..20 -> itemType = SilmarArmorTypes.kevlarArmor.itemType
        21 -> itemType = SilmarArmorTypes.heavyKevlarArmor.itemType
        22 -> itemType = SilmarShieldTypes.riotShield.itemType
    }

    return Pair(itemType, quantity)
}