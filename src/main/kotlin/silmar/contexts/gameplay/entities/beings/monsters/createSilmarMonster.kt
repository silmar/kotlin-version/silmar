package silmar.contexts.gameplay.entities.beings.monsters

import silmar.contexts.gameplay.entities.beings.monsters.specializations.*
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.beings.monsters.Assassin
import silmarengine.contexts.gameplay.entities.beings.monsters.Hider
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.types.MonsterType

typealias Types = SilmarMonsterTypes

fun createSilmarMonster(type: MonsterType): Monster? =
    when (type) {
        Types.acidDragon -> AcidDragon()
        Types.assassin -> Assassin()
        Types.bat -> Bat()
        Types.goblin -> Goblin()
        Types.blob -> Blob()
        Types.skeleton -> Skeleton()
        Types.robotGenerator -> RobotGenerator()
        Types.cetharg -> Cetharg()
        Types.grenadier -> Grenadier()
        Types.giantSpider -> GiantSpider()
        Types.vortex -> Vortex()
        Types.bones -> Bones()
        Types.grave -> Grave()
        Types.giantAnt -> GiantAnt()
        Types.darkKnight -> DarkKnight()
        Types.hulkster -> Hulkster()
        Types.medusa -> Medusa()
        Types.warlock -> Warlock()
        Types.troll -> Troll()
        Types.scumShamble -> Hider()
        Types.casket -> Casket()
        Types.doppleganger -> Doppelganger()
        Types.dragonman -> Dragonman()
        Types.atoman -> Atoman()
        Types.redDragon -> RedDragon()
        Types.moldySkeleton -> MoldySkeleton()
        Types.maniac -> Maniac()
        Types.mirroredArmor -> MirroredArmor()
        Types.visculoid -> Visculoid()
        Types.dreadedPansy -> DreadedPansy()
        Types.floatingSpheroid -> FloatingSpheroid()
        Types.ferrousGolem -> FerrousGolem()
        Types.mummy -> Mummy()
        Types.killerTree -> KillerTree()
        Types.slime -> Slime()
        Types.transparentGelatinoid -> TransparentGelatinoid()
        Types.stalkin -> Stalkin()
        Types.poltergeist -> Poltergeist()
        Types.munchkin -> Munchkin()
        Types.gremlin -> Gremlin()
        Types.vampire -> Vampire()
        Types.gammaVampire -> GammaVampire()
        Types.mutivider -> Mutivider()
        Types.livingDeadWizard -> LivingDeadWizard()
        Types.livingDeadWizard2 -> LivingDeadWizard2()
        else -> null
    }

