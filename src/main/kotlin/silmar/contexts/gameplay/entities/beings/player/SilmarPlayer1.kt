package silmar.contexts.gameplay.entities.beings.player

import silmar.contexts.gameplay.entities.beings.player.extensions.items.checkForNecklaceOfChokingEffects
import silmar.contexts.gameplay.entities.beings.player.extensions.harm.affectDamageAsSilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.*
import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.contexts.gameplay.entities.beings.player.Player1
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.getExperienceNeededForLevel
import silmarengine.contexts.gameplay.entities.beings.player.extensions.inventory.createInventoryItem
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.entities.beings.player.startingPlayerLevel
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.contexts.gamecreation.domain.model.GCPlayer
import silmarengine.getImage
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.types.NonGroupableItemTypes
import silmarengine.sounds.Sound
import java.awt.Image
import kotlin.math.max

class SilmarPlayer1(fromPlayer: GCPlayer) : SilmarPlayer, Player1(fromPlayer) {
    override val silmarLevelValues get() = levelValues as SilmarPlayerLevelValues

    override val silmarStats get() = stats as SilmarPlayerStats

    override val silmarStatuses get() = statuses as SilmarPlayerStatuses

    override val powers: List<PlayerPower>
        get() = silmarLevelValues.levelsRetained.flatMap { it.powers.toList() }

    /**
     * An area from dungeon-level-index to the teleportation location this player has
     * memorized for that level.
     */
    private val memorizedLocations = HashMap<Int, PixelPoint>()

    /**
     * Returns the image that should be used to represent this player,
     * which should be that associated with this player's current experience level.
     */
    @Transient
    override var image: Image? = null
        get() {
            return if (field != null) field
            else {
                val level =
                    silmarLevelValues.levels[max(levelValues.level - startingPlayerLevel,
                        0)]
                val imagePath = level.imagePath
                field = getImage(imagePath)
                field
            }
        }

    override val movementSound: Sound?
        get() = if (silmarStatuses.isStealthy) null else super.movementSound

    override val needsLightToSee: Boolean
        get() = !hasGainedLevel(PlayerLevels.werewolf) || super.needsLightToSee

    init {
        // start this player off as a squire, a player-level that should never be taken away
        silmarLevelValues.onPlayerLevelChosen(PlayerLevels.squire)

        // set this player's starting experience (and therefore, experience level) as an amount
        // below the point where the first level is gained; that amount should be achievable within the first
        // two dungeon levels
        levelValues.onExperienceGained(
            getExperienceNeededForLevel(startingPlayerLevel + 1) - 100)

        // set hit-points to this player's maximum
        hitPoints = maxHitPoints

        // give this player its starting items
        items.createInventoryItem(NonGroupableItemTypes.torch)
        items.createInventoryItem(NonGroupableItemTypes.torch)
        items.createInventoryItem(NonGroupableItemTypes.torch)
        items.createInventoryItem(GroupableItemTypes.ration, 2)
        items.createInventoryItem(GroupableItemTypes.gold, 20)
        items.createInventoryItem(WeaponTypes.dagger.itemType, 20)
    }

    override fun setSizeFromLevelImage() {
        mutableBounds.setSize(
            silmarLevelValues.levels[max(levelValues.level - startingPlayerLevel,
                0)].size)
    }

    override fun checkForGameSpecificPlayerConditions() {
        checkForBarbarianQuickHealing()
        checkForTrollRegeneration()
        checkForNecklaceOfChokingEffects()
        checkForMagicPointRecharge()
        checkForClericalPointRecharge()
        val statuses = silmarStatuses
        if (statuses.hasRetributiveShield) statuses.drainRetributiveShield()
        checkForBionicStrengthDrain()
        checkForQuickenSelfDrain()
        checkForPaladinCuringOfOwnDiseases()
        checkForSuperheroCuringOfOwnPoison()
    }

    override fun applyGameSpecificEffectsOnMaxHitPoints(max: Int): Int =
        max - silmarStatuses.radiationDamage

    override fun onTurnOver() {
        super<Player1>.onTurnOver()

        // if this player is confused and is now a minotaur
        if (isConfused && hasGainedLevel(PlayerLevels.minotaur)) {
            // this player is no longer confused
            confusedTurnsLeft = 0
        }
    }

    override fun getMemorizedLocation(level: Int): PixelPoint? =
        memorizedLocations[level]

    override fun onLocationMemorized() {
        memorizedLocations[(area as SilmarArea).level!!.index] = PixelPoint(location)
    }

    override fun affectDamage(damage: Damage) {
        super.affectDamage(damage)
        affectDamageAsSilmarPlayer(damage)
    }
}