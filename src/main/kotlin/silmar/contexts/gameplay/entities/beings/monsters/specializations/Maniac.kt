package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.beings.monsters.LightSourceMonster
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.damage.Damage

class Maniac : LightSourceMonster() {
    override fun onDamaged(damage: Damage) {
        // if this monster am not about to die, and is not already enraged
        if (hitPoints > 0 && type != SilmarMonsterTypes.maniacEnraged) {
            // this maniac becomes an enraged maniac
            mutableType = SilmarMonsterTypes.maniacEnraged

            // inform the player if it's in LOS
            area.player?.receiveMessageIfInLOSOf(location, "enraged!")
        }
    }
}