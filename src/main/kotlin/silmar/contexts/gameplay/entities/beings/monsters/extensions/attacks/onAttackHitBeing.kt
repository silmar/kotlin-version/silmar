package silmar.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmar.contexts.gameplay.entities.beings.monsters.specializations.Atoman
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.monsters.Assassin
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.util.math.randomInt

fun Monster.onAttackHitBeing(
    being: Being, attack: MonsterAttack, @Suppress("UNUSED_PARAMETER") hitBy: Int
) {
    if (being !is Player) return

    when (this) {
        is Assassin ->
            // if this hider isn't yet visible
            if (!isVisible) {
                // hit the player with an assassination blow
                being.onMessage("Assassination blow!!")
                val damage = randomInt(attack.minDamage * 3, attack.maxDamage * 3)
                being.takeDamage(Damage(damage, DamageForms.pierce))
            }
        is Atoman -> {
            // start a fire in the victim's square
            val fire = createTerrain(SilmarTerrainTypes.fire)
            area.addEntity(fire, being.location)
        }
    }
}
