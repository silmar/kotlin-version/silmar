package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmar.contexts.gameplay.areas.extensions.effects.performPoofEffect
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.healFully
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.extensions.proximity.isWithinDistanceOf
import silmarengine.getImage
import silmarengine.contexts.gameplay.areas.extensions.entities.onEntityChangedAppearance
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import java.awt.Image

open class Vampire : Monster1() {
    /**
     * Whether this vampire is currently in bat form.
     */
    var isBat: Boolean = false
        private set(value) {
            field = value
            onIsBatChange(value)
        }

    @Suppress("SuspiciousVarProperty")
    override var image: Image? = null
        get() = if (isBat) getImage(SilmarMonsterTypes.bat.imagePath!!)
        else super.image

    override val maxMovementPoints: Int
        get() = if (isBat) SilmarMonsterTypes.bat.movementPoints else super.maxMovementPoints

    override val movementType: MovementType
        get() = if (isBat) MovementType.FLYING else super.movementType

    override val canCurrentlyMakeMeleeAttacks: Boolean
        get() = !isBat

    /**
     * Informs this vampire that one of its drain strike attacks worked.
     */
    fun onDrainStrikeWorked() {
        // this vampire is healed
        healFully()
    }

    override fun act() {
        // if this vampire is damaged
        if (hitPoints < maxHitPoints) {
            // it regenerates
            takeHealing(3)
        }

        // if this vampire isn't currently in bat form
        if (!isBat) {
            // if there is a player in the area
            val player = area.player
            if (player != null) {
                // if the player is more than a turn's movement away from
                // this vampire
                if (!isDistanceAtMost(location, player.location,
                        minDistanceToBecomeBat)) {
                    // become a bat, to speed this vampire's movement
                    isBat = true
                }
                // else, if this vampire is critically wounded
                else if (hitPoints <= maxHitPoints / 4) {
                    // become a bat and flee the player
                    isBat = true
                    isFleeing = true
                    fleeFromPlayer = player
                }
            }
        }
        // else, this vampire is in bat form
        else {
            // if this vampire is fleeing
            if (isFleeing) {
                // if this vampire has healed itself back to a more safe level
                if (hitPoints > maxHitPoints / 2) {
                    // stop fleeing
                    isFleeing = false
                }
            }

            checkToExitBatForm()
        }

        super.act()
    }

    override fun endTurn() {
        if (isBat) checkToExitBatForm()
        super.endTurn()
    }

    private fun checkToExitBatForm() {
        // if this vampire is not fleeing and is activated
        if (!isFleeing && isActivated) {
            // if this vampire is a turn or so's movement away from the player
            if (area.player?.isWithinDistanceOf(location,
                    minDistanceToBecomeBat) == true) {
                // change back to normal form, to make attacks look normal
                isBat = false
            }
        }
    }

    private fun onIsBatChange(newValue: Boolean) {
        // determine this vampire's size, according to whether it's a bat
        mutableBounds.setSize(
            if (newValue) SilmarMonsterTypes.bat.size else SilmarMonsterTypes.vampire.size)

        // do a poof, for effect
        area.performPoofEffect(location, size)

        // inform the area
        onImageToChange()
        area.onEntityChangedAppearance()
    }

    companion object {
        /**
         * The minimum distance this vampire should be from its nearest player in order
         * for changing into a bat (to reach that player quicker) to be a good idea.
         */
        private val minDistanceToBecomeBat =
            PixelDistance(SilmarMonsterTypes.vampire.movementPoints * tileWidth * 2)
    }
}