package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmar.contexts.gameplay.entities.beings.player.PlayerPowers
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.extensions.proximity.isNextTo

fun Player.onDamagedAsSilmarPlayer(damage: Damage) {
    // if more non-exertion-based damage would be sustained than is incurred
    // during a paladin's laying on of hands, remember that this player
    // was damaged since the last such healing act
    this as SilmarPlayer
    if (damage.amount > PlayerPowers.repelLivingDeadCostForPaladin && damage.form != DamageForms.exertion) silmarStatuses.onDamagedSinceLayHands()

    // if the damage was from a foe
    if (damage.foe != null) {
        // if this player has a retributive shield going
        if (silmarStatuses.hasRetributiveShield) {
            // if the foe is adjacent to this player
            if (isNextTo(damage.foe!!)) {
                // the foe is damaged for a like amount
                damage.foe!!.takeDamage(Damage(damage.amount, DamageForms.bluntImpact))
            }
        }
    }
}