package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.PlayerPower
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.healFully
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Has this player use the given power on the given pixel-location.
 */
fun SilmarPlayer.usePower(power: PlayerPower, on: PixelPoint? = null) {
    if (isParalyzed) return

    // carry out the power use
    var turnUsed = true
    when (power) {
        PlayerPowers.healWounds -> healWounds()
        PlayerPowers.curePoison -> curePoison()
        PlayerPowers.cureDisease -> cureDisease()
        PlayerPowers.achieveBionicStrength -> achieveBionicStrength()
        PlayerPowers.conjureDeathFog -> conjureDeathFog(on!!)
        PlayerPowers.emitLightning -> emitLightning(on!!)
        PlayerPowers.emitRadWave -> emitRadWave()
        PlayerPowers.employSuperVision -> employSuperVision()
        PlayerPowers.senseCreatures -> senseCreatures()
        PlayerPowers.throwBoulder -> throwBoulder(on!!)
        PlayerPowers.spinWeb -> {
            spinWeb()
            turnUsed = false
        }
        PlayerPowers.leap -> turnUsed = leap(on!!)
        PlayerPowers.senseSecretDoors -> senseSecretDoors()
        PlayerPowers.memorizeLocation -> memorizeLocation()
        PlayerPowers.hurlFireball -> hurlFireball(on!!)
        PlayerPowers.healFully -> healFully()
        PlayerPowers.revealTraps -> revealTraps()
        PlayerPowers.placeVengefulSymbol -> {
            placeVengefulSymbol(on!!)
            turnUsed = false
        }
        PlayerPowers.findTreasure -> findTreasure()
        PlayerPowers.findExit -> findExit()
        PlayerPowers.projectRetributiveShield -> projectRetributiveShield()
        PlayerPowers.summonMeteorShower -> summonMeteorShower()
        PlayerPowers.quickenSelf -> quickenSelf()
        PlayerPowers.freezeBeing -> freezeBeing(on!!)
        PlayerPowers.repelLivingDeadCleric -> repelLivingDead()
        PlayerPowers.repelLivingDeadPaladin -> repelLivingDead()
        PlayerPowers.fireEyeBeams -> fireEyeBeams(on!!)
        PlayerPowers.layHands -> layHands()
        PlayerPowers.teleportSelf -> teleportToLevel(on!!.x)
        PlayerPowers.transmuteGoldToBars -> transmuteGoldToBars()
        PlayerPowers.transmuteBarsToGold -> transmuteBarsToGold()
    }

    // if determined above that this power use took up the rest of this player's current turn
    if (turnUsed) {
        // this player has acted for this turn
        endTurn()
    }
}

