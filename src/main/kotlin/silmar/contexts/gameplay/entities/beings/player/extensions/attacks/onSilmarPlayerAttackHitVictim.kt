package silmar.contexts.gameplay.entities.beings.player.extensions.attacks

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.areas.extensions.effects.performDisintegrationEffect
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.extensions.location.teleportRandomlyWithinArea
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.util.math.randomInt

fun Player.onSilmarAttackHitVictim(weapon: Weapon, victim: Being, damage: Int) {
    // if this player is a subvampire
    this as SilmarPlayer
    if (hasGainedLevel(PlayerLevels.subvampire)) {
        // if the weapon used was the hand
        if (weapon.isOfType(WeaponTypes.hand)) {
            // if this player makes a +0 agility check
            val roll = randomInt(1, 20)
            if (roll <= attributeValues.agility) {
                // heal this player for half the damage done
                takeHealing(damage / 2)
            }
        }
    }

    // if this player is a demon; note that we want this check to precede that below
    // for being a pixie, since the demon effect is more powerful
    if (hasGainedLevel(PlayerLevels.demon)) {
        // if the victim is still alive
        if (!victim.isDead) {
            // if the attack was made with the hand
            if (weapon.isOfType(WeaponTypes.hand)) {
                // if this player makes a +0 endurance check
                val roll = randomInt(1, 20)
                if (roll <= attributeValues.endurance) {
                    // if the victim doesn't avoid the attack
                    val result = victim.avoid(-4)
                    if (!result.avoided) {
                        // the victim is disintegrated
                        area.performDisintegrationEffect(victim.location)
                        victim.takeDamage(
                            Damage(victim.hitPoints, DamageForms.disintegration))
                    }
                }
            }
        }
    }

    // if this player is a pixie
    if (hasGainedLevel(PlayerLevels.pixie)) {
        // if the victim is still alive
        if (!victim.isDead) {
            // if the attack was with the hand
            if (weapon.isOfType(WeaponTypes.hand)) {
                // if the victim fails an avoidance check
                val result = victim.avoid(0)
                if (!result.avoided) {
                    // the victim is teleported
                    victim.teleportRandomlyWithinArea()
                }
            }
        }
    }
}