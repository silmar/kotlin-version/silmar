package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onCureDiseaseReceived
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel

fun SilmarPlayer.checkForPaladinCuringOfOwnDiseases() {
    // if this player is diseased but is now a paladin
    val afflictions = statuses.diseaseAfflictions
    if (afflictions.isNotEmpty() && hasGainedLevel(PlayerLevels.paladin)) {
        // cure all of this player's diseases
        while (afflictions.isNotEmpty()) onCureDiseaseReceived()
    }
}