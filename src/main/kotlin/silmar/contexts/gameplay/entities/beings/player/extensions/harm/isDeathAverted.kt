package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.healFully
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.Item

fun Player.isDeathAverted(): Boolean {
    // if this player is holding a phylactery of resurrection
    val phylactery = items.inventory.getFirstItemOfType(
        SilmarMagicItemTypes.phylacteryOfResurrection.itemType) as Item?
    if (phylactery != null) {
        // it goes off, healing this player fully (and being destroyed is the process)
        onMessage("phylactery of resurrection!")
        area.onSoundIssued(SilmarSounds.phylacteryOfResurrection, location)
        phylactery.destroy(false)
        healFully()

        // this player's death is prevented
        return true
    }

    return false
}