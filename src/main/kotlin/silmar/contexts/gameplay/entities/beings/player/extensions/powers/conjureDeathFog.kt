package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.clericalCastingLevel
import silmar.contexts.gameplay.areas.extensions.effects.performDeathFogEffect
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Has this player conjure a death-fog at the given pixel-location.
 */
fun SilmarPlayer.conjureDeathFog(location: PixelPoint) {
    performPowerDischargeEffect(this)

    val adjustedLocation =
        if (isConfused) modifyLocationDueToConfusion(area, location, this.location)
        else location
    area.performDeathFogEffect(adjustedLocation, clericalCastingLevel)
}

