package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.Names
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.monsters.createMonster
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.CreateMonsterReasons
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.createMonsterForReason
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomBoolean
import silmarengine.util.sleepThread

class RobotGenerator : Monster1() {
    init {
        maxHitPoints = 200
    }

    override fun usePreMovementPower(player: Player): Boolean {
        // do this every fourth game turn
        if (Game.currentGame!!.turn % 4 == 0) {
            // emit a robot-generation sound, and pause a bit so that it is mostly heard
            // before the generation occurs below
            area.onSoundIssued(SilmarSounds.robotGenerator, location, true)
            sleepThread(1000)

            // generate a new robot
            createMonsterForReason(area, location,
                if (randomBoolean) SilmarMonsterTypes.sentryborg else SilmarMonsterTypes.deathbot,
                CreateMonsterReasons.generation)
            return true
        }

        return false
    }

    override fun die(damage: Damage) {
        // remember these values, as super.die might change them
        val area = area
        val location = PixelPoint(this.location)

        super.die(damage)

        // create the wizard
        val wizard = createMonster(SilmarMonsterTypes.livingDeadWizard2)

        // find a passible spot near this generator to place the wizard
        var wizardLocation = area.getNearbyPassibleRectangleLocation(location, wizard.size,
            type.movementType, PixelDistance(3 * tileWidth))
        if (wizardLocation == null) wizardLocation = location

        // do a teleport-out effect for the wizard's teleporting-in
        val lastFrame = area.performTeleportEffect(wizardLocation, true, true)

        // add the wizard to the area
        area.addEntity(wizard, wizardLocation)

        // do a teleport-in effect to finish the wizard's teleporting-in
        area.removeEntity(lastFrame!!)
        area.performTeleportEffect(wizardLocation, false, false)

        // for each line of the greeting this living-dead-wizard-2 will give the player
        val greetingLines = arrayOf(Names.archEnemy + " towers above you and bellows",
            "\"You have destroyed my precious machine!",
            "This time you shall face my full might...",
            "And once I have dispensed with you,",
            "I will wreak my vengeance upon the lands",
            "and establish my rule, which will be without end!\"")
        greetingLines.forEach { line ->
            // display this line of the greeting to the player
            area.player?.receiveMessageIfInLOSOf(wizardLocation, line)

            // pause a bit, so the user has time to read this line before the next appears
            sleepThread(3000)
        }
    }
}