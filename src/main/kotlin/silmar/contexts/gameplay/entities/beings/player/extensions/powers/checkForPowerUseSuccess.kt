package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.PlayerPower
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayerAttributeModifierProvider
import silmarengine.util.math.randomInt

/**
 * Returns whether this player successfully makes a check to be able to use the given power.
 */
fun SilmarPlayer.checkForPowerUseSuccess(power: PlayerPower): Boolean {
    var modifier = 0
    val provider = SilmarPlayerAttributeModifierProvider
    val values = attributeValues
    when {
        // if the power is a magic-user spell
        power.isMagicUserSpell -> {
            modifier += -power.magicPointCost
            modifier += provider.castingModifierForIntelligence(values.intelligence)
        }
        // else, if the power is a clerical spell
        power.isClericalSpell -> {
            modifier += -power.clericalPointCost
            modifier += provider.castingModifierForJudgement(values.judgement)
        }
        // otherwise, no check is needed
        else -> return true
    }

    // determine if a roll is low enough for success
    val roll = randomInt(1, 20)
    return roll <= 13 + modifier
}

