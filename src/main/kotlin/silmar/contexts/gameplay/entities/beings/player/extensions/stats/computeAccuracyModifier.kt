package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.cyborgIntelligencePerBonus
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes

fun Player.computeAccuracyModifier(): Int {
    // if this player is a cyborg
    var modifier = 0
    this as SilmarPlayer
    if (hasGainedLevel(PlayerLevels.cyborg)) {
        // its accuracy is improved by its intelligence
        modifier += attributeValues.intelligence / cyborgIntelligencePerBonus
    }

    // if this player is an archer
    if (hasGainedLevel(PlayerLevels.archer)) {
        // if the weapon in use is a subtype of the bow
        if (weapon.type.isDescendedFromType(WeaponTypes.bow.itemType)) {
            modifier += levelValues.level / 5
        }
    }

    return modifier
}