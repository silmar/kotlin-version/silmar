package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onConfusionAttempt
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.util.math.randomInt

class Hulkster : Monster1() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if the given player isn't confused, and it's time to do a gaze attack
        if (!player.isConfused && randomInt(1, 5) <= 2) {
            // do a power discharge
            area.performPowerDischargeEffect(location)

            // inform the player of this monster's gaze
            player.onConfusionAttempt(powerRating)

            return true
        }

        return false
    }
}