package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeNotParalyzed
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeParalyzed
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.util.math.randomInt

/**
 * Informs this player that it has been hit by a medusa gaze of the given strength.
 */
fun Player.onMedusaGaze(strength: Int) {
    if (isParalyzed) return

    // if this player can't avoid the gaze
    val result = avoid(strength / 2)
    if (!result.avoided) {
        // this player is paralyzed
        becomeParalyzed(randomInt(1, strength))
    }
}

/**
 * Informs this player that a medusa has died on this player's area.
 */
fun Player.onMedusaDeath() = becomeNotParalyzed()

