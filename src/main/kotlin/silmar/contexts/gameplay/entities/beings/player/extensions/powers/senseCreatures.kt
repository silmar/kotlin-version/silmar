package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.extensions.pathfinding.findPassiblePath
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * Has this player try to sense if there are any monsters nearby.
 */
fun Player.senseCreatures() {
    area.onSoundIssued(SilmarSounds.senseCreatures, location)

    // for each monster within a certain range of this player
    val maxRange = PixelDistance(10 * tileWidth)
    val inRange = area.monsters.getEntitiesInRange(location, maxRange)
    val maxPathLength = TileDistance(20)
    inRange.forEach { monster ->
        // if a passible path exists from this player to this monster (using the flying
        // movement type, so monsters may be sensed across water)
        if (area.findPassiblePath(location, monster.location, this, monster,
                maxPathLength, MovementType.FLYING, false, false).path != null) {
            // this creature is considered nearby for the purposes of this power
            onMessage("You sense one or more creatures nearby")
            return
        }
    }

    onMessage("You sense no creatures nearby.")
}

