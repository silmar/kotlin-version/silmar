package silmar.contexts.gameplay.entities.beings.talkerBeings.types

import silmar.Names

/**
 * The flyweight game-specific talker-being types.
 */
object SilmarTalkerBeingTypes {
    val storekeeper = createSilmarTalkerBeingType("storekeeper", "storekeeper", 0, 1,
        "\"Welcome to my little general store. My selection is limited and the prices are high, but that's because I'm having to risk my life just to sell here.  What would you like to purchase?\"")
    val priest = createSilmarTalkerBeingType("priest", "priest", 0, 1,
        "\"I am the priest known as Ramulon. Years ago, " + Names.archEnemy + " destroyed the town where my mission was based. I now aid any who would stand against him. I provide my services for cost of materials only. How may I help you, friend?\"")
    val porter = createSilmarTalkerBeingType("porter", "porter", 0, 1,
        "This is your hireling who is responsible for arranging transport of the items and gold you give him back to your devastated homelands. \"Greetings! I hope your quest goes well. What shall I transport for you?\"")
    val chamberlain = createSilmarTalkerBeingType("chamberlain", "chamberlain", 0, 1,
        "\"I am Grundle, chamberlain of " + Names.dwarvenKing + ". I am authorized by His Majesty to purchase items of value from you for possession by the kingdom.  What do you wish to sell?\"")
    val blacksmith = createSilmarTalkerBeingType("blacksmith", "blacksmith", 0, 1,
        "This person appears to be a blacksmith. \"I lost my whole family to that fiend " + Names.archEnemy + "...  If you're going after him, I'll gladly repair any normal weapons or armor you have, real cheap!\"")
    val nightdwarf = createSilmarTalkerBeingType("nightdwarf", "nightdwarf", 0, 1,
        "This fellow is a nightdwarf. \"Pay me well, and I will fix your weapons and armor, even those that are magical!\"")
    val demonicWizard =
        createSilmarTalkerBeingType("demonic wizard", "demonicWizard", 0, 1,
            "This creature appears to be a demonic wizard. \"For a fee, I can restore and recharge your noncombative magical items. Otherwise, leave me to my work!\"")
    val sage = createSilmarTalkerBeingType("sage", "sage", 0, 1,
        "This gnarled old man is so frail that you wonder how he made the trip here. \"Through experience and magic I am able to identify most of the items that exist in these parts. Have you any you want me to look at?\"")
    val alienBuyer = createSilmarTalkerBeingType("alien buyer", "alienBuyer", 1, 1,
        "\"Greetings!  I am from the City of Gold on the Plane of Metal. I have traveled here to purchase an alien artifact to bring back to my world.  What one item will you sell to me?\"")
    val medicalRobot =
        createSilmarTalkerBeingType("medical robot", "medicalRobot", 1, 1, "")
}