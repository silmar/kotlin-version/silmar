package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.game.Game

/**
 * Checks to see if it's time for this player to healed due to the
 * barbarian's quick-healing power.
 */
fun SilmarPlayer.checkForBarbarianQuickHealing() {
    // if this player is a barbarian
    val game = Game.currentGame
    if (hasGainedLevel(PlayerLevels.barbarian)) {
        // once every so many turns
        if (game!!.turn % 3 == 0) {
            // if this player is hurt
            if (isDamaged) {
                // heal one hit-point
                addHitPoints(1)
            }
        }
    }
}

