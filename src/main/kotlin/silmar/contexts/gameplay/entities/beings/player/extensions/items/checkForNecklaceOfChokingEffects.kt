package silmar.contexts.gameplay.entities.beings.player.extensions.items

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms

/**
 * Checks to see if this player is being affected by a necklace of choking.
 */
fun SilmarPlayer.checkForNecklaceOfChokingEffects() {
    // if this player is wearing a necklace of choking
    if (items.isItemOfTypeEquipped(SilmarMagicItemTypes.necklaceOfChoking.itemType)) {
        onMessage("CHOKING!")
        takeDamage(Damage(1, DamageForms.choking))
    }
}

