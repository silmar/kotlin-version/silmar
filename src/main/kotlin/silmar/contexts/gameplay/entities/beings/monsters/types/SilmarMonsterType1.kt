package silmar.contexts.gameplay.entities.beings.monsters.types

import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationTypes
import silmar.contexts.gameplay.areas.AreaLevel
import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.contexts.gameplay.entities.beings.Alignment
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.monsters.types.MonsterType1
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.sounds.Sound

class SilmarMonsterType1(
    frequency: Int, name: String, imageName: String?, powerRating: Int,
    alignment: Alignment, experienceValue: Int, movementPoints: Int, defenseNormal: Int,
    defenseFirearm: Int, defenseLaser: Int, isLivingDead: Boolean,
    meleeAttacks: Array<MonsterAttack>?, rangedAttacks: Array<MonsterAttack>?,
    rangedAttackFrequency: Int, override val validLevels: Array<AreaLevel>?,
    movementType: MovementType, weaponPlusNeededToHit: Int, avoidanceModifier: Int,
    isLessVulnerableToEdgedWeapons: Boolean, sound: Sound?, soundFrequency: Float,
    isStationary: Boolean, mustSeePlayerToUsePower: Boolean, lightRadiusInTiles: Int?
) : SilmarMonsterType,
    MonsterType1(frequency, name, imageName, powerRating, alignment, experienceValue,
        movementPoints, defenseNormal, isLivingDead, meleeAttacks, rangedAttacks,
        rangedAttackFrequency, movementType, weaponPlusNeededToHit, avoidanceModifier,
        isLessVulnerableToEdgedWeapons, sound, soundFrequency, isStationary,
        mustSeePlayerToUsePower, lightRadiusInTiles) {
    override val defenses: IntArray = IntArray(3)

    init {
        defenses[PenetrationTypes.normal.rank] = defenseNormal
        defenses[PenetrationTypes.firearm.rank] = defenseFirearm
        defenses[PenetrationTypes.laser.rank] = defenseLaser
    }

    override fun isValidForArea(area: Area): Boolean {
        return validLevels?.any { it == (area as SilmarArea).level } ?: false
    }
}