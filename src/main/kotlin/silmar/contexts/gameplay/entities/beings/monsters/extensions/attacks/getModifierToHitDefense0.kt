package silmar.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster

fun Monster.getModifierToHitDefense0(): Int = when (type) {
    SilmarMonsterTypes.darkKnight -> 3
    else -> 0
}