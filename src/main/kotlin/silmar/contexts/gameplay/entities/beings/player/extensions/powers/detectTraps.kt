package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRange
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt

/**
 * Has this player attempt to detect any nearby traps.
 *
 * @return  Whether any traps were found.
 */
fun SilmarPlayer.detectTraps(): Boolean {
    // if this player is not a thief
    if (!hasGainedLevel(PlayerLevels.thief)) return false

    // for each trap near this player
    val traps =
        area.traps.getEntitiesInRange(location, PixelDistance(3 * tileWidth / 2))
    var trapsFound = false
    traps.filterNot { it.isDetected }.forEach { trap ->
        // if this player makes its detect check
        if (randomInt(1, 20) <= (attributeValues.intelligence + attributeValues.agility) / 4) {
            // inform the trap it's been detected
            trap.onDetected()

            onMessage("You detect a trap!")

            trapsFound = true
        }
    }

    return trapsFound
}

