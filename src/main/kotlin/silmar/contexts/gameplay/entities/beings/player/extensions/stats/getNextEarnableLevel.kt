package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerClass
import silmar.contexts.gameplay.entities.beings.player.PlayerLevel
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer

/**
 * Returns the next player-level earnable within the given player-class.
 */
fun SilmarPlayer.getNextEarnableLevel(playerClass: PlayerClass): PlayerLevel? {
    // for each level in this player's experience-level array, starting from the last
    // one received
    silmarLevelValues.levels.reversed().forEach { level ->
        // if this level belongs to the given class
        val levels = playerClass.levels
        val index = levels.indexOf(level)
        if (index >= 0) {
            // if this level is the last one in this class, no further level is
            // available; otherwise, return the next level in the given class that's
            // after this one
            return if (index == levels.size - 1) null else levels[index + 1]
        }
    }

    // return the first level in the given class
    return playerClass.levels[0]
}

