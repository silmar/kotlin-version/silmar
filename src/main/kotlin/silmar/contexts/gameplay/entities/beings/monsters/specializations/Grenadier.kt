package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performFireballEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.LightSourceMonster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt

class Grenadier : LightSourceMonster() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if it's time to hurl a grenade (which we treat like a
        // low-power fireball), and the player is not too far away or too close
        val minRange = PixelDistance(2 * tileWidth)
        val maxRange = PixelDistance(5 * tileWidth)
        val range = getDistance(location, player.location)
        if (randomInt(1, 5) < 4 && range.distance >= minRange.distance && range.distance <= maxRange.distance) {
            area.performFireballEffect(location, player.location, 4)
            return true
        }

        return false
    }
}