package silmar.contexts.gameplay.entities.beings.player.extensions.items

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player

fun Player.updateOnItemEquipped() {
    this as SilmarPlayer
    silmarStatuses.updateIsStealthy()
}