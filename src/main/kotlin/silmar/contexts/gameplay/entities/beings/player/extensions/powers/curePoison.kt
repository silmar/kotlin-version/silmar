package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onCurePoisonReceived
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Has this player perform a cure-poison power on itself.
 */
fun Player.curePoison() {
    performPowerDischargeEffect(this)

    onCurePoisonReceived()
}

