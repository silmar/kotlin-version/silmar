package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.areas.extensions.effects.performBoulderThrowEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Has this player throw a boulder at the given to-pixel-location.
 */
fun Player.throwBoulder(to: PixelPoint) {
    val from = location
    val adjustedTo = if (isConfused) modifyLocationDueToConfusion(area, to, from) else to
    val terrains = area.terrains.getEntitiesInRectangle(from, size)
    val boulder = terrains.firstOrNull { it.isOfType(SilmarTerrainTypes.boulder) }
    if (boulder != null) area.performBoulderThrowEffect(boulder, from, adjustedTo,
        attributeValues.strength, attributeValues.agility)
}

