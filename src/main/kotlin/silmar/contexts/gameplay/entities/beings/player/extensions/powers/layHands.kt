package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.util.math.randomInt

/**
 * Has this player lay hands on himself for healing purposes.
 */
fun SilmarPlayer.layHands() {
    // if this player hasn't been damaged since the last time it employed this power
    val statuses = silmarStatuses
    if (!statuses.damagedSinceLayHands) {
        // don't allow the power use
        onMessage("No effect")
        return
    }

    performPowerDischargeEffect(this)

    takeHealing(randomInt(1, levelValues.level))

    statuses.onLayHandsUse()
}

