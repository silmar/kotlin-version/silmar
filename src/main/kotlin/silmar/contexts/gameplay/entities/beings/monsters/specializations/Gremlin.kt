package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1

class Gremlin : Monster1() {
    /**
     * Informs this gremlin that it has managed to swipe an item.
     */
    fun onItemSwiped() {
        // this gremlin teleports away for good
        area.performTeleportEffect(location, true, false)
        area.removeEntity(this)
    }
}