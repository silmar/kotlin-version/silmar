package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.util.math.randomInt

class Goblin : Monster1() {
    init {
        maxHitPoints = randomInt(1, 5)
    }
}