package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.game.Game
import silmarengine.isDebugOn

/**
 * Checks to see if it's time for this player to recharge a clerical point.
 */
fun SilmarPlayer.checkForClericalPointRecharge() {
    // if this player is a cleric
    if (hasGainedLevel(PlayerLevels.cleric)) {
        // if it's time to recharge a clerical point
        val clericalRechargeInterval = 5
        val game = Game.currentGame
        if (game!!.turn % clericalRechargeInterval == 0 || isDebugOn) {
            silmarStats.rechargeClericalPoint()
        }
    }
}

