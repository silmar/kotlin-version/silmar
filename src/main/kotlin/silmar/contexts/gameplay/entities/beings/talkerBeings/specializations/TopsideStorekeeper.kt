package silmar.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.ItemSeller
import silmarengine.contexts.gameplay.entities.items.types.*
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.contexts.gameplay.entities.talkers.extensions.createItemsForSaleFromTypes

class TopsideStorekeeper : ItemSeller() {
    override val itemsForSale = createItemsForSaleFromTypes(itemTypesForSale)

    companion object {
        /**
         * The types of items sold by this storekeeper.
         */
        private val itemTypesForSale: Array<ItemType> =
            arrayOf(GroupableItemTypes.ration, NonGroupableItemTypes.torch,
                AmmoTypes.arrow, AmmoTypes.quarrel, AmmoTypes.slingStone,
                WeaponTypes.dagger.itemType, WeaponTypes.club.itemType,
                WeaponTypes.mace.itemType, WeaponTypes.greatMace.itemType,
                WeaponTypes.staff.itemType, WeaponTypes.shortSword.itemType,
                WeaponTypes.longSword.itemType, WeaponTypes.twoHandedSword.itemType,
                WeaponTypes.sling.itemType, WeaponTypes.bow.itemType,
                WeaponTypes.crossbow.itemType, ArmorTypes.leatherArmor,
                ArmorTypes.chainMail, ArmorTypes.plateMail, ShieldTypes.smallShield,
                ShieldTypes.largeShield, ShieldTypes.greatShield)
    }
}