package silmar.contexts.gameplay.entities.beings.player

interface SilmarPlayerLevelValues {
    /**
     * A list of the player-levels this player has gained.  The levels in this list
     * are in the order in which they were achieved, so that level draining will
     * remove them in the reverse order.
     */
    val levels: List<PlayerLevel>

    val levelsRetained: List<PlayerLevel>

    /**
     * Informs this player that the given level has been chosen as the one just gained
     * by this player.
     */
    fun onPlayerLevelChosen(level: PlayerLevel)
}