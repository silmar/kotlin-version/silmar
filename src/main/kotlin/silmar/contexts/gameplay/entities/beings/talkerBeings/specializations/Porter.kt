package silmar.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing1
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.ItemDonationReceiver

class Porter : TalkerBeing1(), ItemDonationReceiver