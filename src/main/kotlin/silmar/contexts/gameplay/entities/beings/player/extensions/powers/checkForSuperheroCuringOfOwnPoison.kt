package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onCurePoisonReceived
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel

fun SilmarPlayer.checkForSuperheroCuringOfOwnPoison() {
    // if this player is poisoned
    if (statuses.isPoisoned) {
        // if this player is now a superhero
        if (hasGainedLevel(PlayerLevels.superhero)) {
            // cure this player's poisons
            while (statuses.isPoisoned) onCurePoisonReceived()
            return
        }
    }
}