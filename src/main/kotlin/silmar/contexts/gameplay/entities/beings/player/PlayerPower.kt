package silmar.contexts.gameplay.entities.beings.player

import silmar.contexts.gameplay.entities.beings.player.PlayerPowers.conjureDeathFog
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers.emitLightning
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers.fireEyeBeams
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers.freezeBeing
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers.hurlFireball
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers.leap
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers.placeVengefulSymbol
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers.throwBoulder
import silmar.gui.LocationSelections
import silmar.contexts.playerpowerselection.domain.model.PPSPower
import silmarengine.gui.LocationSelection
import silmarengine.util.html.breakTextAtSpaces
import java.util.*

/**
 * A special ability a player may possess.
 */
class PlayerPower(
    /**
     * The player-level that grants this power.
     */
    override val level: PlayerLevel,
    /**
     * A descriptive name of this power.
     */
    override val name: String,
    /**
     * Whether this power is explicity activated by a player (vs. being used passively).
     */
    override val isActiveUse: Boolean, pointCost: Int, info: String
) : PPSPower {
    /**
     * How many magic points this power requires (and drains), if any.
     */
    override val magicPointCost = if (PlayerClasses.magicUser.contains(level)) pointCost else 0

    /**
     * How many clerical points this power requires (and drains), if any.
     */
    override val clericalPointCost = if (PlayerClasses.cleric.contains(level)) pointCost else 0

    /**
     * A description of this power meant for the user.
     */
    val info = breakTextAtSpaces(info, 50)

    /**
     * The tooltip text to display for this power.
     */
    override val tooltipText: String

    /**
     * Returns whether this power is a magic-user spell that must be cast.
     */
    val isMagicUserSpell: Boolean
        get() = PlayerClasses.magicUser.contains(level)

    /**
     * Returns whether this power is a clerical spell that must be cast.
     */
    val isClericalSpell: Boolean
        get() = PlayerClasses.cleric.contains(level)

    /**
     * Returns a location-selection if one needs to be performed when utilizing this power.
     */
    val locationSelection: LocationSelection?
        get() = when (this) {
            fireEyeBeams -> LocationSelections.fireEyeBeams
            leap -> LocationSelections.leap
            emitLightning -> LocationSelections.emitLightning
            conjureDeathFog -> LocationSelections.conjureDeathFog
            placeVengefulSymbol -> LocationSelections.placeVengefulSymbol
            throwBoulder -> LocationSelections.throwBoulder
            freezeBeing -> LocationSelections.freezeBeing
            hurlFireball -> LocationSelections.hurlFireball
            else -> null
        }

    init {
        powers.add(this)
        powersMap[name] = this

        tooltipText = "<B>$name</B><br>$info"
    }

    override fun equals(other: Any?): Boolean {
        return other === this || (other is PlayerPower && other.name == name)
    }

    override fun toString(): String {
        return name
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    companion object {
        /**
         * A list of all the powers.
         */
        var powers = ArrayList<PlayerPower>()

        /**
         * A mapping of all the powers by their names.
         */
        var powersMap = HashMap<String, PlayerPower>()
    }
}
