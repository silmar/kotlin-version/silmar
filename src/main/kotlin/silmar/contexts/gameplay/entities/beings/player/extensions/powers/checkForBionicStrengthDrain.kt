package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.updateAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage

/**
 * Checks to see if this player is employing bionic strength, and if the number of
 * turns left on it should be decremented.
 */
fun SilmarPlayer.checkForBionicStrengthDrain() {
    val statuses = silmarStatuses
    if (statuses.hasBionicStrength) {
        statuses.drainBionicStrength()

        if (!statuses.hasBionicStrength) {
            updateAttributes()
            onMessage("Your bionic strength has ended.")
        }
    }
}

