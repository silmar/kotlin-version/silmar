package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performDragonBreathEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.damage.extensions.affectForm
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.util.math.randomInt

class Dragonman : Monster1() {
    override fun affectDamage(damage: Damage) {
        super.affectDamage(damage)
        damage.affectForm(DamageForms.fire, 0f)
    }

    override fun usePreMovementPower(player: Player): Boolean {
        // if this dragon-man is close enough to a player, and it's time to breath
        val maxRange = TileDistance(4)
        if (isDistanceAtMost(location, player.location,
                PixelDistance(maxRange)) && randomInt(1, 5) <= 2) {
            area.performDragonBreathEffect(location, player.location, DamageForms.fire,
                maxRange, powerRating, hitPoints, this)
            return true
        }

        return false
    }
}