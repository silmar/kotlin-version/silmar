package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.entities.damage.Damage

fun SilmarPlayer.affectDamageAsSilmarPlayer(damage: Damage) {
    // if this player is a kangadillo
    if (hasGainedLevel(PlayerLevels.kangadillo)) {
        // if the damage is of a form that is lessened if the victim has a tough exterior
        if (damage.form.isLessenedByToughExterior) {
            // halve, or almost halve, the amount
            damage.amount = damage.amount / 2 + damage.amount % 2
        }
    }
}