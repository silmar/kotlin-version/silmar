package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmar.contexts.gameplay.entities.beings.extensions.location.leap
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.actions.move
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector

/**
 * Has this player leap to the given pixel-location, if that location is determined
 * to be allowable.
 *
 * @return   Whether the leap was allowed.
 */
fun Player.leap(location: PixelPoint): Boolean {
    val adjustedLocation =
        if (isConfused) modifyLocationDueToConfusion(area, location, this.location)
        else location

    // attempt the leap
    val direction = getUnitVector(this.location, adjustedLocation)
    val distance = getDistance(this.location, adjustedLocation)
    val allowed = leap(adjustedLocation, attributeValues.strength, attributeValues.agility)

    // if the leap was allowed
    if (allowed) {
        // have this player move a short distance in the leap direction
        // from its leap destination, to model inertia, and also so
        // that it will likely encounter any terrains where it lands
        val stopLocation = MutablePixelPoint(this.location)
        val inertiaMagnitude = distance.distance / 4
        stopLocation.translate((inertiaMagnitude * direction.x).toInt(),
            (inertiaMagnitude * direction.y).toInt())
        move(stopLocation)
    }

    return allowed
}

