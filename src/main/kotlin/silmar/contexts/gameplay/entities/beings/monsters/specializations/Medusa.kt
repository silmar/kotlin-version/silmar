package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.entities.beings.player.extensions.harm.onMedusaDeath
import silmar.contexts.gameplay.entities.beings.player.extensions.harm.onMedusaGaze
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.util.math.randomInt

class Medusa : Monster1() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if it's time to do a gaze attack
        if (randomInt(1, 5) <= 2) {
            // if the player isn't already paralyzed
            if (!player.isParalyzed) {
                // do a power discharge
                area.performPowerDischargeEffect(location)

                // inform the player of this monster's gaze
                player.onMedusaGaze(powerRating)

                return true
            }
        }

        return false
    }

    override fun die(damage: Damage) {
        // inform the player of this monster's death
        area.player?.onMedusaDeath()

        super.die(damage)
    }
}