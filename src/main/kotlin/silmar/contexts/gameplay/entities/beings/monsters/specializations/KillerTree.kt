package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.damage.extensions.affectForm

class KillerTree : Monster1() {
    override fun affectDamage(damage: Damage) {
        super.affectDamage(damage)
        damage.affectForm(DamageForms.fire, 2f)
    }
}