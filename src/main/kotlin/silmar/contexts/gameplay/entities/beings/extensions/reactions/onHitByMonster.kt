package silmar.contexts.gameplay.entities.beings.extensions.reactions

import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeParalyzed
import silmarengine.contexts.gameplay.entities.beings.extensions.location.teleportRandomlyWithinArea
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.util.math.randomInt
import silmarengine.util.math.randomBoolean
import silmarengine.util.sleepThread

typealias Attacks = SilmarMonsterAttacks

fun Being.onHitByMonster_PreDamage(
    monster: Monster, attack: MonsterAttack, @Suppress("UNUSED_PARAMETER")
    damageAmount: Int, hitBy: Int
): Boolean {
    when (attack) {
        Attacks.guardianSwordSlash ->
            // if the hit was by a certain amount
            if (hitBy >= 8) {
                // this being is decapitated; pause for a bit so the user can
                // see the message before the being dies
                area.player?.receiveMessageIfInLOSOf(location, "DECAPITATED!!")
                sleepThread(1000)
                takeDamage(Damage(hitPoints, DamageForms.decapitation))
                return true
            }
        Attacks.giantWormBite ->
            // if the hit was by a certain amount
            if (hitBy >= 8) {
                sufferBeingSwallowedWhole(monster)
                return true
            }
    }

    return false
}

fun Being.onHitByMonster_PostDamage(
    @Suppress("UNUSED_PARAMETER") monster: Monster, attack: MonsterAttack, @Suppress("UNUSED_PARAMETER")
    damageAmount: Int, hitBy: Int
) {
    when (attack) {
        Attacks.transparentGelatinoidAcid, Attacks.giantCentipedeBite -> {
            // if the attack isn't avoided
            val result = avoid(hitBy)
            if (!result.avoided) {
                becomeParalyzed(1)
            }
        }
        Attacks.ghoulClaw -> {
            // if the attack isn't avoided
            val result = avoid(hitBy)
            if (!result.avoided) {
                becomeParalyzed(randomInt(1, 5))
            }
        }
        Attacks.sentryborgStunRay -> {
            // if the attack isn't avoided
            val result = avoid(hitBy)
            if (!result.avoided) {
                becomeParalyzed(2)
            }
        }
        Attacks.pretegClaw -> {
            // if the attack isn't avoided
            val result = avoid(hitBy)
            if (!result.avoided) {
                teleportRandomlyWithinArea()
            }
        }
        Attacks.painkillerInjection ->
            // if it's time to do a heal
            if (randomBoolean) {
                takeHealing(randomInt(1, 12))
            }
        Attacks.livingDeadWizardTouch -> {
            // if the attack isn't avoided
            val result = avoid(hitBy + 8)
            if (!result.avoided) {
                becomeParalyzed(2)
            }
        }
    }
}

/**
 * Has this being be swallowed whole by the given swallower.
 */
fun Being.sufferBeingSwallowedWhole(swallower: Being) {
    // issue a gulp sound, and pause a bit, so it can be heard by the user
    area.onSoundIssued(SilmarSounds.gulp, location)
    sleepThread(200)

    // move this being to the swallower's location, to give the appearance of it
    // being swallowed
    area.moveEntity(this, swallower.location)
    sleepThread(50)

    // inform the player if it's in los; pause for a bit so the user can see the message
    // before the being dies
    area.player?.receiveMessageIfInLOSOf(location, "SWALLOWED WHOLE!")
    sleepThread(1000)

    // this being is killed
    takeDamage(Damage(hitPoints, DamageForms.swallow))
}

