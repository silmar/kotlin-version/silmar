package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmar.contexts.gameplay.entities.items.types.SilmarTechItemTypes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage

/**
 * Informs this player that it has been exposed to a dose of radiation.
 */
fun SilmarPlayer.onRadiationExposure() {
    if (hasGainedLevel(PlayerLevels.neonKnight)) return

    if (items.isItemOfTypeEquipped(SilmarTechItemTypes.leadLinedCloak.itemType)) return

    // if this player has been exposed enough times
    val statuses = silmarStatuses
    statuses.incrementRadiationExposure()
    if (statuses.isOverSafeRadiationExposureLimit) {
        // this player experiences radiation sickness
        statuses.resetRadiationExposure()
        statuses.incrementRadiationDamage()
        updateMaxHitPoints()
        onMessage("RADIATION SICKNESS!")
    }
}

