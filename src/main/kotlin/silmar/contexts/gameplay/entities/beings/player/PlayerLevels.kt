package silmar.contexts.gameplay.entities.beings.player

object PlayerLevels {
    /**
     * The flyweight warrior levels.
     */
    var squire = PlayerLevel("Squire", "acolyte")
    var fighter = PlayerLevel("Fighter", "human")
    var monk = PlayerLevel("Monk", "monk")
    var paladin = PlayerLevel("Paladin", "paladin")
    var archer = PlayerLevel("Archer", "archer")
    var superhero = PlayerLevel("Superhero", "superhero")
    var neonKnight = PlayerLevel("Neon Knight", "neonKnight")

    /**
     * The flyweight thief levels.
     */
    var barbarian = PlayerLevel("Barbarian", "barbarian")
    var thief = PlayerLevel("Thief", "thief")
    var ninja = PlayerLevel("Ninja", "ninja")

    /**
     * The flyweight monster-1 levels.
     */
    var werewolf = PlayerLevel("Werewolf", "werewolf")
    var troll = PlayerLevel("Troll", "troll")
    var pixie = PlayerLevel("Pixie", "pixie")
    var giant = PlayerLevel("Giant", "giant")
    var demon = PlayerLevel("Demon", "demon")

    /**
     * The flyweight monster-2 levels.
     */
    var minotaur = PlayerLevel("Minotaur", "minotaur")
    var mutatedSpider = PlayerLevel("Mutated Spider", "mutatedSpider")
    var kangadillo = PlayerLevel("Kandadillo", "kangadillo")
    var subvampire = PlayerLevel("Subvampire", "subvampire")
    var cyborg = PlayerLevel("Cyborg", "cyborg")

    /**
     * The flyweight cleric levels.
     */
    var cleric = PlayerLevel("Cleric", "cleric")
    var druid = PlayerLevel("Druid", "druid")
    var priest = PlayerLevel("Priest", "priest")
    var deathCaster = PlayerLevel("Death Caster", "deathCaster")

    /**
     * The flyweight magic-user levels.
     */
    var wizard = PlayerLevel("Wizard", "wizard")
    var alchemist = PlayerLevel("Alchemist", "alchemist")
    var sorcerer = PlayerLevel("Sorcerer", "sorcerer")
    var battlemage = PlayerLevel("Battlemage", "battlemage")
    var scientist = PlayerLevel("Scientist", "scientist")

    /**
     * The powers for the warrior levels.
     * Note: These must be defined after all levels have been defined to avoid mutually-dependent
     * static definitions.
     */
    init {
        squire.powers = arrayOf()
        fighter.powers = arrayOf(PlayerPowers.combatProwess)
        monk.powers = arrayOf(PlayerPowers.unarmedCombatProwess,
            PlayerPowers.unarmoredDefensiveProwess)
        paladin.powers = arrayOf(PlayerPowers.immunityToDisease, PlayerPowers.layHands,
            PlayerPowers.protectionAgainstEvil, PlayerPowers.repelLivingDeadPaladin)
        archer.powers = arrayOf(PlayerPowers.archerySkill)
        superhero.powers =
            arrayOf(PlayerPowers.immunityToPoison, PlayerPowers.fireEyeBeams,
                PlayerPowers.employSuperVision)
        neonKnight.powers =
            arrayOf(PlayerPowers.immunityToRadiation, PlayerPowers.emitRadWave)
    }

    /**
     * The powers for the thief levels.
     * Note: These must be defined after all levels have been defined to avoid mutually-dependent
     * static definitions.
     */
    init {
        barbarian.powers =
            arrayOf(PlayerPowers.hunterSpeed, PlayerPowers.fastRecuperation)
        thief.powers = arrayOf(PlayerPowers.detectTraps, PlayerPowers.disarmTraps)
        ninja.powers = arrayOf(PlayerPowers.stealth)
    }

    /**
     * The powers for the monster-1 levels.
     * Note: These must be defined after all levels have been defined to avoid mutually-dependent
     * static definitions.
     */
    init {
        werewolf.powers = arrayOf(PlayerPowers.nightVision, PlayerPowers.claws,
            PlayerPowers.invulnerabilityToLesserCreatures)
        troll.powers = arrayOf(PlayerPowers.regeneration)
        pixie.powers = arrayOf(PlayerPowers.teleportingTouch)
        giant.powers = arrayOf(PlayerPowers.giantStrength, PlayerPowers.throwBoulder)
        demon.powers = arrayOf(PlayerPowers.disintegratingTouch)
    }

    /**
     * The powers for the monster-2 levels.
     * Note: These must be defined after all levels have been defined to avoid mutually-dependent
     * static definitions.
     */
    init {
        minotaur.powers = arrayOf(PlayerPowers.horns, PlayerPowers.senseCreatures,
            PlayerPowers.senseSecretDoors, PlayerPowers.immunityToConfusion)
        mutatedSpider.powers = arrayOf(PlayerPowers.spinWeb)
        kangadillo.powers = arrayOf(PlayerPowers.leap, PlayerPowers.toughExterior)
        subvampire.powers = arrayOf(PlayerPowers.invulnerabilityToAverageCreatures,
            PlayerPowers.immunityToParalyzation, PlayerPowers.lifeDraining)
        cyborg.powers = arrayOf(PlayerPowers.cyberneticAccuracy)
    }

    /**
     * The powers for the cleric levels.
     * Note: These must be defined after all levels have been defined to avoid mutually-dependent
     * static definitions.
     */
    init {
        cleric.powers = arrayOf(PlayerPowers.healWounds, PlayerPowers.curePoison,
            PlayerPowers.repelLivingDeadCleric, PlayerPowers.cureDisease)
        druid.powers = arrayOf(PlayerPowers.emitLightning)
        priest.powers = arrayOf(PlayerPowers.healFully, PlayerPowers.revealTraps,
            PlayerPowers.placeVengefulSymbol)
        deathCaster.powers = arrayOf(PlayerPowers.conjureDeathFog)
    }

    /**
     * The powers for the magic-user levels.
     * Note: These must be defined after all levels have been defined to avoid mutually-dependent
     * static definitions.
     */
    init {
        wizard.powers = arrayOf(PlayerPowers.teleportSelf, PlayerPowers.memorizeLocation,
            PlayerPowers.hurlFireball, PlayerPowers.findExit)
        alchemist.powers =
            arrayOf(PlayerPowers.transmuteGoldToBars, PlayerPowers.transmuteBarsToGold)
        sorcerer.powers =
            arrayOf(PlayerPowers.findTreasure, PlayerPowers.projectRetributiveShield)
        battlemage.powers = arrayOf(PlayerPowers.freezeBeing, PlayerPowers.quickenSelf)
        scientist.powers =
            arrayOf(PlayerPowers.achieveBionicStrength, PlayerPowers.summonMeteorShower)
    }
}