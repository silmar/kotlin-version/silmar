package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.Names
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.areas.extensions.effects.performConfusionEffect
import silmar.contexts.gameplay.areas.extensions.effects.performMeteorShowerEffect
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.damage.extensions.affectForm
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt
import silmarengine.util.sleepThread

open class LivingDeadWizard : Monster1() {

    /**
     * Whether this wizard has already presented its greeting message to the players.
     */
    private var greetedPlayers = false

    /**
     * The first game turn in which this wizard acted.
     */
    private var firstTurnToAct = 0

    override fun act() {
        // if this monster is actually a livingDeadWizard2
        if (type == SilmarMonsterTypes.livingDeadWizard2) {
            // this method doesn't apply to it
            super.act()
            return
        }

        // record whether or not this is this monster's first turn to act
        if (firstTurnToAct == 0) firstTurnToAct = Game.currentGame!!.turn

        // if this wizard hasn't yet presented its greeting
        if (!greetedPlayers) {
            // greet the player if it's in LOS
            val pause1 = 3000
            area.player?.receiveMessageIfInLOSOf(location,
                Names.archEnemy + ": \"Meddlesome adventurer...", duration = pause1)
            sleepThread(pause1.toLong())
            val pause2 = 4000
            area.player?.receiveMessageIfInLOSOf(location,
                "Your invasion into my sanctuary ends here!\"", duration = pause2)
            sleepThread(pause2.toLong())
            greetedPlayers = true
        }

        super.act()
    }

    override fun die(damage: Damage) {
        // if this monster is actually a living-dead-wizard-2
        if (type == SilmarMonsterTypes.livingDeadWizard2) {
            // this method doesn't apply to it
            super.die(damage)
            return
        }

        // if there is a throne in the area
        val throne = area.getTerrainOfType(TerrainTypes.throne)
        if (throne != null) {
            // place a down-exit next to the throne
            val terrain = createTerrain(SilmarTerrainTypes.downExit)
            val throneLocation = throne.location
            area.addEntity(terrain,
                PixelPoint(throneLocation.x + tileWidth, throneLocation.y))
        }

        // yell at the player if it's in LOS
        val message =
            Names.archEnemy + " screams in rage, \"You will yet perish by my wrath!\""
        area.player?.receiveMessageIfInLOSOf(location, message)

        // sleep for a bit, so that the above message fully precedes this wizard's poof
        sleepThread(3000)

        // remember this value, as super.doDie will change it below
        val area = this.area

        super.die(damage)

        // appear to teleport away
        area.performTeleportEffect(location, true, false)
        area.performTeleportEffect(location, false, false)
    }

    override fun affectDamage(damage: Damage) {
        super.affectDamage(damage)

        damage.affectForm(DamageForms.electricity, 0f)
        damage.affectForm(DamageForms.deathFog, 0.25f)
    }

    override fun usePreMovementPower(player: Player): Boolean {
        // if the given player isn't already confused, and it's time to cast
        // mass confusion (where we want this wizard to cast it on
        // its first turn to act)
        if (!player.isConfused && (Game.currentGame!!.turn == firstTurnToAct || randomInt(
                1, 3) == 1)) {
            area.performConfusionEffect(location, powerRating, false)
            return true
        }

        // if it's time to summon a meteor shower
        if (randomInt(1, 3) == 1) {
            // if the given player is far enough away from this wizard that there's little
            // chance of a meteor hitting this wizard
            if (getDistance(location, player.location).distance >= 2 * tileWidth) {
                // do a meteor shower targeting all players in los
                area.performPowerDischargeEffect(location)
                area.performMeteorShowerEffect(location, false, powerRating / 2)
                return true
            }
        }

        return false
    }
}