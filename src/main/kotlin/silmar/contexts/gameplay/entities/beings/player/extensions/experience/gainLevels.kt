package silmar.contexts.gameplay.entities.beings.player.extensions.experience

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayerListener
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.startingPlayerLevel
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds

fun Player.gainLevels(toLevel: Int) {
    // while this player has less levels in its levels list than it should for
    // the new value given (taking into account the beginning level this player
    // starts with and cannot lose)
    this as SilmarPlayer
    while (silmarLevelValues.levels.size < toLevel - startingPlayerLevel + 1) {
        // if this is the first level gained by this player
        if (toLevel - startingPlayerLevel == 1) {
            // choose the wizard level automatically for this player, since without
            // its teleport power, the game is practically unplayable
            silmarLevelValues.onPlayerLevelChosen(PlayerLevels.wizard)
        }
        // otherwise, a later level is being gained
        else {
            // issue a level-choice sound
            area.onSoundIssued(Sounds.levelChoice, location)

            // have the user choose the next level gained by this player
            reportToListeners { (it as SilmarPlayerListener).onMustChooseLevelGained() }
        }
    }
}