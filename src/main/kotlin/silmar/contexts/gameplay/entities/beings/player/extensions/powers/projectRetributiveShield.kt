package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.magicUserCastingLevel
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.areas.effects.LightSourceVisualEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.util.sleepThread

/**
 * Has this player project a retributive shield around itself.
 */
fun SilmarPlayer.projectRetributiveShield() {
    performPowerDischargeEffect(this)

    // issue an accompanying sound
    area.onSoundIssued(SilmarSounds.retributiveShieldProjection, location)

    // add a projection effect at the given location
    val effect = LightSourceVisualEffect(null, PixelDistance(TileDistance(4)))
    area.addEntity(effect, location)

    // for each step in the effect
    for (i in 1..5) {
        // perform this step
        effect.imageName = "retributiveShieldProjection$i"

        // pause a bit for this step to be seen
        sleepThread(200)
    }

    // remove the visual effect from the area
    area.removeEntity(effect)

    // start the shield's duration
    val duration = magicUserCastingLevel * 5
    silmarStatuses.onRetributiveShieldRaised(duration)
}

