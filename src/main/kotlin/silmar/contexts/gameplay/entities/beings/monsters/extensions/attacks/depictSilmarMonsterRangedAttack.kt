package silmar.contexts.gameplay.entities.beings.monsters.extensions.attacks

import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttacks
import silmar.contexts.gameplay.areas.extensions.effects.performLaserEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

private typealias Attacks = SilmarMonsterAttacks

fun depictSilmarMonsterRangedAttack(
    area: Area, from: PixelPoint, to: PixelPoint, attack: MonsterAttack
): Boolean = when (attack) {
    Attacks.sentryborgStunRay, Attacks.deathbotRay, Attacks.livingDeadWizard2VortexBeam -> {
        area.onSoundIssued(attack.attackType.sound, from)
        area.performLaserEffect(from, to, attack.attackType.imageName)
        true
    }
    else -> false
}
