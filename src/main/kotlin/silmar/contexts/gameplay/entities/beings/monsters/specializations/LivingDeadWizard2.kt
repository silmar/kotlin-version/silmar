package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.Names
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.areas.extensions.effects.performFireballEffect
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.sounds.Sounds
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.list.removeRandomElement
import silmarengine.util.sleepThread
import java.util.*

class LivingDeadWizard2 : LivingDeadWizard() {
    override fun die(damage: Damage) {
        // remember these, as super.die might change them
        val area = area
        val location = this.location

        // send the player the dying message
        area.player?.receiveMessageIfInLOSOf(location,
            Names.archEnemy + " screams, \"No... This cannot be!!!\"")

        // do explosions at and around the wizard
        val strength = 5
        val spot = MutablePixelPoint(location)
        val dx = tileWidth
        val dy = tileHeight
        spot.translate(-dx, -dy)
        area.performFireballEffect(null, spot, strength)
        spot.translate(2 * dx, 2 * dy)
        area.performFireballEffect(null, spot, strength)
        spot.translate(0, -2 * dy)
        area.performFireballEffect(null, spot, strength)
        spot.translate(-2 * dx, 2 * dy)
        area.performFireballEffect(null, location, strength)

        super.die(damage)

        // for each monster in los
        val monsters = ArrayList(area.getEntitiesInLOS(location, area.monsters))
        while (monsters.isNotEmpty()) {
            val monster = monsters.removeRandomElement()

            // while this monster is still alive
            while (!monster.isDead) {
                // do an explosion at this monster's location
                area.performFireballEffect(null, monster.location, strength)
            }
        }

        // for each terrain in los
        val terrains = ArrayList(area.getEntitiesInLOS(location, area.terrains))
        while (terrains.isNotEmpty()) {
            val terrain = terrains.removeRandomElement()

            // if this terrain is of one of a couple certain types
            if (terrain.isOfType(SilmarTerrainTypes.machinery2) || terrain.isOfType(
                    TerrainTypes.bones)) {
                // remove this terrain
                val terrainLocation = terrain.location
                area.removeEntity(terrain)

                // do an explosion where this terrain was
                area.performFireballEffect(null, terrainLocation, strength)
            }
        }

        // wait a bit for the last of the above explosions to subside, and to have a brief pause afterwards
        sleepThread(4000)

        Sounds.gameFinished.play()

        // wait a bit for the above sound to finish playing
        sleepThread(5000)

        // inform the user that the game has been finished
        val game = Game.currentGame!!
        game.player.onMessage("", MessageType.gameFinished)

        // the game is over
        game.stopProcessingTurns()
    }
}