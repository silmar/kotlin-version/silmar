package silmar.contexts.gameplay.entities.beings.extensions.reactions

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.util.sleepThread

/**
 * Informs this being it has been stuck in the given web for
 * the given amount of turns.
 */
fun Being.onStuckInWeb(web: Terrain, turnsStuck: Int) {
    // if this being hasn't yet stuck long enough
    val breaksOut = turnsStuck > (if (this is Player) (20 - attributeValues.strength)
    else (12 - powerRating)) / 2
    if (!breaksOut) {
        // it cannot move any more this turn
        endTurn()
        area.onSoundIssued(SilmarSounds.inWeb, location)
        area.player?.receiveMessageIfInLOSOf(location, "stuck in web")

        // if this being is the player, pause a bit so the user may better follow
        // what is happening
        if (this is Player) sleepThread(700)
    }
    else {
        // destroy the web
        area.removeEntity(web)
    }
}

