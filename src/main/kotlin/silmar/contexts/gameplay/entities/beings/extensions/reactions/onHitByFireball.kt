package silmar.contexts.gameplay.entities.beings.extensions.reactions

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.util.math.randomBellCurveInt

/**
 * Informs this being that it has been hit by a fireball
 * of the given strength.
 */
fun Being.onHitByFireball(strength: Int) {
    // determine the damage caused by the fireball
    var damage = randomBellCurveInt(1, 6, strength)

    // if this being avoids the blast
    val result = avoid(strength / 2)
    if (result.avoided) {
        // the damage is halved
        damage /= 2
    }

    // if this being didn't super-avoid the blast
    if (!result.superAvoided) {
        // take the damage, half from explosive power, and the other half from fire
        takeDamage(Damage(damage / 2, DamageForms.explosion))
        takeDamage(Damage(damage / 2, DamageForms.fire))
    }
}

