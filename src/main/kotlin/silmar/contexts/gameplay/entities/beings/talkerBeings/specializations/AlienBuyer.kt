package silmar.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.specializations.Buyer

class AlienBuyer : Buyer() {
    override val leavesAfterPurchase: Boolean
        get() = true

    override fun getAdjustedItemOffering(item: Item, normalOffering: Int): Int {
        return normalOffering * 2
    }

    override fun onItemBought() {
        // remember this value, as removeEntity() will change it below
        val area = area

        // teleport away for good
        val finalFrame = area.performTeleportEffect(location, true, true)
        area.removeEntity(this@AlienBuyer)
        area.removeEntity(finalFrame!!)
    }
}