package silmar.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.getDampenedStandardModifier

object SilmarPlayerAttributeModifierProvider {
    fun castingModifierForIntelligence(value: Int): Int = value - 10
    fun castingModifierForJudgement(value: Int): Int = value - 10

    fun magicPointsModifierForIntelligence(value: Int): Int =
        getDampenedStandardModifier(value)

    fun clericalPointsModifierForJudgement(value: Int): Int =
        getDampenedStandardModifier(value)

    fun magicPointsModifierForEndurance(value: Int): Int =
        getDampenedStandardModifier(value)

    fun clericalPointsModifierForEndurance(value: Int): Int =
        getDampenedStandardModifier(value)

    fun stealthAvoidancePenalty(agility: Int, intelligence: Int): Int =
        (agility + intelligence) / 4
}