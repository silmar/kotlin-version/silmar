package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performElectricalDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import kotlin.math.max

class MirroredArmor : Monster1() {
    override fun onDamaged(damage: Damage) {
        // if this monster is still alive
        if (hitPoints > 0) {
            // if there is a nearest player that this monster can see
            val player = area.player
            if (player?.isInLOSOf(location) == true) {
                // zap him, with a damage of up to that inflicted on this armor with this call, dependent
                // on how far away he is
                val distance = getDistance(location, player.location)
                val maxDistance = 10 * tileWidth
                area.performElectricalDischargeEffect(
                    location, player.location, powerRating, max(1,
                        (damage.amount * ((maxDistance - distance.distance) / maxDistance.toFloat())).toInt()))
            }
        }
    }
}