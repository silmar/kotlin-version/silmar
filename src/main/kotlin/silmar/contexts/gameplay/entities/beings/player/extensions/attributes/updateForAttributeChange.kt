package silmar.contexts.gameplay.entities.beings.player.extensions.attributes

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes

fun Player.updateOnAttributeChange(attribute: PlayerAttribute) {
    this as SilmarPlayer
    val stats = silmarStats
    when (attribute) {
        PlayerAttributes.intelligence -> stats.updateMaxMagicPoints()
        PlayerAttributes.judgement -> stats.updateMaxClericalPoints()
        PlayerAttributes.endurance -> {
            stats.updateMaxMagicPoints()
            stats.updateMaxClericalPoints()
        }
    }
}