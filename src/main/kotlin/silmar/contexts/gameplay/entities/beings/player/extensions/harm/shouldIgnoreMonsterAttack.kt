package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationTypes
import silmar.contexts.gameplay.entities.beings.monsters.SilmarMonsterAttack
import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.extensions.reactions.onAttackedAndMissed
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.MonsterAttack
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel

fun Player.shouldIgnoreMonsterAttack(monster: Monster, attack: MonsterAttack) : Boolean {
    // if this player is a werewolf or subvampire
    this as SilmarPlayer
    val subvampire = hasGainedLevel(PlayerLevels.subvampire)
    if (hasGainedLevel(PlayerLevels.werewolf) || subvampire) {
        // if the attacker is of low enough power rating
        if (monster.powerRating <= (if (subvampire) 6 else 3)) {
            // if the attack is of normal penetration
            attack as SilmarMonsterAttack
            if (attack.penetrationType == PenetrationTypes.normal) {
                // it doesn't hit
                onAttackedAndMissed()
                return true
            }
        }
    }

    return false
}