package silmar.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.PlayerListener

interface SilmarPlayerListener : PlayerListener {
    fun onSuperVisionEmployed() {}
    fun onMustChooseLevelGained() {}
}