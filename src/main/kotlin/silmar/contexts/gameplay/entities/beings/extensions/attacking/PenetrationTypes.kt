package silmar.contexts.gameplay.entities.beings.extensions.attacking

/**
 * The flyweight penetration-types.
 */
object PenetrationTypes {
    val normal = PenetrationType(0)
    val firearm = PenetrationType(1)
    val laser = PenetrationType(2)
}