package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmar.contexts.gameplay.areas.extensions.effects.performRadWaveEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.util.math.randomInt

/**
 * Has this player emit a rad-wave from its present location.
 */
fun Player.emitRadWave() {
    // if this player doesn't have at least half its hit points remaining
    if (hitPoints < maxHitPoints / 2) {
        onMessage("You don't have enough of your hit points left to do this.",
            MessageType.important)
        return
    }

    // if this player fails a +0 endurance check
    val roll = randomInt(1, 20)
    if (roll > attributeValues.endurance) {
        onMessage("You failed your endurance check!")

        // the rest of this player's is assumed to be passed by the caller
        return
    }

    performPowerDischargeEffect(this)

    // this player is reduced to 1 hit point
    takeDamage(Damage(hitPoints - 1, DamageForms.exertion))

    area.performRadWaveEffect(location, levelValues.level)
}

