package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDirectionString

/**
 * Has this player sense the direction of the down-exit in the current area-level.
 */
fun Player.findExit() {
    performPowerDischargeEffect(this)

    // if there is a down-exit
    val exit = area.getTerrainOfType(SilmarTerrainTypes.downExit)
    if (exit != null) {
        onMessage(
            "The exit is to the " + getDirectionString(location, exit.location) + ".")
    }
    else {
        onMessage("You don't sense an exit!")
    }
}

