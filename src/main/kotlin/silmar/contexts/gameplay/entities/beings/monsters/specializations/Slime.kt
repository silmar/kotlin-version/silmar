package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.items.extensions.condition.avoidOrDegrade

class Slime : Monster1() {
    override fun onDamaged(damage: Damage) {
        // if a weapon caused the damage, and that weapon isn't ranged
        if (damage.weapon != null && !damage.weapon!!.weaponType.isRanged) {
            // inform the weapon that it struck a slime
            damage.weapon!!.avoidOrDegrade(0)
        }
    }
}