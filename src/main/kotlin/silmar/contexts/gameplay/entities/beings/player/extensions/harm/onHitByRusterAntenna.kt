package silmar.contexts.gameplay.entities.beings.player.extensions.harm

import silmar.contexts.gameplay.entities.items.extensions.condition.onHitByRusterAntenna
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.offhandWeapon
import silmarengine.contexts.gameplay.entities.beings.player.extensions.items.weapon
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

/**
 * Informs this player that it has been hit by an attack from a ruster antenna.
 *
 * @param hitBy     How much the attack hit by.
 */
fun Player.onHitByRusterAntenna(hitBy: Int) {
    // try to affect any armor this played is wearing
    var item = items.getEquippedItem(EquipLocation.BODY)
    if (item != null && item.onHitByRusterAntenna(hitBy)) return

    // try to affect the weapon this player is using
    item = weapon
    if (item.onHitByRusterAntenna(hitBy)) return

    // try to affect the offhand weapon this player is using
    item = offhandWeapon
    if (item != null && item.onHitByRusterAntenna(hitBy)) return

    // try to affect a random item on this player
    item = items.randomItem
    if (item != null && item.onHitByRusterAntenna(hitBy)) return
}

