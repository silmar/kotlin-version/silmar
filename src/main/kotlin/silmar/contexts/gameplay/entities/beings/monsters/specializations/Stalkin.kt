package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1

class Stalkin : Monster1() {
    override val isVisible: Boolean
        get() = false
}