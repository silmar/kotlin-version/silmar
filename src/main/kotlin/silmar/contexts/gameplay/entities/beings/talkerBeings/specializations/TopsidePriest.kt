package silmar.contexts.gameplay.entities.beings.talkerBeings.specializations

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.TalkerBeing1
import silmarengine.contexts.gameplay.entities.talkers.Service
import silmarengine.contexts.gameplay.entities.talkers.ServiceSeller
import silmarengine.contexts.gameplay.entities.talkers.Services
import kotlin.math.max

class TopsidePriest : TalkerBeing1(), ServiceSeller {
    override val servicesForSale: Array<Service>
        get() = Companion.servicesForSale

    override fun getServicePrice(service: Service, forPlayer: Player): Int {
        return when (service) {
            Services.curePoison -> service.price * forPlayer.statuses.poisonLevel
            Services.cureDisease -> service.price * forPlayer.statuses.numDiseases
            Services.removeCursedItems -> // make it cost at least twice as much to remove an unidentified cursed item as to
                // identify it, so that it doesn't make more sense to just try out a cursed
                // item and then have it removed, vs. identifying it
                max(service.price * forPlayer.items.numEquippedCursedItems,
                    forPlayer.items.equippedUnidentifiedCursedItemsTotalIdentificationValue * 2)
            Services.restoreLevels -> service.price * forPlayer.levelValues.numLevelsLost
            else -> service.price
        }
    }

    companion object {
        /**
         * The services provided by this priest.
         */
        val servicesForSale =
            arrayOf(Services.curePoison,
                Services.cureDisease,
                Services.removeCursedItems,
                Services.healWounds,
                Services.healFully,
                Services.restoreLevels)
    }
}