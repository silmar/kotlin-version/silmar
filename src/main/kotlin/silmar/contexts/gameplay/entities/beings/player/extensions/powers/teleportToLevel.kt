package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.game.SilmarGame
import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType

/**
 * Has this player teleport to the area-level of the given index.
 */
fun SilmarPlayer.teleportToLevel(levelIndex: Int) {
    // do a power discharge at this player's location
    performPowerDischargeEffect(this)

    // determine whether the teleport is to another level
    val fromArea = area
    val game = Game.currentGame as SilmarGame
    val toArea = game.getArea(levelIndex)
    val isAreaChanging = toArea != fromArea

    // if this player has no memorized location for the selected level
    var to = getMemorizedLocation((toArea!! as SilmarArea).level!!.index)
    if (to == null) {
        // if the level has an up-exit
        val upExit = toArea.getTerrainOfType(SilmarTerrainTypes.upExit)
        to = if (upExit != null) {
            // use it as the location
            upExit.location
        }
        else {
            // use the down-exit
            val downExit = toArea.getTerrainOfType(SilmarTerrainTypes.downExit)
            downExit!!.location
        }
    }

    // execute a teleport-out effect
    fromArea.performTeleportEffect(location, true, false)

    // find a passible spot near the destination location
    to = toArea.getNearbyPassibleRectangleLocation(to, size, movementType)

    // if the teleport is within the same level
    if (!isAreaChanging) {
        // just move this player
        fromArea.moveEntity(this, to!!)
    }
    else {
        reportToListeners { it.onAreaLoading() }

        fromArea.removeEntity(this)

        // add this player at this its final teleport destination
        toArea.addEntity(this, to!!)
    } // otherwise

    toArea.performTeleportEffect(to, false, false)
}

