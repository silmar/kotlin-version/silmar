package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player

fun Player.adjustMaxMovementPoints(startingPoints: Float): Float {
    var points = startingPoints

    this as SilmarPlayer
    if (hasGainedLevel(PlayerLevels.barbarian)) {
        points = points * 3 / 2
    }

    if (silmarStatuses.isQuickened) {
        points *= 2f
    }

    if (items.isItemOfTypeEquipped(SilmarMagicItemTypes.bootsOfQuickness.itemType)) {
        points = points * 3 / 2
    }

    if (items.isItemOfTypeEquipped(SilmarMagicItemTypes.bootsOfLethargy.itemType)) {
        points /= 2f
    }

    return points
}