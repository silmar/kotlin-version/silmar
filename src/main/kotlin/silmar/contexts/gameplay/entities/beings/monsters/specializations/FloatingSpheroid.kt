package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performFireballEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.damage.Damage

class FloatingSpheroid : Monster1() {
    init {
        maxHitPoints = 1
    }

    override fun die(damage: Damage) {
        // remember these values since they might be wiped out by super.doDie()
        val area = area
        val location = this.location

        super.die(damage)

        area.performFireballEffect(null, location, 6)
    }
}