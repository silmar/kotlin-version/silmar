package silmar.contexts.gameplay.entities.beings.talkerBeings.types

import silmar.contexts.gameplay.areas.SilmarAreaMotif
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.TalkerBeingType1
import silmarengine.contexts.gameplay.areas.Area

class SilmarTalkerBeingType1(
    name: String, imageName: String, frequency: Int, movementPoints: Int, greeting: String
) : TalkerBeingType1(name, imageName, frequency, movementPoints, greeting) {
    override fun isValidForArea(area: Area): Boolean {
        val tech = (area.motif as SilmarAreaMotif).isTech
        return when (this) {
            SilmarTalkerBeingTypes.medicalRobot -> tech
            else -> super.isValidForArea(area)
        }
    }
}
