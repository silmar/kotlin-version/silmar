package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.extensions.affectForAttacks

class Mummy : Monster1() {
    override fun affectDamage(damage: Damage) {
        super.affectDamage(damage)
        damage.affectForAttacks(0.5f)
    }
}