package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmar.contexts.gameplay.entities.terrains.specializations.Web

/**
 * Has this player spin a web at its current location.
 */
fun Player.spinWeb() {
    performPowerDischargeEffect(this)

    // add a web to the area at this player's location
    val web = createTerrain(SilmarTerrainTypes.web) as Web
    web.setSpinner(this)
    area.addEntity(web, location)

    takeDamage(Damage(1, DamageForms.exertion))

    endTurn()
}

