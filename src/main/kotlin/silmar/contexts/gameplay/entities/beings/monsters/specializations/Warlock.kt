package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.entities.extensions.teleportNearPlayer
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.monsters.LightSourceMonster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.tiles.tileWidth

class Warlock : LightSourceMonster() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if this warlock is close enough to the player
        if (isDistanceAtMost(location, player.location,
                PixelDistance(4 * tileWidth))) {
            // have this warlock teleport a bit farther away
            teleportNearPlayer()
            return true
        }
        else if (hitPoints <= maxHitPoints / 2) {
            // heal this monster back up to its full hit-points
            takeHealing(maxHitPoints - hitPoints)
            return true
        } // else, if this warlock has lost over half its hit-points

        return false
    }
}