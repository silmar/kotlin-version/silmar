package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.CreateMonsterReasons
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.creation.createMonsterForReason
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.util.math.randomInt

class GiantAnt : Monster1() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if it's time to summon another ant (note that, as is reflected in this
        // monster's type, unlike a generator, this monster actually needs to
        // see the player to summon another ant)
        if (randomInt(1, 4) == 1) {
            createMonsterForReason(area, location, type, CreateMonsterReasons.summoning)
            return true
        }

        return false
    }
}