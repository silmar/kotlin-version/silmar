package silmar.contexts.gameplay.entities.beings.player.extensions.experience

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player

fun Player.updateOnLevelChange() {
    this as SilmarPlayer
    val stats = silmarStats
    stats.updateMaxMagicPoints()
    stats.updateMaxClericalPoints()
    silmarStatuses.updateIsStealthy()
}