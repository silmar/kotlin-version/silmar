package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmar.contexts.gameplay.areas.extensions.effects.performDragonBreathEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.util.math.randomInt

/**
 * Note that acid dragons have no immunity to acid attacks, as their own acid is held
 * in an internal acid-proof organ, while the rest of their bodies are vulnerable.
 */
class AcidDragon : Monster1() {

    override fun usePreMovementPower(player: Player): Boolean {
        // if this monster has at least one quarter of its hit points left,
        // is close enough to the given player, and it's time to breath
        val maxRange = TileDistance(9)
        if (hitPoints >= maxHitPoints / 4 && isDistanceAtMost(
                location, player.location, PixelDistance(maxRange)) && randomInt(
                1, 5) <= 2) {
            area.performDragonBreathEffect(location,
                player.location, DamageForms.acid, maxRange, powerRating, hitPoints, this)
            return true
        }

        return false
    }
}