package silmar.contexts.gameplay.entities.beings.player.extensions.stats

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.cyborgIntelligencePerBonus
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.getStandardModifier
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes

fun Player.adjustToHitModifier(
    startingModifier: Int, victim: Being, weapon: Weapon
): Int {
    var modifier = startingModifier

    if (items.isItemOfTypeEquipped(SilmarMagicItemTypes.blessedFigurine.itemType)) {
        modifier += 1
    }

    if (items.isItemOfTypeEquipped(SilmarMagicItemTypes.cursedFigurine.itemType)) {
        modifier -= 1
    }

    // if the weapon in use is a strength bow
    if (weapon.isOfType(SilmarWeaponTypes.strengthBow)) {
        modifier += getStandardModifier(attributeValues.strength)
    }

    // if this player is a monk
    this as SilmarPlayer
    if (hasGainedLevel(PlayerLevels.monk)) {
        // if the weapon in use is the hand
        if (weapon.isOfType(WeaponTypes.hand)) {
            modifier += levelValues.level / 3
        }
    }

    // if this player is an archer
    if (hasGainedLevel(PlayerLevels.archer)) {
        // if the weapon in use is a subtype of the bow
        if (weapon.type.isDescendedFromType(WeaponTypes.bow.itemType)) {
            modifier += levelValues.level / 5
        }
    }

    // if this player is a cyborg
    if (hasGainedLevel(PlayerLevels.cyborg)) {
        // if the weapon in use is ranged
        if (weapon.weaponType.isRanged) {
            modifier += attributeValues.intelligence / cyborgIntelligencePerBonus
        }
    }

    // if this player is a paladin
    if (hasGainedLevel(PlayerLevels.paladin)) {
        // if the victim is evil
        if (victim.isEvil) {
            modifier += 2
        }
    }

    return modifier
}