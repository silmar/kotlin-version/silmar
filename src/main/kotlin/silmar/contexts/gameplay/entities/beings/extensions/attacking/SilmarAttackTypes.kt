package silmar.contexts.gameplay.entities.beings.extensions.attacking

import silmar.sounds.SilmarSounds

object SilmarAttackTypes {
    val bullet = SilmarAttackType("bullet", SilmarSounds.bullet, 5,
        penetrationType = PenetrationTypes.firearm)
    val laserCell = SilmarAttackType("laserCell", SilmarSounds.laserCell, 5, 1,
        PenetrationTypes.laser)
    val laser =
        SilmarAttackType("laser", SilmarSounds.laser, 5, 1, PenetrationTypes.laser)
    val web = SilmarAttackType("terrain/web", SilmarSounds.web)
    val fireball = SilmarAttackType("fireball", SilmarSounds.fireballWhoosh, 2, 4,
        PenetrationTypes.firearm)
    val electricity = SilmarAttackType("electricity", SilmarSounds.electricity, 1)
    val slimeChuck = SilmarAttackType("slimeChuck", SilmarSounds.slimeChucker, 25)
    val deathRay = SilmarAttackType("deathRay", SilmarSounds.deathRay, 1)
    val vortexBeam = SilmarAttackType("vortexBeam", SilmarSounds.vortexBeam, 1)
    val boulder = SilmarAttackType("terrain/boulder", SilmarSounds.grunt,
        penetrationType = PenetrationTypes.firearm)
    val laserMeleeAttack = SilmarAttackType("meleeAttack", SilmarSounds.laserMeleeAttack,
        penetrationType = PenetrationTypes.laser)
}