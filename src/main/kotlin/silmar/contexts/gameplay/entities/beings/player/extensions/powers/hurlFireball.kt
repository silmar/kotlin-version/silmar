package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.areas.extensions.effects.performFireballEffect
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.magicUserCastingLevel
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.extensions.location.modifyLocationDueToConfusion
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getSkeweredTargetLocation
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt
import kotlin.math.ceil
import kotlin.math.min

/**
 * Has this player hurl a fireball at the given location.
 */
fun SilmarPlayer.hurlFireball(to: PixelPoint) {
    performPowerDischargeEffect(this)

    val from = location
    var adjustedTo = if (isConfused) modifyLocationDueToConfusion(area, to, from) else to

    // if this player fails a +5 agility check modified by the distance to the to-location
    val distance = getDistance(from, adjustedTo)
    val roll = randomInt(1, 20)
    val missedBy =
        roll + distance.distance / 2 / tileWidth - (attributeValues.agility + 5)
    if (missedBy > 0) {
        // find a spot near (and in LOS of) the target location that is visible
        // from this player; the max potential error increases by how badly the
        // agility test failed
        val maxError = PixelDistance(min(distance.distance / 2,
            ceil((missedBy / 3f).toDouble()).toInt() * tileWidth))
        adjustedTo = getSkeweredTargetLocation(area, from, adjustedTo, maxError)
    }

    area.performFireballEffect(from, adjustedTo, 2 + magicUserCastingLevel)
}

