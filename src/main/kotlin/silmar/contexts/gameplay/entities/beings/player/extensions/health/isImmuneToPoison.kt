package silmar.contexts.gameplay.entities.beings.player.extensions.health

import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel
import silmarengine.contexts.gameplay.entities.beings.player.Player

fun Player.isImmuneToPoison(): Boolean {
    this as SilmarPlayer
    return hasGainedLevel(PlayerLevels.superhero)
}