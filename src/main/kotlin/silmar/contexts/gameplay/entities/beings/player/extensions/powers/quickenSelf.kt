package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.magicUserCastingLevel
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Has this player perform a quicken-self power on itself.
 */
fun SilmarPlayer.quickenSelf() {
    // perform a few quick power-discharges in quick succession at this player's location
    for (i in 0..2) {
        area.performPowerDischargeEffect(location, 50)
    }

    // start the quickening's duration
    val duration = magicUserCastingLevel * 4
    silmarStatuses.onQuickened(duration)

    reportToListeners { it.onVitalStatChanged() }
}

