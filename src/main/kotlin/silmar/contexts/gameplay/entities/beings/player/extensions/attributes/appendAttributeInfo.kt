package silmar.contexts.gameplay.entities.beings.player.extensions.attributes

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayerAttributeModifierProvider
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.Attribute
import silmarengine.util.html.addLineBreakPlusIndent
import silmarengine.util.text.formatModifier

fun appendAttributeInfo(attribute: Attribute, value: Int, to: StringBuffer) {
    val provider = SilmarPlayerAttributeModifierProvider
    fun append(text: String) = to.append(text)
    when (attribute) {
        Attribute.INTELLIGENCE -> {
            addLineBreakPlusIndent(to)
            append("magic-user spell-casting success modifier: ")
            append(formatModifier(
                provider.castingModifierForIntelligence(value)))

            addLineBreakPlusIndent(to)
            append("magic-points per-casting-level modifier: ")
            append(formatModifier(
                provider.magicPointsModifierForIntelligence(value)))
        }
        Attribute.JUDGEMENT -> {
            addLineBreakPlusIndent(to)
            append("clerical spell-casting success modifier: ")
            append(
                formatModifier(provider.castingModifierForJudgement(value)))

            addLineBreakPlusIndent(to)
            append("clerical-points per-casting-level modifier: ")
            append(formatModifier(
                provider.clericalPointsModifierForJudgement(value)))
        }
        Attribute.ENDURANCE -> {
            addLineBreakPlusIndent(to)
            append("magic-points per-casting-level modifier: ")
            append(formatModifier(
                provider.magicPointsModifierForEndurance(value)))

            addLineBreakPlusIndent(to)
            append("clerical-points per-casting-level modifier: ")
            append(formatModifier(
                provider.clericalPointsModifierForEndurance(value)))
        }
        else -> Unit
    }
}