package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player

class Munchkin : Monster1() {

    /**
     * Informs this munchkin that the given player it is attacking has no more rations
     * left to steal.
     */
    fun onNoRationsLeft(player: Player) {
        fleeFromPlayer = player
        isFleeing = true
    }
}