package silmar.contexts.gameplay.entities.beings.monsters.extensions.harm

import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationTypes
import silmar.contexts.gameplay.entities.beings.extensions.attacking.SilmarAttackType
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterType
import silmarengine.contexts.gameplay.entities.beings.extensions.attacking.AttackType
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster

fun Monster.computeSilmarMonsterDefense(attackType: AttackType): Int {
    val penetrationType = when (attackType) {
        is SilmarAttackType -> attackType.penetrationType
        else -> PenetrationTypes.normal
    }
    return (type as SilmarMonsterType).defenses[penetrationType.rank]
}