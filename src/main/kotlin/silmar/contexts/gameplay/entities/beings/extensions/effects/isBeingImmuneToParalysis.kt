package silmar.contexts.gameplay.entities.beings.extensions.effects

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.hasGainedLevel

fun isBeingImmuneToParalysis(being: Being): Boolean {
    // if the being is a monster which is immune
    if (being is Monster) {
        val type = being.type
        //@formatter:off
        if (type.isLivingDead || type == SilmarMonsterTypes.grave ||
            type == SilmarMonsterTypes.bones || type == SilmarMonsterTypes.vortex ||
            type == SilmarMonsterTypes.casket || type == SilmarMonsterTypes.robotGenerator) {
            //@formatter:on
            being.area.player?.receiveMessageIfInLOSOf(being.location, "No effect")
            return true
        }
    }

    // if this being is a player who is a sub-vampire, it is immune
    if (being is SilmarPlayer && being.hasGainedLevel(PlayerLevels.subvampire)) return true

    return false
}
