package silmar.contexts.gameplay.entities.beings.player.extensions.powers

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.onCureDiseaseReceived
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect

/**
 * Has this player perform a cure-disease power on itself.
 */
fun Player.cureDisease() {
    performPowerDischargeEffect(this)

    onCureDiseaseReceived()
}

