package silmar.contexts.gameplay.entities.beings.monsters.extensions

import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes

private typealias Types = SilmarMonsterTypes

fun Monster.isRobot() =
    when (type) {
        Types.cetharg, Types.deathbot, Types.sentryborg -> true
        else -> false
    }