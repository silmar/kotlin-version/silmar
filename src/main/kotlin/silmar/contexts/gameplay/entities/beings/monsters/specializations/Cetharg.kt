package silmar.contexts.gameplay.entities.beings.monsters.specializations

import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmar.contexts.gameplay.areas.extensions.effects.performStunFlashEffect
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.geometry.isDistanceAtMost
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomInt

class Cetharg : Monster1() {
    override fun usePreMovementPower(player: Player): Boolean {
        // if a player was given, and that player isn't already paralyzed,
        // and it's time to emit a stun flash, and the player is within range of such a flash
        val strength = powerRating
        if (!player.isParalyzed && randomInt(1, 4) == 1 && isDistanceAtMost(location,
                player.location, PixelDistance(strength * tileWidth))) {
            area.performPowerDischargeEffect(location)
            area.performStunFlashEffect(location, strength, this)
            return true
        }

        return false
    }
}