package silmar.contexts.gameplay.entities.beings.monsters.types

import silmar.contexts.gameplay.areas.AreaLevel
import silmarengine.contexts.gameplay.entities.beings.monsters.types.MonsterType

interface SilmarMonsterType : MonsterType {
    /**
     * The area-levels for which monsters of this type may be generated.
     */
    val validLevels: Array<AreaLevel>?

    /**
     * The defense ratings (corresponding to the various penetration levels - see PenetrationType)
     * of monsters of this type.
     */
    val defenses: IntArray
}