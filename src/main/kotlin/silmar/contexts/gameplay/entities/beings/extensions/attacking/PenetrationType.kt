package silmar.contexts.gameplay.entities.beings.extensions.attacking

import java.util.*

/**
 * Specifies constants corresponding to weapon penetration levels.
 */
class PenetrationType(
    /**
     * The rank of this penetration-type amongst all others.
     */
    val rank: Int
) {
    init {
        types.add(this)
    }

    /**
     * Returns whether the given penetration type is the same as this one.
     */
    override fun equals(other: Any?): Boolean {
        return other is PenetrationType && other.rank == rank
    }

    override fun hashCode(): Int {
        return rank
    }

    companion object {
        /**
         * A list of all penetration types.
         */
        val types = ArrayList<PenetrationType>()
    }
}
