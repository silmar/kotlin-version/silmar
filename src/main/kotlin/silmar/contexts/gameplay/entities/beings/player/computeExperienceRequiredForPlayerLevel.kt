package silmar.contexts.gameplay.entities.beings.player

import silmar.contexts.gameplay.areas.AreaLevels

/**
 * Returns an estimate of the amount of experience that should be available for gain
 * by the player, for the given dungeon level.
 *
 * The estimate is based on the following guidelines:
 *
 * There is an average of 20 items in a dungeon level
 * * 1000 gold max value per item (per dungeon level)
 * / 2 for taking the average of 0 and the max value of the item
 * * .625 for average condition
 * * 2/3 for the greater frequency of lesser items (and the presence of
 * cursed items) vs. more valuable ones
 * / 10 gold per 1 exp (as monster exp is specified divided by 10)
 * / 2 due to un-found items, items used, gold spent on supplies /
 * healing/curing / item identification / item repair and other costs
 *
 * +
 *
 * There is an average of 30 monsters in a dungeon level
 * * 14 exp per monster (per dungeon level) on average
 * * 9/10 as I expect the player will vanquish 90% of the monsters on the level
 *
 * = 208 + 378 = 586 per dungeon level
 **/
fun getExperienceAvailableInAreaLevel(index: Int): Int = 586 * index

private val experienceAvailableThroughDungeonLevel =
    computeExperienceAvailableThroughDungeonLevels()

private fun computeExperienceAvailableThroughDungeonLevels(): IntArray {
    // for each dungeon level
    val numLevels = AreaLevels.moonEnd.index
    val result = IntArray(AreaLevels.moonEnd.index + 1)
    result[0] = 0
    for (i in 1..numLevels) {
        // the experience available through this level is the amount available through
        // the preceding level, plus the amount available in this level, taking
        // into account that every third level has only a fractional amount available
        result[i] = result[i - 1] + (getExperienceAvailableInAreaLevel(
            i) * (if ((i - 1) % 3 == 2) 0.3f else 1f)).toInt()
    }
    //    result.map { println(it) }
    return result
}

fun computeExperienceRequiredForPlayerLevel(): IntArray {
    // compute the values for the starting level to gain, and onward;
    // these correspond to the experience available through every three dungeon levels
    // (taking into account that every 3rd level has only a fractional amount available,
    // on average), and then doubling per level once we're past the total number
    // of dungeon levels
    val maxLevel = 25
    val result = IntArray(maxLevel)
    val firstLevelToGain = 7 // starting level is 5, starting exp is close to level 6
    val expAvailable = experienceAvailableThroughDungeonLevel
    for (i in firstLevelToGain until maxLevel) {
        result[i] = when (val dungeonLevel = (i - firstLevelToGain + 1) * 3) {
            in 1 until expAvailable.size -> expAvailable[dungeonLevel]
            else -> result[i - 1] * 2
        }
    }

    // compute the values for levels up until the starting level to gain, as a linear
    // progression toward the amount needed for that level
    for (i in 0 until firstLevelToGain) result[i] =
        (result[firstLevelToGain] * i / firstLevelToGain.toFloat()).toInt()

    //    result.map { println(it) }
    return result
}