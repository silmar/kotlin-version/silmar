package silmar.contexts.gameplay.entities.beings.player

import silmarengine.contexts.gameplay.entities.beings.player.Disease
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes

object SilmarDiseases {
    val rat = Disease("rat", PlayerAttributes.endurance, 50)
    val mummy = Disease("mummy", PlayerAttributes.strength, 10)
    val scumShamble = Disease("scum shamble", PlayerAttributes.agility, 25)
    val typhoidZombie1 = Disease("typhoid zombie 1", PlayerAttributes.intelligence, 35)
    val typhoidZombie2 = Disease("typhoid zombie 2", PlayerAttributes.agility, 30)
    val typhoidZombie3 = Disease("typhoid zombie 3", PlayerAttributes.endurance, 25)
    val mutatedRat = Disease("mutated rat", PlayerAttributes.strength, 20)
}