package silmar.contexts.gameplay.entities.beings.extensions.location

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getUnitVector
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.sleepThread

/**
 * Knocks the given being away from the given knocker-location with the given
 * power.
 *
 * @param damage            The damage caused by the initial hit, to be conferred
 * also on those with whom the being collides.
 */
fun Being.knockBack(knockerLocation: PixelPoint, damage: Int, power: Int) {
    // determine the knockback distance
    val stepSize = 4
    val knockbackDistance = PixelDistance(TileDistance((15 - powerRating) / 2))
    knockbackDistance.distance += power * tileWidth
    if (knockbackDistance.distance < 1) return

    // determine the knockback direction
    val location = MutablePixelPoint(location)
    val direction = getUnitVector(knockerLocation, location)

    // for each step of knockback distance (but, stop if the being dies)
    val area = area
    val numSteps = knockbackDistance.distance / stepSize
    var i = 0
    while (i < numSteps && !isDead) {
        // determine the next location in the knockback line segment as a step
        // from the previous location in the knockback direction
        location.translate((direction.x * stepSize).toInt(),
            (direction.y * stepSize).toInt())

        // if the new location is impassible
        val beingSize = size
        if (!area.isRectanglePassible(location, beingSize, MovementType.WALKING, true, this, null,
                false).passible) {
            // the victim takes the same damage again
            takeDamage(Damage(damage, DamageForms.bluntImpact))

            // for each being in the impassible square
            var shouldBreak = true
            var closest: Being? = null
            var closestDistance: PixelDistance? = null
            val beings =
                area.beings.getEntitiesInRectangle(location, beingSize, null, this)
            beings.forEach { test ->
                // if this being isn't the knockback being
                if (test !== this) {
                    // if this is the closest being to the knockback being found so far
                    val distanceToTest = getDistance(test.location, location)
                    if (closestDistance == null || distanceToTest.distance < closestDistance!!.distance) {
                        // remember this as the new closest being
                        closest = test
                        closestDistance = distanceToTest
                    }
                }
            }

            // if there was a closest being knocked into determined above
            if (closest != null) {
                // that being takes the same damage, too
                closest!!.takeDamage(Damage(damage, DamageForms.bluntImpact))

                // if the being in the way is now dead, and the rectangle tested above
                // is now passible due to its absence
                if (closest!!.isDead && area.isRectanglePassible(location, beingSize,
                        MovementType.WALKING, true, this, null, false).passible) {
                    // the knockback victim's knocking back shouldn't end here
                    shouldBreak = false
                }
            }

            if (shouldBreak) break
        }

        // move the victim to the new location
        area.moveEntity(this, location)
        sleepThread(0, 10)
        i++
    }
}
