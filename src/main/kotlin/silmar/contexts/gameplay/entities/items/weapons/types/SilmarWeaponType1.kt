package silmar.contexts.gameplay.entities.items.weapons.types

import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationType
import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationTypes
import silmar.contexts.gameplay.entities.items.types.SilmarItemType
import silmarengine.contexts.gameplay.entities.items.types.ItemSubType
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType1

data class SilmarWeaponType1(override val silmarItemType: SilmarItemType,
    override val weaponType: WeaponType,
    override val penetrationType: PenetrationType = PenetrationTypes.normal,
    override val isFirearm: Boolean = false, override val isLaserWeapon: Boolean = false,
    override val ammoCapacity: Int = 0) : SilmarWeaponType, ItemSubType {
    init {
        (weaponType as WeaponType1).weaponSubType = this
    }

    override fun getExtraInfo(): String? = null
}