package silmar.contexts.gameplay.entities.items.weapons

import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponType
import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationTypes
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType

fun canWeaponTypeHit(type: WeaponType, plusNeededToHit: Int): Boolean {
    val subType = type.weaponSubType
    if (subType is SilmarWeaponType) {
        // if this weapon has firearm penetration and the
        // plus needed is low enough
        if (subType.penetrationType == PenetrationTypes.firearm && plusNeededToHit <= 2) return true

        // if this weapon has laser penetration and the plus needed
        // is low enough
        if (subType.penetrationType == PenetrationTypes.laser && plusNeededToHit <= 4) return true
    }

    return false
}
