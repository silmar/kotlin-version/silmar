package silmar.contexts.gameplay.entities.items.weapons

import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponType
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon1
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType

class SilmarWeapon1(type: WeaponType, condition: ItemCondition) : SilmarWeapon,
    Weapon1(type, condition) {
    override var firedCountSinceReload = 0
        private set

    override fun onFired() {
        firedCountSinceReload++
    }

    override fun onReloaded() {
        firedCountSinceReload = 0
    }

    override fun onMadeAttack() {
        super<Weapon1>.onMadeAttack()

        val subType = weaponType.weaponSubType
        if (subType is SilmarWeaponType && subType.ammoCapacity > 0) onFired()
    }
}