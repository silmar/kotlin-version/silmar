package silmar.contexts.gameplay.entities.items.types

object SilmarShieldTypes {
    val kevlarShield =
        createSilmarShieldType("kevlar shield", "kevlarShield", futureShieldName, 5f, 6,
            1, 1, 0, 1, false, 8, 500, true)
    val largeKevlarShield =
        createSilmarShieldType("large kevlar shield", "largeKevlarShield",
            futureShieldName, 10f, 5, 2, 2, 0, 2, false, 12, 1000, true)
    val riotShield =
        createSilmarShieldType("riot shield", "riotShield", futureShieldName, 25f, 4, 5,
            5, 1, 5, true, 12, 2500, true, takesTwoHands = true)
    val reflecShield =
        createSilmarShieldType("reflec shield", "reflecShield", futureShieldName, 5f, 6,
            1, 1, 1, 1, false, 8, 1500, true)
    val largeReflecShield =
        createSilmarShieldType("large reflec shield", "largeReflecShield",
            futureShieldName, 10f, 5, 2, 2, 2, 2, false, 12, 3000, true)
}

/**
 * The name shown for an unidentified shield from the future.
 */
const val futureShieldName = "strange shield"