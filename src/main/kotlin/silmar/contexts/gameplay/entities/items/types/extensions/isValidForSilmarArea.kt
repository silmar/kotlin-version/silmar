package silmar.contexts.gameplay.entities.items.types.extensions

import silmar.contexts.gameplay.entities.items.types.SilmarItemType
import silmar.contexts.gameplay.areas.AreaLevels
import silmar.contexts.gameplay.areas.SilmarArea
import silmar.contexts.gameplay.areas.SilmarAreaMotif
import silmar.contexts.gameplay.entities.items.types.SilmarArmorTypes
import silmar.contexts.gameplay.entities.items.types.SilmarShieldTypes
import silmar.contexts.gameplay.entities.items.types.SilmarAmmoTypes
import silmar.contexts.gameplay.entities.items.types.SilmarTechItemTypes
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.areas.Area
import kotlin.math.abs

fun ItemType.isValidForSilmarArea(area: Area): Boolean {
    // if this item-type is technological in nature
    area as SilmarArea
    val subType = subType
    if (subType is SilmarItemType && subType.isTech) {
        // if the given area's level comes before the city level, this type is not valid
        if (area.level.index < AreaLevels.city1.index) return false

        // don't allow futuristic items until the base level
        when (this) {
            //@formatter:off
            SilmarArmorTypes.reflecArmor.itemType,
            SilmarArmorTypes.heavyReflecArmor.itemType,
            SilmarArmorTypes.poweredReflecArmor.itemType,
            SilmarShieldTypes.reflecShield.itemType,
            SilmarShieldTypes.largeReflecShield.itemType,
            SilmarAmmoTypes.laserPistolCell.itemType,
            SilmarAmmoTypes.laserAssaultRifleCell.itemType,
            SilmarTechItemTypes.leadLinedCloak.itemType,
            SilmarWeaponTypes.stunWhip.weaponType.itemType,
            SilmarWeaponTypes.energySaber.weaponType.itemType,
            SilmarWeaponTypes.laserPistol.weaponType.itemType,
            SilmarWeaponTypes.laserAssaultRifle ->
                if (area.level.index < AreaLevels.base1.index) return false
            //@formatter:on
        }
    }

    // don't allow magical items on technological levels
    if (isMagical && (area.motif as SilmarAreaMotif).isTech) return false

    // if this item-type has a "plus" magical modifier, don't allow it if the given area
    // is too early for the modifier's magnitude (specifically, don't allow +5 before
    // level 22, +4 before level 16, +3 before level 10, +2 before level 4)
    val restrictedLevelsPerPlus = 6
    if (plus != 0 && area.level.index <= abs(
            plus) * restrictedLevelsPerPlus - 8) return false

    // if this item-type is too valuable for the given area, this type isn't valid
    return price <= area.level.maxItemPlacementValue
}