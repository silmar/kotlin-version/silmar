package silmar.contexts.gameplay.entities.items.types

import silmarengine.contexts.gameplay.entities.items.types.ItemType1
import silmarengine.contexts.gameplay.entities.items.types.getFrequencyForValue
import silmarengine.contexts.gameplay.entities.items.types.shieldTypes
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

fun createSilmarShieldType(name: String, imageName: String?, unidentifiedName: String?,
    encumbrance: Float, avoidance: Int, defenseBonusNormal: Int, defenseBonusFirearm: Int,
    defenseBonusLaser: Int, avoidanceModifier: Int, isMetal: Boolean,
    strengthRequired: Int, price: Int, isTech: Boolean, takesTwoHands: Boolean = false): SilmarItemType1 {
    val itemType = ItemType1(name, imageName,
        frequency = getFrequencyForValue(price),
        unidentifiedName = unidentifiedName,
        equipLocation = EquipLocation.RIGHT_HAND,
        encumbrance = encumbrance, avoidance = avoidance,
        defenseModifier = defenseBonusNormal,
        avoidanceModifier = avoidanceModifier, isMetal = isMetal,
        strengthRequired = strengthRequired, price = price, takesTwoHands = takesTwoHands)
    val result = SilmarItemType1(itemType, isTech = isTech,
        defenseModifiers = intArrayOf(defenseBonusNormal, defenseBonusFirearm,
            defenseBonusLaser))
    itemType.register()
    shieldTypes.add(itemType)
    return result
}
