package silmar.contexts.gameplay.entities.items.types

import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.entities.items.types.ItemType1

data class SilmarItemType1(
    val itemType: ItemType,
    override val isTech: Boolean = false,
    override val defenseModifiers: IntArray = noDefenseModifiers) : SilmarItemType {
    init {
        (itemType as ItemType1).subType = this
    }

    override fun getExtraInfo(): String? = null
}