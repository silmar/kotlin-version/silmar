package silmar.contexts.gameplay.entities.items.weapons

import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponType
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType

fun createSilmarWeapon(type: WeaponType, condition: ItemCondition): Weapon? =
    when (type.weaponSubType) {
        SilmarWeaponTypes.clubOfImpact -> SilmarWeapons.ClubOfImpact(condition)
        SilmarWeaponTypes.energySaber.weaponType -> SilmarWeapons.EnergySaber(condition)
        SilmarWeaponTypes.stunWhip.weaponType -> SilmarWeapons.StunWhip(condition)
        SilmarWeaponTypes.staffOfHarming -> SilmarWeapons.StaffOfHarming(condition)
        SilmarWeaponTypes.maceOfPurity -> SilmarWeapons.MaceOfPurity(condition)
        is SilmarWeaponType -> SilmarWeapon1(type, condition)
        else -> null
    }