package silmar.contexts.gameplay.entities.items.weapons

import silmarengine.contexts.gameplay.entities.items.weapons.Weapon

interface SilmarWeapon : Weapon {
    val firedCountSinceReload: Int

    fun onFired()

    fun onReloaded()
}