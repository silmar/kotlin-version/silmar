package silmar.contexts.gameplay.entities.items.specializations

import silmar.contexts.gameplay.entities.items.types.SilmarTechItemTypes
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.Item1
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.terrains.createTerrain

class LandMine(condition: ItemCondition) :
    Item1(SilmarTechItemTypes.landMine.itemType, condition), Item.Usable {
    override fun onUsed(user: Player) {
        // create a land mine terrain and add it
        // to the area at the user's location
        val area = user.area
        val location = user.location
        val landMine = createTerrain(SilmarTerrainTypes.landMine)
        area.addEntity(landMine, location)

        quantity--
    }
}