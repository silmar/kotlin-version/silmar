package silmar.contexts.gameplay.entities.items.extensions.condition

import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.extensions.condition.avoidOrDegrade

/**
 * Informs this item that it has been hit by a ruster-antenna attack of the
 * given strength.
 *
 * @return      Whether this item was susceptible to damage from such an attack.
 */
fun Item.onHitByRusterAntenna(strength: Int): Boolean {
    // if this item isn't made of metal
    if (!type.isMetal) return false

    avoidOrDegrade(strength / 2)

    return true
}

