package silmar.contexts.gameplay.entities.items.types

import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

object SilmarMagicItemTypes {
    val bootsOfQuickness =
        createSilmarMagicItemType("pair of boots of quickness", "boots", "pair of boots",
            5f, true, EquipLocation.FEET, 10, false, 5000, false, "Speeds movement.")
    val bootsOfLethargy =
        createSilmarMagicItemType("pair of boots of lethargy", "boots", "pair of boots",
            5f, true, EquipLocation.FEET, 10, false, 5000, true, "Slows movement.")
    val capeOfGoodLuck =
        createSilmarMagicItemType("cape of good luck", "cape", "cape", 3f, true,
            EquipLocation.BACK, 15, false, 3000, false,
            "Adds a +2 bonus to avoidance checks.", avoidanceModifier = 2)
    val capeOfDisplacement =
        createSilmarMagicItemType("cape of displacement", "cape", "cape", 3f, true,
            EquipLocation.BACK, 15, false, 4000, false,
            "Makes wearer appear to be a few feet from actual position, adding a bonus to defense.",
            defenseModifiers = intArrayOf(2, 2, 2))
    val blessedFigurine =
        createSilmarMagicItemType("blessed figurine", "figurine", "figurine", 1f, true,
            EquipLocation.NECK, 17, false, 1500, false, "Provides a +1 bonus to hit.")
    val cursedFigurine =
        createSilmarMagicItemType("cursed figurine", "figurine", "figurine", 1f, true,
            EquipLocation.NECK, 17, false, 1500, true, "Imposes a -1 penalty to hit.")
    val helmOfCombatMastery =
        createSilmarMagicItemType("helm of combat mastery", "helm", "helm", 5f, true,
            EquipLocation.HEAD, 8, true, 3000, false,
            "Grants an extra melee attack per turn.")
    val skullcapOfReasoning =
        createSilmarMagicItemType("skullcap of reasoning", "skullcap", "skullcap", 2f,
            true, EquipLocation.HEAD, 10, true, 6000, false, "Increases intelligence.",
            attributeModifiers = intArrayOf(0, 4, 0, 0, 0))
    val necklaceOfOgreStrength =
        createSilmarMagicItemType("necklace of ogre strength", "magicNecklace", 
            "necklace", 1f, true, EquipLocation.NECK, 12, false, 12000, false,
            "Grants a 19 strength.")
    val necklaceOfEssence =
        createSilmarMagicItemType("necklace of essence", "magicNecklace", "necklace", 1f,
            true, EquipLocation.NECK, 12, false, 9000, false,
            "Absorbs level drains.  Degrades once per drain.")
    val necklaceOfGoodLuck =
        createSilmarMagicItemType("necklace of good luck", "magicNecklace", "necklace",
            1f, true, EquipLocation.NECK, 12, true, 3000, false,
            "Adds a +2 bonus to avoidance checks.", avoidanceModifier = 2)
    val necklaceOfChoking =
        createSilmarMagicItemType("necklace of choking", "magicNecklace", "necklace", 1f,
            true, EquipLocation.NECK, 12, true, 3000, true, "Chokes the wearer to death.")
    val phylacteryOfResurrection =
        createSilmarMagicItemType("phylactery of resurrection", "phylactery", 
            "phylactery", 1f, false, null, 17, false, 5000, false,
            "At the moment of death, heals the possessor back to full health.")
    val ringOfPowerStorage =
        createSilmarMagicItemType("ring of power storage", "ring", "ring", 0.1f, true,
            EquipLocation.RIGHT_RING, 11, true, 5000, false,
            "Absorbs the magic- and clerical- point cost of powers used. Is degraded with each usage with a probability determined by the amount of points that would have been spent.")
    val ringOfStrength =
        createSilmarMagicItemType("ring of strength", "ring", "ring", 0.1f, true,
            EquipLocation.RIGHT_RING, 11, true, 5500, false, "Increases strength.",
            attributeModifiers = intArrayOf(2, 0, 0, 0, 0))
    val ringOfStrengthSapping =
        createSilmarMagicItemType("ring of strength sapping", "ring", "ring", 0.1f, true,
            EquipLocation.RIGHT_RING, 11, true, 5500, true, "Decreases strength.",
            attributeModifiers = intArrayOf(-4, 0, 0, 0, 0))
    val robeOfMagicalEnergy =
        createSilmarMagicItemType("robe of magical energy", "robe", "robe", 4f, true,
            EquipLocation.BACK, 16, false, 13000, false,
            "Lessens magic- and clerical- point cost of powers used by one-third.")
}