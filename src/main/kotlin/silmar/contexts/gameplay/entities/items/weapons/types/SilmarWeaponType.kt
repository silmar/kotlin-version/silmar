package silmar.contexts.gameplay.entities.items.weapons.types

import silmar.contexts.gameplay.entities.items.types.SilmarItemType
import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationType
import silmarengine.contexts.gameplay.entities.items.types.ItemSubType
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType

interface SilmarWeaponType : ItemSubType {
    /**
     * The more general weapon type associated with this type.
     */
    val weaponType: WeaponType

    /**
     * The more general Silmar item-type associated with this type.
     */
    val silmarItemType: SilmarItemType

    /**
     * The penetration type of a weapon of this type.
     */
    val penetrationType: PenetrationType

    val isFirearm: Boolean

    val isLaserWeapon: Boolean

    val ammoCapacity: Int
}