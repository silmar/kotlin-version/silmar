package silmar.contexts.gameplay.entities.items.weapons.types

import silmar.contexts.gameplay.entities.beings.extensions.attacking.PenetrationTypes
import silmar.contexts.gameplay.entities.beings.extensions.attacking.SilmarAttackTypes
import silmar.contexts.gameplay.entities.items.types.SilmarAmmoTypes
import silmar.contexts.gameplay.entities.items.types.SilmarItemType1
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.items.types.ItemType1
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponType1
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.contexts.gameplay.entities.items.weapons.types.createWeaponItemType

@Suppress("unused")
object SilmarWeaponTypes {
    /**
     * The flyweight firearm types.
     */
    private val pistolItemType =
        createWeaponItemType("pistol", "pistol", futureRangedWeaponName, encumbrance = 3f,
            avoidance = 10, isMetal = true, strengthRequired = 1, price = 2000)
    val pistol = SilmarWeaponType1(SilmarItemType1(pistolItemType, isTech = true),
        WeaponType1(pistolItemType, maxDamage = 5, isRanged = true,
            maxRange = TileDistance(20), attackType = SilmarAttackTypes.bullet,
            ammoType = SilmarAmmoTypes.pistolBullet.itemType, numAttacks = 3,
            damageForm = DamageForms.pierce), PenetrationTypes.firearm, isFirearm = true,
        ammoCapacity = 10)
    private val shotgunItemType =
        createWeaponItemType("shotgun", "shotgun", futureRangedWeaponName,
            encumbrance = 10f, takesTwoHands = true, avoidance = 10, isMetal = true,
            strengthRequired = 2, price = 3000)
    val shotgun = SilmarWeaponType1(SilmarItemType1(shotgunItemType, isTech = true),
        WeaponType1(shotgunItemType, minDamage = 3, maxDamage = 20, isRanged = true,
            maxRange = TileDistance(6), attackType = SilmarAttackTypes.bullet,
            ammoType = SilmarAmmoTypes.shotgunShell.itemType, numAttacks = 2,
            toHitModifier = 8, damageForm = DamageForms.pierce), PenetrationTypes.firearm,
        isFirearm = true, ammoCapacity = 6)
    private val assaultRifleItemType =
        createWeaponItemType("assault rifle", "assaultRifle", futureRangedWeaponName,
            encumbrance = 15f, takesTwoHands = true, avoidance = 9, isMetal = true,
            strengthRequired = 2, price = 4000)
    val assaultRifle =
        SilmarWeaponType1(SilmarItemType1(assaultRifleItemType, isTech = true),
            WeaponType1(assaultRifleItemType, minDamage = 2, maxDamage = 7,
                isRanged = true, maxRange = TileDistance(30),
                attackType = SilmarAttackTypes.bullet,
                ammoType = SilmarAmmoTypes.assaultRifleBullet.itemType, numAttacks = 4,
                damageForm = DamageForms.pierce), PenetrationTypes.firearm,
            isFirearm = true, ammoCapacity = 20)

    /**
     * The flyweight laser/energy weapon types.
     */
    private val energySaberItemType =
        createWeaponItemType("energy saber", "energySaber", "strange sword",
            encumbrance = 3f, avoidance = 6, isMetal = true, strengthRequired = 5,
            price = 8000, info = ItemType1.degradesInfo)
    val energySaber =
        SilmarWeaponType1(SilmarItemType1(energySaberItemType, isTech = true),
            WeaponType1(energySaberItemType, minDamage = 5, maxDamage = 22,
                attackType = SilmarAttackTypes.laserMeleeAttack,
                damageForm = DamageForms.laser), PenetrationTypes.laser,
            isLaserWeapon = true)
    private val stunWhipItemType =
        createWeaponItemType("stun whip", "stunWhip", "strange whip", encumbrance = 2f,
            avoidance = 9, isMetal = true, strengthRequired = 3, price = 4000,
            info = ItemType1.degradesInfo)
    val stunWhip = SilmarWeaponType1(SilmarItemType1(stunWhipItemType, isTech = true),
        WeaponType1(stunWhipItemType, maxDamage = 1,
            attackType = SilmarAttackTypes.electricity,
            damageForm = DamageForms.electricity), PenetrationTypes.laser,
        isLaserWeapon = true)
    private val laserPistolItemType =
        createWeaponItemType("laser pistol", "laserPistol", futureRangedWeaponName,
            encumbrance = 3f, avoidance = 7, isMetal = true, strengthRequired = 1,
            price = 4000)
    val laserPistol =
        SilmarWeaponType1(SilmarItemType1(laserPistolItemType, isTech = true),
            WeaponType1(laserPistolItemType, minDamage = 2, maxDamage = 7,
                isRanged = true, maxRange = TileDistance(30),
                attackType = SilmarAttackTypes.laserCell,
                ammoType = SilmarAmmoTypes.laserPistolCell.itemType, numAttacks = 3,
                damageForm = DamageForms.laser), PenetrationTypes.laser,
            isLaserWeapon = true, ammoCapacity = 16)
    private val laserAssaultRifleItemType =
        createWeaponItemType("laser assault rifle", "laserAssaultRifle",
            futureRangedWeaponName, encumbrance = 15f, takesTwoHands = true,
            avoidance = 7, isMetal = true, strengthRequired = 2, price = 6000)
    val laserAssaultRifle: SilmarWeaponType =
        SilmarWeaponType1(SilmarItemType1(laserPistolItemType, isTech = true),
            WeaponType1(laserPistolItemType, minDamage = 2, maxDamage = 10,
                isRanged = true, maxRange = TileDistance(40),
                attackType = SilmarAttackTypes.laserCell,
                ammoType = SilmarAmmoTypes.laserAssaultRifleCell.itemType, numAttacks = 4,
                damageForm = DamageForms.laser), PenetrationTypes.laser,
            isLaserWeapon = true, ammoCapacity = 32)

    /**
     * The flyweight magical melee weapon types.
     */
    val clubOfImpact = WeaponType1.createMagicalVersion(WeaponTypes.club,
        (WeaponTypes.club.itemType as ItemType1).createMagicalVersion(4)
            .copy(encumbrance = 7f, takesTwoHands = true, avoidance = 10, isMetal = true,
                strengthRequired = 16, unidentifiedName = "strange club"), 4,
        "club of impact +4", price = 5000,
        info = "Knocks opponents back according to the wielder's strength.  Those knocked into something take further damage.")

    val maceOfPurity = WeaponType1.createMagicalVersion(WeaponTypes.mace, plus = 3,
        name = "mace of purity +3", price = 3000,
        info = "Does massive extra damage against living dead creatures.")
    val staffOfHarming = WeaponType1.createMagicalVersion(WeaponTypes.staff, plus = 3,
        name = "staff of harming +3", price = 1000, info = ItemType1.degradesInfo)

    /**
     * The flyweight magical ranged weapon types.
     */
    val slingOfHoming =
        WeaponType1.createMagicalVersion(WeaponTypes.sling.copy(toHitModifier = 40),
            (WeaponTypes.sling.itemType as ItemType1).createMagicalVersion(5)
                .copy(unidentifiedName = "magnificent sling",
                    frequency = WeaponTypes.sling.itemType.entityType.frequency / 5), 0,
            "sling of homing", price = 4000, info = "Always hits.")

    val strengthBow = WeaponType1.createMagicalVersion(WeaponTypes.bow, plus = 3,
        name = "strength bow +3", price = 5000,
        info = "Lets the wielder add his strength-related bonuses to-hit and to damage.")
    val crossbowOfQuickness =
        WeaponType1.createMagicalVersion(WeaponTypes.crossbow, plus = 3,
            name = "crossbow of quickness +3", price = 3000, numAttacks = 2)
}

/**
 * The name shown for unidentified range weapons from the future.
 */
const val futureRangedWeaponName = "strange ranged weapon"