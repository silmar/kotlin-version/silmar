package silmar.contexts.gameplay.entities.items.specializations

import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmarengine.contexts.gameplay.entities.items.Item1
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.util.math.randomInt

class RingOfPowerStorage(condition: ItemCondition) :
    Item1(SilmarMagicItemTypes.ringOfPowerStorage.itemType, condition) {
    /**
     * Informs this item that it has supplied the given amount of magic points.
     */
    fun onSuppliedMagicPoints(amount: Int) {
        // if we choose a random # that's less than the amount of points supplied
        if (randomInt(1, 15) <= amount) {
            degrade()
        }
    }
}