package silmar.contexts.gameplay.entities.items.types

import silmarengine.contexts.gameplay.entities.items.types.ItemType1
import silmarengine.contexts.gameplay.entities.items.types.getFrequencyForValue
import silmarengine.contexts.gameplay.entities.items.types.noAttributeModifiers
import silmarengine.contexts.gameplay.entities.items.types.nonGroupableItemTypes
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

fun createSilmarMagicItemType(name: String, imageName: String, unidentifiedName: String?,
    encumbrance: Float, isMustEquip: Boolean, equipLocation: EquipLocation?,
    avoidance: Int, isMetal: Boolean, price: Int, isCursed: Boolean, info: String?,
    defenseModifiers: IntArray = noDefenseModifiers, avoidanceModifier: Int = 0,
    attributeModifiers: IntArray? = null): SilmarItemType1 {
    val itemType = ItemType1(name, imageName, unidentifiedName = unidentifiedName,
        frequency = getFrequencyForValue(price),
        encumbrance = encumbrance, isMustEquip = isMustEquip,
        equipLocation = equipLocation, avoidance = avoidance, isMetal = isMetal,
        price = price, isCursed = isCursed, info = info, isMagical = true,
        defenseModifier = defenseModifiers[0],
        avoidanceModifier = avoidanceModifier,
        attributeModifiers = attributeModifiers ?: noAttributeModifiers)
    val result =
        SilmarItemType1(itemType, isTech = false, defenseModifiers = defenseModifiers)
    itemType.register()
    nonGroupableItemTypes.add(itemType)
    return result
}
