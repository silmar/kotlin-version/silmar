package silmar.contexts.gameplay.entities.items.types

import silmarengine.contexts.gameplay.entities.items.types.*

fun createSilmarAmmoType(name: String, imageName: String, frequency: Int,
    unidentifiedName: String?, encumbrance: Float, chanceOfInChest: Float,
    quantityToCreate: Int, price: Int, avoidance: Int,
    isTech: Boolean = true): SilmarItemType1 {
    val itemType =
        ItemType1(name, imageName, frequency = frequency, pluralName = "${name}s",
            unidentifiedName = unidentifiedName, isGroupable = true,
            encumbrance = encumbrance, chanceOfInChest = chanceOfInChest,
            quantityToCreate = quantityToCreate, price = price, avoidance = avoidance)
    val result = SilmarItemType1(itemType, isTech = isTech)
    itemType.register()
    ammoTypes.add(itemType)
    return result
}

/**
 * The name shown for unidentified ammo from the future.
 */
const val futureAmmoName = "strange ammo"
