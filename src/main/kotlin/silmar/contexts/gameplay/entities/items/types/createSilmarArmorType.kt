package silmar.contexts.gameplay.entities.items.types

import silmarengine.contexts.gameplay.entities.items.types.ItemType1
import silmarengine.contexts.gameplay.entities.items.types.armorTypes
import silmarengine.contexts.gameplay.entities.items.types.getFrequencyForValue
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

fun createSilmarArmorType(name: String, imageName: String?, unidentifiedName: String?,
    encumbrance: Float, avoidance: Int, defenseBonusNormal: Int, defenseBonusFirearm: Int,
    defenseBonusLaser: Int, avoidanceModifier: Int, isMetal: Boolean, isNoisy: Boolean,
    strengthRequired: Int, price: Int, isTech: Boolean): SilmarItemType1 {
    val itemType = ItemType1(name, imageName,
        frequency = getFrequencyForValue(price),
        unidentifiedName = unidentifiedName,
        equipLocation = EquipLocation.BODY,
        encumbrance = encumbrance, avoidance = avoidance,
        defenseModifier = defenseBonusNormal,
        avoidanceModifier = avoidanceModifier, isMetal = isMetal, isNoisy = isNoisy,
        strengthRequired = strengthRequired, price = price)
    val result = SilmarItemType1(itemType, isTech = isTech,
        defenseModifiers = intArrayOf(defenseBonusNormal, defenseBonusFirearm,
            defenseBonusLaser))
    itemType.register()
    armorTypes.add(itemType)
    return result
}
