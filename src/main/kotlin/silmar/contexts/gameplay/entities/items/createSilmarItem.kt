package silmar.contexts.gameplay.entities.items

import silmar.contexts.gameplay.entities.items.specializations.LandMine
import silmar.contexts.gameplay.entities.items.specializations.RingOfPowerStorage
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmar.contexts.gameplay.entities.items.types.SilmarTechItemTypes
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.types.ItemType

fun createSilmarItem(type: ItemType, condition: ItemCondition): Item? =
    when (type) {
        SilmarTechItemTypes.landMine.itemType -> LandMine(condition)
        SilmarMagicItemTypes.ringOfPowerStorage.itemType -> RingOfPowerStorage(condition)
        else -> null
    }
