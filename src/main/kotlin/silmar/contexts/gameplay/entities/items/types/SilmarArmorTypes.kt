package silmar.contexts.gameplay.entities.items.types

object SilmarArmorTypes {
    val plasticArmor =
        createSilmarArmorType("plastic armor", "plasticArmor", 
            futureArmorName, 20f, 6, 7,
            1, 1, 1, false, true, 7, 3000, true)
    val kevlarArmor =
        createSilmarArmorType("kevlar armor", "kevlarArmor", 
            futureArmorName, 20f, 7, 5,
            5, 1, 1, false, false, 7, 3500, true)
    val heavyKevlarArmor =
        createSilmarArmorType("heavy kevlar armor", "heavyKevlarArmor", 
            futureArmorName,
            35f, 6, 7, 7, 1, 2, false, false, 11, 5000, true)
    val reflecArmor =
        createSilmarArmorType("reflec armor", "reflecArmor", 
            futureArmorName, 25f, 6, 6,
            6, 6, 2, false, false, 8, 7500, true)
    val heavyReflecArmor =
        createSilmarArmorType("heavy reflec armor", "heavyReflecArmor", 
            futureArmorName,
            40f, 5, 9, 9, 9, 3, false, false, 12, 15000, true)
    val poweredReflecArmor =
        createSilmarArmorType("powered reflec armor", "poweredReflecArmor", 
            futureArmorName, 20f, 4, 11, 11, 11, 4, true, true, 7, 26000, true)
}

/**
 * The name shown for unidentified armor from the future.
 */
const val futureArmorName = "strange armor"