package silmar.contexts.gameplay.entities.items.types

import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

object SilmarTechItemTypes {
    val leadLinedCloak =
        createSilmarTechItemType("lead-lined cloak", "cape", "cloak", 10f, true,
            EquipLocation.BACK, 14, true, 2000, false, "Shields from radiation.")
    val landMine =
        createSilmarTechItemType("land mine", "landMine", "strange device", 3f, false,
            null, 9, true, 600, false,
            "Becomes an invisible part of its location.  Is set off the first time a being walks over it.",
            isIndividuallyIdentified = false)
}