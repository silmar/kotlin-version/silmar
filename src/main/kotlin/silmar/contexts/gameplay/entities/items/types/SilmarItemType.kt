package silmar.contexts.gameplay.entities.items.types

import silmarengine.contexts.gameplay.entities.items.types.ItemSubType

interface SilmarItemType : ItemSubType {
    /**
     * Whether an item of this type is technological in nature.
     */
    val isTech: Boolean

    /**
     * The modifiers to defense added by an item of this type versus the
     * various penetration levels (see their declaration order in the
     * PenetrationType class for their order within this array).
     */
    val defenseModifiers: IntArray
}

/**
 * Set as an item type's defense modifiers when that type offers none.
 */
val noDefenseModifiers = intArrayOf(0, 0, 0)
