package silmar.contexts.gameplay.entities.items.weapons

import silmar.contexts.gameplay.entities.beings.extensions.location.knockBack
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeParalyzed
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributeModifierProvider
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.receiveMessageIfInLOSOf
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.items.ItemCondition
import silmarengine.contexts.gameplay.entities.items.weapons.Weapon1
import silmarengine.util.math.randomInt
import kotlin.math.max

class SilmarWeapons {
    class ClubOfImpact(condition: ItemCondition) :
        Weapon1(SilmarWeaponTypes.clubOfImpact, condition) {
        override fun onHitVictim(victim: Being, wielder: Being, damage: Int) {
            if (victim.isDead) return

            // knock the victim back
            val provider = PlayerAttributeModifierProvider
            victim.knockBack(wielder.location, damage,
                if (wielder is Player) provider.damageModifierForStrength(
                    wielder.attributeValues.strength)
                else 0)
        }
    }

    class EnergySaber(condition: ItemCondition) :
        Weapon1(SilmarWeaponTypes.energySaber.weaponType, condition) {
        override fun onHitVictim(victim: Being, wielder: Being, damage: Int) {
            // if it's time to degrade
            if (randomInt(1, 10) == 1) {
                degrade()
            }
        }
    }

    class StunWhip(condition: ItemCondition) :
        Weapon1(SilmarWeaponTypes.stunWhip.weaponType, condition) {
        override fun onHitVictim(victim: Being, wielder: Being, damage: Int) {
            // if the victim is still alive, it is paralyzed
            if (!victim.isDead) victim.becomeParalyzed(max(1, 6 - victim.powerRating / 2))

            // if it's time to degrade
            if (randomInt(1, 10) == 1) {
                degrade()
            }
        }
    }

    class StaffOfHarming(condition: ItemCondition) :
        Weapon1(SilmarWeaponTypes.staffOfHarming, condition) {
        override fun onHitVictim(victim: Being, wielder: Being, damage: Int) {
            // if the victim is still alive, it takes extra damage
            if (!victim.isDead) victim.takeDamage(
                Damage(randomInt(2, 27), DamageForms.holyPower))

            // if it's time to degrade
            if (randomInt(1, 10) == 1) {
                degrade()
            }
        }
    }

    class MaceOfPurity(condition: ItemCondition) :
        Weapon1(SilmarWeaponTypes.maceOfPurity, condition) {
        override fun onHitVictim(victim: Being, wielder: Being, damage: Int) {
            // if the victim is a living dead monster, it takes extra damage
            if (!victim.isDead && victim is Monster && victim.type.isLivingDead) {
                victim.area.player?.receiveMessageIfInLOSOf(victim.location, "Holy might")
                victim.takeDamage(Damage(randomInt(3, 20), DamageForms.holyPower))
            }
        }
    }
}
