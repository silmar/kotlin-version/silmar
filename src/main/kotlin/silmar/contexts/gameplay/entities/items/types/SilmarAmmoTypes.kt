package silmar.contexts.gameplay.entities.items.types

object SilmarAmmoTypes {
    val pistolBullet =
        createSilmarAmmoType("pistol bullet", "pistolBullet", 5, futureAmmoName, 0.1f,
            0.7f, 10, 3, 8)
    val shotgunShell =
        createSilmarAmmoType("shotgun shell", "shotgunShell", 3, futureAmmoName, 0.3f,
            0.7f, 10, 5, 10)
    val assaultRifleBullet =
        createSilmarAmmoType("assault rifle bullet", "assaultRifleBullet", 3,
            futureAmmoName, 0.2f, 0.7f, 10, 5, 8)
    val laserPistolCell =
        createSilmarAmmoType("laser pistol cell", "laserPistolCell", 1, futureAmmoName,
            0.1f, 0.7f, 10, 7, 8)
    val laserAssaultRifleCell =
        createSilmarAmmoType("laser assault rifle cell", "laserAssaultRifleCell", 1,
            futureAmmoName, 0.2f, 0.07f, 10, 10, 8)
}