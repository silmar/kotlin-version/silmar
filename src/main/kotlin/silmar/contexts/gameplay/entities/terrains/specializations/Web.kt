package silmar.contexts.gameplay.entities.terrains.specializations

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Being
import silmar.contexts.gameplay.entities.beings.extensions.reactions.onStuckInWeb
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued

/**
 * A web that is considered strung from ceiling to floor.
 */
class Web : Terrain1() {

    /**
     * The being (if any) which spun this web.
     */
    private var spinner: Being? = null

    /**
     * The game turn when the current being stuck in this web (if any) got stuck.
     */
    private var turnBeingFirstStuck = 0

    override fun onBeingArrival(being: Being): Boolean {
        // this web will not affect its spinner
        if (being == spinner) return false

        // this web will not affect creatures that move ethereally
        if (being.movementType == MovementType.ETHEREAL) return false

        area.onSoundIssued(SilmarSounds.inWeb, location)

        being.onStuckInWeb(this, 0)

        return true
    }

    override fun onBeingHere(being: Being) {
        // this web will not affect its spinner
        if (being == spinner) return

        // this web will not affect creatures that move ethereally
        if (being.movementType == MovementType.ETHEREAL) return

        // if a being has yet to be stuck in this web
        val game = Game.currentGame
        if (turnBeingFirstStuck == 0) {
            // remember this turn as that on which the being got stuck
            turnBeingFirstStuck = game!!.turn
        }

        // tell the being how long it's been stuck in the web, so it can see if it breaks
        // free (which will destroy this web)
        val turnsStuck = game!!.turn - turnBeingFirstStuck
        being.onStuckInWeb(this, turnsStuck)
    }

    fun setSpinner(spinner: Being) {
        this.spinner = spinner
    }

    override fun isHarmfulTo(monster: Monster): Boolean {
        // the only way a web wouldn't be considered harmful is if the monster is
        // moving ethereally
        return monster.movementType != MovementType.ETHEREAL
    }
}