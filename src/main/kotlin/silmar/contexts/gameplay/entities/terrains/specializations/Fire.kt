package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.arealighting.domain.model.LightSource
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.TileDistance
import silmarengine.util.math.randomInt

private typealias Types = SilmarMonsterTypes

class Fire : Terrain1(), LightSource {
    override val isEmittingLight: Boolean
        get() = true

    override fun onBeingArrival(being: Being): Boolean {
        being.takeDamage(Damage(randomInt(1, 6), DamageForms.fire))
        return false
    }

    override fun onBeingHere(being: Being) {
        onBeingArrival(being)
    }

    /**
     * How far fires shed light.
     */
    override val lightRadius = PixelDistance(TileDistance(7))

    override fun isHarmfulTo(monster: Monster): Boolean {
        val type = monster.type
        return (type != Types.dragonman && type != Types.atoman && type != Types.redDragon && type != Types.ferrousGolem)
    }
}