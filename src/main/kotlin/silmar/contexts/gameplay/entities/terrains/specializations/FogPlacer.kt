package silmar.contexts.gameplay.entities.terrains.specializations

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.sounds.SilmarSounds
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.areas.extensions.geometry.asPixelLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesAt
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesOccurred
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesToOccur
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.MutableTilePoint
import silmarengine.util.math.randomInt

/**
 * Controls the creation and removal of death-fog terrains in the region of the area
 * defined by the box that is all in LOS to the lower right of this placer.
 */
class FogPlacer : Terrain1() {

    override fun act() {
        // inform the area that a group of changes is about to occur
        area.onGroupOfChangesToOccur()

        // for each location across from this terrain, until we hit a location whose
        // tile blocks los
        val placerTileLocation = location.asTileLocation()
        val spot = MutableTilePoint(placerTileLocation)
        while (!area.getTile(spot).blocksLOS) {
            // for each location up from this terrain, until we hit a location whose
            // tile blocks los
            spot.translate(0, -1)
            while (!area.getTile(spot).blocksLOS) {
                processFogSpot(spot)
                spot.translate(0, -1)
            }

            // for each location down from this terrain, until we hit a location whose
            // tile blocks los
            spot.set(spot.x, placerTileLocation.y)
            while (!area.getTile(spot).blocksLOS) {
                processFogSpot(spot)
                spot.translate(0, 1)
            }
            spot.set(spot.x + 1, placerTileLocation.y)
        }

        // inform the area that a group of changes has now occurred
        area.onGroupOfChangesOccurred()
    }

    /**
     * Has this fog-placer decide whether to add or remove (or leave alone) the given
     * spot.
     */
    private fun processFogSpot(spot: TilePoint) {
        // for each death-fog terrain at the given fog-spot
        val pixelSpot = MutablePixelPoint()
        spot.asPixelLocation(pixelSpot)
        val terrains = getEntitiesAt(pixelSpot, area.terrains)
        var fogRemoved = false
        terrains.filter {
            it.isOfType(SilmarTerrainTypes.deathFog)
        }.forEach forEach@{
            // if a random chance says yes
            if (randomInt(1, 2) == 1) {
                // remove this death-fog
                area.removeEntity(it)
                fogRemoved = true
                return@forEach
            }
        }

        // if a death-fog wasn't removed above, and a random chance says yes
        if (!fogRemoved && randomInt(1, 2) == 1) {
            // put a weakened death-fog here
            val fog = createTerrain(SilmarTerrainTypes.deathFog) as DeathFog
            fog.setStrength(SilmarMonsterTypes.ferrousGolem.powerRating / 5)
            area.addEntity(fog, pixelSpot)

            // only once per so many death-fogs added, so that not too many death-fog
            // sounds are played at once
            if (randomInt(1, 50) == 1) {
                // issue a death-fog sound
                area.onSoundIssued(SilmarSounds.deathFog, pixelSpot)
            }
        }
    }
}