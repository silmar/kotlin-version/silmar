package silmar.contexts.gameplay.entities.terrains.types

import silmarengine.contexts.gameplay.entities.terrains.types.createTerrainType

@Suppress("unused")
object SilmarTerrainTypes {
    /**
     * The flyweight terrain-types for special terrains.
     */
    val radiation =
        createSilmarTerrainType("radiation", "radiation", true, 1, false, false)
    val upExit = createSilmarTerrainType("up exit", "upExit", true, 0, false, false)
    val downExit = createSilmarTerrainType("down exit", "downExit", true, 0, false, false)
    val boulder = createSilmarTerrainType("boulder", "boulder", true, 1, false, false)
    val web = createSilmarTerrainType("web", "web", true, 0, false, false, 10)
    val vengefulSymbol =
        createSilmarTerrainType("vengeful symbol", "vengefulSymbol", true, 0, false,
            false, 15)
    val deathFog =
        createSilmarTerrainType("death fog", "deathFog", true, 0, false, false, 18)
    val fire = createSilmarTerrainType("fire", "fire", true, 0, false, false, 18)
    val trollCarcass =
        createSilmarTerrainType("troll carcass", null, true, 0, false, false, 15)
    val landMine = createSilmarTerrainType("land mine", null, true, 0, false, false)
    val crusher = createSilmarTerrainType("crusher", null, true, 0, false, false)
    val fogPlacer = createSilmarTerrainType("fog placer", null, true, 0, false, false)

    /**
     * The flyweight terrain-types for encounters.
     */
    val fountain =
        createSilmarTerrainType("fountain", "fountain", true, 1, true, false, 6)
    val statue = createSilmarTerrainType("statue", "statue", true, 1, true, false, 2)
    val garden = createSilmarTerrainType("garden", "garden", true, 1, false, false, 12)
    val library = createSilmarTerrainType("library", "library", true, 1, false, false, 12)
    val sphinx = createSilmarTerrainType("sphinx", "sphinx", true, 1, false, false, -6)
    val mirroredBall =
        createSilmarTerrainType("mirrored ball", "mirroredBall", true, 1, false, false,
            14)
    val mine = createSilmarTerrainType("mine", "mine", true, 1, true, false, -10)
    val moatLever =
        createSilmarTerrainType("moat lever", "moatLever", true, 1, false, false, 8)
    val repairMachine =
        createSilmarTerrainType("repair machine", "repairMachine", true, 1, false, false,
            8, isTalker = true, isFixer = true)
    val vendingMachine =
        createSilmarTerrainType("vending machine", "vendingMachine", true, 4, false,
            false, 8, isTalker = true, isSeller = true)
    val brokenVendingMachine =
        createSilmarTerrainType("broken vending machine", "vendingMachine", true, 0,
            false, false, 8, isTalker = true)
    val robotCommandInterface =
        createSilmarTerrainType("robot command interface", "robotCommandInterface", true,
            4, false, false, 12)
    val holographicTrainer =
        createSilmarTerrainType("holographic trainer", "holographicTrainer", true, 2,
            false, false, 14)

    /**
     * The flyweight terrain-types for decorations.
     */
    val vacuumHole =
        createSilmarTerrainType("vacuum hole", "vacuumHole", true, 0, false, false)
    val machinery1 =
        createSilmarTerrainType("machinery 1", "machinery1", false, 0, false, false, 5)
    val machinery2 =
        createSilmarTerrainType("machinery 2", "machinery2", false, 0, false, false, 5)

    /**
     * The flyweight terrain-types for traps.
     */
    val teleportTrap = createTerrainType("teleport trap", null, true, 1, false, true)
    val fireballTrap =
        createSilmarTerrainType("fireball trap", null, true, 2, false, true)
    val gasserTrap = createSilmarTerrainType("gasser trap", null, true, 1, false, true)
    val lightningBoltTrap =
        createSilmarTerrainType("lightning bolt trap", null, true, 2, false, true)
}