package silmar.contexts.gameplay.entities.terrains.traps.specializations

import silmarengine.contexts.gameplay.entities.beings.extensions.location.teleportRandomlyWithinArea
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap1

class TeleportTrap : Trap1() {
    override fun spring(player: Player) {
        player.teleportRandomlyWithinArea()
    }
}