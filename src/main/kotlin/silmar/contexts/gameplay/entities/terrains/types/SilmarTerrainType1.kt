package silmar.contexts.gameplay.entities.terrains.types

import silmar.contexts.gameplay.areas.AreaLevels
import silmar.contexts.gameplay.areas.SilmarArea
import silmar.contexts.gameplay.areas.SilmarAreaMotif
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainType1
import silmarengine.contexts.gameplay.areas.Area

class SilmarTerrainType1(
    name: String, imageName: String?, isPassible: Boolean, frequency: Int,
    firesPlayerAtEvents: Boolean, isTrap: Boolean, avoidance: Int, isTalker: Boolean,
    isSeller: Boolean, isFixer: Boolean
) : TerrainType1(name, imageName, isPassible, frequency, firesPlayerAtEvents, isTrap,
    avoidance, isTalker, isSeller, isFixer) {
    override fun isValidForArea(area: Area): Boolean {
        // don't let boulders appear before the ruins level, since the player will likely not have gained the
        // giant level before then
        area as SilmarArea
        if (equals(SilmarTerrainTypes.boulder)) {
            if (area.level!!.index < AreaLevels.ruins1.index) return false
        }

        // if this type is one of the ones that is of a technological nature
        val motif = area.motif as SilmarAreaMotif
        if (equals(SilmarTerrainTypes.vendingMachine) || equals(
                SilmarTerrainTypes.radiation) || equals(
                SilmarTerrainTypes.repairMachine) || equals(
                SilmarTerrainTypes.robotCommandInterface) || equals(
                SilmarTerrainTypes.holographicTrainer)) {
            // if the given area isn't of a tech motif, this type isn't valid
            if (!motif.isTech) return false

            // don't allow the futuristic types to appear before the base level
            if (equals(SilmarTerrainTypes.robotCommandInterface) || equals(
                    SilmarTerrainTypes.holographicTrainer)) {
                if (area.level!!.index < AreaLevels.base1.index) return false
            }
        }
        // else, if this type doesn't represent a trap
        else if (!isTrap) {
            // if the given area *is* of a tech motif, this type isn't valid
            if (motif.isTech) return false
        }

        // spread out the introduction of the more damaging trap types across the dungeon
        // levels; don't allow gust-of-wind traps on self-lit levels, where they are
        // useless
        return when {
            equals(
                SilmarTerrainTypes.fireballTrap) && area.level!!.index < AreaLevels.swamp1.index -> false
            equals(
                SilmarTerrainTypes.lightningBoltTrap) && area.level!!.index < AreaLevels.city1.index -> false
            equals(
                SilmarTerrainTypes.gasserTrap) && area.level!!.index < AreaLevels.dungeonB1.index -> false
            else -> super.isValidForArea(area)
        }
    }
}