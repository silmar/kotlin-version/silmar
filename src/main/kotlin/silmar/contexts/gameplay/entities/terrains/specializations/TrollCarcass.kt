package silmar.contexts.gameplay.entities.terrains.specializations

import silmar.contexts.gameplay.entities.beings.monsters.specializations.Troll
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.tiles.tileWidth

/**
 * When a troll dies, this is left behind, to be reanimated shortly thereafter.
 */
class TrollCarcass : Terrain1() {

    /**
     * The troll for which this is a carcass.
     */
    private lateinit var troll: Monster

    /**
     * The game turn in which this carcass was created.
     */
    private var turnCreated = 0

    /**
     * How many game turns it will take for this carcass to become a troll again.
     */
    private var duration: Int = 0

    fun setTroll(troll: Monster) {
        this.troll = troll

        // set this carcass's duration to how many turns it would take the troll to
        // regenerate how many hit points below 1 it is
        duration = (1 - troll.hitPoints) / Troll.regenerationRate + 1
    }

    override fun act() {
        val game = Game.currentGame
        if (turnCreated == 0) {
            turnCreated = game!!.turn
        }

        // if it's time for this carcass to turn back into a troll
        if (game!!.turn - turnCreated > duration) {
            // add the troll back into its area at a location near this carcass
            val trollLocation =
                area.getNearbyPassibleRectangleLocation(location, size, movementType,
                    PixelDistance(2 * tileWidth)) ?: return
            troll.moveTo(trollLocation)
            area.addEntity(troll, trollLocation)

            // heal the troll such that it is just alive
            troll.takeHealing(1 - troll.hitPoints)

            // remove this carcass from the area
            area.removeEntity(this)
        }
    }
}