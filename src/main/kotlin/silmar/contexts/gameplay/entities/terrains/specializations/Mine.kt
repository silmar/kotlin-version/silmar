package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.monsters.createMonster
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.entities.items.createItemEntity
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.entities.items.types.TreasureTypes
import silmarengine.util.math.randomInt
import silmarengine.util.math.randomBoolean

class Mine : Terrain1() {
    /**
     * The random value determining what kind of contents this mine holds (if any).
     */
    private val contentsRoll = randomInt(1, 10)

    /**
     * Whether this mine has been visited before.
     */
    private var visited = false

    /**
     * Informs this mine that the given player has entered it.
     */
    fun onPlayerEnters(player: Player) {
        // if this mine has already been visited
        if (visited) {
            // it has no further effect
            player.onMessage("This mine is empty.")
            return
        }

        visited = true

        // produce what is in the mine (if anything)
        when (contentsRoll) {
            in (1..4) -> player.onMessage("This mine is empty.")
            in (5..7) -> {
                player.onMessage("You find a precious gemstone!", MessageType.important)

                // create a new precious gemstone item nearby
                val item = createItem(gemstoneTypes[randomInt(0, gemstoneTypes.size - 1)])
                val itemEntity = createItemEntity(item)
                val nearby =
                    area.getNearbyPassibleRectangleLocation(location, itemEntity.size,
                        MovementType.WALKING_NO_HANDS) ?: location
                area.addEntity(itemEntity, nearby)
            }
            else -> {
                player.onMessage("This mine is the lair of a powerful creature!",
                    MessageType.important)

                // create a new acid dragon or giant worm nearby
                val type = if (randomBoolean) SilmarMonsterTypes.giantWorm
                else SilmarMonsterTypes.acidDragon
                val monster = createMonster(type)
                val nearby =
                    area.getNearbyPassibleRectangleLocation(location, monster.size,
                        monster.movementType) ?: location
                area.addEntity(monster, nearby)
            }
        }
    }

    companion object {
        /**
         * The gemstone types which may be produced by this mine.
         */
        private val gemstoneTypes =
            arrayOf(TreasureTypes.emerald, TreasureTypes.sapphire, TreasureTypes.ruby,
                TreasureTypes.diamond, TreasureTypes.jacinthGemstone,
                TreasureTypes.largeEmerald, TreasureTypes.largeSapphire,
                TreasureTypes.largeRuby, TreasureTypes.largeDiamond,
                TreasureTypes.largeJacinthGemstone)
    }
}