package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.entities.items.createItemEntity
import silmarengine.contexts.gameplay.entities.items.types.randomItemType
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation

class Statue : Terrain1() {

    /**
     * Whether this statue has been used already.
     */
    var isUsed = false
        private set

    fun onGoldInserted(gold: Int) {
        isUsed = true

        // while we have yet to create an item
        var item: Item? = null
        while (item == null) {
            // choose a non-groupable type at random from all that exist
            val type = randomItemType
            if (type.isGroupable) continue

            // if the value of the chosen type is at most
            // 50 times what was inserted
            if (type.price <= 50 * gold) {
                item = createItem(type)
            }
        }

        // determine where the item will be placed
        val itemEntity = createItemEntity(item)
        val location =
            area.getNearbyPassibleRectangleLocation(location, itemEntity.size,
                MovementType.WALKING_NO_HANDS) ?: location

        area.performPowerDischargeEffect(location)

        // add the item to the area
        area.addEntity(itemEntity, location)
    }
}