package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.areas.extensions.effects.performTeleportEffect
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.isMorePowerfulThan
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.ItemValueContext
import silmarengine.contexts.gameplay.entities.items.extensions.getValue
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation

class Sphinx : Terrain1() {

    /**
     * Whether this sphinx has been visited before.
     */
    private var visited = false

    override fun onBeingArrival(being: Being): Boolean {
        if (being !is Player) return false

        // if this sphinx has already been visited
        if (visited) {
            // it is silent
            being.onMessage("This sphinx is silent.")
            return false
        }

        visited = true

        performPowerDischargeEffect(this)

        // if the player's judgement is high enough
        if (being.attributeValues.judgement > 10) {
            // if there are no items in the area
            val itemEntities = area.items
            if (itemEntities.isEmpty()) {
                being.onMessage("This sphinx is silent.")
                return false
            }

            // teleport the player to the most valuable item
            val mostValuable = itemEntities.reduce { acc, itemEntity ->
                if (itemEntity.item.getValue(ItemValueContext.SELL) > acc.item.getValue(
                        ItemValueContext.SELL)) itemEntity
                else acc
            }
            being.onMessage(
                "The sphinx bellows, \"You have shown good judgement.  Reap thy reward!\"",
                MessageType.important)
            area.performTeleportEffect(being.location, true, false)
            val nearby =
                area.getNearbyPassibleRectangleLocation(mostValuable.location, being.size,
                    being.movementType)
            area.moveEntity(being, nearby!!)
            area.performTeleportEffect(nearby, false, false)

            return true
        }
        else {
            // if there are no monsters in the area
            val monsters = area.monsters
            if (monsters.isEmpty()) {
                being.onMessage("This sphinx is silent.")
                return false
            }

            // teleport the player to the most powerful monster
            val mostPowerful = monsters.reduce { acc, monster ->
                if (monster.isMorePowerfulThan(acc)) monster else acc
            }
            being.onMessage(
                "The sphinx bellows, \"I have found you lacking in judgement.  Face thy enemy!\"",
                MessageType.important)
            area.performTeleportEffect(being.location, true, false)
            val nearby =
                area.getNearbyPassibleRectangleLocation(mostPowerful.location, being.size,
                    being.movementType)
            area.moveEntity(being, nearby!!)
            area.performTeleportEffect(nearby, false, false)

            return true
        }
    }
}