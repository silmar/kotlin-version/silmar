package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.incrementBaseAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.terrains.Terrain1

class MirroredBall : Terrain1() {

    /**
     * Whether this mirrored-ball has been visited before.
     */
    private var visited = false

    override fun onInteractionWithPlayer(player: Player): Boolean {
        // if this mirrored-ball has already been visited
        if (visited) {
            // it has no further effect
            player.onMessage(
                "You see nothing but your own reflection in this mirrored ball.",
                MessageType.important)
            return false
        }

        performPowerDischargeEffect(this)
        visited = true

        // affect the player
        player.onMessage(
            "This mirrored ball shows you some of your mistakes in the near future, providing you with the judgement to not make them in the first place!",
            MessageType.important)
        player.incrementBaseAttribute(PlayerAttributes.judgement)

        return true
    }
}