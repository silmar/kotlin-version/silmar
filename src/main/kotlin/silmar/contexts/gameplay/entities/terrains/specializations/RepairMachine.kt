package silmar.contexts.gameplay.entities.terrains.specializations

import silmar.contexts.gameplay.entities.items.types.SilmarItemType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.talkers.ItemFixer
import silmarengine.contexts.gameplay.entities.terrains.Terrain1

class RepairMachine : Terrain1(), ItemFixer {
    override val greeting: String
        get() = "This machine whirs to life: \"What may I fix for you?\""

    override fun getAdjustedItemPrice(item: Item, normalPrice: Int): Int {
        return normalPrice
    }

    override fun getCanFixItem(item: Item): Boolean {
        val type = item.type
        val subType = type.subType
        return subType is SilmarItemType && subType.isTech && !type.isGroupable
    }

    override fun onTalkedTo() {}
    override fun onGreetingHeardByPlayer(player: Player) {}
}