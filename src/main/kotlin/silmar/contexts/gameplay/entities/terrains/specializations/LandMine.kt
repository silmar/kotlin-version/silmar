package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmar.contexts.gameplay.areas.extensions.effects.performFireballEffect
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.sounds.Sounds
import silmarengine.util.sleepThread

class LandMine : Terrain1() {
    override fun onBeingArrival(being: Being): Boolean {
        area.onSoundIssued(Sounds.trapSprung, location)
        sleepThread(200)
        area.performFireballEffect(null, location, 7)
        area.removeEntity(this)

        return true
    }
}