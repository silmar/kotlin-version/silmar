package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.becomeParalyzed
import silmar.contexts.gameplay.entities.beings.monsters.extensions.isRobot
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS

class RobotCommandInterface : Terrain1() {

    /**
     * Returns whether the given player is capable of using this interface
     */
    private fun getCanPlayerUseThis(player: Player): Boolean {
        // return whether the player's intelligence is high enough
        return player.attributeValues.intelligence >= 15
    }

    override fun onBeingArrival(being: Being): Boolean {
        if (being !is Player) return false

        // if the player has a high enough intelligence
        if (getCanPlayerUseThis(being)) {
            being.onMessage(
                "With your high intelligence, you are able understand this command interface just enough to temporarily shut down nearby robots!",
                MessageType.important)
            onBeingHere(being)
        }
        else {
            being.onMessage(
                "Whatever this device is, it seems beyond your limited understanding...",
                MessageType.important)
        }

        return true
    }

    override fun onBeingHere(being: Being) {
        if (being !is Player) return

        // if the player can use this interface
        if (getCanPlayerUseThis(being)) {
            // each robot in los is paralyzed this turn
            val monsters = area.getEntitiesInLOS(location, area.monsters)
            monsters.filter { it.isRobot() }.forEach { it.becomeParalyzed(1) }
        }
    }
}