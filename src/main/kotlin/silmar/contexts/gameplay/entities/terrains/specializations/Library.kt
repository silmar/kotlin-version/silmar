package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.incrementBaseAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.terrains.Terrain1

class Library : Terrain1() {

    /**
     * Whether this library has been visited before.
     */
    private var visited = false

    override fun onBeingArrival(being: Being): Boolean {
        if (being !is Player) return false

        // if this library has already been visited
        if (visited) {
            // it has no further effect
            being.onMessage(
                "There's a lot of valuable knowledge in these books, but who has time to read them?",
                MessageType.important)
            return false
        }

        performPowerDischargeEffect(this)
        visited = true

        // affect the player
        being.onMessage(
            "This library imparts the knowledge it contains directly into your mind!",
            MessageType.important)
        being.incrementBaseAttribute(PlayerAttributes.intelligence)

        return true
    }
}