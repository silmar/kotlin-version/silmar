package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.extensions.geometry.getDistance
import silmarengine.contexts.gameplay.areas.extensions.geometry.getLineLocations
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesInLOS
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.PixelDistance
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileWidth
import kotlin.math.max
import kotlin.math.min

class VacuumHole : Terrain1() {
    override fun act() {
        performPowerDischargeEffect(this)

        area.onSoundIssued(SilmarSounds.vacuumHole, location, true)

        // for each being in los
        val beings = area.getEntitiesInLOS(location, area.beings)
        beings.forEach forEach@{ being ->
            // determine how far this vacuum hole can pull this being in this turn,
            // according to what the being says, and the range between the hole and the being
            val suckDistance = computeVacuumHoleSuckDistance(being)
            val beingLocation = PixelPoint(being.location)
            val range = getDistance(beingLocation, location)
            val maxRange = 25f * tileWidth
            suckDistance.distance = (suckDistance.distance * max(0f,
                (maxRange - range.distance) / maxRange)).toInt()

            // the hole should not pull this being past itself
            suckDistance.distance = min(suckDistance.distance, range.distance)

            // if the hole can't (or shouldn't) affect this being, skip it
            if (suckDistance.distance <= 0) return@forEach

            // if this being is already at the hole, skip it
            if (beingLocation == location) return@forEach

            if (being is Player) {
                being.onMessage("A vacuum hole sucks you towards it!")
            }

            // for each location between the being and this hole, up to the suck-distance
            val locations = getLineLocations(beingLocation, location, 2, suckDistance, 0)
            locations.forEach { spot ->
                // if the new location is impassible, we are done moving this being
                val beingSize = being.size
                if (!area.isRectanglePassible(spot, beingSize, MovementType.WALKING, true, being, null,
                        false).passible) return

                // move the victim to the new location
                area.moveEntity(being, spot)
            }
        }
    }

    /**
     * Note that the suck distance can't be as much as the being's max movement points
     * otherwise the being will get sucked in permanently.
     */
    private fun computeVacuumHoleSuckDistance(being: Being): PixelDistance {
        val base = if (being is Player) 4 else 5 - being.powerRating / 3
        return PixelDistance(
            min(being.maxMovementPoints - 1, max(base, 0)) * tileWidth)
    }
}