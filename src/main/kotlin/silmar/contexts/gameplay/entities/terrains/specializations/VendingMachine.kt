package silmar.contexts.gameplay.entities.terrains.specializations

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.areas.AreaLevels
import silmar.contexts.gameplay.areas.SilmarArea
import silmar.contexts.gameplay.entities.items.types.SilmarAmmoTypes
import silmar.contexts.gameplay.entities.items.types.SilmarTechItemTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.items.Item
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.entities.talkers.ItemSeller
import silmarengine.contexts.gameplay.entities.talkers.Talker
import silmarengine.contexts.gameplay.entities.talkers.extensions.createItemsForSaleFromTypes
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.util.math.randomInt

class VendingMachine : Terrain1(), ItemSeller {
    override val greeting: String
        get() = "This machine lights up, and then speaks: \"Please use correct change when purchasing items.  Thank you.\""

    override lateinit var itemsForSale: List<Item>
        private set

    override fun onPlacedInArea() {
        // sell either the items from the firearm-related list above, or the laser-related
        // list, depending on what area-level this vending machine is on
        itemsForSale = createItemsForSaleFromTypes(
            if ((area as SilmarArea).level!!.index < AreaLevels.moon1.index) fireArmItemTypesForSale
            else laserItemTypesForSale)
    }

    override fun onGreetingHeardByPlayer(player: Player) {}
    override fun onTalkedTo() {
        // if this machine breaks
        if (randomInt(1, 10) <= 7) {
            // replace this machine with a broken one
            area.removeEntity(this)
            area.addEntity(createTerrain(SilmarTerrainTypes.brokenVendingMachine),
                location)
        }
    }

    override fun getAdjustedItemPrice(item: Item, normalPrice: Int): Int {
        return normalPrice
    }

    companion object {
        /**
         * The types of items offered by this vending machine if it's on a contemporary-tech
         * area-level.
         */
        private val fireArmItemTypesForSale: Array<ItemType> =
            arrayOf(SilmarAmmoTypes.pistolBullet.itemType,
                SilmarAmmoTypes.shotgunShell.itemType,
                SilmarAmmoTypes.assaultRifleBullet.itemType,
                SilmarTechItemTypes.landMine.itemType)

        /**
         * The types of items offered by this vending machine if it's on a futuristic-tech
         * area-level.
         */
        private val laserItemTypesForSale: Array<ItemType> =
            arrayOf(SilmarAmmoTypes.laserPistolCell.itemType,
                SilmarAmmoTypes.laserAssaultRifleCell.itemType)
    }
}

class BrokenVendingMachine : Terrain1(), Talker {
    override val greeting: String
        get() = "This machine appears to be broken."

    override fun onGreetingHeardByPlayer(player: Player) {}
    override fun onTalkedTo() {}
}

