package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.decrementBaseAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.incrementBaseAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.extensions.effects.performPowerDischargeEffect
import silmarengine.util.math.randomBoolean

class Fountain : Terrain1() {
    /**
     * Whether this fountain has been used before.
     */
    private var used = false

    /**
     * Whether this fountain's effect upon drinking from it are beneficial (vs. harmful).
     */
    private var beneficial: Boolean = randomBoolean

    /**
     * Informs this fountain that the given player has just drank from it.
     */
    fun onPlayerDrinks(player: Player) {
        // if this fountain has already been used, it has no further effect
        if (used) {
            player.onMessage("Nothing happens.")
            return
        }
        used = true

        performPowerDischargeEffect(this)

        // affect the player
        if (beneficial) player.incrementBaseAttribute(PlayerAttribute.randomAttribute)
        else player.decrementBaseAttribute(PlayerAttribute.randomAttribute)
    }
}