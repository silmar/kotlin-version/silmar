package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.monsters.createMonster
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.items.createGroupableItem
import silmarengine.contexts.gameplay.entities.items.createItem
import silmarengine.contexts.gameplay.entities.items.createItemEntity
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.types.NonGroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.types.TreasureTypes
import silmarengine.util.math.randomInt

class Garden : Terrain1() {
    /**
     * The random value determining what kind of contents this garden holds.
     */
    private val contentsRoll = randomInt(1, 5)

    /**
     * Whether this garden has been visited before.
     */
    private var visited = false

    override fun onBeingArrival(being: Being): Boolean {
        if (being !is Player || visited) {
            return false
        }

        visited = true

        // if rations are to be produced
        when (contentsRoll) {
            in (1..3) -> {
                // create a new item of rations nearby
                val item = createGroupableItem(GroupableItemTypes.ration)
                val itemEntity = createItemEntity(item)
                val nearby =
                    area.getNearbyPassibleRectangleLocation(location, itemEntity.size,
                        MovementType.WALKING_NO_HANDS) ?: location
                area.addEntity(itemEntity, nearby)

                being.onMessage("You glean some food from this garden!",
                    MessageType.important)
            }
            4 -> {
                // create a new a item nearby
                val item = createItem(itemTypes[randomInt(0, itemTypes.size - 1)])
                val itemEntity = createItemEntity(item)
                val nearby =
                    area.getNearbyPassibleRectangleLocation(location, itemEntity.size,
                        MovementType.WALKING_NO_HANDS) ?: location
                area.addEntity(itemEntity, nearby)

                being.onMessage("You find an item buried in this garden!",
                    MessageType.important)
            }
            else -> {
                // create a new scum shamble nearby
                val monster = createMonster(SilmarMonsterTypes.scumShamble)
                val nearby =
                    area.getNearbyPassibleRectangleLocation(location, monster.size,
                        monster.movementType) ?: location
                area.addEntity(monster, nearby)

                being.onMessage("Hidden within this garden is a scum shamble!",
                    MessageType.important)
            }
        }

        return true
    }

    companion object {
        /**
         * The item-types which may be produced by this garden.
         */
        private val itemTypes = arrayOf(NonGroupableItemTypes.potionOfStrength,
            NonGroupableItemTypes.potionOfIntelligence,
            NonGroupableItemTypes.potionOfJudgement,
            NonGroupableItemTypes.potionOfAgility,
            NonGroupableItemTypes.potionOfEndurance, TreasureTypes.pearl,
            TreasureTypes.aquamarineGemstone, TreasureTypes.peridotGemstone,
            TreasureTypes.opalGemstone)
    }
}