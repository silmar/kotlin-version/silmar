package silmar.contexts.gameplay.entities.terrains.traps

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.getDisarmsTrap
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap

@Suppress("unused")
fun Trap.shouldNotSpring(player: Player): Boolean =
    (player as SilmarPlayer).getDisarmsTrap()