package silmar.contexts.gameplay.entities.terrains.extensions

import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmar.contexts.gameplay.areas.extensions.effects.performPoofEffect
import silmarengine.contexts.gameplay.entities.terrains.extensions.avoid

/**
 * Informs this terrain that it has been hit by a fireball of the given strength
 * (in terms of damage dice).
 */
fun Terrain.onHitByFireball(strength: Int) {
    // if this terrain fails to make its avoidance check
    val result = avoid(-strength * 2)
    if (!result.success) {
        onDestroyed(result.failedBy)

        // do a poof at this terrain's location
        area.performPoofEffect(location, size)

        // this terrain is destroyed; remove it from its area
        area.removeEntity(this)
    }
}

