package silmar.contexts.gameplay.entities.terrains.traps.specializations

import silmar.contexts.gameplay.areas.SilmarArea
import silmar.contexts.gameplay.areas.extensions.effects.performLightningBoltEffect
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap1
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.util.math.randomBoolean

class LightningBoltTrap : Trap1() {
    override fun spring(player: Player) {
        // launch a lightning bolt from this trap's location to a one of the
        // adjacent tile-locations
        val from = location
        val to = PixelPoint(
            from.x + if (randomBoolean) tileWidth else -tileWidth,
            from.y + if (randomBoolean) tileHeight else -tileHeight)
        area.performLightningBoltEffect(from, to,
            (area as SilmarArea).level!!.lightningBoltTrapStrength, true)
    }
}