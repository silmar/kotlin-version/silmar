package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmar.contexts.gameplay.areas.extensions.effects.performFireballEffect
import silmarengine.util.math.randomInt

class HolographicTrainer : Terrain1() {
    /**
     * Whether this trainer will malfunction when used.
     */
    private val willMalfunction = randomInt(1, 3) > 1

    /**
     * Whether this trainer has already functioned once.
     */
    private var functioned = false

    override fun onBeingArrival(being: Being): Boolean {
        if (being !is Player) return false

        // if this trainer hasn't already done one training
        if (!functioned) {
            if (!willMalfunction) {
                // train the player
                being.onMessage(
                    "This holographic trainer teaches you the next evolution in your path!",
                    MessageType.important)
                being.levelValues.gainExperienceLevel()
                functioned = true
            }
            else {
                // the trainer explodes; tell the player with a regular message
                // so he can see the explosion
                being.onMessage("This holographic trainer overloads!")
                area.performFireballEffect(null, location, 12)
                area.removeEntity(this)
            }

            return true
        }
        else {
            being.onMessage("This holographic trainer is burnt out.")
            return false
        }
    }
}