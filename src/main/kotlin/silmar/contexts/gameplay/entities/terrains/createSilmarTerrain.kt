package silmar.contexts.gameplay.entities.terrains

import silmar.contexts.gameplay.entities.terrains.specializations.*
import silmar.contexts.gameplay.entities.terrains.traps.specializations.FireballTrap
import silmar.contexts.gameplay.entities.terrains.traps.specializations.GasserTrap
import silmar.contexts.gameplay.entities.terrains.traps.specializations.LightningBoltTrap
import silmar.contexts.gameplay.entities.terrains.traps.specializations.TeleportTrap
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainType

fun createSilmarTerrain(type: TerrainType): Terrain? {
    return when (type) {
        SilmarTerrainTypes.trollCarcass -> TrollCarcass()
        SilmarTerrainTypes.web -> Web()
        SilmarTerrainTypes.repairMachine -> RepairMachine()
        SilmarTerrainTypes.fountain -> Fountain()
        SilmarTerrainTypes.statue -> Statue()
        SilmarTerrainTypes.garden -> Garden()
        SilmarTerrainTypes.library -> Library()
        SilmarTerrainTypes.sphinx -> Sphinx()
        SilmarTerrainTypes.mirroredBall -> MirroredBall()
        SilmarTerrainTypes.mine -> Mine()
        SilmarTerrainTypes.brokenVendingMachine -> BrokenVendingMachine()
        SilmarTerrainTypes.vendingMachine -> VendingMachine()
        SilmarTerrainTypes.fire -> Fire()
        SilmarTerrainTypes.landMine -> LandMine()
        SilmarTerrainTypes.robotCommandInterface -> RobotCommandInterface()
        SilmarTerrainTypes.holographicTrainer -> HolographicTrainer()
        SilmarTerrainTypes.moatLever -> MoatLever()
        SilmarTerrainTypes.vacuumHole -> VacuumHole()
        SilmarTerrainTypes.vengefulSymbol -> VengefulSymbol()
        SilmarTerrainTypes.deathFog -> DeathFog()
        SilmarTerrainTypes.radiation -> Radiation()
        SilmarTerrainTypes.fogPlacer -> FogPlacer()
        SilmarTerrainTypes.crusher -> Crusher()
        SilmarTerrainTypes.teleportTrap -> TeleportTrap()
        SilmarTerrainTypes.fireballTrap -> FireballTrap()
        SilmarTerrainTypes.gasserTrap -> GasserTrap()
        SilmarTerrainTypes.lightningBoltTrap -> LightningBoltTrap()
        else -> null
    }
}