package silmar.contexts.gameplay.entities.terrains.specializations

import silmar.contexts.gameplay.entities.beings.monsters.specializations.FerrousGolem
import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.avoid
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.util.math.randomInt
import java.awt.geom.Point2D

class DeathFog : Terrain1() {

    /**
     * The current potency of this death-fog.
     */
    private var strength: Int = 0

    /**
     * The game turn when this death-fog first sprang into existence.
     */
    private var startTurn: Int = 0

    /**
     * The direction in which this fog will drift during its existence.
     */
    private var driftDirection: Point2D.Float? = null
    override val shouldDrawOnTop: Boolean
        get() = true

    fun setStrength(strength: Int) {
        this.strength = strength
    }

    fun setDriftDirection(driftDirection: Point2D.Float) {
        this.driftDirection = driftDirection
    }

    override fun onBeingArrival(being: Being): Boolean {
        onBeingHere(being)
        return false
    }

    override fun onBeingHere(being: Being) {
        // ferrous golems are immune
        if (being is FerrousGolem) return

        // if the being avoids the fog
        var damage = randomInt(strength / 4, strength / 2)
        val result = being.avoid(strength / 2)
        if (result.avoided) {
            // the damage is halved
            damage /= 2
        }

        // if the damage amount is still positive, and the being didn't super-avoid the fog
        if (damage > 0 && !result.superAvoided) {
            // take damage
            being.takeDamage(Damage(damage, DamageForms.deathFog))
        }
    }

    override fun act() {
        // if this fog doesn't yet have a starting turn recorded
        val game = Game.currentGame
        if (startTurn == 0) {
            startTurn = game!!.turn
        }

        // if this fog's time is up
        if (game!!.turn - startTurn > 7 && randomInt(0, 3) == 0) {
            // remove it
            area.removeEntity(this)
            return
        }

        // if this fog has a drift direction, and a random chance says yes
        if (driftDirection != null && randomInt(0, 1) == 1) {
            // if the area slightly in the drift direction of this fog is passible
            // to this fog
            val driftMagnitude = 4
            val driftTo = MutablePixelPoint(location)
            driftTo.translate((driftMagnitude * driftDirection!!.x).toInt(),
                (driftMagnitude * driftDirection!!.y).toInt())
            if (area.isRectanglePassible(driftTo, size, MovementType.FLYING, false, null,
                    null, false).passible) {
                // have this fog drift in its drift direction
                area.moveEntity(this, driftTo)
            }
        }
    }

    override fun isHarmfulTo(monster: Monster): Boolean {
        return !monster.isOfType(SilmarMonsterTypes.ferrousGolem)
    }
}