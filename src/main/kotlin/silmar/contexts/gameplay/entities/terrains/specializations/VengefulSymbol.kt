package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.util.math.randomInt

class VengefulSymbol : Terrain1() {

    /**
     * How much power is left in this symbol.
     */
    private var strength: Int = 0

    fun setStrength(strength: Int) {
        this.strength = strength
    }

    override fun onBeingArrival(being: Being): Boolean {
        // if the given being is an evil monster
        if (being is Monster && being.isEvil) {
            // it takes damage
            val damage = randomInt(strength, strength * 2)
            being.takeDamage(Damage(damage, DamageForms.holyPower))

            // this symbol's strength is diminished
            strength /= 2

            // if this symbol is now weak enough
            if (strength == 0) {
                // remove it
                area.removeEntity(this)
            }

            return true
        }

        return false
    }
}