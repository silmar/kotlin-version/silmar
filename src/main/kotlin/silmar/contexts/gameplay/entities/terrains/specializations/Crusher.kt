package silmar.contexts.gameplay.entities.terrains.specializations

import silmar.sounds.SilmarSounds
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeDamage
import silmarengine.contexts.gameplay.entities.damage.Damage
import silmarengine.contexts.gameplay.entities.damage.DamageForms
import silmarengine.contexts.gameplay.entities.extensions.proximity.getEntitiesInRectangle
import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmarengine.contexts.gameplay.areas.extensions.geometry.asPixelLocation
import silmarengine.contexts.gameplay.areas.extensions.geometry.asTileLocation
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesOccurred
import silmarengine.contexts.gameplay.areas.extensions.reporting.onGroupOfChangesToOccur
import silmarengine.contexts.gameplay.areas.extensions.reporting.onSoundIssued
import silmarengine.contexts.gameplay.areas.geometry.TilePoint
import silmarengine.contexts.gameplay.areas.geometry.MutableTilePoint
import silmarengine.contexts.gameplay.tiles.Tiles.floor
import silmarengine.contexts.gameplay.tiles.Tiles.roomWall
import silmarengine.contexts.gameplay.tiles.tileSize
import silmarengine.util.sleepThread

/**
 * A terrain feature that controls a big crushing-of-two-big-walls effect in
 * a region of an area.
 */
class Crusher : Terrain1() {
    /**
     * Whether this crusher has been activated yet.  If not, we don't want it to be
     * wasting cycles doing its effect each turn.
     */
    private var activated = false

    override fun act() {
        // if this crusher isn't yet activated
        if (!activated) {
            // if this crusher can see the player
            if (area.player?.isInLOSOf(location) == true) {
                activated = true
            }
            else return
        }

        // determine the two locations, at the left and right center of the crush (so that
        // nearby players will hear sounds from one or the other), from which
        // the crush-related sounds will emanate below
        val tileLocation = location.asTileLocation()
        val crushTileHeight = 7
        val crushTileWidth = 4
        val leftSoundLocation = TilePoint(tileLocation.x - 1,
            tileLocation.y + crushTileHeight).asPixelLocation()
        val rightSoundLocation = TilePoint(tileLocation.x + crushTileWidth,
            tileLocation.y + crushTileHeight).asPixelLocation()

        // issue the walls-closing sounds
        area.onSoundIssued(SilmarSounds.wallsClosing, leftSoundLocation)
        area.onSoundIssued(SilmarSounds.wallsClosing, rightSoundLocation)

        // for each location in the area of effect, in an order such that the two
        // walls seem to be crushing together
        val wall = MutableTilePoint()
        for (j in 0 until crushTileHeight) {
            area.onGroupOfChangesToOccur()
            for (i in 0 until crushTileWidth) {
                // put a wall tile on the up side of the crush
                wall.set(tileLocation.x + i, tileLocation.y + j)
                putWall(wall)

                // put a wall tile on the down side of the crush
                wall.set(tileLocation.x + i, tileLocation.y + crushTileHeight * 2 - j - 1)
                putWall(wall)
            }
            area.onGroupOfChangesOccurred()

            // pause for a bit, unless we're on the last iteration, on which a pause would
            // make the wall-shut sound seem late
            if (j < crushTileHeight - 1) pauseForUsersToSee(100)
        }

        // issue the walls-shut sounds
        area.onSoundIssued(SilmarSounds.wallsShut, leftSoundLocation)
        area.onSoundIssued(SilmarSounds.wallsShut, rightSoundLocation)

        // pause a bit while the crusher is shut
        pauseForUsersToSee(1000)

        // issue the walls-opening sounds
        area.onSoundIssued(SilmarSounds.wallsOpening, leftSoundLocation)
        area.onSoundIssued(SilmarSounds.wallsOpening, rightSoundLocation)

        // for each location in the area of effect, in an order such that the two
        // walls seem to be pulling back apart
        for (j in 0 until crushTileHeight) {
            area.onGroupOfChangesToOccur()
            for (i in 0 until crushTileWidth) {
                // put a floor tile on the up side of the crush
                wall.set(tileLocation.x + i, tileLocation.y + crushTileHeight - j)
                area.setTile(wall, floor)

                // put a floor tile on the down side of the crush
                wall.set(tileLocation.x + i, tileLocation.y + crushTileHeight + j)
                area.setTile(wall, floor)
            }
            area.onGroupOfChangesOccurred()

            pauseForUsersToSee(250)
        }
    }

    /**
     * Sleeps for the given length (in ms), so the user can see the updates just made.
     */
    private fun pauseForUsersToSee(length: Int) {
        sleepThread(length.toLong())
    }

    /**
     * Puts a wall tile at the given tile-location and checks to see
     * if any beings are there, for damage purposes.
     */
    private fun putWall(location: TilePoint) {
        // set the tile at the given location to a wall
        area.setTile(location, roomWall)

        // each being within the given tile-location dies
        val beings =
            area.beings.getEntitiesInRectangle(location.asPixelLocation(), tileSize)
        beings.forEach {
            it.takeDamage(Damage(it.hitPoints, DamageForms.crushing))
        }
    }
}