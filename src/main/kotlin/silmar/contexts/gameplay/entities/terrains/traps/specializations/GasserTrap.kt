package silmar.contexts.gameplay.entities.terrains.traps.specializations

import silmar.contexts.gameplay.areas.SilmarArea
import silmar.contexts.gameplay.areas.extensions.effects.performDeathFogEffect
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap1

class GasserTrap : Trap1() {
    override fun spring(player: Player) {
        area.performDeathFogEffect(location,
            (area as SilmarArea).level!!.gasserTrapStrength)
    }
}