package silmar.contexts.gameplay.entities.terrains.specializations

import silmarengine.contexts.gameplay.entities.terrains.Terrain1
import silmar.contexts.gameplay.areas.extensions.effects.performRadiationEffect
import silmarengine.util.math.randomInt

class Radiation : Terrain1() {
    override fun act() {
        // if it's time to radiate
        if (randomInt(1, 15) == 1) {
            area.performRadiationEffect(location)
        }
    }
}