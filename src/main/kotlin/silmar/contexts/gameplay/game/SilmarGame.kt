package silmar.contexts.gameplay.game

import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.areas.Area

interface SilmarGame : Game {
    /**
     * A list of all the areas created for this game so far, in order of their level
     * progression.
     */
    val areas: List<Area>

    /**
     * Returns the area of the level with the given index.
     */
    fun getArea(levelIndex: Int): Area?

    val furthestDungeonLevelReached: Int
    fun onDungeonLevelReached(levelIndex: Int)

    /**
     * Adds the dungeon level of the given index to this game's sequence of levels.
     */
    fun addLevel(levelIndex: Int)
}