package silmar.contexts.gameplay.game.extensions

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.game.SilmarGame
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType

/**
 * Has this game move the given player through the up-exit of its
 * current map, thus putting it at the down-exit of the previous map.
 */
fun SilmarGame.movePlayerThroughUpExit(player: Player) {
    // put the player at the previous level's down-exit
    var area = player.area
    area.removeEntity(player)
    area = areas[areas.indexOf(area) - 1]
    val downExit = area.getTerrainOfType(SilmarTerrainTypes.downExit)
    val location =
        area.getNearbyPassibleRectangleLocation(downExit!!.location, player.size,
            player.movementType) ?: throw RuntimeException(
            "Could not find a location to place player")
    area.addEntity(player, location)
}

