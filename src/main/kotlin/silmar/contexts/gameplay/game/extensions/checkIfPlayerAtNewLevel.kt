package silmar.contexts.gameplay.game.extensions

import silmar.contexts.gameplay.game.SilmarGame
import silmar.contexts.gameplay.areas.SilmarArea
import silmar.contexts.gameplay.areas.extensions.levels.introduceToPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.areas.Area

/**
 * Has this game check to see if the given area is a new one
 * for the given player, and if it is, displays the area's intro
 * and does whatever other processing is needed.
 */
fun SilmarGame.checkIfPlayerAtNewLevel(player: Player, area: Area) {
    // if the player has reached the given area before
    area as SilmarArea
    val levelReached = furthestDungeonLevelReached
    val levelIndex = area.level!!.index
    if (levelReached >= levelIndex) return

    // record that the player has reached a new level
    onDungeonLevelReached(levelIndex)

    area.level!!.introduceToPlayer(player)
}
