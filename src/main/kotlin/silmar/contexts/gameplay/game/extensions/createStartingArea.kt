package silmar.contexts.gameplay.game.extensions

import silmar.contexts.gameplay.areas.SilmarArea1
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gamecreation.domain.model.GCArea
import silmarengine.contexts.gamecreation.domain.model.GCPixelPoint
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType

fun createStartingArea(): Area = SilmarArea1(0)

fun createStartingGCArea(forArea: Area): GCArea = object : GCArea {
    override fun findStart(): GCPixelPoint {
        val upExit = forArea.getTerrainOfType(SilmarTerrainTypes.upExit) ?: throw Exception(
            "Starting level up-exit not found")
        forArea.removeEntity(upExit)
        val location = upExit.location
        return GCPixelPoint(location.x, location.y)
    }
}
