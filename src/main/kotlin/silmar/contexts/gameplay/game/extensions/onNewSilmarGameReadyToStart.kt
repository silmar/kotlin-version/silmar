package silmar.contexts.gameplay.game.extensions

import silmar.contexts.gameplay.game.SilmarGame
import silmar.contexts.gameplay.areas.SilmarArea
import silmar.contexts.gameplay.areas.extensions.levels.introduceToPlayer
import silmarengine.contexts.gameplay.game.Game

fun onNewSilmarGameReadyToStart(game: Game) {
    // add the player to the area at its initial location
    game as SilmarGame
    val area = game.areas[0] as SilmarArea
    val player = game.player
    area.addEntity(player, player.location)

    // show the intro for the starting area
    area.level!!.introduceToPlayer(player)
}
