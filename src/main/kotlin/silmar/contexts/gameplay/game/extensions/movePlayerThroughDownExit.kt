package silmar.contexts.gameplay.game.extensions

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.game.SilmarGame
import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.MessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.game.extensions.save
import silmarengine.contexts.gameplay.areas.extensions.passibility.getNearbyPassibleRectangleLocation
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType
import silmarengine.util.file.copyFile

/**
 * Has this game move the given player through the down-exit of its
 * current area, thus putting it at the up-exit of the next area.
 */
fun SilmarGame.movePlayerThroughDownExit(player: Player) {
    // if the next level hasn't yet been created
    var area = player.area
    if (area == areas[areas.size - 1]) {
        // save an extra copy of the game before the new level is generated;
        // such a saved game is useful for debugging purposes; we rewrite over
        // the same file each time because the clutter of having separate such
        // files for each game tends to confuse users
        save("$name-end_of_level")

        // create the next level
        addLevel((area as SilmarArea).level!!.index + 1)
    }

    // put the player at the next level's up-exit
    area.removeEntity(player)
    area = areas[areas.indexOf(area) + 1]
    val upExit = area.getTerrainOfType(SilmarTerrainTypes.upExit)
    val location = area.getNearbyPassibleRectangleLocation(upExit!!.location, player.size,
        player.movementType) ?: throw RuntimeException(
        "Could not find a location to place player")
    area.addEntity(player, location)

    checkIfPlayerAtNewLevel(player, area)

    // save the game at this point
    player.onMessage("Saving your progress...")
    val saved = save()

    // if the game was successfully saved
    if (saved) {
        // make a copy of the game file, in case the next save fails during the
        // writing out of the data, which corrupts the file; this way,
        // a whole level's progress won't be lost (as has happened to me personally);
        // we rewrite over the same file each time because the clutter of having
        // separate such files for each game tends to confuse users
        copyFile("$path/$name", "$path/copy_of_saved_game")

        // inform the user
        player.onMessage("Your progress has been saved.")
    }
    else {
        // let the user know there was an error
        player.onMessage("Your progress could not be saved!  Please contact the author.",
            MessageType.important)
    }
}

