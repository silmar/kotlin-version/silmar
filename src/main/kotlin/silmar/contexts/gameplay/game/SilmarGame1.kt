package silmar.contexts.gameplay.game

import silmar.contexts.gameplay.areas.SilmarArea
import silmar.contexts.gameplay.areas.SilmarArea1
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.game.Game1
import silmarengine.contexts.gamecreation.domain.model.GCGame
import silmarengine.contexts.gameplay.areas.Area
import java.util.ArrayList

class SilmarGame1(fromGame: GCGame, player: Player, startingArea: Area) : SilmarGame,
    Game1(fromGame, player) {
    override val areas = ArrayList<Area>()

    init {
        areas.add(startingArea)
    }

    override fun getArea(levelIndex: Int): Area? =
        areas.find { (it as SilmarArea).level!!.index == levelIndex }

    override var furthestDungeonLevelReached = 0
    override fun onDungeonLevelReached(levelIndex: Int) {
        furthestDungeonLevelReached = levelIndex
    }

    override fun addLevel(levelIndex: Int) {
        // create the new area
        val area = SilmarArea1(levelIndex)
        areas.add(area)
    }
}