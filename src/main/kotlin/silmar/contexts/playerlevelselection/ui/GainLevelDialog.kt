package silmar.contexts.playerlevelselection.ui

import silmar.contexts.playerlevelselection.domain.model.Level
import silmar.contexts.playerlevelselection.domain.model.PlayerClass
import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.Font
import javax.swing.*
import javax.swing.border.EmptyBorder

/**
 * A dialog that lets the user select which level its player has just gained.
 */
class GainLevelDialog(owner: JFrame, getPlayerClasses: () -> List<PlayerClass>,
    getNextEarnableLevel: (playerClass: PlayerClass) -> Level?,
    onLevelChosen: (level: Level) -> Unit) : JDialog(owner) {
    init {
        // configure this dialog
        val pane = contentPane
        pane.background = owner.contentPane.background
        title = "Gain Level"
        isModal = true
        isResizable = false
        pane.layout = BorderLayout()

        // prevent this dialog's close button from working
        defaultCloseOperation = WindowConstants.DO_NOTHING_ON_CLOSE

        // add the main panel
        val mainPanel = JPanel()
        mainPanel.border = EmptyBorder(5, 5, 10, 5)
        mainPanel.isOpaque = false
        mainPanel.layout = BoxLayout(mainPanel, BoxLayout.Y_AXIS)
        pane.add(mainPanel, BorderLayout.CENTER)

        // add a label describing the purpose of this dialog to the user
        val instructionsLabel = Label(
            "<html>You have earned enough experience to advance a level!<br/><br/>You must choose which level you wish to gain<br/>from the following choices:</html>",
            Fonts.mediumText)
        instructionsLabel.border = EmptyBorder(5, 5, 5, 5)
        mainPanel.add(instructionsLabel)

        mainPanel.add(Box.createRigidArea(Dimension(0, 5)))

        // add the choices panel
        val choicesPanel = JPanel()
        choicesPanel.isOpaque = false
        choicesPanel.layout = BoxLayout(choicesPanel, BoxLayout.Y_AXIS)
        mainPanel.add(choicesPanel)

        mainPanel.add(Box.createVerticalGlue())

        // add the headings panel
        val headingsPanel = JPanel()
        headingsPanel.isOpaque = false
        headingsPanel.layout = BoxLayout(headingsPanel, BoxLayout.X_AXIS)
        choicesPanel.add(headingsPanel)

        headingsPanel.add(Box.createRigidArea(Dimension(50, 0)))

        // add the class heading
        var label: JLabel = Label("Class", Fonts.mediumText)
        val font = Font("sans-serif", Font.BOLD, 15)
        label.font = font
        label.alignmentX = 0f
        headingsPanel.add(label)

        headingsPanel.add(Box.createHorizontalGlue())

        // add the next-level heading
        label = Label("Next Level", Fonts.mediumText)
        label.setFont(font)
        label.setAlignmentX(1f)
        headingsPanel.add(label)

        headingsPanel.add(Box.createRigidArea(Dimension(50, 0)))

        // for each player-class
        val buttonFont = Font("sans-serif", Font.BOLD, 13)
        getPlayerClasses().forEach { playerClass ->
            choicesPanel.add(Box.createRigidArea(Dimension(0, 5)))

            // add the panel for this class's choice
            val classPanel = JPanel()
            classPanel.isOpaque = false
            classPanel.layout = BoxLayout(classPanel, BoxLayout.X_AXIS)
            choicesPanel.add(classPanel)

            classPanel.add(Box.createRigidArea(Dimension(50, 0)))

            // add the name label for this class
            val nameLabel = Label(playerClass.name, Fonts.mediumText)
            nameLabel.alignmentX = 0f
            classPanel.add(nameLabel)

            classPanel.add(Box.createHorizontalGlue())

            // if there is no further level to select for this class
            val next = getNextEarnableLevel(playerClass)
            if (next == null) {
                // add a label indicating that fact
                label = Label("None Left!", Fonts.mediumText)
                label.alignmentX = 1f
                classPanel.add(label)
            }
            // otherwise
            else {
                // add the button for selecting this class's next level
                val button = Button(" " + next.name + " ")
                button.font = buttonFont
                button.alignmentX = 1f
                classPanel.add(button)
                button.addActionListener {
                    onLevelChosen(next)

                    isVisible = false
                }
            }

            classPanel.add(Box.createRigidArea(Dimension(50, 0)))
        }

        pack()
        setLocationRelativeTo(owner)
    }
}
