package silmar.contexts.playerlevelselection.domain.model

typealias PLSPlayerClass = PlayerClass

interface PlayerClass {
    val name: String
}