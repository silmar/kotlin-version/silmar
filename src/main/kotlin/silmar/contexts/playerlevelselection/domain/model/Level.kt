package silmar.contexts.playerlevelselection.domain.model

typealias PLSLevel = Level

interface Level {
    val name: String
}