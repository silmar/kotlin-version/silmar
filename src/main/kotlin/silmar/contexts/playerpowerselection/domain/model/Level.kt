package silmar.contexts.playerpowerselection.domain.model

typealias PPSLevel = Level

interface Level {
    val name: String
}