package silmar.contexts.playerpowerselection.domain.model

typealias PPSPower = Power

interface Power {
    val name: String
    val level: Level
    val tooltipText: String
    val isActiveUse: Boolean
    val magicPointCost: Int
    val clericalPointCost: Int
}