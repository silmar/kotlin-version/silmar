package silmar.contexts.playerpowerselection.ui

import silmar.contexts.playerpowerselection.domain.model.Level
import silmar.contexts.playerpowerselection.domain.model.Power
import silmarengine.gui.Fonts
import silmarengine.gui.widgets.Button
import silmarengine.gui.widgets.Label
import silmarengine.gui.windowLayout
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.Frame
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.*
import javax.swing.border.EmptyBorder
import javax.swing.border.LineBorder

/**
 * A non-modal dialog that displays the powers usable by the player and allows the
 * user to select from them and read info about them.
 */
class PowersDialog(parent: Frame, private val getPowers: () -> List<Power>,
    private val onPowerSelected: (power: Power) -> Unit) : JDialog(
    parent, "Powers") {
    /**
     * The panel in which the controls for the various powers are displayed.
     */
    private var powersPanel: JPanel? = null

    init {
        isResizable = false

        val pane = contentPane
        pane.layout = BorderLayout()
        pane.background = Color.black

        val panel = JPanel()
        panel.isOpaque = false
        panel.border = EmptyBorder(5, 5, 5, 5)
        panel.layout = BoxLayout(panel, BoxLayout.Y_AXIS)
        pane.add(panel, BorderLayout.CENTER)

        // add the powers panel in a scroll pane
        val panel2 = JPanel()
        powersPanel = panel2
        panel2.background = Color.black
        panel2.layout = BoxLayout(panel2, BoxLayout.Y_AXIS)
        powersPanel!!.border = EmptyBorder(5, 5, 5, 5)
        val scrollPane = JScrollPane(powersPanel)
        scrollPane.border = LineBorder(Color.lightGray, 1)
        scrollPane.isOpaque = false
        scrollPane.horizontalScrollBarPolicy = JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
        val bar = scrollPane.verticalScrollBar
        bar.unitIncrement = 32
        bar.blockIncrement = 160
        panel.add(scrollPane, BorderLayout.CENTER)

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        panel.add(Label("mouseover for info", Fonts.smallText))

        panel.add(Box.createRigidArea(Dimension(0, 10)))

        createPowersWidgets()

        addWindowListener(object : WindowAdapter() {
            override fun windowOpened(e: WindowEvent?) {
                // if a location value for this dialog is available, use it
                val location = windowLayout.powersDialogLocation
                if (location != null) setLocation(location)

                // otherwise, center this dialog on its parent
                else setLocationRelativeTo(parent)
            }

            override fun windowClosing(event: WindowEvent?) {
                // store this dialog's location
                windowLayout.powersDialogLocation = location
            }

            override fun windowDeactivated(e: WindowEvent?) = windowClosing(e)
        })
    }

    /**
     * Clears this dialog's powers-panel, then populates it with a line of controls per
     * power currently possessed by the player.
     */
    private fun createPowersWidgets() {
        powersPanel!!.removeAll()

        // for each player power
        val powers = getPowers()
        val spacing = 5
        var level: Level? = null
        powers.forEachIndexed { i, power ->
            // if this power's granting level is different than that of the last power displayed
            if (power.level != level) {
                level = power.level

                // if this isn't the first power, add in a separator
                if (i != 0) powersPanel!!.add(Box.createRigidArea(Dimension(0, 5)))

                // add a heading for the level's name, under which the level's powers
                // will be displayed
                val label = Label(level!!.name, Fonts.mediumTextBold)
                label.horizontalAlignment = SwingConstants.LEFT
                powersPanel!!.add(label)
            }

            // add a panel to hold the widgets for this power
            val panel = JPanel()
            panel.isOpaque = false
            panel.layout = BoxLayout(panel, BoxLayout.X_AXIS)
            powersPanel!!.add(panel)

            // add the power's name label
            var label = Label(power.name, Fonts.mediumText)
            panel.add(label)
            label.hasTooltipImpl.setTooltipText(power.tooltipText, 70)

            panel.add(Box.createRigidArea(Dimension(spacing, 0)))

            // if the power isn't passive
            if (power.isActiveUse) {
                // add a 'use' button
                val button = Button(" Use ")
                panel.add(button)

                // when the use-button is clicked
                button.addActionListener {
                    // close this dialog
                    isVisible = false

                    onPowerSelected(power)
                }
            }
            else {
                // add a label indicating the power is passively used
                label = Label("PASSIVE", Fonts.mediumText)
                label.foreground = Color.gray
                panel.add(label)
                label.hasTooltipImpl.setTooltipText("This power is always in effect.", 0)
            } // otherwise

            panel.add(Box.createRigidArea(Dimension(spacing, 0)))

            // if the power has a magic point cost
            var cost = power.magicPointCost
            if (cost > 0) {
                // add a label displaying the cost
                label = Label("mp:$cost", Fonts.smallText)
                panel.add(label)
                label.hasTooltipImpl.setTooltipText(
                    "This power costs<br>$cost magic points to use.", 0)
            }

            // if the power has a clerical point cost
            cost = power.clericalPointCost
            if (cost > 0) {
                // add a label displaying the cost
                label = Label("cp:$cost", Fonts.smallText)
                panel.add(label)
                label.hasTooltipImpl.setTooltipText(
                    "This power costs<br>$cost clerical points to use.", 0)
            }

            panel.add(Box.createHorizontalGlue())

            powersPanel!!.add(Box.createRigidArea(Dimension(0, 2)))
        }

        // if the player has no powers
        if (powers.isEmpty()) {
            // add a label to that effect
            powersPanel!!.add(Label("No powers currently possessed.", Fonts.mediumText))
        }

        pack()

        // if this dialog's preferred height is too tall
        val maxDialogHeight = 500
        if (height > maxDialogHeight) {
            // limit it to a reasonable amount
            val scrollbarWidth = 32
            setSize(width + scrollbarWidth, maxDialogHeight)
            (powersPanel!!.parent as JComponent).revalidate()
        }
    }

    /**
     * Informs this dialog that the list of powers it is to display has changed.
     */
    fun onPowersChanged() {
        createPowersWidgets()
    }
}

