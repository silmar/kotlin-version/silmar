package silmar

/**
 * Various name constants.
 */
object Names {
    const val archEnemy = "Syrilboltus"
    const val dwarvenKing = "King Thelbar"
}
