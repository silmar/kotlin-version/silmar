package silmar.gui

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmar.contexts.gameplay.entities.beings.player.PlayerPower
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.usePower
import silmarengine.contexts.gameplay.entities.beings.MovementType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.gui.LocationSelection
import silmarengine.contexts.gameplay.areas.extensions.passibility.isRectanglePassible
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * The flyweight Silmar-specific location selections.
 */
object LocationSelections {
    val fireEyeBeams = LocationSelection(
        { location, player -> player.usePower(
            PlayerPowers.fireEyeBeams, location) })
    val leap = LocationSelection({ location, player ->
        // if the target rectangle isn't passible
        val area = player.area
        val playerSize = player.size
        if (!area.isRectanglePassible(location, playerSize, MovementType.WALKING, true,
                null, null, false).passible) {
            // don't allow the leap
            player.onMessage("You cannot leap there.")
        }
        else player.usePower(PlayerPowers.leap, location)
    })
    val emitLightning = LocationSelection(
        { location, player -> player.usePower(
            PlayerPowers.emitLightning, location) })
    val conjureDeathFog = LocationSelection(
        { location, player -> player.usePower(
            PlayerPowers.conjureDeathFog, location) })
    val placeVengefulSymbol = LocationSelection({ location, player ->
        player.usePower(PlayerPowers.placeVengefulSymbol, location)
    }, true)
    val throwBoulder = LocationSelection(
        { location, player -> player.usePower(
            PlayerPowers.throwBoulder, location) })
    val freezeBeing = LocationSelection(
        { location, player -> player.usePower(
            PlayerPowers.freezeBeing, location) })
    val hurlFireball = LocationSelection(
        { location, player -> player.usePower(
            PlayerPowers.hurlFireball, location) })
}

private fun Player.usePower(power: PlayerPower, location: PixelPoint) =
    (this as SilmarPlayer).usePower(power, location)