package silmar.gui.playui

import silmarengine.gui.playui.PlayUI

interface SilmarPlayUI : PlayUI {
    /**
     * Has this play-UI display the powers available for use by the player.
     */
    fun showPowers()
}