package silmar.gui.playui

import silmar.contexts.gameplay.entities.beings.player.*
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.getNextEarnableLevel
import silmar.gui.frames.playframe.SilmarPlayFrame1
import silmar.gui.frames.playframe.onPlayerEmployedSuperVision
import silmar.gui.frames.playframe.onPowerSelected
import silmar.contexts.playerlevelselection.ui.GainLevelDialog
import silmar.contexts.playerpowerselection.ui.PowersDialog
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.playui.PlayUI1
import silmarengine.scheduler
import java.util.concurrent.TimeUnit

class SilmarPlayUI1(player: Player) : SilmarPlayUI, PlayUI1(player) {
    /**
     * The dialog that is displayed when the user wishes to select one of its player's powers
     * for use.
     */
    private val powersDialog: PowersDialog =
        PowersDialog(playFrame.frame, { (player as SilmarPlayer).powers }, {
            if (!playFrame.onPowerSelected(it as PlayerPower)) onPowerCouldNotBeUsed()
        })

    override fun createPlayFrame() = SilmarPlayFrame1(this, player)

    override fun showPowers() {
        powersDialog.isVisible = true
    }

    inner class UISilmarPlayerListener : SilmarPlayerListener, UIPlayerListener() {
        override fun onSuperVisionEmployed() {
            playFrame.onPlayerEmployedSuperVision()
        }

        override fun onLevelChanged() {
            powersDialog.onPowersChanged()
        }

        override fun onMustChooseLevelGained() {
            player as SilmarPlayer
            GainLevelDialog(playFrame.frame, { PlayerClass.classes },
                { player.getNextEarnableLevel(it as PlayerClass) }, {
                    player.silmarLevelValues.onPlayerLevelChosen(it as PlayerLevel)
                }).isVisible = true
        }
    }

    private fun onPowerCouldNotBeUsed() {
        // pause a bit, so any failure message may be seen by the user
        scheduler.schedule({
            // then, bring the powers-dialog back up, in case the
            // user would like to select a different power
            powersDialog.isVisible = true
        }, 1, TimeUnit.SECONDS)
    }
}