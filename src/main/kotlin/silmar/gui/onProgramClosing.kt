package silmar.gui

import silmarengine.getBooleanConfigurationValue
import silmarengine.gui.dialogs.DonationPleaDialog
import silmarengine.gui.dialogs.hasDonatedPropertyKey
import silmarengine.gui.frames.TitleFrame

fun showDonationPleaDialog(titleFrame: TitleFrame) {
    // don't display the dialog if the user has indicated they have already donated
    if (getBooleanConfigurationValue(hasDonatedPropertyKey)) return

    val websiteURI = "http://dunjax.com/silmar"
    //@formatter:off
    val message =
        "If you have enjoyed playing this game, please consider making a donation to" +
            " support its development.  Visit $websiteURI to donate." +
            " There are two more games (named Ranadinn and Requiem) which we would like" +
            " to develop for this game's engine if we can get enough support."
    //@formatter:on
    DonationPleaDialog(titleFrame, message,
        websiteURI) { it.isAltDown && it.keyChar == 'y' }.isVisible = true
}
