package silmar.gui.frames.playframe

import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.gui.views.areaview.extensions.visibility.recomputeView
import silmarengine.util.sleepThread

fun PlayFrame.onPlayerEmployedSuperVision() {
    // turn los-blocking and lighting considerations off in the area-view
    areaView.shouldUseLOSBlocking = false
    areaView.shouldConsiderLighting = false
    areaView.recomputeView()

    // wait 7 seconds
    sleepThread(7000)

    // turn los-blocking and lighting considerations back on in the area-view
    areaView.shouldUseLOSBlocking = true
    areaView.shouldConsiderLighting = true
    areaView.recomputeView()
}

