package silmar.gui.frames.playframe

import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.terrains.Terrain
import silmar.contexts.gameplay.entities.terrains.specializations.Fountain
import silmar.contexts.gameplay.entities.terrains.specializations.Mine
import silmar.contexts.gameplay.entities.terrains.specializations.Statue
import silmarengine.gui.dialogs.OkCancelDialog
import javax.swing.JFrame

fun onNearbyTerrainClicked(
    terrain: Terrain, player: Player, parentFrame: JFrame
): Boolean {
    var resultOccurred = false
    when (terrain.type) {
        SilmarTerrainTypes.fountain -> if (OkCancelDialog.show(parentFrame,
                "Drink from this fountain?")) {
            (terrain as Fountain).onPlayerDrinks(player)
            resultOccurred = true
        }
        SilmarTerrainTypes.mine -> if (OkCancelDialog.show(parentFrame,
                "Enter this dark, old mining shaft?")) {
            (terrain as Mine).onPlayerEnters(player)
            resultOccurred = true
        }
        SilmarTerrainTypes.statue -> onStatueClicked(terrain as Statue, player, parentFrame)
        else -> return false
    }
    return resultOccurred
}
