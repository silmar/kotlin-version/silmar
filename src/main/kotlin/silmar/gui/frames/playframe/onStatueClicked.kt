package silmar.gui.frames.playframe

import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.gold.removeGold
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.LocationalMessageType
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmar.contexts.gameplay.entities.terrains.specializations.Statue
import silmarengine.gui.dialogs.InputIntDialog
import silmarengine.gui.dialogs.MessageDialog
import silmarengine.gui.dialogs.OkCancelDialog
import javax.swing.JFrame

fun onStatueClicked(
    statue: Statue, player: Player, parentFrame: JFrame
): Boolean {
    // if the statue has already been used
    val playerGold = player.stats.gold
    if (statue.isUsed) {
        // don't let it be used again
        player.onMessage("This statue is non-descript.",
            LocationalMessageType(statue.location))
    }

    // else, if the player wishes to insert gold
    else if (OkCancelDialog.show(parentFrame,
            "An inscription on this statue reads, \"Insert gold into my mouth, and I will reward you with an item!\" Do so?")) {
        // if the player has no gold
        if (playerGold < 1) {
            MessageDialog.show(parentFrame, "You have no gold!")
        }
        else {
            // otherwise, if the player doesn't cancel out of inserting the gold
            val result =
                InputIntDialog.show(parentFrame, "How much gold do you wish to insert?",
                    0, playerGold, playerGold)
            if (!result.cancelled && result.valueEntered > 0) {
                // subtract the amount from this player's gold
                val gold = result.valueEntered
                player.removeGold(gold)

                // inform the statue
                statue.onGoldInserted(gold)
            }
        }
    }

    return true
}