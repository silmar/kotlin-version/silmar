package silmar.gui.frames.playframe

import silmar.gui.playui.SilmarPlayUI
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.frames.playframe.PlayFrame1
import silmarengine.gui.playui.PlayUI
import silmarengine.gui.widgets.Button
import java.awt.Dimension
import java.awt.event.ActionListener
import javax.swing.Box
import javax.swing.JPanel

class SilmarPlayFrame1(playUI: PlayUI, player: Player) : PlayFrame1(playUI, player) {
    /**
     * The button that displays the powers dialog.
     */
    private var powersButton: Button? = null

    override fun addButtonHook1(rightPanel: JPanel) {
        // add the powers button
        val button = Button(" Powers ")
        powersButton = button
        button.alignmentX = 0.5f
        rightPanel.add(button)
        button.addActionListener(ActionListener {
            if (!isEnabled) return@ActionListener
            if (locationSelection == null) (playUI as SilmarPlayUI).showPowers()
        })
        setTooltip(button, "Displays the active- and passive-use powers you possess.")

        rightPanel.add(Box.createRigidArea(Dimension(0, 3)))
    }

    override fun enableGameSpecificButtons(enable: Boolean) {
        powersButton!!.isEnabled = enable
    }
}