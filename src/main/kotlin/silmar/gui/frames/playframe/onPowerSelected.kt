package silmar.gui.frames.playframe

import silmar.contexts.gameplay.entities.beings.player.PlayerPower
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.canEmployPowerNow
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.usePower
import silmar.contexts.gameplay.game.SilmarGame
import silmar.contexts.gameplay.areas.AreaLevels
import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.WorkThread
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.messages.onMessage
import silmarengine.contexts.gameplay.game.Game
import silmarengine.gui.dialogs.InputIntDialog
import silmarengine.gui.frames.playframe.PlayFrame
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

/**
 * Is called when the user has selected the given player-power from the powers dialog.
 *
 * @return          Whether the player is able to use the given power right now.
 */
fun PlayFrame.onPowerSelected(power: PlayerPower): Boolean {
    // if the player can't currently employ the given power, then return that result
    val player = player as SilmarPlayer
    if (!player.canEmployPowerNow(power)) return false

    // if the power demands a location selection
    val selection = power.locationSelection
    if (selection != null) {
        // get that location selection going
        locationSelection = selection
        return true
    }

    // if the power is teleport-self
    var location: PixelPoint? = null
    if (power == PlayerPowers.teleportSelf) {
        location = onTeleportSelfPowerSelected(player)
        if (location == null) return true
    }

    // execute the power use on the work-thread
    val finalLocation = location
    WorkThread.queueTask {
        player.usePower(power, finalLocation)
    }

    return true
}

private fun PlayFrame.onTeleportSelfPowerSelected(player: Player): PixelPoint? {
    // if this level is the one of those with the main bad guy in it
    val level = (player.area as SilmarArea).level
    if (level!! == AreaLevels.dungeonCEnd || level == AreaLevels.moonEnd) {
        // don't allow the teleport
        player.onMessage("Your attempt to teleport is blocked!")
        return null
    }

    // ask the player which level to teleport to
    val game = Game.currentGame as SilmarGame
    val maxLevel = game.furthestDungeonLevelReached
    val defaultLevel = if (level.index == 0) maxLevel else 0
    val result =
        InputIntDialog.show(frame, "To which level do you wish to teleport?", 0, maxLevel,
            defaultLevel)

    // if the player wishes to cancel the teleport
    if (result.cancelled) return null

    // use the level index as the x-value of the to-location at which to use the power
    return PixelPoint(result.valueEntered, 0)
}