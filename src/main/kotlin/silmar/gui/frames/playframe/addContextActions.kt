package silmar.gui.frames.playframe

import silmar.contexts.gameplay.areas.AreaLevels
import silmar.contexts.gameplay.areas.SilmarArea
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmar.contexts.gameplay.entities.beings.player.extensions.actions.bashCityCrate
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.Task
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.entities.getEntitiesAt
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint

fun addContextActions(
    area: Area, areaLocation: PixelPoint, player: Player,
    addMenuItem: (text: String, task: Task?) -> Unit
) {
    // if the current area is for the city-end level
    area as SilmarArea
    if (area.level == AreaLevels.cityEnd) {
        // if there is a crate at the given location
        val crate = getEntitiesAt(areaLocation, area.terrains).firstOrNull {
            it.isOfType(TerrainTypes.crate)
        }
        if (crate != null) {
            // add an option to bash the crate
            addMenuItem("Bash crate") { player.bashCityCrate(crate) }
        }
    }
}