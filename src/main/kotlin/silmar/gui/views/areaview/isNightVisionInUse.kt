package silmar.gui.views.areaview

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.isUsingNightVision
import silmarengine.gui.views.areaview.AreaViewInternals

val AreaViewInternals.isNightVisionInUse: Boolean
    get() = viewpointEntity is SilmarPlayer && (viewpointEntity as SilmarPlayer).isUsingNightVision

