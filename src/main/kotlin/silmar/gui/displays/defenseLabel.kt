package silmar.gui.displays

import silmar.contexts.gameplay.entities.beings.extensions.attacking.SilmarAttackTypes
import silmar.contexts.gameplay.game.SilmarGame
import silmar.contexts.gameplay.areas.AreaLevels
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.game.Game
import silmarengine.gui.displays.defenseLabelTooltip

fun appendToSilmarDefenseLabel(buffer: StringBuffer, player: Player) {
    // if the player has reached where firearms are first present
    val reached = hasReachedFirearmAndLaserLevels()
    if (reached.first) {
        // append the firearm defense value
        buffer.append(" / F")
        buffer.append(player.getDefense(SilmarAttackTypes.bullet, null))
    }

    // if the player has reached where lasers are first present
    if (reached.second) {
        // append the laser defense value
        buffer.append(" / L")
        buffer.append(player.getDefense(SilmarAttackTypes.laserCell, null))
    }
}

fun hasReachedFirearmAndLaserLevels(): Pair<Boolean, Boolean> {
    val game = (Game.currentGame ?: return Pair(false, false)) as SilmarGame
    val maxLevel = game.furthestDungeonLevelReached
    val reachedFirearms = maxLevel >= AreaLevels.cityEnd.index
    val reachedLasers = maxLevel >= AreaLevels.moon1.index
    return Pair(reachedFirearms, reachedLasers)
}

fun getSilmarDefenseTooltipText(): String {
    val reached = hasReachedFirearmAndLaserLevels()
    val defenseLabelTooltip2 =
        "$defenseLabelTooltip The second number applies to firearm penetration-level attacks."
    val defenseLabelTooltip3 =
        "$defenseLabelTooltip2 The third number applies to laser penetration-level attacks."
    return when {
        reached.second -> defenseLabelTooltip3
        reached.first -> defenseLabelTooltip2
        else -> defenseLabelTooltip
    }
}
