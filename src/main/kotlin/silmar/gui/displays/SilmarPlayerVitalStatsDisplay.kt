package silmar.gui.displays

import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.gui.displays.PlayerVitalStatsDisplay
import java.awt.Color
import javax.swing.Box
import javax.swing.JLabel

class SilmarPlayerVitalStatsDisplay(player: Player) : PlayerVitalStatsDisplay(player) {
    /**
     * Storing this allows hiding of this box when the player has no such points.
     */
    private val magicPointsBox: Box

    private val magicPointsLabel: JLabel

    /**
     * Storing this allows hiding of this box when the player has no such points.
     */
    private val clericalPointsBox: Box

    private val clericalPointsLabel: JLabel

    init {
        val (magicPointsBox, magicPointsLabel) = addLabel("Magic Points:",
            "How many points you have for casting magic-user spells.", true)
        this.magicPointsBox = magicPointsBox
        this.magicPointsLabel = magicPointsLabel
        val (clericalPointsBox, clericalPointsLabel) = addLabel("Clerical Points:",
            "How many points you have for casting clerical spells.", true)
        this.clericalPointsBox = clericalPointsBox
        this.clericalPointsLabel = clericalPointsLabel
    }

    override fun onAttributeValueChanged() {
        super.onAttributeValueChanged()
        
        val player = player as SilmarPlayer
        val stats = player.silmarStats
        magicPointsBox.isVisible = stats.maxMagicPoints > 0
        magicPointsLabel.text =
            "${stats.magicPoints} / ${stats.maxMagicPoints}"
        clericalPointsBox.isVisible = stats.maxClericalPoints > 0
        clericalPointsLabel.text =
            "${stats.clericalPoints} / ${stats.maxClericalPoints}"

        repaint()
    }

    override fun updateStatusLabel() {
        val player = player as SilmarPlayer
        var color: Color? = null
        var text: String? = null
        when {
            player.silmarStatuses.isQuickened -> {
                color = Color.yellow
                text = "quickened"
            }
        }
        if (color != null) {
            statusLabel.foreground = color
            statusLabel.text = text
        }
        else super.updateStatusLabel()
    }
}