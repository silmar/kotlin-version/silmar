package silmar.gui.dialogs

import silmar.Names
import silmarengine.programName

/**
 * The story leading up to the beginning of the game.
 */
// @formatter:off
const val story =
    "...And so it came to be that upon his passing" +
        " the evil archmage ${Names.archEnemy} did not truly die," +
        " but instead the foulness of his soul sustained his" +
        " being into a state of living death," +
        " and his power grew only greater and more twisted." +
        " From the barren plains of $programName" +
        " surrounding his tower came disturbing" +
        " reports that he was bending the forces of time and space" +
        " to his will, fashioning some great magical construct" +
        " with which to assert his rule over the lands." +
        " Such rumors finally distressed ${Names.dwarvenKing} so much" +
        " that he formed an army and led an assault on the remote structure." +
        " Though the tower was leveled in the onslaught," +
        " ${Names.archEnemy} escaped into what the dwarves soon realized" +
        " was his infamous creation, a massive complex" +
        " underneath the tower inhabited by an amazing variety" +
        " of creatures both familiar and alien, all under some sort of spell to" +
        " cooperate in defending him.  So entangled and imbued with" +
        " magic were the dungeons that the dwarven sages sternly warned the king" +
        " against any charge into their bowels." +
        " Accepting their advice, the king pronounced" +
        " victory and dispersed his army, leaving the true threat" +
        " largely undisturbed to this day.\n\n" +
        "Undisturbed, except for the likes of adventurers such as yourself," +
        " who have heard the stories about great wealth from across time" +
        " and space being sucked into the labyrinths, waiting to be plucked" +
        " from their powerful grasp.  The area has garnered enough attention" +
        " that humble structures have arisen near the tower ruins," +
        " housing a few brave souls willing" +
        " to aid those who would dare enter the depths." +
        " But no one seems to survive more than a few scavenging trips.\n\n" +
        "You have come from a poor village which" +
        " needs funds to sustain its war-torn existence." +
        " You know that you must acquire wealth from the dungeons for your brethren" +
        " to survive.  You also know that you perhaps possess an advantage over" +
        " those who have preceded you.  Raised as an orphan, your village's elders" +
        " have long contended that you possess a unique magical nature." +
        " Now, they believe that your special gift might draw from" +
        " the power imbued in the spells woven into the dungeons." +
        " This power could allow you to transform yourself in ways" +
        " only imaginable by ordinary mortals.  That said, you suspect this ability," +
        " if it really exists in you, would only be revealed once you have acclimated" +
        " to the magical environment.\n\n" +
        "The depths await..."
// @formatter:on

/**
 * The message displayed to the player when it has finished the game.
 */
// @formatter:off
const val gameFinishedMessage =
    "And so, the evil presence of ${Names.archEnemy} was rid forever from the land of Silmar." +
        " And with his passing, the labyrinths beneath the tower began to slowly dissipate back to the" +
        " existences from which they were summoned.  In time they vanished completely," +
        " and those who arrived later in search of riches found only a ladder leading" +
        " down into solid dirt, and the little structures of the suppliers who were to aid them abandoned" +
        " and collapsing.  ${Names.dwarvenKing} used the sudden demise of ${Names.archEnemy}" +
        " to justify his belief that there was no further threat to his kingdom once the" +
        " tower was razed, and the dwarven people hold him in high regard for his foresight" +
        " to this day.\n\n" +
        "More importantly, though, your humble village was able to escape the hard times it was facing," +
        " thanks in large part to the treasure with which you filled its coffers." +
        " Your fellow villagers were able to rebuild their war-torn society and go on" +
        " to prosper, being well-fed and well-defended for a long time to come.\n\n" +
        "Congratulations!  You have succeeded where many others have failed, and you will" +
        " be known as a hero for generations hence!\n\n" +
        "Thank you for playing this game to its finish."
// @formatter:on