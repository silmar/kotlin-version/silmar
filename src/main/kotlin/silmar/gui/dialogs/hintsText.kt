package silmar.gui.dialogs

//@formatter:off
const val silmarHintsText =
    "<b>Hints and gameplay details</b><br><br>" +
        "The game is saved each time you descend to a different dungeon level.<br><br>" +
        "Some monsters are affected more by blunt weapons than edged weapons.<br><br>" +
        "Buy a sling, so you have a ranged weapon.  Upgrade to a bow or crossbow as soon as you can afford it.  Use such weapons to get in attacks on your enemies before they close in to hit you.  With slower monsters like goblins, stay out of their reach, and keep hitting them from afar.<br><br>" +
        "If you cannot find the exit to a level, it is likely behind a secret door.  You can quickly check for such doors by going up to one end of a wall, then clicking on the opposite end, right on the wall itself.  You will move along the wall, and will push through the first secret door encountered, if any are present.<br><br>" +
        "The chirp emitted by a monster indicates it has become aware of your presence.  If you can attack before this occurs, you will cause double damage!<br><br>" +
        "Explore the first dungeon level in a piecemeal fashion.  Break it down into item-scavenging incursions, where each foray is financed by items found during the previous one.  If you don't find enough treasure before having to retreat (and get healed), " +
        "reload the game and try a different direction, or different tactics.<br><br>" +
        "Equipping two weapons at once imposes penalties to-hit according to your agility, as listed in the attributes dialog.  Your right hand always attacks before your left.<br><br>" +
        "The first experience level you gain is chosen for you, as it features a power you must possess to prevent the game from becoming laborious.<br><br>" +
        "Good luck!  If you keep trying, you will eventually succeed.<br><br>";
//@formatter:on

