package silmar

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmar.contexts.gameplay.entities.beings.player.PlayerLevels
import silmar.contexts.gameplay.entities.beings.talkerBeings.types.SilmarTalkerBeingTypes
import silmar.contexts.gameplay.entities.items.types.SilmarArmorTypes
import silmar.contexts.gameplay.entities.items.types.SilmarShieldTypes
import silmar.contexts.gameplay.entities.items.types.SilmarAmmoTypes
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmar.contexts.gameplay.entities.items.types.SilmarTechItemTypes
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmarengine.contexts.gameplay.entities.beings.player.PlayerAttributes
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.types.TalkerBeingTypes
import silmarengine.contexts.gameplay.entities.items.types.*
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.startup

fun main(args: Array<String>) {
    // these references are here to ensure that all the different entity types
    // get created before they are first employed
    TalkerBeingTypes
    SilmarTalkerBeingTypes.alienBuyer
    SilmarMonsterTypes.acidDragon
    TerrainTypes.altar
    SilmarTerrainTypes.boulder
    AmmoTypes.arrow
    SilmarAmmoTypes.assaultRifleBullet
    WeaponTypes.bow
    SilmarWeaponTypes.assaultRifle
    PlayerAttributes.agility
    PlayerLevels.alchemist
    ArmorTypes.chainMail
    SilmarArmorTypes.heavyKevlarArmor
    ShieldTypes.smallShield
    SilmarShieldTypes.kevlarShield
    GroupableItemTypes.gold
    NonGroupableItemTypes.potionOfAgility
    TreasureTypes.amber
    SilmarMagicItemTypes.blessedFigurine
    SilmarTechItemTypes.landMine

    injectEngineExtensionPoints()

    startup(args, "silmar", creditsText, copyrightText)
}

// @formatter:off
const val creditsText =
    "<html><p style='color:black;font-size:12pt;text-align:center'>" +
        "by<br>" +
        "Jeff Mather<br><br>" +
        "entity characteristics developed by<br>" +
        "David Niecikowski<br><br>" +
        "tile graphics<br>" +
        "Copyright (c) 1998, 1999, 2000 by David E. Gervais<br>" +
        "and used with his express permission<br><br>" +
        "logo by<br>" +
        "Howard Kistler"
const val copyrightText =
    "<html><p style='color:black;font-size:11pt;text-align:center'>" +
        "Copyright (c) 1999-2004, 2019-2020 Jeff Mather"
// @formatter:on
