package silmar

import silmar.contexts.gameplay.entities.beings.monsters.types.SilmarMonsterTypes
import silmar.contexts.gameplay.entities.beings.player.PlayerPowers
import silmar.contexts.gameplay.entities.beings.player.SilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.powers.usePower
import silmar.contexts.gameplay.entities.beings.talkerBeings.types.SilmarTalkerBeingTypes
import silmar.contexts.gameplay.entities.items.types.SilmarMagicItemTypes
import silmar.contexts.gameplay.entities.items.weapons.types.SilmarWeaponTypes
import silmar.contexts.gameplay.entities.terrains.specializations.Fountain
import silmar.contexts.gameplay.entities.terrains.specializations.Garden
import silmar.contexts.gameplay.entities.terrains.specializations.HolographicTrainer
import silmar.contexts.gameplay.entities.terrains.specializations.Mine
import silmar.contexts.gameplay.entities.terrains.types.SilmarTerrainTypes
import silmar.contexts.gameplay.game.SilmarGame
import silmarengine.WorkThread
import silmarengine.contexts.gameplay.areas.extensions.entities.getTerrainOfType
import silmarengine.contexts.gameplay.areas.geometry.MutablePixelPoint
import silmarengine.contexts.gameplay.areas.geometry.PixelPoint
import silmarengine.contexts.gameplay.entities.beings.extensions.damage.takeHealing
import silmarengine.contexts.gameplay.entities.beings.monsters.createMonster
import silmarengine.contexts.gameplay.entities.beings.player.Player
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.getExperienceNeededForLevel
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.loseExperienceLevel
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.createTalkerBeing
import silmarengine.contexts.gameplay.entities.items.*
import silmarengine.contexts.gameplay.entities.items.types.AmmoTypes
import silmarengine.contexts.gameplay.entities.items.types.GroupableItemTypes
import silmarengine.contexts.gameplay.entities.items.weapons.types.WeaponTypes
import silmarengine.contexts.gameplay.entities.items.weapons.types.weaponTypes
import silmarengine.contexts.gameplay.entities.terrains.createTerrain
import silmarengine.contexts.gameplay.entities.terrains.specializations.Chest
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.gameplay.entities.terrains.types.TerrainTypes
import silmarengine.contexts.gameplay.game.Game
import silmarengine.contexts.gameplay.tiles.tileHeight
import silmarengine.contexts.gameplay.tiles.tileWidth
import silmarengine.contexts.playeritemsmanagement.domain.model.EquipLocation

fun performDebugActionOne(player: Player) {
    performDebugAction(player, 7)
    performDebugAction(player, 10)
}

fun performDebugActionTwo(player: Player) {
    performDebugAction(player, 9)
}

fun performDebugAction(player: Player, choice: Int, subChoice: Int = 0,
    distance: Int = 1) {
    player as SilmarPlayer
    WorkThread.queueTask {
        val area = player.area
        when (choice) {
            1 -> player.takeHealing(player.maxHitPoints)
            2 -> player.levelValues.onExperienceGained(getExperienceNeededForLevel(
                player.levelValues.level + 1) - player.levelValues.experience)
            3 -> player.usePower(PlayerPowers.emitLightning,
                MutablePixelPoint(player.location).translate(-5 * tileWidth, 0))
            4 -> (player.items.inventory.getFirstItemOfType(
                GroupableItemTypes.gold) as Item).quantity -= 2
            5 -> player.items.getEquippedItem(EquipLocation.LEFT_HAND)?.identify()
            6 -> area.moveEntity(player,
                area.terrains.filter { it !is Chest && it !is Trap }.random().location)
            7 -> area.monsters./*filter {
                        it.isOfType(MonsterTypes.bat)
                    }.*/map { it }.forEach { area.removeEntity(it) }
            8 -> area.moveEntity(player,
                PixelPoint(2 * tileWidth, area.size.height * tileHeight - 2 * tileHeight))
            9 -> area.moveEntity(player,
                area.getTerrainOfType(SilmarTerrainTypes.downExit)!!.location)
            10 -> {
                val item = area.items.random()
                item.item.identify()
                area.moveEntity(player,
                    MutablePixelPoint(item.location).translate(20, 0))
            }
            11 -> (Game.currentGame as SilmarGame).areas.forEach { area ->
                area.terrains.filter { it is Mine || it is Fountain || it is HolographicTrainer || it is Garden }
                    .forEach { area.removeEntity(it) }
            }
            12 -> player.loseExperienceLevel()
            else -> {
                val location = MutablePixelPoint(player.location)
                location.translate(distance * tileWidth, 0)
                val entity = when (subChoice) {
                    1 -> createMonster(SilmarMonsterTypes.giantSpider)
                    2 -> createTerrain(SilmarTerrainTypes.mine)
                    3 -> createTalkerBeing(SilmarTalkerBeingTypes.priest)
                    4 -> createTerrain(SilmarTerrainTypes.fountain)
                    5 -> createTerrain(SilmarTerrainTypes.garden)
                    6 -> createItemEntity(
                        createItem(SilmarMagicItemTypes.necklaceOfOgreStrength.itemType))
                    7 -> createItemEntity(createItem(
                        weaponTypes.first { it.itemType.name == "dagger -1" }.itemType))
                    8 -> createItemEntity(createItem(WeaponTypes.twoHandedSword.itemType))
                    9 -> createItemEntity(
                        createItem(SilmarWeaponTypes.laserPistol.weaponType.itemType))
                    10 -> createItemEntity(
                        createItem(SilmarWeaponTypes.slingOfHoming.itemType))
                    else -> createItemEntity(createGroupableItem(AmmoTypes.slingStone, 20))
                }
                if (entity is ItemEntity) entity.item.identify()
                player.area.addEntity(entity, location)
            }
        }
    }
}
