package silmar

import silmar.contexts.gameplay.entities.beings.extensions.effects.isBeingImmuneToConfusion
import silmar.contexts.gameplay.entities.beings.extensions.effects.isBeingImmuneToParalysis
import silmar.contexts.gameplay.entities.beings.extensions.reactions.onHitByMonster_PostDamage
import silmar.contexts.gameplay.entities.beings.extensions.reactions.onHitByMonster_PreDamage
import silmar.contexts.gameplay.entities.beings.monsters.createSilmarMonster
import silmar.contexts.gameplay.entities.beings.monsters.extensions.activation.shouldNotActivate
import silmar.contexts.gameplay.entities.beings.monsters.extensions.attacks.depictSilmarMonsterRangedAttack
import silmar.contexts.gameplay.entities.beings.monsters.extensions.attacks.getModifierToHitDefense0
import silmar.contexts.gameplay.entities.beings.monsters.extensions.attacks.getToHitModifier
import silmar.contexts.gameplay.entities.beings.monsters.extensions.attacks.onAttackHitBeing
import silmar.contexts.gameplay.entities.beings.monsters.extensions.harm.computeSilmarMonsterDefense
import silmar.contexts.gameplay.entities.beings.monsters.extensions.harm.indicateSilmarDeath
import silmar.contexts.gameplay.entities.beings.player.*
import silmar.contexts.gameplay.entities.beings.player.extensions.actions.onMovedOntoNewTile
import silmar.contexts.gameplay.entities.beings.player.extensions.attacks.checkToAbortRangedAttack
import silmar.contexts.gameplay.entities.beings.player.extensions.attacks.onSilmarAttackHitVictim
import silmar.contexts.gameplay.entities.beings.player.extensions.attributes.appendAttributeInfo
import silmar.contexts.gameplay.entities.beings.player.extensions.attributes.updateAttribute
import silmar.contexts.gameplay.entities.beings.player.extensions.attributes.updateOnAttributeChange
import silmar.contexts.gameplay.entities.beings.player.extensions.experience.gainLevels
import silmar.contexts.gameplay.entities.beings.player.extensions.experience.onLevelChanged
import silmar.contexts.gameplay.entities.beings.player.extensions.experience.shouldNotLoseExperienceLevel
import silmar.contexts.gameplay.entities.beings.player.extensions.experience.updateOnLevelChange
import silmar.contexts.gameplay.entities.beings.player.extensions.harm.isDeathAverted
import silmar.contexts.gameplay.entities.beings.player.extensions.harm.onDamagedAsSilmarPlayer
import silmar.contexts.gameplay.entities.beings.player.extensions.harm.onHitByGameSpecificMonster
import silmar.contexts.gameplay.entities.beings.player.extensions.harm.shouldIgnoreMonsterAttack
import silmar.contexts.gameplay.entities.beings.player.extensions.health.affectPlayerAsSilmarDisease
import silmar.contexts.gameplay.entities.beings.player.extensions.health.isImmuneToPoison
import silmar.contexts.gameplay.entities.beings.player.extensions.health.shouldIgnoreDisease
import silmar.contexts.gameplay.entities.beings.player.extensions.items.updateOnItemEquipped
import silmar.contexts.gameplay.entities.beings.player.extensions.stats.*
import silmar.contexts.gameplay.entities.beings.player.extensions.terrains.doesInteractWithTerrain
import silmar.contexts.gameplay.entities.beings.talkerBeings.createSilmarTalkerBeing
import silmar.contexts.gameplay.entities.items.createSilmarItem
import silmar.contexts.gameplay.entities.items.types.extensions.isValidForSilmarArea
import silmar.contexts.gameplay.entities.items.weapons.canWeaponTypeHit
import silmar.contexts.gameplay.entities.items.weapons.createSilmarWeapon
import silmar.contexts.gameplay.entities.terrains.createSilmarTerrain
import silmar.contexts.gameplay.entities.terrains.traps.shouldNotSpring
import silmar.contexts.gameplay.game.SilmarGame1
import silmar.contexts.gameplay.game.extensions.createStartingArea
import silmar.contexts.gameplay.game.extensions.onNewSilmarGameReadyToStart
import silmar.gui.dialogs.gameFinishedMessage
import silmar.gui.dialogs.silmarHintsText
import silmar.gui.dialogs.story
import silmar.gui.displays.SilmarPlayerVitalStatsDisplay
import silmar.gui.displays.appendToSilmarDefenseLabel
import silmar.gui.displays.getSilmarDefenseTooltipText
import silmar.gui.frames.playframe.addContextActions
import silmar.gui.frames.playframe.onNearbyTerrainClicked
import silmar.gui.playui.SilmarPlayUI1
import silmar.gui.views.areaview.isNightVisionInUse
import silmar.contexts.gameplay.areas.extensions.generation.*
import silmar.contexts.gameplay.areas.extensions.levels.isOpenAndConnected
import silmar.contexts.gameplay.game.extensions.createStartingGCArea
import silmar.gui.showDonationPleaDialog
import silmar.sounds.SilmarSounds
import silmarengine.*
import silmarengine.contexts.gameplay.entities.beings.Being
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.isBeingImmuneToConfusion
import silmarengine.contexts.gameplay.entities.beings.extensions.effects.isBeingImmuneToParalysis
import silmarengine.contexts.gameplay.entities.beings.monsters.Monster
import silmarengine.contexts.gameplay.entities.beings.monsters.computeGameSpecificMonsterDefense
import silmarengine.contexts.gameplay.entities.beings.monsters.createGameSpecificMonster
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.activation.shouldMonsterNotActivateForGameSpecificReason
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks.depictGameSpecificMonsterRangedAttack
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks.getMonsterModifierToHitDefense0
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks.getMonsterToHitModifier
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.attacks.onMonsterAttackHitBeing
import silmarengine.contexts.gameplay.entities.beings.monsters.extensions.harm.indicateMonsterDeath
import silmarengine.contexts.gameplay.entities.beings.player.*
import silmarengine.contexts.gameplay.entities.beings.player.extensions.actions.onGameSpecificPlayerMovedOntoNewTile
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attacks.checkToAbortGameSpecificPlayerRangedAttack
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attacks.onGameSpecificPlayerAttackHitVictim
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.appendGameSpecificAttributeInfo
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.updateGameSpecificPlayerAttribute
import silmarengine.contexts.gameplay.entities.beings.player.extensions.attributes.updateGameSpecificPlayerForAttributeChange
import silmarengine.contexts.gameplay.entities.beings.player.extensions.experience.*
import silmarengine.contexts.gameplay.entities.beings.player.extensions.harm.isGameSpecificPlayerDeathAverted
import silmarengine.contexts.gameplay.entities.beings.player.extensions.harm.onDamagedAsGameSpecificPlayer
import silmarengine.contexts.gameplay.entities.beings.player.extensions.health.shouldGameSpecificPlayerIgnoreDisease
import silmarengine.contexts.gameplay.entities.beings.player.extensions.stats.*
import silmarengine.contexts.gameplay.entities.beings.player.extensions.terrains.doesPlayerInteractWithGameSpecificTerrain
import silmarengine.contexts.gameplay.entities.beings.talkerBeings.createGameSpecificTalkerBeing
import silmarengine.contexts.gameplay.entities.items.createGameSpecificItem
import silmarengine.contexts.gameplay.entities.items.extensions.quantity.getGameSpecificAverageGoldItemQuantity
import silmarengine.contexts.gameplay.entities.items.types.ItemType
import silmarengine.contexts.gameplay.entities.items.types.isItemTypeValidForArea
import silmarengine.contexts.gameplay.entities.items.weapons.createGameSpecificWeapon
import silmarengine.contexts.gameplay.entities.items.weapons.extensions.canWeaponTypeHit
import silmarengine.contexts.gameplay.entities.terrains.createGameSpecificTerrain
import silmarengine.contexts.gameplay.entities.terrains.extensions.getGameSpecificAreaMaxItemPlacementValue
import silmarengine.contexts.gameplay.entities.terrains.traps.Trap
import silmarengine.contexts.gameplay.entities.terrains.traps.extensions.shouldTrapNotSpringForGameSpecificReason
import silmarengine.contexts.gamecreation.domain.model.functions.storyText
import silmarengine.gui.dialogs.hintsText
import silmarengine.gui.dialogs.websiteUrl
import silmarengine.gui.displays.appendToDefenseLabel
import silmarengine.gui.displays.getDefenseTooltipText
import silmarengine.gui.frames.playframe.createPlayerVitalStatsDisplay
import silmarengine.gui.frames.playframe.extensions.addGameSpecificContextActions
import silmarengine.gui.frames.playframe.extensions.checkForNearbyGameSpecificTerrainClicked
import silmarengine.gui.frames.playframe.extensions.gameFinishedText
import silmarengine.gui.playui.createGameSpecificPlayerListener
import silmarengine.gui.views.areaview.AreaViewInternals
import silmarengine.gui.views.areaview.isNightVisionInUse
import silmarengine.contexts.gameplay.areas.Area
import silmarengine.contexts.gameplay.areas.extensions.generation.createGameSpecificAreaEntrance
import silmarengine.contexts.gameplay.areas.extensions.generation.createGameSpecificAreaExit
import silmarengine.contexts.gameplay.areas.extensions.generation.stockGameSpecificEntities
import silmarengine.contexts.gameplay.areas.extensions.reporting.isGameSpecificAreaOpenAndConnected
import silmarengine.contexts.gameplay.entities.beings.onBeingHitByMonster_PostDamage
import silmarengine.contexts.gameplay.entities.beings.onBeingHitByMonster_PreDamage
import silmarengine.gui.frames.onProgramClosing
import silmarengine.gui.views.areaview.extensions.drawing.isGameSpecificNightVisionInUse

fun injectEngineExtensionPoints() {
    programVersion = "3.0.0"
    websiteUrl = "http://dunjax.com/silmar"
    createGame = ::SilmarGame1
    onNewGameReadyToStart = ::onNewSilmarGameReadyToStart
    createPlayUI = ::SilmarPlayUI1
    hintsText = silmarHintsText
    storyText = story
    gameFinishedText = gameFinishedMessage
    appendToDefenseLabel = ::appendToSilmarDefenseLabel
    getDefenseTooltipText = ::getSilmarDefenseTooltipText
    performDebugActionOne = ::performDebugActionOne
    performDebugActionTwo = ::performDebugActionTwo
    stockGameSpecificEntities = ::stockSilmarEntities
    addGameSpecificContextActions = ::addContextActions
    checkForNearbyGameSpecificTerrainClicked = ::onNearbyTerrainClicked
    createGameSpecificItem = ::createSilmarItem
    createGameSpecificWeapon = ::createSilmarWeapon
    canWeaponTypeHit = ::canWeaponTypeHit
    computeGameSpecificPlayerDamageModifier = Player::computeSilmarDamageModifier
    createGameSpecificTerrain = ::createSilmarTerrain
    isBeingImmuneToConfusion = ::isBeingImmuneToConfusion
    isBeingImmuneToParalysis = ::isBeingImmuneToParalysis
    onBeingHitByMonster_PreDamage = Being::onHitByMonster_PreDamage
    onBeingHitByMonster_PostDamage = Being::onHitByMonster_PostDamage
    createGameSpecificTalkerBeing = ::createSilmarTalkerBeing
    createGameSpecificMonster = ::createSilmarMonster
    computeGameSpecificMonsterDefense = Monster::computeSilmarMonsterDefense
    isItemTypeValidForArea = ItemType::isValidForSilmarArea
    getMonsterToHitModifier = Monster::getToHitModifier
    depictGameSpecificMonsterRangedAttack = ::depictSilmarMonsterRangedAttack
    getMonsterModifierToHitDefense0 = Monster::getModifierToHitDefense0
    onMonsterAttackHitBeing = Monster::onAttackHitBeing
    indicateMonsterDeath = Monster::indicateSilmarDeath
    startingPlayerLevel = startingSilmarPlayerLevel
    experienceRequiredForPlayerLevel = computeExperienceRequiredForPlayerLevel()
    createPlayer = ::SilmarPlayer1
    shouldMonsterNotActivateForGameSpecificReason = Monster::shouldNotActivate
    appendGameSpecificAttributeInfo = ::appendAttributeInfo
    updateGameSpecificPlayerOnItemEquipped = Player::updateOnItemEquipped
    shouldTrapNotSpringForGameSpecificReason = Trap::shouldNotSpring
    isNightVisionInUse = AreaViewInternals::isNightVisionInUse
    createPlayerLevelValues = ::SilmarPlayerLevelValues1
    createPlayerStats = ::SilmarPlayerStats1
    updateGameSpecificPlayerForAttributeChange = Player::updateOnAttributeChange
    updateGameSpecificPlayerOnLevelChange = Player::updateOnLevelChange
    createPlayerVitalStatsDisplay = ::SilmarPlayerVitalStatsDisplay
    createPlayerStatuses = ::SilmarPlayerStatuses1
    onGameSpecificPlayerMovedOntoNewTile = Player::onMovedOntoNewTile
    updateGameSpecificPlayerAttribute = Player::updateAttribute
    shouldGameSpecificPlayerNotLoseExperienceLevel = Player::shouldNotLoseExperienceLevel
    affectPlayerAsGameSpecificDisease = Disease::affectPlayerAsSilmarDisease
    isGameSpecificPlayerDeathAverted = Player::isDeathAverted
    onGameSpecificPlayerAttackHitVictim = Player::onSilmarAttackHitVictim
    onDamagedAsGameSpecificPlayer = Player::onDamagedAsSilmarPlayer
    shouldIgnoreGameSpecificMonsterAttackOnPlayer = Player::shouldIgnoreMonsterAttack
    onPlayerHitByGameSpecificMonster = Player::onHitByGameSpecificMonster
    havePlayerGainGameSpecificLevels = Player::gainLevels
    onGameSpecificPlayerLevelChanged = Player::onLevelChanged
    shouldGameSpecificPlayerIgnoreDisease = Player::shouldIgnoreDisease
    computeGameSpecificPlayerAccuracyModifier = Player::computeAccuracyModifier
    computeExtraAttacksPerTurnForGameSpecificPlayer = Player::computeExtraAttacksPerTurn
    computeItemDefenseModifierVsGameSpecificAttack = ::computeItemDefenseModifierVsAttack
    computeGameSpecificPlayerDefenseModifierVsAttack =
        Player::computeDefenseModifierVsAttack
    adjustGameSpecificPlayerMaxMovementPoints = Player::adjustMaxMovementPoints
    adjustGameSpecificPlayerToHitModifier = Player::adjustToHitModifier
    doesPlayerInteractWithGameSpecificTerrain = Player::doesInteractWithTerrain
    getGameSpecificAreaMaxItemPlacementValue = Area::getAreaMaxItemPlacementValue
    getGameSpecificAverageGoldItemQuantity = ::getAverageGoldItemQuantity
    createGameSpecificAreaEntrance = ::createAreaEntrance
    createGameSpecificAreaExit = ::createAreaExit
    isGameSpecificAreaOpenAndConnected = Area::isOpenAndConnected
    isGameSpecificPlayerImmuneToPoison = Player::isImmuneToPoison
    titleSound = SilmarSounds.title
    checkToAbortGameSpecificPlayerRangedAttack = Player::checkToAbortRangedAttack
    createGameSpecificStartingArea = ::createStartingArea
    createGameSpecificStartingGCArea = ::createStartingGCArea
    createGameSpecificPlayerListener = { (it as SilmarPlayUI1).UISilmarPlayerListener() }
    isGameSpecificNightVisionInUse = AreaViewInternals::isNightVisionInUse
    onProgramClosing = ::showDonationPleaDialog
}

